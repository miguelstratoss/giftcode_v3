if Rails.env.production?
  CarrierWave.configure do |config|
    config.storage = :fog
    config.fog_credentials = {
      :provider               => 'AWS',       # required
      :aws_access_key_id      => 'AKIAJUXFDIGW65P5VUBQ',       # required
      :aws_secret_access_key  => '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',       # required
      :region                 => 'us-east-1'  # optional, defaults to 'us-east-1'
    }
    config.fog_directory  = 'giftcodeprov2'                     # required
    config.asset_host       = 'https://giftcodeprov2.s3.amazonaws.com'            # optional, defaults to nil
    config.fog_public     = true                                   # optional, defaults to true
    config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}
  end
elsif Rails.env.staging?
  CarrierWave.configure do |config|
    config.storage = :fog
    config.fog_credentials = {
      :provider               => 'AWS',       # required
      :aws_access_key_id      => 'AKIAJUXFDIGW65P5VUBQ',       # required
      :aws_secret_access_key  => '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',       # required
      :region                 => 'us-east-1'  # optional, defaults to 'us-east-1'
    }
    config.fog_directory  = 'giftcodeprov2staging'                     # required
    config.asset_host      = 'https://giftcodeprov2staging.s3.amazonaws.com'            # optional, defaults to nil
    config.fog_public     = true                                   # optional, defaults to true
    config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}
  end
elsif Rails.env.prestaging?
  CarrierWave.configure do |config|
    config.storage = :fog
    config.fog_credentials = {
      :provider               => 'AWS',       # required
      :aws_access_key_id      => 'AKIAJUXFDIGW65P5VUBQ',       # required
      :aws_secret_access_key  => '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',       # required
      :region                 => 'us-east-1'  # optional, defaults to 'us-east-1'
    }
    config.fog_directory  = 'giftcodeprov2prestaging'                     # required
    #config.fog_host       = 'https://giftcodeprov2prestaging.s3.amazonaws.com'            # optional, defaults to nil
    config.asset_host     = 'https://giftcodeprov2prestaging.s3.amazonaws.com'            # optional, defaults to nil
    config.fog_public     = true                                   # optional, defaults to true
    config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}
  end
elsif Rails.env.development?
  CarrierWave.configure do |config|
    config.storage = :file
  end
elsif Rails.env.test?
  CarrierWave.configure do |config|
    config.storage = :file
    config.enable_processing = false
  end
end
