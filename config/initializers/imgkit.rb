IMGKit.configure do |config|
 unless Rails.env.development?
   # config.wkhtmltoimage = Rails.root.join('bin', 'wkhtmltoimage').to_s
   config.wkhtmltoimage = Rails.root.join('bin', 'wkhtmltoimage-amd64-2').to_s
 end
 config.default_options = {
   :encoding => 'UTF-8',
   :quality => 100
 }
 config.default_format = :png
end