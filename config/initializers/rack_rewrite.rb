if Rails.env.production?
  Greencode::Application.config.middleware.insert_before(Rack::Lock, Rack::Rewrite) do
    r301 %r{.*}, 'http://www.giftcode.co$&', :if => Proc.new {|rack_env|
    rack_env['SERVER_NAME'] == 'giftcode.co'
    }
  end
end