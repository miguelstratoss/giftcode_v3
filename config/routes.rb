Greencode::Application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  post '/office_admin' => 'office_admin/dashboard#index', :as => :office_admin_root

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'pages#home'


  # ActiveAdmin.routes(self)
  # devise_for :admin_users, ActiveAdmin::Devise.config, ActiveAdmin::Devise.config
  # ActiveAdmin.routes(self)
  devise_for :users,
             :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" },
             :skip => [ :password, :registration ]

  match '/delayed_job' => DelayedJobWeb, :anchor => false

  #og
  get '/gracias' => 'pages#gracias'
  get '/fbshared' => 'pages#fbshared'
  get '/fbshared2' => 'pages#fbshared2'
  get '/fbshared3' => 'pages#fbshared3'
  match 'sitemap.xml.gz', :controller => 'sitemap', :action => 'index'

    # match '/que-es-giftcode' => 'pages#what_is', :as => :what_is_pages
    match '/como-funciona' => 'pages#how_it_works', :as => :how_it_works_pages

    match '/eco-amigable' => 'pages#projects', :as => :ecoamigable_pages
    match '/retailers' => 'pages#customers', :as => :customers_pages
    match '/retailers_beneficios' => 'pages#customers2', :as => :customers2_pages
    match '/terminos-y-condiciones' => 'pages#tos', :as => :tos_pages
    match '/politicas-de-privacidad' => 'pages#politica', :as => :politica_pages
    match '/terminos-y-condiciones-concurso' => 'pages#concurso', :as => :tycc_pages
    match '/concurso2014' => 'pages#concurso2014', :as => :tycc2014_pages
    #match '/equipo' => 'pages#equipo', :as => :team_pages

    # match '/contacto' => 'contacts#new', :via => :get , :as => :contacts
    match '/contacto' => 'contacts#create', :via => :post , :as => :contacts

    match '/mis-marcas-favoritas' => 'favorites#index', :via => :get , :as => :favorites
    match '/selecciona-tus-marcas-favoritas' => 'favorites#index', :via => :get , :as => :favorites_first_time
    match '/confirma-tus-datos' => 'settings#edit', :via => :get , :as => :first_time
    match '/mi-marca-favorita/:brand_id/actualizar' => 'favorites#add', :via => :put , :as => ''
    match '/compartir-en-facebook'  => 'favorites#facebook_wall', :via => :post, :as => ''

    match '/bonos-recibidos' => 'gift_codes#index_angular', :via => :get, :as => :gift_codes
    match '/bonos-enviados' => 'gift_codes#index_sent_angular', :via => :get, :as => :gift_codes_sent
    match '/bono/:view_id/token/:token' => 'gift_codes#show', :via => :get, :as => :gift_code
    match '/bono/crear' => 'gift_codes#create', :via => :post, :as => :gift_code_create
    match '/bono/:ref_sale' => 'gift_codes#download', :via => :get, :as => :gift_code_download
    match '/bono/:ref_sale/view' => 'gift_codes#view', :via => :get, :as => :gift_code_view

    match '/perfil' => 'settings#edit', :via => :get, :as => :edit_settings
    match '/perfil' => 'settings#update', :via => :put, :as => :settings
    match '/perfil/validar_correo/:token' => 'settings#validate_account', :via => :get, :as => :validate_account_settings
    match '/confirmation_email' => 'settings#confirmation_email', :via => :put
    match '/confirmation_birthday' => 'settings#confirmation_birthday', :via => :put
    match '/confirmation_city' => 'settings#confirmation_city', :via => :put

    match '/regalar-bono' => 'give#index_brands_angular', :via => :get, :as => :give
    match '/regalar-bono-amigo' => 'give#index_friends_angular', :via => :get, :as => :friend_give

=begin
    # todo lo de puntos
    match '/puntos' => 'points#index', :via => :get, :as => :points
    match '/puntos/giftcode/:ref_sale' => 'points#gift_code', :via => :get, :as => :gift_codes_points
    match '/services/points/campaigns' => 'points#json_campaigns_points', :via => :get, :as => :services_points
    match '/services/points/campaigns/:campaign_id/gift_codes/create' => 'points#create_gift_code', :via => :post, :as => :services_points_create_gift_code
=end

=begin
    # se quita interface, para hace nueva interface con regalar
    match '/amigos' => 'friends#index_angular', :via => :get, :as => :friends
    match '/invitar-amigo/:fbk_user_id' => 'friends#invite', :via => :post, :as => :invite_friend
    match '/perfil/amigo/:user_id' => 'friends#profile', :via => :get, :as => :profile_friend
    match '/amigos/referidos' => 'friends#referrals', :via => :get, :as => :services_friends_referrals
    match '/amigos/invitados' => 'friends#guests', :via => :get, :as => :services_friends_guests
    match '/amigos/por-invitar' => 'friends#for_inviting', :via => :get, :as => :services_friends_for_inviting
    match '/amigos/en-giftcode' => 'friends#in_gift_code', :via => :get, :as => :services_friends_in_gift_code
=end


    match '/json/amigos' => 'json#friends', :via => :get, :as => :friends_json
    match '/json/referidos-amigos' => 'json#referrals_friends', :via => :get, :as => :referrals_friends_json
    match '/amigos/referidos' => 'friends#referrals', :via => :get, :as => :services_friends_referrals
    match '/json/parametros-marca-giftcode' => 'json#brand_parameters_gift_code',  :via => :post, :as => :brand_parameters_gift_code_json
    match '/json/marcas' => 'json#brands',  :via => :get, :as => :brands_json
    match '/json/marcas-favoritas-amigo-regalar' => 'json#brands_favorites_friend_give',  :via => :post, :as => :brands_favorites_friend_give_json
    match '/json/marcas-favoritas' => 'json#favorites',  :via => :get, :as => :brand_favorites_json
    match '/json/marcas-favoritas-amigo/:friend_id' => 'json#favorites_friend',  :via => :get, :as => :brand_favorites_friend_json
    match '/json/marcas-favoritas-amigos' => 'json#friends_favorite_brands',  :via => :get, :as => :friends_favorite_brands_json
    match '/json/marca-favoritas-amigos/:brand_id' => 'json#friends_favorite_brand',  :via => :get, :as => :friends_favorite_brand_json
    match '/json/invite-amigos' => 'json#invite_friends', :via => :get, :as => :invite_friend_json
    match '/json/gift_code' => 'json#gift_code', :via => :post, :as => :gift_code_json
    match '/json/gift_code_sent' => 'json#gift_code_sent', :via => :post, :as => :gift_code_sent_json
    match '/json/gift_codes' => 'json#gift_codes', :via => :get, :as => :gift_codes_json
    match '/json/gift_codes_sent' => 'json#gift_codes_sent', :via => :get, :as => :gift_codes_sent_json
    match '/json/date_time_server' => 'json#get_date_time_server', :via => :get, :as => :get_date_time_server_json
    # match '/json/yo_quiero_facebook' => 'json#yo_quiero_facebook', :via => :post, :as => :yo_quiero_facebook

    match '/payments/:ref_sale' => 'payments#index', :via => :get, :as => :payments
    match '/payments/:ref_sale' => 'payments#index', :via => :put, :as => :payments
    match '/payments/confirmation/:ref_sale' => 'payments#confirmation', :via => :get, :as => :confirmation_payments
    match '/payments/demo/:ref_sale' => 'payments#demo', :via => :get, :as => :demo_payments


  match '/favorites/suggest' => 'favorites#suggest', :via => :post
  #end

  #third_party
  match '/redemption' => 'third_party#redemption', :via => :post
  match '/reverse_redemption' => 'third_party#reverse_redemption', :via => :post
  match '/pending_redemption' => 'third_party#pending_redemption', :via => :post
  match '/confirm_redemption' => 'third_party#confirm_redemption', :via => :post
  match '/get_balance' => 'third_party#get_balance', :via => :post
  match '/embebed_giftcode' => 'third_party#embebed_giftcode', :via => :get, :as => :embebed_giftcode
  match '/embebed_giftcode_create' => 'third_party#embebed_giftcode_create', :via => :post, :as => :embebed_giftcode_create
  match '/embebed_giftcode_confirmation' => 'third_party#embebed_giftcode_confirmation', :via => :get, :as => :embebed_giftcode_create
  match '/get_payments' => 'third_party#get_brand_payments', :via => :post


  # resources :gift_code_free
  get '/bonos_gratuitos' => 'gift_code_free#index'


  #landing pages
  get '/nosotros' => 'pages#team'
  # get '/yo_quiero' => 'pages#yo_quiero'
  # match '/yo_quiero' => 'pages#yo_quiero', :via => :post
  #get '/landing_1' => 'pages#landing_page1'
  get '/landing_2' => 'pages#landing2'
  get '/landing_3' => 'pages#landing_3'
  get '/landing_4' => 'pages#landing_4'
  #get '/new_landing' => 'pages#nuevo'
  match '/:brand_name' => 'landing_page#index', :via => :get, :as => :landing_page



  #scope "en" do

  #end

  #map.resources :pages,   :as => 'app/pages'
  use_doorkeeper


  #For the API
  namespace :api do
    namespace :v1 do
      resource  :settings do
        collection do
          get :profile
          put :update_image
          put :update_birthday
          put :update
          get :birthday_confirmation
        end
      end
      resources :categories, :only => [:index]
      resources :friends, :only => [:index, :friends_in_gift_code]
      resource :brands, :only => [] do
        collection do
          get :all
          get 'friends_favorite_brand/:brand_id', to: :friends_favorite_brand
          get :favorite_brands
        end
      end
      resource :gift_codes, :only => [] do
        collection do
          get :sent
          get :received
          get 'received/:ref_sale', to: :gift_code_received
          post :create
          get 'parameters_brand/:brand_id', to: :parameters_brand
        end
      end
      resource :favorites, :only => [] do
        collection do
          post :add
        end
      end

    end
  end


  namespace :api do
    namespace :off do
      namespace :v1 do
        resources :pages,  :only => [:index] do
          collection do
            get :':brand_name', to: :index
          end
        end
        resources :brands, :only => [:index, :show]
        resources :gift_codes, :only => [:create]
      end
    end
  end
  #activeadmin
  # match '/admin/brands/:brand_id/update_certificates' => 'admin/updatecertificates#index', :as => :admin_update_certificates, :via => :get
  # match '/admin/update_certificates' => 'admin/updatecertificates#index', :as => :admin_update_certificates, :via => :get
  # match '/admin/brands/:brand_id/update_certificates/load' => 'admin/updatecertificates#load', :as => :admin_update_certificates_load


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
