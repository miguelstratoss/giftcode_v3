# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'http://www.giftcode.co'
SitemapGenerator::Sitemap.sitemaps_host = 'https://giftcodeprov2.s3.amazonaws.com'
SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'
SitemapGenerator::Sitemap.adapter = SitemapGenerator::WaveAdapter.new

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  add root_path, :priority => 1.0
  add ecoamigable_pages_path
  add customers_pages_path
  add tos_pages_path, :priority  => 0.3
  add politica_pages_path, :priority  => 0.3
  add nosotros_path
  add how_it_works_pages_path
  add 'blog.giftcode.co'


  Brand.brands_enabled_for_sending_a_gift_code( 1 ).each do | brand |
    add landing_page_path(brand.name), :priority  => 0.4
  end

end
