#encoding: utf-8
Greencode::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = true

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log
  # :debug, :info, :warn, :error, :fatal, and :unknown, corresponding to the log level numbers from 0 up to 5 respectively. To change the default log level
  config.log_level = :error
  
  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  config.active_record.auto_explain_threshold_in_seconds = 0.5

  #compile
  config.assets.compile = true

  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true

  config.action_mailer.default_url_options = { :host => 'https://giftcodev3-miguelseguray-1.c9.io' }

  config.action_mailer.raise_delivery_errors = true

  #ActionMailer::Base.smtp_settings = {
  #  :address => "smtp.gmail.com",
  #  :enable_starttls_auto => true,
  #  :port => 587,
  #  :authentication => :plain,
  #  :user_name => "dev-noreply@greencode.com.co",
  #  :password => 'giftcode01012012'
  #}
  ActionMailer::Base.smtp_settings = {
      :address => "smtp.gmail.com",
      :enable_starttls_auto => true,
      :port => 587,
      :authentication => :plain,
      :user_name => "dev-noreply@greencode.com.co",
      :password => 'giftcode01012012',
      :domain => 'greencode.com.co'
  }

end
