# == Schema Information
#
# Table name: third_party_transactions
#
#  id                      :integer          not null, primary key
#  third_party_entity_id   :integer
#  brand_code_id           :integer
#  transaction_value       :float
#  transaction_currency    :string(255)
#  new_balance             :float
#  received_hash           :string(1000)
#  dir_ip                  :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  tp_transaction_type_id  :integer
#  tp_transaction_state_id :integer
#  related_transaction_id  :integer
#  gift_code_id            :integer
#  office_id               :integer
#  terminal_id             :integer
#  reversed                :boolean
#  confirmed               :boolean
#  dir_ip_client           :string(255)
#

require 'test_helper'

class ThirdPartyTransactionTest < ActiveSupport::TestCase
  test "the truth" do
     assert true
  end
end
