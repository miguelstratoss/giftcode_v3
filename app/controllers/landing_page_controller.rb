class LandingPageController < ApplicationWebController

  def index
    #se quema el valor de country id en uno, cuando se habilite para las demas paise se debe hacer la logica
    country_id = 1
    @brand = Brand.brand_enable_for_selling( params[:brand_name], country_id )
    if @brand.present?
      if user_signed_in?
        session.delete(:brand_name)
      else
        session[:brand_name] = params[:brand_name]
      end
      @campaign = @brand.campaign_enable_for_selling
      if @campaign.present?
        @cities = @campaign.cities
        @gift_cards = @campaign.gift_cards_enable_for_selling
      else
        redirect_to root_path
      end
    else
      redirect_to root_path
    end
  end

end