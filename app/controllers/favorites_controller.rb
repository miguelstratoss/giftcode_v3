class FavoritesController < ApplicationWebController

  before_filter :auth_user

  def index
    @categories = Category.order("position").where("enabled = true")
    @brands = Brand.where(:enable_for_selling => true).collect do |b|
      { id: b.id, name: b.name, categories: b.categories.map{|category| category.name } }
    end
  end

  def add
    favorite = current_user.favorites.find_by_brand_id( params[:brand_id] )
    msg = { :type => 'error', :text => 'No se actualizo marca favorita' }
    if favorite.nil?
      favorite = current_user.favorites.new( :brand_id => params[:brand_id], :enabled => true )
      if favorite.save
        msg = { :type => 'success', :text => 'Guardado' }
      end
    else
      if favorite.update_attributes( :enabled => favorite.enabled ? false : true )
        msg = { :type => 'success', :text => 'Guardado' }
      end
    end
    count = []
    current_user.categories.each do |c|
      count << { :id => c.id, :count => c.favorites.where(:user_id => current_user.id).count }
    end

    favorites = current_user.favorites.all
    array_favorites = {}
    favorites.each do |bf|
      if bf.brand.enable_for_selling && bf.categories.count > 0
        array_favorites[ bf.brand_id ] = bf.enabled
      end
    end

    json = { flash: msg, favorites: array_favorites.as_json , count_favorites: count.as_json }.to_json
    render json: json
  end

  def facebook_wall
    if current_user.favorites.where(:enabled => true ).count >= 1
      current_user.update_attributes( :show_introduction => false )
      current_user.delay.post_facebook_wall
    end
    render :json => ''
  end

  def suggest
    Suggest.create :name => params[:name], :user_id => current_user.id
    render :json => ''
  end
end
