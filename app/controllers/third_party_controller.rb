class ThirdPartyController < ApplicationWebController

  include PaymentsHelper

  respond_to :json

  # giftcode redemption - NO confirmation needed
  def redemption
    third_party = ThirdParty.new
    render json: third_party.redemption(params, request.remote_ip.to_s).to_json
  end

  # Confirmation Needed
  def pending_redemption
    third_party = ThirdParty.new
    render json: third_party.pending_redemption(params, request.remote_ip.to_s).to_json
  end


  # Confirm a pending redemption
  def confirm_redemption
    third_party = ThirdParty.new
    render json: third_party.confirm_redemption(params, request.remote_ip.to_s).to_json
  end

  # Reverse redemptions
  def reverse_redemption
    third_party = ThirdParty.new
    render json: third_party.reverse_redemption(params, request.remote_ip.to_s).to_json
  end

  # funds Request
  def get_balance
    third_party = ThirdParty.new
    render json: third_party.get_balance(params, request.remote_ip.to_s).to_json
  end

  # Get payments of a brand
  def get_brand_payments
    payments = BrandServices.new
    render json: payments.get_brand_payments(params[:commerce_id],params[:incocredito_id], params[:passphrase], params[:init_date], params[:end_date], request.remote_ip.to_s)
  end

  # giftcode sales form
  def embebed_giftcode
    transaction = ThirdPartyTransaction.new
    transaction.received_hash=params.inspect.to_s.gsub(', "controller"=>"third_party", "action"=>"embebed_giftcode"','')  # Registro del Hash
    transaction.tp_transaction_type_id = TpTransactionType::TYPES[:marketplace_request]  # Registra tipo de transacción
    transaction.dir_ip = request.remote_ip.to_s # Registra dirección IP del cliente
    transaction.tp_transaction_state_id = TpTransactionState::STATES[:unknown_error]
    if verify_black_list (transaction.dir_ip)
      third=ThirdPartyEntity.find_by_incocredito_id(params[:cc])
      if third
        transaction.third_party_entity_id = third.id   # Registra id de tercero
        brand_code=BrandCode.find_by_third_party_entity_id_and_code(third.id,params[:b])
        campaign_id = brand_code.campaign_id
        if brand_code
          transaction.brand_code_id = brand_code.id  # Registra código de la marca del tercero
          key = brand_code.code.to_s+'||'+third.incocredito_id.to_s+'|'+params[:td].to_s+'|||'+params[:cn].to_s+'|'+third.secret.to_s
          #puts key
          firm = Digest::MD5.hexdigest(key)
          #puts firm
          registration = ThirdPartyRegistration.new
          registration.brand_code = brand_code
          registration.received_date = params[:td]
          registration.received_sign = params[:f]
          if firm == params[:f] && registration.save
            transaction.tp_transaction_state_id=TpTransactionState::STATES[:success]
            @status_id=TpTransactionState::STATES[:success]
            @campaign = Campaign.find(campaign_id)
            @brand=@campaign.brand
            session[:sales_code]=params[:sales_code]
            if @campaign
              @cities = @campaign.brand.cities( @campaign.country_id )
              @gift_cards = @campaign.gift_cards
              @gift_code = GiftCode.new
            end
          else
            puts registration.errors.messages.inspect
            transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_sign]
            @status_id = TpTransactionState::STATES[:wrong_sign]
            @status = "wrong credentials" # wrong sign
            @message = "Contacte al administrador del sistema" # La firma emitida no coincide - contacte al administrador del sistema
          end
        else
          transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_brand]
          @status_id = TpTransactionState::STATES[:wrong_credentials_brand]
          @status = "wrong credentials" # wrong credentials - brand
          @message = "Contacte al administrador del sistema"  # Codigo de marca invalida - contacte al administrador del sistema
        end
      else
        transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_commerce_id]
        @status_id = TpTransactionState::STATES[:wrong_credentials_commerce_id]
        @status = "wrong credentials" # wrong credentials - commerce_id
        @message = "Contacte al administrador del sistema" #  Codigo de comercio invalido - contacte al administrador del sistema
      end
    else
      transaction.tp_transaction_state_id=TpTransactionState::STATES[:dir_ip_blocked]
      @status_id = TpTransactionState::STATES[:dir_ip_blocked]
      @status = "dir_ip_blocked" # dir_ip_blocked
      @message = "Su IP ha sido bloqueada - contacte al administrador del sistema" #  Su dirección IP ha sido bloqueada
    end
    render :layout => 'external'
  ensure
    puts transaction.inspect
    transaction.save
    puts transaction.errors.messages.inspect
  end


  def embebed_giftcode_create
    msg = { :type => 'error', :text => 'Ocurrio un error, por favor intentalo de nuevo' }
    bt = nil
    rs = nil
    gift_code = GiftCode.new( params[:gift_code] )
    from_user = User.find_by_email ( gift_code.from_email )
    if from_user.nil?
      from_user = User.user_new( gift_code.from_first_name, gift_code.from_last_name, gift_code.from_email )
      puts from_user.errors.messages
    end
    to_user = User.find_by_email ( gift_code.to_email )
    if to_user.nil?
      to_user = User.user_new( gift_code.to_first_name, gift_code.to_last_name, gift_code.to_email )
      puts from_user.errors.messages
    end
    gift_code.from_user = from_user
    gift_code.to_user = to_user
    gift_code = load_data( gift_code )
    gift_code.gift_code_state_id = GiftCodeState::STATES[:generated_external_web_pending]
    city = ""
    cost_current = ""
    if gift_code.save
      msg = { :type => 'success', :text => 'Pendiente de confirmando'}
      rs = gift_code.ref_sale
      # city = gift_code.to_city.name
      cost_current = gift_code.gift_card.cost_current
      session[:gift_code_WE] = gift_code.id
    else
      puts msg.inspect
      puts gift_code.errors.messages
    end
    render :json => { :flash => msg, :gift_code => { :cost_current => cost_current }.as_json, :ref_sale => rs }.to_json
  end

  def embebed_giftcode_redirect_entity
    if session[:gift_code_WE].present?
      if GiftCode.exists?(:id => session[:gift_code_WE])

      end
    else

    end
  end

  def load_data( gift_code = nil )
    unless gift_code.gift_card_id.nil?
      gift_code.gift_card = GiftCard.find gift_code.gift_card_id
      gift_code.cost = gift_code.gift_card.cost
      if gift_code.send_notifications_now
        gift_code.send_date_time = Time.zone.now
      end
      gift_code.valid_for_days = gift_code.gift_card.valid_for_days
      gift_code.iva = gift_code.gift_card.campaign.iva
      gift_code.calculate_base_iva
      gift_code.generate_ref_sale
    end
    return gift_code
  end

  # the received date must be same than today
  def validate_received_date(received_date)
    puts "received date"+received_date.to_s
    if received_date.strftime('%m/%d/%Y') != Time.now.strftime('%m/%d/%Y')
      errors.add(:received_date, 'Fecha erronea')
      false
    else
      true
    end
  end

  def verify_black_list(dir_ip)
    black_list = ThirdPartyBlackList.find_by_blocked_and_dir_ip(true, dir_ip)
    if black_list
      false
    else
      failed_transactions = ThirdPartyTransaction.where('created_at BETWEEN ? AND ? AND dir_ip = ? AND tp_transaction_state_id in (5,2,10)', Time.now - 1.hours, Time.now, dir_ip).count
      if failed_transactions > 10
        black_list = ThirdPartyBlackList.find_by_dir_ip(dir_ip)
        if black_list
          black_list.blocked=true
        else
          black_list = ThirdPartyBlackList.new
          black_list.dir_ip=dir_ip
          black_list.blocked=true
        end
        black_list.save
        false
      else
        true
      end
    end
  end

end
