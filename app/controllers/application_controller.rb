class ApplicationController < ActionController::Base

  protect_from_forgery

  before_filter :set_user_language

  ## METODOS ACTIVEADMIN
  #This method is called by ActiveAdmin for every controller action
  #pre: An admin user (brand_admin or admin) has initiated session and it has already been redirected to its default and proper/valid namespace
  def access_control_to_admin_namespace
    @admin_user_for_validation = current_admin_user
    if @admin_user_for_validation.present?
      @role_id = @admin_user_for_validation.role.id
      @namespace = self.controller_path.split('/').first.to_s
      if @namespace == "admin" && @role_id != Role::STATES[:admin_gift_code]
        #Registration of the invalid access (for auditing processes)
        @admin_user_for_validation.admin_control_access_alerts.build(:alert_for_access_path => request.fullpath.to_s).save
        redirect_to admin_path_for_role(@role_id)
      elsif @namespace == "brand_admin" && @role_id != Role::STATES[:admin_brand]
        #Registration of the invalid access (for auditing processes)
        @admin_user_for_validation.admin_control_access_alerts.build(:alert_for_access_path => request.fullpath.to_s).save
        redirect_to admin_path_for_role(@role_id)
      elsif @namespace == "office_admin" && @role_id != Role::STATES[:admin_brand_office]
        #Registration of the invalid access (for auditing processes)
        @admin_user_for_validation.admin_control_access_alerts.build(:alert_for_access_path => request.fullpath.to_s).save
        redirect_to admin_path_for_role(@role_id)
      end
    end
  end

  def admin_path_for_role(role_id)
    if role_id == Role::STATES[:admin_gift_code]
      admin_root_path
    elsif role_id == Role::STATES[:admin_brand]
      brand_admin_root_path
    elsif role_id == Role::STATES[:admin_brand_office]
      puts "Ïngreso admin"
      puts request.remote_ip
      if current_admin_user.office.ip.blank?
        office_admin_root_path
      elsif current_admin_user.office.ip == request.remote_ip
        office_admin_root_path
      else
        destroy_admin_user_session_path
      end
    else
      admin_root_path
    end
  end

  #This methos is called by device after a user (final or administrator) has done a sucessfull signed in
  def after_sign_in_path_for(resource_or_scope)
    if current_admin_user.present?
      role_id = current_admin_user.role.id
    end
    if user_signed_in?
      root_path
    elsif role_id == Role::STATES[:admin_gift_code]
      admin_root_path
    elsif role_id == Role::STATES[:admin_brand]
      brand_admin_root_path
    elsif role_id == Role::STATES[:admin_brand_office]
      puts "Ïngreso admin 2"
      puts request.remote_ip
      if current_admin_user.office.ip.blank?
        office_admin_root_path
      elsif current_admin_user.office.ip == request.remote_ip
        office_admin_root_path
      else
        destroy_admin_user_session_path
      end
    else
      root_path
    end
  end
  ## FIN METODOS ACTIVEAMDIN

  def set_user_language
    I18n.locale = :es
  end

  protected
    def mixpanel
      @mixpanel ||= Mixpanel::Tracker.new( GlobalConstants::token_for_mixpanel ) do |type, message|
         MixpanelJob.delay.perform( [type, message].to_json )
      end
    end

#   cedar14

end
