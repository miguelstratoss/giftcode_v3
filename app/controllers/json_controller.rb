
class JsonController < ApplicationWebController
  include ActionView::Helpers::NumberHelper
  before_filter :auth_user

  #friends_controller
  def friends
    friends = current_user.friends.find(:all, :select => "users.id, users.first_name, users.last_name, CASE WHEN ( (users.email LIKE '%facebook.com%') OR (users.user_state_id = #{UserState::STATES[:removed]}) ) THEN '' ELSE users.email	END, users.fbk_user_id, users.user_state_id, users.referral_id, users.image", :order => :first_name)
    render :json => friends.as_json(
        :only => [:id, :first_name, :last_name, :email, :fbk_user_id, :user_state_id, :referral_id, :mobile_phone, :image  ],
        :methods => :complete_name
    )
  end

  #friends_controller
  def referrals_friends
    referrals = current_user.referrals.find( :all, :order => :first_name )
    render :json => referrals.as_json(:only => [:id, :first_name, :last_name ])
  end

  #friends_controller
  def invite_friends
    invites = current_user.invite_friends.select( :friend_id ).uniq(:friend_id)
    render :json => invites.as_json(:only => [:friend_id])
  end

  #give_controller/index_brands_angular
  def brands
    country =  country()
    json = Array.new
    if !country.nil?
      brands =  Brand.brands_enabled_for_sending_a_gift_code( country.id ).ordered_by_name_asc
      brands.each do |b|
        if b.campaign_enable_for_selling
          json << { :id => b.id, :name => b.name, :url => b.image, :amounts_favorites_brand => b.favorites.where( "enabled = #{true} AND user_id != #{current_user.id}").count }
        end
      end
    end
    render :json => json.as_json
  end

  #give_controller/index_friends_angular
  def brands_favorites_friend_give
    country =  country()
    json = Array.new
    if !country.nil?
      friend = current_user.friends.find(params[:friend_id])
      if friend
        brands = Brand.brands_enabled_for_sending_a_gift_code( country.id ).joins(:favorites => :user).where( 'users.id'=>friend.id, 'favorites.enabled' => true ).order('brands.name')
        brands.each do |b|
          json << { :id => b.id, :name => b.name, :url => b.image, :amounts_favorites_brand => b.favorites.where( "enabled = #{true} AND user_id != #{current_user.id}").count }
        end
      end
    end
    render :json => json.as_json
  end

  #give_controller/index_brands_angular
  def friends_favorite_brands
    brands = current_user.friends.joins(:favorites => :brand).where( 'favorites.enabled' => true, 'brands.enable_for_selling' => true).select('brands.id AS brand_id, users.first_name, users.last_name, users.id AS user_id')
    render :json => brands.as_json(:only => [:brand_id, :user_id, :first_name, :last_name ])
  end

  def friends_favorite_brand
    brands = current_user.friends.joins(:favorites => :brand).where( 'favorites.enabled' => true, 'brands.enable_for_selling' => true, 'brands.id' => params[:brand_id] ).select('users.first_name, users.last_name, users.id AS user_id')
    render :json => brands.as_json(:only => [:user_id, :first_name, :last_name ])
  end

  def brand_parameters_gift_code
    brand = Brand.find params[:brand_id]
    json = Array.new
    if brand
      country =  country()
      campaign = brand.campaigns.find_by_country_id_and_campaign_state_id(country.id, CampaignState::STATES[:enable])
      cities = campaign.cities
      gift_cards = campaign.gift_cards_enable_for_selling
      json = {
          :cities => cities.as_json(:only => [:id, :name]),
          :gift_cards => gift_cards.as_json(
                    :only => [:id ],
                    :methods => [:cost_current, :cost_plus_cost_total_admin_current ],
                    :include => { :campaign => {
                                    :include => {
                                      :currency => { :only => [ :short_name ] }
                                    },
                                    :only => ""
                                }
                    }
                ),
          :cost => {
              :admin => { :value => campaign.cost_total_admin_current, :currency => { :short_name => campaign.currency.short_name } }.as_json
            }.as_json,
          :url => brand.image.as_json
      }
    end
    render :json => json.as_json
  end

  #favorites_controller
  def favorites
    favorites = current_user.favorites.all
    array_favorites = {}
    favorites.each do |bf|
      if bf.brand.enable_for_selling && bf.categories.count > 0
        array_favorites[ bf.brand_id ] = bf.enabled
      end
    end
    render :json => array_favorites.as_json
  end

  #friends_controller/perfil
  def favorites_friend
    friend = current_user.friends.find params[:friend_id]
    if friend
      favorites = friend.favorites.all
      array_favorites = {}
      favorites.each do |bf|
        if bf.brand.enable_for_selling && bf.categories.count > 0
          array_favorites[ bf.brand_id ] = bf.enabled
        end
      end
      render :json => array_favorites.as_json
    else
      render :json => {}.as_json
    end
  end

  #gift_codes_controller/bono
  def gift_code
    gift_code =  current_user.gift_codes_received.find_by_ref_sale( params[:ref_sale] )
    if !gift_code.nil?
      if gift_code.is_a_free_gift_code?
        brand = gift_code.free_campaign.office.brand
        country = ''
        office = gift_code.free_campaign.office
      else
        if gift_code.type == "PUNTOS"
          brand = gift_code.campaign_point.brand
          country = gift_code.campaign_point.country.name
        else
          brand = gift_code.gift_card.campaign.brand
          country = gift_code.gift_card.campaign.country.name
        end
        office = ''
      end

      if gift_code.binary_qr_code_image.blank?
        qr = ChunkyPNG::Image.from_blob(File.read(Rails.root.to_s + '/app/assets/images/icono_grande.png')).to_data_url
      else
        qr = ChunkyPNG::Image.from_blob(gift_code.binary_qr_code_image).to_data_url
      end

      render :json => {
          :ref_sale => gift_code.ref_sale,
          :data => gift_code.as_json(:only => [:from_first_name, :from_last_name, :to_first_name, :to_last_name, :body_message]),
          :is_free => gift_code.is_a_free_gift_code?,
          :cost =>  number_to_currency(gift_code.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :available_balance =>  number_to_currency(gift_code.available_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :qr => qr,
          # :qr => ChunkyPNG::Image.from_blob(gift_code.binary_qr_code_image).to_data_url,
          :pin => gift_code.redemption_pin_formatted(' '),
          :decrypt => gift_code.decrypt,
          :card_number => gift_code.redemption_pin_formatted(' ')[0..-5],
          :card_key => gift_code.redemption_pin_formatted(' ')[-4..gift_code.redemption_pin_formatted.length],
          # :city =>  gift_code.to_city.name,
          :country => country,
          :office => office.as_json(:only => [:office_name,]),
          :valid => l( gift_code.valid_until, :format => :web),
          :brand => brand.as_json(:only => [:image, :name ])
      }
    else
      render :json => {}
    end
  end

  #gift_codes_controller/bono
  def gift_code_sent
    gift_code =  current_user.gift_codes_sent.find_by_ref_sale( params[:ref_sale] )
    if !gift_code.nil?
      if gift_code.is_a_free_gift_code?
        brand = gift_code.free_campaign.office.brand
        country = ''
        office = gift_code.free_campaign.office
      else
        if gift_code.type == "PUNTOS"
          brand = gift_code.campaign_point.brand
          country = gift_code.campaign_point.country.name
        else
          brand = gift_code.gift_card.campaign.brand
          country = gift_code.gift_card.campaign.country.name
        end
        office = ''
      end

      render :json => {
          :is_free => false,
          :data => gift_code.as_json(:only => [:from_first_name, :from_last_name, :to_first_name, :to_last_name, :body_message]),
          :cost =>  number_to_currency(gift_code.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :available_balance =>  number_to_currency(gift_code.available_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :qr => ChunkyPNG::Image.from_blob(File.read(Rails.root.to_s + '/app/assets/images/icono_grande.png')).to_data_url,
          # :city =>  gift_code.to_city.name,
          :country => country,
          :office => office.as_json(:only => [:office_name,]),
          :valid => l( gift_code.valid_until, :format => :web),
          :brand => brand.as_json(:only => [:image, :name ])
      }
    else
      render :json => {}
    end
  end

  #gift_codes_controller
  def gift_codes
    gift_codes =  current_user.gift_codes_received
    data  = gift_codes.collect {
      |gc| {
        :ref_sale => gc.ref_sale,
        :cost => number_to_currency(gc.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
        :redeemed => gc.redeemed_state.as_json,
        :from_name => (
          if gc.is_a_free_gift_code?
            gc.office.brand.name
          else
            gc.from_first_name + ' ' + gc.from_last_name
          end
        ),
        :img_url => (
          if gc.is_a_free_gift_code?
            gc.office.brand.image.url(:thumb195).to_s
          else
            if gc.type == "PUNTOS"
              gc.campaign_point.brand.image.url(:thumb195).to_s
            else
              gc.gift_card.campaign.brand.image.url(:thumb195).to_s
            end
          end
        )
      }
    }
    render :json => data.as_json
  end

  #gift_codes_controller
  def gift_codes_sent
    gift_codes =  current_user.gift_codes_sent
    data  = gift_codes.collect {
        |gc| {
          :ref_sale => gc.ref_sale,
          :cost => number_to_currency(gc.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :redeemed => gc.redeemed_state.as_json,
          :to_name => gc.complete_to_name,
          :img_url => (
          if gc.is_a_free_gift_code?
            gc.office.brand.image.url(:thumb195).to_s
          else
            if gc.type == "PUNTOS"
              gc.campaign_point.brand.image.url(:thumb195).to_s
            else
              gc.gift_card.campaign.brand.image.url(:thumb195).to_s
            end
          end
          )
      }
    }
    render :json => data.as_json
  end

  def yo_quiero_facebook
    fbk_brand = params['data']['facebook']
    @link = "#{fbshared3_url}"
    puts @link
    brand = Brand.find_by_name(fbk_brand)

    #puts params.inspect
    begin
      #if Rails.env.production?
      fbk = Facebook.new
      graph = fbk.conn(current_user.facebook_token)
      response = graph.put_wall_post(
          '#YoQuiero un @giftcode de @'+brand.name,
          {
              'name' => '¿Y tu qué quieres?. Comparte aquí tus marcas favoritas.',
              'link' => @link + "?name_brand=#{brand.name}",
              'caption' => 'GIFTCODE',
              #'description' => "#YoQuiero un @giftcode.co de @RopaARMI",
              # 'picture' => ( brand.image_yo_quiero_url != '/assets/brand_yo_quiero_195x195.jpg' ) ? brand.image_yo_quiero_url : GlobalConstants::YO_QUIERO_IMAGE_URL
              # 'picture' => brand.image_email_url(:thumb180).to_s
              'picture' => ( brand.image_yo_quiero_url.to_s != '/assets/brand_yo_quiero_195x195.jpg' ) ? brand.image_yo_quiero_url.to_s : GlobalConstants::YO_QUIERO_IMAGE_URL
          },
          current_user.fbk_user_id
      )
      #el retorno es id de usuario en facebook seguido de un guion y id de post ej 456123_45678987443132
      post_id = response['id'].split('_')
      puts response.inspect
      fw = FacebookWall.new(:user_id => current_user.id, :fbk_post_id => post_id[1] )
      if brand.yo_quiero_facebook==nil
        brand.yo_quiero_facebook=0
      end
      brand.yo_quiero_facebook += 1
      brand.save
      mixpanel.track current_user.email, 'Web - YoQuiero Facebook Enviado' if GlobalConstants.use_mix_panel? && current_user
      render :json => (fw.save).to_json

        #end
    rescue
      puts "Error #{$!}"
      render :json => 'false'
    end

  end

  private
    def country
      country_id = params[:country_id].nil? ? 1 : params[:country_id]
      Country.find country_id
    end


end
