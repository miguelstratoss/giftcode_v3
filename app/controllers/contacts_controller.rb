class ContactsController < ApplicationWebController
  
  def create
    contact = Contact.new( params[:contact])
    msg = { :type => 'error', :text => 'Error al enviar mensaje' }
    if contact.save
      mixpanel.track contact.email, 'Web - Mensaje de contacto enviado' if GlobalConstants.use_mix_panel? && contact.email
      msg = { :type => 'success', :text => 'Se envio mensaje' }
      GiftCodeNotifier.contact_message(contact).deliver
      if session[:retailers] == 1
        mixpanel.track contact.email, 'Web - Contacto desde Retailers' if GlobalConstants.use_mix_panel?
      elsif session[:retailers] == 2
        mixpanel.track contact.email, 'Web - Contacto desde Retailers Beneficios' if GlobalConstants.use_mix_panel?
      else
        mixpanel.track contact.email, 'Web - Contacto desde Footer' if GlobalConstants.use_mix_panel?
      end
    end
    json = { flash: msg }.to_json
    render :json => json
  end

end
