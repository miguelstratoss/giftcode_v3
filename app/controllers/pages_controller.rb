# encoding: UTF-8
require 'rest-client'

class PagesController < ApplicationWebController

  before_filter :auth_user, :except => [:home, :projects, :customers2, :customers, :tos, :politica, :concurso, :concurso2014, :gracias, :fbshared, :fbshared2, :fbshared3, :team, :landing_page1, :landing2, :landing_3, :landing_4, :how_it_works ]


  def home
    set_meta_tags :title => 'Giftcode, el regalo ideal.'[0,70],
                  # :description => 'La forma más sencilla de enviar y recibir bonos electrónicos de regalo. Regístrate ahora y acumula  puntos para enviar giftcodes gratis para ti y tus amigos.​'[0,160],
                  :description => 'La forma más sencilla de enviar y recibir bonos electrónicos de regalo.​'[0,160],
                  :keywords => 'bonos de regalo, bono regalo, bonos de regalo bogota, tarjeta regalo, bonos para regalar, bonos de regalo colombia, bonos de regalo spa, bonos regalo viajes, regalos online, gift cards, giftcards'[0,255],
                  :open_graph => {
                      :title => '¡Unete a Giftcode!',
                      # :description => 'La forma más sencilla de enviar y recibir bonos electrónicos de regalo. Regístrate ahora y acumula  puntos para enviar giftcodes gratis para ti y tus amigos.',
                      :description => 'La forma más sencilla de enviar y recibir bonos electrónicos de regalo.',
                      :type  => :website,
                      :image => GlobalConstants::HOME_IMAGE_IRL
                  }
    if user_signed_in?
        if GlobalConstants.full_version?
          redirect_to give_path
        else
          redirect_to friends_path
        end
    else
      version_layout
    end
  end


  def projects
    set_meta_tags :title => 'no más plástico.'[0,70],
                  :description => 'Nuestros bonos electrónicos de regalo son una alternativa a las actuales tarjetas plásticas altamente contaminantes.'[0,160],
                  :keywords => 'giftcode, bonos electrónicos de regalo, tarjetas plásticas, no más plástico, fácil, rápido, seguro, marcas favoritas, amigos, planeta, más árboles, huella de carbono, eco, green'[0,255],
                  :open_graph => {
                      :title => 'No más plástico.',
                      :description =>'Nuestros bonos electrónicos de regalo son una alternativa a las actuales tarjetas plásticas altamente contaminantes.',
                      :type  => :website,
                      :image => GlobalConstants::HOME_IMAGE_IRL
                  }

  end

  def customers
    set_meta_tags :title => 'Retailers'[0,70],
                  :description => 'Registra tu marca en el canal de e-giftcards de mayor potencial en Colombia.'[0,160],
                  :keywords => 'giftcode, retailer, retailers, clientes corporativos, marcas, negocio, canal online multimarca, marketplace, bonos electrónicos de regalo, gift cards colombia, nuevos clientes, aumentar ventas, responsabilidad social empresarial'[0,255],
                  :open_graph => {
                      :title => 'Retailers',
                      :description =>'Registra tu marca en el canal de e-giftcards de mayor potencial en Colombia.',
                      :type  => :website,
                      :image => GlobalConstants::HOME_IMAGE_IRL
                  }
    session[:retailers] = 1
  end

  def customers2
    set_meta_tags :title => 'Retailers'[0,70],
                  :description => 'Registra tu marca en el canal de e-giftcards de mayor potencial en Colombia.'[0,160],
                  :keywords => 'giftcode, retailer, retailers, clientes corporativos, marcas, negocio, canal online multimarca, marketplace, bonos electrónicos de regalo, gift cards colombia, nuevos clientes, aumentar ventas, responsabilidad social empresarial'[0,255],
                  :open_graph => {
                      :title => 'Retailers',
                      :description =>'Registra tu marca en el canal de e-giftcards de mayor potencial en Colombia.',
                      :type  => :website,
                      :image => GlobalConstants::HOME_IMAGE_IRL
                  }
    session[:retailers] = 2
  end

  def gracias
    set_meta_tags :title => 'Giftcode, el regalo ideal'[0,70],
                  :description => 'Con giftcode puedes sorprender en fechas especiales a tus familiares y amigos con bonos de regalo de sus marcas favoritas. Es facil, rapido y seguro.'[0,160],
                  :keywords => 'bonos de regalo, bono regalo, bonos de regalo bogota, tarjeta regalo, bonos para regalar, bonos de regalo colombia, bonos de regalo spa, bonos regalo viajes, regalos online, gift cards, giftcards'[0,255],
                  :open_graph => {
                      :title => 'Gracias!',
                      :description => 'Acabo de recibir un giftcode, me voy de shopping.',
                      :type  => :website,
                      :image => GlobalConstants::HOME_IMAGE_IRL
                  }
    version_layout
  end

  def yo_quiero
    set_meta_tags :title => '#YoQuiero'[0,70],
                  :description => 'Con giftcode puedes sorprender en fechas especiales a tus familiares y amigos con bonos de regalo de sus marcas favoritas. Es facil, rapido y seguro.'[0,160],
                  :keywords => 'bonos de regalo, bono regalo, bonos de regalo bogota, tarjeta regalo, bonos para regalar, bonos de regalo colombia, bonos de regalo spa, bonos regalo viajes, regalos online, gift cards, giftcards'[0,255],
                  :open_graph => {
                      :title => 'Lo que verdaderamente quieres.',
                      :description => '#YoQuiero muchos bonos electronicos de regalo de mis marcas favoritas y tu? enterate cómo aqui.',
                      :type  => :website,
                      :image => GlobalConstants::YO_QUIERO_IMAGE_URL
                  }
    @brands_tw = Brand.where("sn_twitter_name is not null AND sn_twitter_name <> '' ").order(:name)
    @brands_fbk = Brand.select( :name).order(:name)
  end

  def fbshared
    set_meta_tags :title => 'Giftcode, el regalo ideal.'[0,70],
                  # :description => 'La forma más sencilla de enviar y recibir bonos electrónicos de regalo. Regístrate ahora y acumula  puntos para enviar giftcodes gratis para ti y tus amigos.​'[0,160],
                  :description => 'La forma más sencilla de enviar y recibir bonos electrónicos de regalo.'[0,160],
                  :keywords => 'bonos de regalo, bono regalo, bonos de regalo bogota, tarjeta regalo, bonos para regalar, bonos de regalo colombia, bonos de regalo spa, bonos regalo viajes, regalos online, gift cards, giftcards'[0,255],
                  :open_graph => {
                      :title => 'Es posible dar regalos sin gastar dinero.',
                      :description => 'Gánate tus primeros 100 puntos para canjear por bonos de regalo de tus marcas favoritas.',
                      :type  => :website,
                      :image => 'http://greencode.co/images/imagen_1.jpg'
                  }
    version_layout
  end

  def fbshared2
    set_meta_tags :title => 'Giftcode, el regalo ideal.'[0,70],
                  :description => 'La forma más sencilla de enviar y recibir bonos electrónicos de regalo. Regístrate ahora y acumula  puntos para enviar giftcodes gratis para ti y tus amigos.​'[0,160],
                  :keywords => 'bonos de regalo, bono regalo, bonos de regalo bogota, tarjeta regalo, bonos para regalar, bonos de regalo colombia, bonos de regalo spa, bonos regalo viajes, regalos online, gift cards, giftcards'[0,255],
                  :open_graph => {
                      :title => 'Es posible dar regalos sin gastar dinero.',
                      :description => 'Gánate tus primeros 100 puntos para canjear por bonos de regalo de tus marcas favoritas.',
                      :type  => :website,
                      :image => 'http://greencode.co/images/imagen_2.jpg'
                  }
    version_layout
  end

  def fbshared3

    brand = Brand.find_by_name(params[:name_brand])
    image = ( brand.image_yo_quiero_url.to_s != '/assets/brand_yo_quiero_195x195.jpg' ) ? brand.image_yo_quiero_url.to_s : GlobalConstants::YO_QUIERO_IMAGE_URL

    set_meta_tags :title => 'Giftcode, el regalo ideal.'[0,70],
                  :description => 'La forma más sencilla de enviar y recibir bonos electrónicos de regalo. Regístrate ahora y acumula  puntos para enviar giftcodes gratis para ti y tus amigos.​'[0,160],
                  :keywords => 'bonos de regalo, bono regalo, bonos de regalo bogota, tarjeta regalo, bonos para regalar, bonos de regalo colombia, bonos de regalo spa, bonos regalo viajes, regalos online, gift cards, giftcards'[0,255],
                  :open_graph => {
                      :title => '¿Y tu qué quieres?.',
                      :description => 'Comparte aqui tus marcas favoritas',
                      :type  => :website,
                      :image => image
                      # :image => GlobalConstants::YO_QUIERO_IMAGE_URL
                  }
    version_layout
  end

  def landing_3
    render layout: 'nuevo'
  end

  def landing_4
    render layout: 'nuevo'
  end

  private
    def version_layout
      render 'home'
    end
end
