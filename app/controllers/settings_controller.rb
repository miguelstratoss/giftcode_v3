require 'rest-client'
# encoding: UTF-8
class SettingsController < ApplicationWebController

  before_filter :auth_user

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    msg = { :type => 'error', :text => 'No se logro guardar perfil, por favor intentelo de nuevo' }
    change = @user.change_email(params[:email])
    if change == 'change' || change == true
      # , :language_id => params[:language_id]
      if @user.update_attributes(:image => params[:file], :first_name => params[:first_name], :last_name => params[:last_name], :sex_id => params[:sex_id], :fbk_location_name => params[:fbk_location_name], :identification => params[:identification], :office_phone => params[:office_phone], :mobile_phone => params[:mobile_phone], :birthday => params[:birthday]  )
        if change == 'change'
          msg = { :type => 'success', :text => 'Se actualizo perfil y se envio correo electrónico con instrucciones sobre como validar el nuevo correo electrónico.' }
        else
          msg = { :type => 'success', :text => 'Se actualizo perfil' }
        end
      else
        if change == 'change' || change == true
          msg = { :type => 'error', :text => 'No se actualizaron todos lo datos, se envio correo electrónico con instrucciones sobre como validar el nuevo correo electrónico.' }
        end
      end
      puts @user.errors.full_messages
      json = { 
        flash: msg, 
        redirect_to: @user.show_introduction ? favorites_first_time_path : '' ,
        profile: @user.as_json( only: [ :image, :first_name, :last_name, :sex_id, :language_id, :fbk_location_name, :identification, :office_phone, :mobile_phone]) 
      }.to_json
      render :json => json
    else
      puts @user.errors.full_messages
      if change == 'email'
        msg = { :type => 'error', :text => 'Correo electronico ya esta en uso o invalido.' }
      else
        msg = { :type => 'error', :text => 'Error desconocido al validar email.' }
      end     
      
      json = { 
        flash: msg, 
        profile: @user.as_json( only: [ :image, :first_name, :last_name, :sex_id, :language_id, :fbk_location_name, :identification, :office_phone, :mobile_phone])
      }.to_json
      render :json => json
      
    end
  end

  def validate_account
    if current_user.email_fbk_or_temp.present? && !current_user.email_fbk_or_temp.blank? && current_user.token_validate_email.present? && !current_user.token_validate_email.blank? && current_user.token_validate_email == session[:token_validate_email]
      email = current_user.email
      current_user.email = current_user.email_fbk_or_temp
      current_user.email_fbk_or_temp = email
      current_user.data_email_confirmation = true
      current_user.token_validate_email = nil
      session[:token_validate_email] = nil
      if current_user.save
        flash[:notice] = 'ok'
      else
        if current_user.valid?(:email)
          flash[:alert] = 'Correo electronico ya esta en uso o invalido.'
        else
          flash[:alert] = 'Error desconocido al validar email. Por favor intente de nuevo cambiar el correo electronico.'
        end
      end
      redirect_to edit_settings_path
    else
      redirect_to root_path
    end
  end

  def confirmation_email
    render :json => current_user.change_email(params[:email])
  end

  def confirmation_birthday
    if params[:birthday].present? && !params[:birthday].blank?
      if current_user.birthday != params[:birthday]
        current_user.birthday = params[:birthday]
        current_user.birthday_confirmation = true
        if current_user.valid?
          if current_user.save
            render :json => true
          else
            puts current_user.errors.full_messages
            render :json => false
          end
        else
          puts current_user.errors.full_messages
          unless current_user.valid?(:birthday)
            render :json => 'birthday'
          else
            render :json => 'unknown'
          end
        end
      else
        current_user.birthday_confirmation = true
        if current_user.save
          render :json => true
        else
          render :json => false
        end
      end
    else
      render :json => false
    end
  end

  def confirmation_city
    if params[:city].present? && !params[:city].blank?
      if current_user.fbk_location_name != params[:city]
        current_user.fbk_location_name = params[:city]
        if current_user.valid?
          current_user.city_confirmation = true
          if current_user.save
            render :json => true
          else
            puts current_user.errors.full_messages
            render :json => false
          end
        else
          puts current_user.errors.full_messages
          unless current_user.valid?(:fbk_location_name)
            render :json => 'city'
          else
            render :json => 'unknown'
          end
        end
      else
        current_user.city_confirmation = true
        if current_user.save
          render :json => true
        else
          puts current_user.errors.full_messages
          render :json => false
        end
      end
    else
      render :json => false
    end
  end

  def recommend_brand
    session[:main_tab]=""
    session[:settings_tab]="services"
    description = params[:service][:name]
    if description.blank?
      flash[:error]="Por favor ingresa la información de tus marcas favoritas."
    else
      current_user.recommendations.create(:description => description)
      flash[:success]="Gracias por tus sugerencias. Nos esforzaremos porque próximamente encuentres todas tus marcas favoritas en GiftCode."
    end
    redirect_to brands_path
  end

end
