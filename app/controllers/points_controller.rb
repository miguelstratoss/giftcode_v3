class PointsController <  GiveController

  def index
    @points = current_user.available_points
    @campaigns = CampaignPoint.enable_for_selling.count
  end

  def create_gift_code
    campaign = CampaignPoint.find params[:campaign_id]
    msg = { :type => 'error', :text => 'Cantidad de puntos insuficientes' }
    json = { flash: msg }.as_json
    if current_user.available_points >= campaign.points_required
      gift_code = campaign.point_gift_codes.new (params[:gift_code])
      msg = { :type => 'error', :text => 'Error al crear bono' }
      json = { flash: msg }.as_json
      unless gift_code.nil?
        gift_code.from_user = current_user
        gift_code.from_first_name = current_user.first_name
        gift_code.from_last_name = current_user.last_name
        gift_code.from_email = current_user.email
        gift_code.gift_code_state_id = GiftCodeState::STATES[:free_points]
        gift_code.cost = gift_code.campaign_point.cost
        gift_code.iva = 0
        gift_code.points_used = campaign.points_required
        if gift_code.send_notifications_now
          gift_code.send_date_time = Time.zone.now
        end
        gift_code.valid_for_days = gift_code.campaign_point.number_days_missing
        gift_code.calculate_base_iva
        gift_code.generate_ref_sale
        to_user = User.find_by_email( gift_code.to_email )
        if to_user.nil?
          to_user = User::user_new( gift_code.to_first_name,gift_code.to_last_name,gift_code.to_email )
        end
        gift_code.to_user = to_user
        gift_code.process_successful_payment
        if !gift_code.redemption_pin.nil? && gift_code.save
          msg = { :type => 'success', :text => 'Se guardo bono.' }
        else
          msg = { :type => 'error', :text => 'Error al guardar bono, por favor recargue/actualize  la pagina.', :errors => gift_code.errors.to_json }
        end
        json = {
            flash: msg,
            ref_sale: gift_code.ref_sale
        }.as_json
      end
    end
    render :json => json
  end

  def gift_code
    @gift_code = PointGiftCode.find_by_ref_sale params[:ref_sale]
    if @gift_code.present?

    else
      @error = "No se encontro GiftCode"
      render :error
    end
  end

  def json_campaigns_points
    #CampaignPoint.where( "enabled = :enabled and end_date >= :today", { enabled: true, today: Time.zone.now }  ).joins("LEFT JOIN gift_codes ON gift_codes.type = 'PointGiftCode' AND gift_codes.campaign_point_id = campaigns.id").group("campaigns.id").having("count(campaigns.id) < campaigns.number_of_free_gift_codes")
    # campaigns = CampaignPoint.campaign_enabled.order("points_required ASC")
    campaigns = CampaignPoint.enable_for_selling
    render json: campaigns.to_json(
        only: [ :id, :begin_date, :end_date, :points_path, :number_of_free_gift_codes, :points_required, :observations, :cost ],
        methods: [ :cost_current, :available_gift_codes, :cities ],
        include: [ brand: {
            only: [ :id, :name, :image ],
        } ]
    )
  end

end