class SitemapController < ApplicationWebController

  def index
    redirect_to 'https://giftcodeprov2.s3.amazonaws.com/sitemaps/sitemap.xml.gz'
  end

end