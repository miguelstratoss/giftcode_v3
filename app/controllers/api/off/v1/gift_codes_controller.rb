class Api::Off::V1::GiftCodesController < Api::Off::V1::ApplicationController

  respond_to :json

  def create
    @gift_code = GiftCode.new

    @gift_code.send_notifications_now = params['send_notifications_now']
    @gift_code.send_date_time = params['send_date_time']
    @gift_code.from_first_name = params['from_first_name']
    @gift_code.from_last_name = params['from_last_name']
    @gift_code.from_email = params['from_email']
    @gift_code.to_first_name = params['to_first_name']
    @gift_code.to_last_name = params['to_last_name']
    @gift_code.to_mobile_phone = params['to_mobile_phone']
    @gift_code.to_email = params['to_email']
    @gift_code.gift_card_id = params['gift_card_id']
    @gift_code.to_country = 'CO'
    @gift_code.body_message = params['body_message']
    @gift_code.to_address = params['to_address']
    @gift_code.to_state = params['to_state']
    @gift_code.to_city = params['to_city']


    from_user = User.find_by_email ( @gift_code.from_email )
    if from_user.nil?
      from_user = User.user_new( @gift_code.from_first_name, @gift_code.from_last_name, @gift_code.from_email )
      puts from_user.errors.messages
    end
    to_user = User.find_by_email( @gift_code.to_email )
    if to_user.nil?
      to_user = User.user_new( @gift_code.to_first_name, @gift_code.to_last_name, @gift_code.to_email )
      puts to_user.errors.messages
    end

    @gift_code.from_user = from_user
    @gift_code.to_user = to_user
    @gift_code = load_data( @gift_code )
    @gift_code.gift_code_state_id = GiftCodeState::STATES[:generated_and_payment_pending]

    campaign = @gift_code.gift_card.campaign
    brand_codes = campaign.brand_codes.find_by_active_for_payment true

    if brand_codes
      @entity = brand_codes.third_party_entity
    else
      @entity = nil
    end

    @gift_code.save

    respond_with @gift_code

  end

  private

    def load_data( gift_code = nil )
      unless gift_code.gift_card_id.nil?
        gift_code.gift_card = GiftCard.find gift_code.gift_card_id
        gift_code.cost = gift_code.gift_card.cost
        if gift_code.send_notifications_now
          gift_code.send_date_time = Time.zone.now
        end
        gift_code.valid_for_days = gift_code.gift_card.valid_for_days
        gift_code.iva = gift_code.gift_card.campaign.iva
        gift_code.calculate_base_iva
        gift_code.generate_ref_sale
      end
      return gift_code
    end

end