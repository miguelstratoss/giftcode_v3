class Api::Off::V1::PagesController < Api::Off::V1::ApplicationController

  respond_to :html

  def index
    if params[:brand_name]
      @brand = Brand.brands_enabled_for_sending_a_gift_code( 1 ).where( name: params[:brand_name] ).first
      if @brand
        unless @brand.campaign_enable_for_selling
          raise ActiveRecord::RecordNotFound
        end
      else
        raise ActiveRecord::RecordNotFound
      end
    end
  end

end