class Api::Off::V1::BrandsController < Api::Off::V1::ApplicationController

  respond_to :json

  def index
    json = Array.new
    brands =  Brand.brands_enabled_for_sending_a_gift_code( 1 ).ordered_by_name_asc
    brands.each do |b|
      if b.campaign_enable_for_selling
        json << { :id => b.id, :name => b.name, :url => b.image }
      end
    end
    render :json => json.as_json
  end

  def show
    brand = Brand.find params[:id]
    json = Array.new
    if brand
      campaign = brand.campaigns.find_by_country_id_and_campaign_state_id(1, CampaignState::STATES[:enable])
      # cities = campaign.cities
      gift_cards = campaign.gift_cards_enable_for_selling
      json = {
          :id => brand.id, :name => brand.name, :url => brand.image,
          :gift_cards => gift_cards.as_json(
              :only => [:id ],
              :methods => [:cost_current, :cost_plus_cost_total_admin_current ],
              :include => { :campaign => {
                  :include => {
                      :currency => { :only => [ :short_name ] }
                  },
                  :only => ""
              }
              }
          ),
          :cost => {
              :admin => { :value => campaign.cost_total_admin_current, :currency => { :short_name => campaign.currency.short_name } }.as_json
          }.as_json
      }
    end
    render :json => json.as_json
  end

end