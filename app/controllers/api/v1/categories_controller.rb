class Api::V1::CategoriesController < Api::V1::BaseController

  doorkeeper_for :all

  def index
    categories = Category.all
    send = categories.collect{
        |category|{
          id: category.id,
          _class: category._class,
          name: category.name,
          position: category.position,
          user_favorites: user_favorites(current_user, category),
          brands: (category.brands).collect {
              |brand|{
                id: brand.id,
                name: brand.name,
                favorite: brand.favorite(current_user)
            }
          }
      }
    }
    render json: send.to_json
  end


  private
    def user_favorites user, category
      c = user.categories.where(id: category.id).uniq
      if c.count > 0
        c = c.first
        c.favorites.where(:user_id => user.id).count
      else
        0
      end
    end
end