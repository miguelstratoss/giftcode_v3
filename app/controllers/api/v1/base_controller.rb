class Api::V1::BaseController < ActionController::Base

  respond_to :json, :xml

  def current_user
    User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end

  def country
    country_id = params[:country_id].nil? ? 1 : params[:country_id]
    Country.find country_id
  end

end