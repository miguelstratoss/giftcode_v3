class Api::V1::FavoritesController < Api::V1::BaseController

  doorkeeper_for :all

  def add
    favorite = current_user.favorites.find_by_brand_id( params[:brand_id] )
    msg = { :type => 'error', :text => 'No se actualizo marca favorita' }
    if favorite.nil?
      favorite = current_user.favorites.new( :brand_id => params[:brand_id], :enabled => true )
      if favorite.save
        msg = { :type => 'success', :text => 'Guardado' }
      end
    else
      if favorite.update_attributes( :enabled => favorite.enabled ? false : true )
        msg = { :type => 'success', :text => 'Guardado' }
      end
    end
    render json: msg.to_json
  end
end