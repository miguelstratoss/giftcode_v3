class Api::V1::GiftCodesController < Api::V1::BaseController
  include ActionView::Helpers::NumberHelper
  doorkeeper_for :all

  def sent
    gift_codes =  current_user.gift_codes_sent
    data  = gift_codes.collect {
        |gc| {
          :ref_sale => gc.ref_sale,
          :cost => number_to_currency(gc.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :redeemed => gc.redeemed_state.as_json,
          :to_name => gc.complete_to_name,
          :body_message => gc.body_message,
          :img_url => (
          if gc.is_a_free_gift_code?
            gc.office.brand.image.url(:thumb195).to_s
          else
            if gc.type == "PUNTOS"
              gc.campaign_point.brand.image.url(:thumb195).to_s
            else
              gc.gift_card.campaign.brand.image.url(:thumb195).to_s
            end
          end
          )
      }
    }
    render :json => data.as_json
  end

  def received
    gift_codes =  current_user.gift_codes_received
    data  = gift_codes.collect {
        |gc| {
          :ref_sale => gc.ref_sale,
          :cost => number_to_currency(gc.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :redeemed => gc.redeemed_state.as_json,
          :body_message => gc.body_message,
          :available_balance =>  number_to_currency(gc.available_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :from_name => (
          if gc.is_a_free_gift_code?
            gc.office.brand.name
          else
            gc.from_first_name + ' ' + gc.from_last_name
          end
          ),
          :img_url => (
          if gc.is_a_free_gift_code?
            gc.office.brand.image.url(:thumb195).to_s
          else
            if gc.type == "PUNTOS"
              gc.campaign_point.brand.image.url(:thumb195).to_s
            else
              gc.gift_card.campaign.brand.image.url(:thumb195).to_s
            end
          end
          )
      }
    }
    render :json => data.as_json
  end

  def gift_code_received
    gift_code =  current_user.gift_codes_received.find_by_ref_sale( params[:ref_sale] )
    if !gift_code.nil?
      if gift_code.is_a_free_gift_code?
        brand = gift_code.free_campaign.office.brand
        country = ''
        office = gift_code.free_campaign.office
      else
        brand = gift_code.gift_card.campaign.brand
        country = gift_code.gift_card.campaign.country.name
        office = ''
      end
      render :json => {
          :data => gift_code.as_json(:only => [:from_first_name, :from_last_name, :to_first_name, :to_last_name, :body_message]),
          :is_free => gift_code.is_a_free_gift_code?,
          :cost =>  number_to_currency(gift_code.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :available_balance =>  number_to_currency(gift_code.available_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),
          :qr => ChunkyPNG::Image.from_blob(gift_code.binary_qr_code_image).to_data_url,
          :pin => gift_code.redemption_pin_formatted(' '),
          # :city =>  gift_code.to_city.name,
          :country => country,
          :office => office.as_json(:only => [:office_name,]),
          :valid => l( gift_code.valid_until, :format => :web),
          :brand => brand.as_json(:only => [:image, :name ])
      }
    else
      render :json => {}
    end
  end

  def create
    if params[:gift_code]
      data = params[:gift_code]
    else
      data = params
    end
    gift_code = GiftCode.new(data)
    msg = { :type => 'error', :text => 'Error al crear bono' }
    json = { flash: msg }.as_json
    if !gift_code.nil?
      gift_code.from_user = current_user
      gift_code.from_first_name = current_user.first_name
      gift_code.from_last_name = current_user.last_name
      gift_code.from_email = current_user.email
      gift_code.gift_code_state_id = GiftCodeState::STATES[:generated_and_payment_pending]
      gift_code.cost = gift_code.gift_card.cost
      gift_code.iva = gift_code.gift_card.campaign.iva
      if gift_code.send_notifications_now
        gift_code.send_date_time = Time.zone.now
      end
      gift_code.valid_for_days = gift_code.gift_card.valid_for_days
      gift_code.calculate_base_iva
      gift_code.generate_ref_sale
      # fb_id = gift_code.to_email.split("@")[0].to_i
      # to_user = User.find_by_fbk_user_id( fb_id )
      to_user = User.find_by_email( gift_code.to_email )
      if to_user.nil?
        to_user = User::user_new( gift_code.to_first_name,gift_code.to_last_name,gift_code.to_email )
      else
        gift_code.to_email = to_user.email
      end
      gift_code.to_user = to_user
      if gift_code.save
        msg = { :type => 'success', :text => 'Se guardo bono' }
      else
        msg = { :type => 'error', :text => 'Error al guardar bono', :errors => gift_code.errors.to_json }
        puts "ERROR AL CREAR GIFTCODE"
      end
      puts msg
      json = {
          flash: msg,
          ref_sale: gift_code.ref_sale,
          # city: gift_code.to_city.name,
          cost_current: gift_code.gift_card.cost_current,
          currency_short_name: gift_code.gift_card.campaign.currency.short_name
      }.as_json
    end
    render :json => json
  end

  def parameters_brand
    brand = Brand.find params[:brand_id]
    json = Array.new
    if brand
      country =  country()
      campaign = brand.campaign_enable_for_selling
      cities = campaign.cities
      gift_cards = campaign.gift_cards_enable_for_selling
      json = {
          :cities => cities.as_json(:only => [:id, :name]),
          :gift_cards => gift_cards.as_json(
              :only => [:id ],
              :methods => [:cost_current, :cost_plus_cost_total_admin_current ],
              :include => { :campaign => {
                  :include => {
                      :currency => { :only => [ :short_name ] }
                  },
                  :only => ""
              }
              }
          ),
          :cost => {
              :admin => { :value => campaign.cost_total_admin_current, :currency => { :short_name => campaign.currency.short_name } }.as_json
          }.as_json,
          :url => brand.image.as_json
      }
    end
    render :json => json.as_json
  end


end