class Api::V1::SettingsController < Api::V1::BaseController

  doorkeeper_for :all

  def profile
    @user = current_user
    respond_with(@user.to_json(:only => [ :email, :first_name, :last_name, :birthday, :mobile_phone, :office_phone, :sex_id, :time_zone, :language_id, :fbk_user_id ], :methods => :image_for_mobile ))
  end

  def update
    @user = current_user
    if @user.update_attributes(:office_phone => params[:office_phone], :birthday => params[:birthday], :first_name => params[:first_name], :last_name => params[:last_name], :image => params[:file] )
      render :json => ({ :success => 'Se actualizo datos' }).to_json
    else
      render :json => @user.errors.to_json(full_messages: true)
    end
  end

  def update_image
    @user = current_user
    if !params[:file].blank?
      if params[:file]
         picture_path_params = params[:file]
         #create a new tempfile named fileupload
         tempfile = Tempfile.new('fileupload')
         tempfile.binmode
         #get the file and decode it with base64 then write it to the tempfile
         tempfile.write(Base64.decode64(picture_path_params['bin']))
         #create a new uploaded file
         uploaded_file = ActionDispatch::Http::UploadedFile.new(:tempfile => tempfile, :filename => picture_path_params['filename'], :original_filename => picture_path_params['original_filename'])
         @user.update_attributes( :image => uploaded_file )
         render :json => ({ :success => 'Se actualizo image' }).to_json
      else
         render :json => @user.errors.to_json(full_messages: true)
      end
    else
      if params[:image].blank?
        render :json => ({ :error => 'Campo vacio'}).to_json
      end
    end
  end

  def update_birthday
    @user = current_user
    if @user.update_attributes(:birthday => params[:birthday], :birthday_confirmation => true)
      render :json => ({ :success => 'Se actualizo dato' }).to_json
    else
      render :json => @user.errors.to_json(full_messages: true)
    end
  end

  def birthday_confirmation
    render :json => ( current_user.birthday_confirmation ? true:false ).to_json
  end
end