class Api::V1::BrandsController < Api::V1::BaseController

  doorkeeper_for :all

  def all
    country =  country()
    json = Array.new
    if !country.nil?
      brands =  Brand.brands_enabled_for_sending_a_gift_code( country.id ).ordered_by_name_asc
      brands.each do |b|
        if b.campaign_enable_for_selling
          json << { :id => b.id, :name => b.name, :url => b.image, :amounts_favorites_brand => b.favorites.where( "enabled = #{true} AND user_id != #{current_user.id}").count }
        end
      end
    end
    render :json => json.as_json
  end

  def friends_favorite_brand
    brands = current_user.friends.joins(:favorites => :brand).where( 'favorites.enabled' => true, 'brands.enable_for_selling' => true, 'brands.id' => params[:brand_id] ).select('users.first_name, users.last_name, users.id AS user_id, users.fbk_user_id, users.email')
    render :json => brands.as_json(:only => [:user_id, :first_name, :last_name, :fbk_user_id, :email ])
  end

  def favorite_brands
    favorites = current_user.favorites.where( enabled: true )
    render :json => favorites.to_json(:only => [ :id ], :methods => [:brand_name])
  end

end