class Api::V1::FriendsController < Api::V1::BaseController

  doorkeeper_for :all

  def index
    @user = current_user
    @friends = @user.friends
    render json: @friends.to_json(:only => [ :email, :first_name, :last_name, :birthday, :mobile_phone, :office_phone, :sex_id, :time_zone, :language_id, :fbk_user_id ], :methods => :image_for_mobile )
  end

  def friends_in_gift_code
    friends = current_user.friends.where(:user_state_id => UserState::STATES[:registered] )
    render json: friends.to_json(:only => [ :id, :fbk_user_id, :email ])
  end

end