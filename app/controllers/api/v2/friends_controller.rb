class Api::V2::FriendsController < Api::V2::BaseController


  def index
    @user = current_user
    @friends = @user.friends
    respond_with(@friends.to_json(:only => [ :email, :first_name, :last_name, :birthday, :mobile_phone, :office_phone, :sex_id, :time_zone, :language_id, :fbk_user_id ], :methods => :image_for_mobile ))
  end

end