class Api::V2::BaseController < ActionController::Base
  respond_to :json, :xml

  def current_user
    User.last
  end
end