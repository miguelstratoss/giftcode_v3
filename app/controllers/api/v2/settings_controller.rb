class Api::V2::SettingsController < Api::V2::BaseController

  def profile
    @user = current_user
    respond_with(@user.to_json(:only => [ :email, :first_name, :last_name, :birthday, :mobile_phone, :office_phone, :sex_id, :time_zone, :language_id, :fbk_user_id ], :methods => :image_for_mobile ))
  end

end