class Api::V2::CategoriesController < Api::V2::BaseController

  def index
    @categories = Category.all
    respond_with(@categories.to_json(:only => [ :id, :_class, :name, :position ] ))
  end

  def show
    @category = Category.find(params[:id])
    @brands = @category.brands
    respond_with(@brands.to_json(:only => [ :id, :name ], :methods => :image_for_mobile ))
  end

end