# encoding: UTF-8

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    #raise request.env["omniauth.auth"].to_yaml
    # You need to implement the method below in your model
    #@user = User.find_for_facebook_oauth(request.env["omniauth.auth"], current_user)
    @user = find_for_facebook_oauth(request.env['omniauth.auth'], current_user)
    #puts "usuario"
    #puts @user.inspect
    if !@user.nil? && @user.persisted?
      #flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      sign_in_and_redirect @user, :event => :authentication
      login = Login.new( :user_id => @user.id )
      login.save
    else
      session['devise.facebook_data'] = request.env['omniauth.auth']
      flash[:error] = @error #'Habilita tu email en Facebook para continuar.'
      redirect_to root_path
    end
  end

  def failure
    redirect_to root_path
  end

  def find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token.extra.raw_info
    credentials = access_token.credentials
    info = access_token.info
    #logger.debug "id:"+data.id
    #logger.debug "name:"+data.name
    #logger.debug "first_name:"+data.first_name
    #logger.debug "last_name:"+data.last_name
    #logger.debug "link:"+data.link
    #logger.debug "username:"+data.username if data.username
    #logger.debug "gender:"+data.gender
    #logger.debug "email:"+data.email
    #logger.debug "timezone:"+data.timezone.to_s
    #logger.debug "token:"+credentials.token
    #logger.debug "expires_at:"+credentials.expires_at.to_s
    #logger.debug "expires:"+credentials.expires.to_s
    #logger.debug "image:"+info.image
    token = credentials.token
    expires_at = Time.at(Integer(credentials.expires_at))
    expires = credentials.expires
    birthday = data.birthday.present? ? data.birthday : nil
    location_name = data.location.present? && data.location.name.present? ? data.location.name : nil
    locale = data.locale.present? ? data.locale : nil
    timezone = data.timezone.present? ? data.timezone : nil

    user = User.find_by_fbk_user_id data.id.to_i

    #If the user has been created
    if user.present?
      puts GlobalConstants::PREFIX_FOR_LOGS + "user_present: " + user.id.to_s
      #If the user has not been registered
      send_welcome_email = false
      if user.user_state_id == UserState::STATES[:non_registered]
        begin
          User.transaction do
            user.fbk_user_id = data.id.to_i
            user.user_state_id = UserState::STATES[:registered]
            user.reverse_friendships.update_all(:friendship_state_id => FriendshipState::STATES[:friend])
            if set_referer.present?
              user.referral_id = set_referer
              user.date_referral = Time.zone.now
            end
            if session[:utm_source].present?
              user.utm_source = session[:utm_source]
              if session[:utm_medium].present?
                user.utm_medium = session[:utm_medium]
              end
            end
            if data.email.present? && !data.email.blank?
              user.email = data.email
            end
            user.email_fbk = data.email
            if data.birthday.present? && !user.birthday.present?
              begin
                user.birthday = Date.strptime("{ #{birthday} }", "{ %m/%d/%Y }")
              rescue
                user.birthday = Date.strptime("{ #{birthday+'/1800'} }", "{ %m/%d/%Y }")
              end
            end
            user.fbk_location_name = location_name if location_name.present?
            user.registration_date = Time.zone.now
            send_welcome_email = true
          end
        rescue ActiveRecord::Rollback
          puts "The process to register a non-registered user could not be executed"
        end
      end

      if info.image.present? && !user.image.present?
        user.remote_image_url = "https://graph.facebook.com/#{ data.id.to_i }/picture?width=140&height=140"
      end


      user.fbk_locale = locale if locale.present?
      user.fbk_timezone = timezone if timezone.present?

      #This information is very useful for future calling to Facebook API
      user.facebook_expires_at = expires_at
      user.facebook_token = token
      user.facebook_expires = expires
      puts GlobalConstants::PREFIX_FOR_LOGS + "user_present before save: " + user.id.to_s
      user.save
      puts GlobalConstants::PREFIX_FOR_LOGS + "user_present after save: " + user.id.to_s

      #If the user has not been registered
      if send_welcome_email == true
        GiftCodeNotifier.delay.welcome(user)
        # Sent SignUp event to MixPanel
        if GlobalConstants.use_mix_panel?
          # puts cookies["mp_#{GlobalConstants::token_for_mixpanel}_mixpanel"].inspect
          mpjson = JSON.parse(cookies["mp_#{GlobalConstants::token_for_mixpanel}_mixpanel"])
          mixpanel.alias( user.email,  mpjson["distinct_id"] )
          mixpanel.track( user.email, 'Web - Registro Nuevo Usuario' )
          # puts mixpanel.inspect
          # mixpanel.set( user.email, {:email => user.email, :first_name => user.first_name, :last_name => user.last_name} )
        end
      else
        # Sent SignIn event to MixPanel
        if GlobalConstants.use_mix_panel?
          mixpanel.track( user.email, 'Web - Inicio Sesion' )
        end
      end
      puts GlobalConstants::PREFIX_FOR_LOGS + "user_present before sync: " + user.id.to_s
      user.verify_fbk_synchronization
      puts GlobalConstants::PREFIX_FOR_LOGS + "user_present after sync: " + user.id.to_s
    else # Create a user with a stub password.
      puts "no existe: ..... "+session[:amigo].to_s
      birthday = nil
      if data.birthday.present?
        begin
          birthday = Date.strptime("{ #{data.birthday} }", "{ %m/%d/%Y }")
        rescue
          birthday = Date.strptime("{ #{data.birthday+'/1800'} }", "{ %m/%d/%Y }")
        end
      end
      #antes :remote_image_url http://prestaging.giftcode.co/regalar-bono#/_=_=> info.image||=""
      sex_id = (data.gender == "male") ? Sex.find_by_name(Sex::SEXES[:male]).id : Sex.find_by_name(Sex::SEXES[:female]).id
      if  data.email.present? && !data.email.blank?
        email = data.email
      else
        # email = f['id']+'@facebook.com'
        email = data.id.to_s+'@facebook.com'
      end
      user = User.create!(:email => email, :email_fbk => data.email, :password => Devise.friendly_token[0, 20],
                            :first_name => data.first_name||="", :last_name => data.last_name||="",
                            :sex_id => sex_id, :facebook_token => token, :facebook_expires_at => expires_at,
                            :facebook_expires => expires, :remote_image_url => "https://graph.facebook.com/#{ data.id.to_i }/picture?width=140&height=140", :show_introduction => true,
                            :sms_authorization => true, :birthday => birthday, :fbk_user_id => data.id.to_i,
                            :user_state_id => UserState::STATES[:registered], :fbk_location_name => location_name,
                            :fbk_locale => locale, :fbk_timezone => timezone, :registration_date => Time.zone.now)

      if set_referer.present?
        user.referral_id = set_referer
        user.date_referral = Time.zone.now
      end
      if session[:utm_source].present?
        user.utm_source = session[:utm_source]
        if session[:utm_medium].present?
          user.utm_medium = session[:utm_medium]
        end
      end
      user.save
      GiftCodeNotifier.delay.welcome(user)
      puts "se envia mensaje de bienvenida"
      user.verify_fbk_synchronization
      # Sent SignUp event to MixPanel
      if GlobalConstants.use_mix_panel?
        mpjson = JSON.parse(cookies["mp_#{GlobalConstants::token_for_mixpanel}_mixpanel"])
        #puts "DISTINCT ID: "+mpjson["distinct_id"]
        mixpanel.alias( user.email, mpjson["distinct_id"] )
        mixpanel.track( user.email, 'Web - Registro Nuevo Usuario' )
        # mixpanel.set( user.email, {:email => user.email, :first_name => user.first_name, :last_name => user.last_name} )
      end
    end
    session[:amigo] = nil
    user
  end

  def set_referer
    if session[:amigo].present?
      if User.exists?(:fbk_user_id => session[:amigo])
        return session[:amigo]
      end
    end
  end

end