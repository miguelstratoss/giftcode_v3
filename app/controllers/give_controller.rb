class GiveController < ApplicationWebController

  before_filter :auth_user
  before_filter :country

  def index_friends_angular
    @count_brands =  Brand.brands_enabled_for_sending_a_gift_code( @country.id ).count
  end

  private
    def country
      country_id = params[:country_id].nil? ? 1 : params[:country_id]
      @country = Country.find country_id
      @cities = []
      @gift_cards = []
      @brand = Brand.new
      @friend = User.new
    end

end
