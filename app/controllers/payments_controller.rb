# encoding: UTF-8
require 'rest-client'

class PaymentsController < ApplicationWebController

  def index
    @payment =  Payments.new( params )
    if @payment.start request
      render :layout => @payment.type
    else
      render :layout => @payment.type, :action => 'error'
    end
  end

  def confirmation
    @payment =  Payments.new( params )
    if @payment.finalize
      render :layout => @payment.type
    else
      render :layout => @payment.type, :action => 'error'
    end
  end


  def demo
    @payment =  Payments.new( params )
    if @payment.demo_finalize
      render :layout => @payment.type, :action => 'confirmation'
    else
      render :layout => @payment.type, :action => 'error'
    end
  end

end