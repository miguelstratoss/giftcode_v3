class FriendsController < ApplicationWebController

  before_filter :auth_user

  def index_angular
    @referrals_count = current_user.referrals.count
    @gift_code_free  = @referrals_count / 5
    @gift_code_free = ( @gift_code_free > 10 ) ? 10 : @gift_code_free
    @go = 0
  end

  def invite
    friend = current_user.friends.find_by_fbk_user_id( params[:fbk_user_id] )
    msg = { :type => 'error', :text => 'No se invito amigo, por favor intentelo de nuevo' }
    save = false
    friend_id = nil
    if !friend.nil? && !params[:data].nil?
      if params[:data][:post_id].present?
        facebook = params[:data][:post_id].split('_')
        post_id = facebook[1]
        save = true
      elsif params[:data][:success]
        post_id = ''
        save = true
      end
      if save
        invite_friend = InviteFriend.new(:user_id => current_user.id, :friend_id => friend.id, :fbk_post_id => post_id )
        if invite_friend.save
          msg = { :type => 'success', :text => 'Se envio invitación' }
          friend_id = friend.id
          # Sent event to MixPanel
          mixpanel.track current_user.email, 'Web - Envio Invitacion Amigo' if GlobalConstants.use_mix_panel?
        else
          msg = { :type => 'notice', :text => 'Se envio mensaje, no guardo registro.' }
        end
      end
    end
    json = { flash: msg, friend_id: friend_id }.to_json
    render :json => json
  end

  def profile
    @friend = current_user.friends.find_by_id_and_user_state_id params[:user_id], UserState::STATES[:registered]
    unless @friend
      redirect_to root_path
    else
      @categories = Category.order("position").where("enabled = true")
      @brands = Brand.where(:enable_for_selling => true).collect do |b|
        { id: b.id, name: b.name, categories: b.categories.map{|category| category.name } }
      end
    end
  end

  #json

  def referrals
    friends = current_user.referrals.find( :all, :order => :first_name )
    render :json => friends.as_json(
        :only => [:id, :first_name, :last_name, :email, :fbk_user_id, :user_state_id, :referral_id, :mobile_phone, :image  ],
        :methods => :complete_name
    )
  end

  def guests
    friends = current_user.invite_friends.select( :friend_id ).uniq(:friend_id)
    users = []
    friends.each do |friend|
        user = friend.friend
        if user.user_state_id == UserState::STATES[:non_registered]
          users.push user
        end
    end
    render :json => users.as_json(
      :only => [:id, :first_name, :last_name, :email, :fbk_user_id, :user_state_id, :referral_id, :mobile_phone, :image  ],
      :methods => :complete_name
    )
  end

  def for_inviting
    guests = current_user.invite_friends.select( :friend_id ).uniq(:friend_id)
    if guests.count > 0
      friends = current_user.friends.where( :user_state_id => UserState::STATES[:non_registered] ).where(['users.id not in (?)', guests.map(&:friend_id)])
    else
      friends = current_user.friends.where( :user_state_id => UserState::STATES[:non_registered] )
    end
    render :json => friends.as_json(
        :only => [:id, :first_name, :last_name, :email, :fbk_user_id, :user_state_id, :referral_id, :mobile_phone, :image  ],
        :methods => :complete_name
    )
  end

  def in_gift_code
    friends = current_user.friends.where( :user_state_id => UserState::STATES[:registered] )
    render :json => friends.as_json(
        :only => [:id, :first_name, :last_name, :email, :fbk_user_id, :user_state_id, :referral_id, :mobile_phone, :image  ],
        :methods => :complete_name
    )
  end

end