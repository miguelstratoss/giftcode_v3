class GiftCodesController < ApplicationWebController

  before_filter :auth_user

  def create
    gift_code = GiftCode.new(params[:gift_code])
    msg = { :type => 'error', :text => 'Error al crear bono' }
    json = { flash: msg }.as_json
    if !gift_code.nil?
      gift_code.to_country = 'CO'
      gift_code.from_user = current_user
      gift_code.from_first_name = current_user.first_name
      gift_code.from_last_name = current_user.last_name
      gift_code.from_email = current_user.email
      gift_code.gift_code_state_id = GiftCodeState::STATES[:generated_and_payment_pending]
      gift_code.cost = gift_code.gift_card.cost
      gift_code.iva = gift_code.gift_card.campaign.iva
      if gift_code.send_notifications_now
        gift_code.send_date_time = Time.zone.now
      end
      gift_code.valid_for_days = gift_code.gift_card.valid_for_days
      gift_code.calculate_base_iva
      gift_code.generate_ref_sale
      to_user = User.find_by_email( gift_code.to_email )
      if to_user.nil?
        to_user = User::user_new( gift_code.to_first_name,gift_code.to_last_name, gift_code.to_email )
      end
      gift_code.to_user = to_user

      if gift_code.save
        msg = { :type => 'success', :text => 'Se guardo bono' }
      else
        msg = { :type => 'error', :text => 'Error al guardar bono', :errors => gift_code.errors.to_json }
      end

      campaign = gift_code.gift_card.campaign
      brand_codes = campaign.brand_codes.find_by_active_for_payment true

      if brand_codes
        entity = brand_codes.third_party_entity
      else
        entity = nil
      end

      json = {
          flash: msg,
          ref_sale: gift_code.ref_sale,
          cost_current: gift_code.gift_card.cost_current,
          currency_short_name: gift_code.gift_card.campaign.currency.short_name,
          entity: ( ( entity ) ?  entity.name : 'none' ),
          fingerprint: gift_code.id,
          from_first_name: gift_code.from_first_name,
          from_last_name: gift_code.from_last_name
      }.as_json

    end

    render :json => json
  end

  def download
    gift_code =  current_user.gift_codes_received.find_by_ref_sale( params[:ref_sale] )
    if !gift_code.nil?
      @gift_code = gift_code
      if @gift_code.is_a_free_gift_code?
        @brand = @gift_code.free_campaign.office.brand
        @campaign =  @gift_code.free_campaign
      else
        if @gift_code.type == "PUNTOS"
          @brand = @gift_code.campaign_point.brand
          @campaign =  @gift_code.campaign_point
        else
          @brand = @gift_code.gift_card.campaign.brand
          @campaign =  @gift_code.gift_card.campaign
        end
      end
      if gift_code.binary_qr_code_image.blank?
        @qr_code = ChunkyPNG::Image.from_blob(File.read(Rails.root.to_s + '/app/assets/images/icono_grande.png')).to_data_url
      else
        @qr_code = ChunkyPNG::Image.from_blob(gift_code.binary_qr_code_image).to_data_url
      end

      @barcode = ChunkyPNG::Image.from_blob(@gift_code.barcode_image).to_data_url
      unless Rails.env.development?
        @image_brand = ChunkyPNG::Image.from_blob(open( @brand.image_email_url(:thumb180).to_s ){|f| f.read }).to_data_url
      end

      kit = IMGKit.new( '<html><body style="background: #F0F0E0;">' + render_to_string(:partial => 'gift_codes/show_version_email') + '</body></html>', quality: 30, width: 700)
      kit.stylesheets << Rails.root.join("app/assets/stylesheets/", "application-email.css").to_s
      kit.stylesheets << Rails.root.join("app/assets/stylesheets/", "style-email.css.1.old").to_s
      send_data  kit.to_png,
                 :filename => "#{params[:ref_sale]}.png",
                 :type => "application/png"
    end

  end

  def view
    gift_code =  current_user.gift_codes_received.find_by_ref_sale( params[:ref_sale] )
    if !gift_code.nil?
      @gift_code = gift_code
      if @gift_code.is_a_free_gift_code?
        @brand = @gift_code.free_campaign.office.brand
      else
        @brand = @gift_code.gift_card.campaign.brand
      end
    end
  end

end
