class ApplicationWebController < ApplicationController


  before_filter :set_referral
  before_filter :go


  def set_referral
    if params[:amigo].present?
      session[:amigo]= params[:amigo]
    end
    if params[:utm_source].present?
      session[:utm_source]= params[:utm_source]
    end
    if params[:utm_medium].present?
      session[:utm_medium]= params[:utm_medium]
    end
    if params[:callback].present?
      session[:callback]= params[:callback]
    end
    if controller_name == 'settings' && action_name == 'validate_account' && params[:token]
      session[:token_validate_email] = params[:token]
    end
  end


  def auth_user
    if user_signed_in?
      unless GlobalConstants.full_version?
        if controller_name == 'give' || controller_name == 'gift_codes' || controller_name == 'payments' || controller_name == 'third_party'
          redirect_to root_path
        end
      end
    else
      redirect_to root_path
    end
  end

  def after_sign_out_path_for(resource)
    mixpanel.track current_user.email, 'Web - Cierre Sesion'  if GlobalConstants.use_mix_panel? && current_user
    root_path
  end

  layout :get_layout


  protected

  def get_layout
    request.xhr? ? 'ajax' : 'application'
  end

  def go
    go_favorites = ( ( controller_name == 'favorites' && action_name == 'index' ) || ( controller_name == 'settings' && action_name == 'edit' ) || ( controller_name == 'landing_page' && action_name == 'index' ) || (controller_name == 'sessions' && action_name == 'destroy') )
    if user_signed_in? && !go_favorites
      if session[:brand_name].present?
        redirect_to landing_page_path( session[:brand_name] )
      else
        @count_favorites = current_user.favorites.where("enabled = true").count
        if !GlobalConstants.full_version? && ( Rails.env.development?)
          current_user.update_attributes( :show_introduction => true )
        end
        if current_user.show_introduction
          if (
              ( controller_name == 'settings' && ( action_name == 'validate_account' || action_name == 'confirmation_email' || action_name == 'confirmation_birthday'|| action_name == 'confirmation_city' || action_name == 'update'))  ||
              ( controller_name == 'favorites' && ( action_name == 'add' || action_name == 'facebook_wall')) ||
              ( controller_name == 'json' && action_name == 'favorites'   )
            )
            true
          else
            redirect_to first_time_path
          end
        end
      end
    end
    true
  end

end