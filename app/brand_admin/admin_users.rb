ActiveAdmin.register AdminUser, :namespace => :brand_admin do

  menu :label => "Usuarios"

  actions :index

  filter :role, :label => 'Rol'
  filter :office, :label => 'Oficina', :collection => proc { Office.where(:brand_id => current_admin_user.brand.id) }
  filter :email, :label => 'e-Mail'

  scope_to :current_admin_user, :association_method => :brand_users

  index :title=>"Usuarios", :download_links => [ :csv, :xml, :json, :pdf ] do
    column :id
    column "e-Mail",:email
    column "Oficina",:office
    column "Creado",:created_at
  end

  controller do
    def index
      index! do |format|
        format.pdf{
          render 'index.pdf.prawn', :layout => false
        }
      end
    end
  end

  scope "Todos" do |admin_user| admin_user end
  scope "Oficina" do |admin_user| admin_user.all_office_users end
  scope "Administradores" do |admin_user| admin_user.all_brand_admin end

end