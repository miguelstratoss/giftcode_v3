ActiveAdmin.register ThirdPartyTransaction, :namespace=>:brand_admin do

  config.clear_action_items!
  actions :index, :show, :new, :create

  scope_to :current_admin_user, :association_method => :brand_third_party_transactions

  # filter :brand_code_campaign_name, :label => 'Campaign', :as => :string ,:collection => proc { ThirdPartyTransaction.campaign.where( :name => Thread.current[:campaign_name] ).active }
  filter :tp_transaction_state
  filter :tp_transaction_type
  # filter :third_party_entity_id
  filter :gift_code_id, :as => :numeric
  # filter :transaction_value
  # filter :transaction_currency
  # filter :new_balance
  # filter :received_hash
  # filter :reversed
  # filter :confirmed
  filter :office
  # filter :related_transaction_id
  filter :created_at
  filter :updated_at

  controller do

    before_filter :only => :index do
      @per_page = 30
    end

    def new
      if params[:ref_sale].present?
        @gift_code = GiftCode.find_by_ref_sale(params[:ref_sale])
        @transaction = ThirdPartyTransaction.new
      else
        flash[:error]="Debe ingresar primero el contenido del QR Code o el PIN del GiftCode."
        redirect_to office_admin_dashboard_path
      end
    end

    def create
      #$transDate = gmstrftime("%Y%m%d%H%M%S");
      transDate = Time.zone.now.strftime("%Y%m%d%H%M%S")
      #$codigoComercio=$this->commerce_id;
      #codigoComercio = current_admin_user.office.brand.campaigns
      #$key= "$brand|$pin|".$codigoComercio."|$transDate||||".$this->secret;

      #$firm = md5($key);
      #$fields = "commerceId=$codigoComercio&brandCode=$brand&pin=$pin&transDate=$transDate&firm=$firm";
      #//echo "firm saldo: $firm<br>";
      #$json=$this->json."get_balance";
    end

  end

  form :partial => "form"

  index do
    column :id
    column :gift_code_id, :sortable => 'third_party_transactions.gift_code_id' do |tpt|
      (tpt.gift_code_id) ? link_to( tpt.gift_code_id, brand_admin_gift_code_path(:id => tpt.gift_code_id) ) : ''
    end
    column :transaction_value,  sortable: 'third_party_transactions.transaction_value' do |third_party_transaction|
      number_to_currency(third_party_transaction.transaction_value, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
    end
    column :new_balance, sortable: 'third_party_transaction.new_balance'  do |third_party_transaction|
      number_to_currency(third_party_transaction.new_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
    end
    column('approbation_number') do |third_party_transaction|
      if third_party_transaction.movements_gift_code
        third_party_transaction.movements_gift_code.id
      end
    end
    column :tp_transaction_type_id, :sortable => 'third_party_transactions.tp_transaction_type_id' do |tpt|
      (tpt.tp_transaction_type_id) ? tpt.tp_transaction_type.name : ''
    end
    column :tp_transaction_state_id, :sortable => 'third_party_transactions.tp_transaction_state_id' do |tpt|
      (tpt.tp_transaction_state_id) ? tpt.tp_transaction_state.name : ''
    end
    column :created_at
  end

end