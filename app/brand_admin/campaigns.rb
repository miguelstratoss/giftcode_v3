ActiveAdmin.register Campaign, :namespace=>:brand_admin do

  menu :label => "Campañas"

  actions :index, :show

  filter :country, :label => "Pais"
  filter :city, :label => "Ciudad"
  filter :currency, :label => "Moneda"
  filter :campaign_state, :label => "Estado de campaña"
  filter :name, :label => "Nombre"
  filter :created_at, :label => "Creado"
  filter :updated_at, :label => "Modificado"

  scope_to :current_admin_user, :association_method => :brand_campaigns

   #ver campaña
  show do |brand|
    attributes_table do
      row :id
      row :iva
      row :name
      row :convenio
      row :message
      row :send_mail
      row :send_sms
      row :country
      row :currency
      row :campaign_state
      row :show_qr
      row :show_barcode
      row :office_redeem_bono
      row :created_at
      row :updated_at
    end

    panel "GiftCards" do
      table_for(campaign.gift_cards) do
        column("id", :sortable => :id) { |gift_card| link_to "#{gift_card.id}", admin_campaign_gift_card_path(campaign, gift_card) }
        # column("image") { |gift_card| image_tag(gift_card.image.url(:thumb160).to_s) }
        column("cost") { |gift_card| gift_card.cost }
        column("gift codes sold") { |gift_card| gift_card.gift_codes.count }
        column("valid_for_days") { |gift_card| gift_card.valid_for_days }

      end
    end

  end


  #:download_links => [:csv, :xml, :json, :pdf]
  index( :title=>"Campañas", download_links: false ) do
    column :id
    column "Nombre",:name
    # column "Pais", :country, :sortable => 'countries.name'
    column "Estado",:campaign_state, :sortable => 'campaign_states.name'
    column "Total bonos Oficina" do |campaign|
      campaign.gift_code_total( "office")
    end
    column "Total giftcodes web" do |campaign|
      campaign.gift_code_total( "web")
    end
    column "Linea de tiempo ventas" do |campaign|
      render "basic-time" , :campaign => campaign
    end
    column('') { |campaign| status_tag( 'RedebanCode', :ok ) if campaign.convenio_redeban? }
    #column :created_at
    #column :updated_at
    default_actions
  end

  #miguel segura --
  scope "Todos" do |campaign| campaign end
  scope "Campañas activas" do |campaign|
    campaign.where(:campaign_state_id => CampaignState::STATES[:enable])
  end
  #--

  controller do
    def scoped_collection
      end_of_association_chain.includes([:country, :campaign_state])
    end
    def index
      index! do |format|
        format.pdf{
          render 'index.pdf.prawn', :layout => false
        }
      end
    end
  end

  member_action :files, method: :get do
    case params[:type]
      when "Movimientos"
        movements
      when "Consolidado"
        consolidated
      else
        send_data "Tipo de archio no valido", :filename => "error.txt"
    end

  end


  controller do

    include ActionView::Helpers::TextHelper
    include ActionView::Helpers::NumberHelper

    def movements
      begin
        #para descargar los movimientos debe tener un numero de convenio
        if resource.convenio.blank?
          redirect_to brand_admin_campaigns_path + "/" + resource.id.to_s
        else
          LogFileDownload.transaction do
            time = Time.zone.parse(params[:q])
            output = Array.new
            file_name = "MV#{ truncate( "%04d" % resource.id.to_s, length: 4 ).rjust(4)  + time.strftime("%Y%m%d") }.txt"
            movements_gift_codes = MovementsGiftCode.includes(:gift_code, :third_party_transaction).where( brand_id: resource.brand_id, campaign_id: resource.id ).where( created_at: time.beginning_of_day .. time.end_of_day )
            if movements_gift_codes.blank? #|| time.beginning_of_day >= Time.zone.now.beginning_of_day
              send_data "NO HAY DATOS #{ 'PARA LA FECHA' if time.beginning_of_day >= Time.zone.now.beginning_of_day }" , :filename => file_name
            else
              LogFileDownload.create!( name: file_name, admin_user_id: current_admin_user.id )
              movements_gift_codes.each do |movement_gift_code|
                output << truncate( resource.brand.trade_name,  length: 17).ljust(17)+
                    truncate( "%04d" % resource.id.to_s,            length: 4 ).rjust(4)+
                    truncate(  "%013d" % movement_gift_code.gift_code.identification_number.to_s, length: 13).rjust(13)+
                    "%012d" %  number_with_delimiter( ("%.2f" %  movement_gift_code.gift_code.cost.round(2)), separator: '',delimiter:'').to_s+
                    movement_gift_code.created_at.strftime("%Y%m%d%H%M%S")+
                    truncate(resource.name, length: 17).ljust(17)+
                    truncate( "%010d" % resource.convenio, length: 10)+
                    (movement_gift_code.third_party_transaction_id.blank?  ? "00000000" : ( "%08d" % movement_gift_code.third_party_transaction.office_id ))+
                    (movement_gift_code.third_party_transaction_id.blank?  ? "HABILITACION" : truncate( "COMPRA", length: 12).ljust(12) )+
                    (movement_gift_code.third_party_transaction_id.blank?  ? "%012d" % number_with_delimiter( ("%.2f" %  movement_gift_code.gift_code.cost.round(2)), separator: '',delimiter:'').to_s : "%012d" % number_with_delimiter( ("%.2f" %  movement_gift_code.third_party_transaction.transaction_value.round(2)), separator: '',delimiter:'').to_s )+
                    "%012d" % movement_gift_code.id
              end
              send_data output.join("\r\n"), :filename => file_name
            end
          end
        end
      rescue ActiveRecord::Rollback
        puts "Error en crear archivo de bonos activos"
        send_data "Error en crear archivo de bonos activos", :filename => "error.txt"
      end
    end

    def consolidated
      begin
        #para descargar los movimientos debe tener un numero de convenio
        if resource.convenio.blank?
          redirect_to brand_admin_campaigns_path + "/" + resource.id.to_s
        else
          LogFileDownload.transaction do
            time = Time.zone.parse(params[:q])
            output = Array.new
            file_name = "C#{ truncate( "%04d" % resource.id.to_s, length: 4 ).rjust(4)  + time.strftime("%Y%m%d") }.txt"
            LogFileDownload.create!( name: file_name, admin_user_id: current_admin_user.id )
            # if movements_gift_codes.blank? #|| time.beginning_of_day >= Time.zone.now.beginning_of_day
            #   send_data "NO HAY DATOS #{ 'PARA LA FECHA' if time.beginning_of_day >= Time.zone.now.beginning_of_day }" , :filename => file_name
            # else
            GiftCode
            .where("date_of_purchase < ?" ,time.end_of_day)
            .where(brand_id: resource.brand_id, campaign_id: resource.id, :gift_code_state_id => [ GiftCodeState::STATES[:payment_accepted],GiftCodeState::STATES[:generated_office_confirmed],GiftCodeState::STATES[:generated_external_web_payment_accepted] ] )
            .where("available_balance != 0")
            .order("identification_number DESC")
            .order("gift_card_id DESC")
            .find_in_batches( batch_size: 100 ) do | batch|
              batch.each do |gift_code|
                value = gift_code.third_party_transactions.where(created_at: time.beginning_of_day .. time.end_of_day, tp_transaction_type_id:TpTransactionType::TYPES[:redemption] , tp_transaction_state_id: TpTransactionState::STATES[:success] ).sum(:transaction_value)
                value = number_with_delimiter( "%.2f" %  value, separator: '',delimiter:'').to_s
                balance = gift_code.third_party_transactions.where("third_party_transactions.created_at < ?" ,time.end_of_day).where( tp_transaction_type_id:TpTransactionType::TYPES[:redemption] , tp_transaction_state_id: TpTransactionState::STATES[:success] ).sum(:transaction_value)
                balance = number_with_delimiter( "%.2f" % (gift_code.cost - balance), separator: '',delimiter:'').to_s
                output << gift_code.id.to_s + "," + gift_code.gift_card_id.to_s + "," +  gift_code.identification_number.to_s + "," + value.to_s + "," + I18n.l(gift_code.date_of_purchase, format: :admin) + "," + balance.to_s
              end
            end
            send_data output.join("\r\n"), :filename => file_name
          end
        end
      rescue ActiveRecord::Rollback
        puts "Error en crear archivo consolidado"
        send_data "Error en crear archivo consolidado", :filename => "error.txt"
      end
    end

  end

  action_item only: [:show] do
    unless resource.convenio.blank?
      render 'form_movements'
    end
     # link_to 'Download Movements GiftCode' , movements_gift_code_brand_admin_campaign_path(id: campaign.id)
  end

end
