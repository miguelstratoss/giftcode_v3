ActiveAdmin.register Office, :namespace=>:brand_admin do

  menu :label => "Oficinas"

  actions :index, :show

  filter :city, :label => "Ciudad"
  #filter :nit
  #filter :contact_name
  #filter :contact_email
  filter :office_name, :label => "Oficina"

  scope_to :current_admin_user, :association_method => :brand_offices

  show do |office|
    attributes_table do
      row :id
      row :office_name
      row :office_address
      row :city do
        office.city.complete_name
      end
      row :nit
      row :contact_name
      row :contact_email
      row :created_at
      row :updated_at
    end
    # panel "Informe" do
    #    table "" do
    #      tr do
    #        # td do
    #        #  table do
    #        #    tr do
    #        #      th "Bonos vendidos"
    #        #      th "Cantidad"
    #        #      th "Valor"
    #        #    end
    #        #    today = office.gift_codes_today
    #        #    tr do
    #        #      td "Hoy"
    #        #      td today.count
    #        #      td today.sum( :cost )
    #        #    end
    #        #    week = office.gift_codes_week
    #        #    tr do
    #        #      td "Semana"
    #        #      td week.count
    #        #      td week.sum( :cost )
    #        #    end
    #        #    month = office.gift_codes_month
    #        #    tr do
    #        #      td "Mes"
    #        #      td month.count
    #        #      td month.sum( :cost )
    #        #    end
    #        #    year = office.gift_codes_year
    #        #    tr do
    #        #      td "Año"
    #        #      td year.count
    #        #      td year.sum( :cost )
    #        #    end
    #        #    todos = office.gift_codes.where("gift_codes.gift_code_state_id=#{GiftCodeState::STATES[:generated_office_confirmed]}")
    #        #    tr do
    #        #      td "Todos"
    #        #      td todos.count
    #        #      td todos.sum( :cost )
    #        #    end
    #        #  end
    #        # end
    #
    #
    #        # td do
    #        #   table do
    #        #     tr do
    #        #       th "Bonos redimidos"
    #        #       th "Cantidad"
    #        #       th "Valor"
    #        #     end
    #        #     today = office.transactions_today
    #        #     tr do
    #        #       td "Hoy"
    #        #       td today.count
    #        #       td today.sum( :cost )
    #        #     end
    #        #     week = office.transactions_week
    #        #     tr do
    #        #       td "Semana"
    #        #       td week.count
    #        #       td week.sum( :cost )
    #        #     end
    #        #     month = office.transactions_month
    #        #     tr do
    #        #       td "Mes"
    #        #       td month.count
    #        #       td month.sum( :cost )
    #        #     end
    #        #     year = office.transactions_year
    #        #     tr do
    #        #       td "Año"
    #        #       td year.count
    #        #       td year.sum( :cost )
    #        #     end
    #        #     todos = office.transactions
    #        #     tr do
    #        #       td "Todos"
    #        #       td todos.count
    #        #       td todos.sum( :cost )
    #        #     end
    #        #   end
    #        # end
    #      end
    #    end
    # end
    # active_admin_comments
  end

  index :title=>"Oficinas", :download_links => [ :csv, :xml, :json, :pdf ] do
    column :id
    column "Oficina", :office_name
    #column :brand do |office|
    #  office.brand.name
    #end
    column "Ciudad",:city,  :sortable => 'offices.city_id' do |office|
      office.city.complete_name
    end
    #column :created_at
    #column :updated_at
    default_actions
  end

  controller do
    def index
      index! do |format|
        format.pdf{
          render 'index.pdf.prawn', :layout => false
        }
      end
    end
  end
  
end
