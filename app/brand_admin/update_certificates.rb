# ActiveAdmin.register_page 'Update certificates', :namespace=>:brand_admin do
#
#   menu false
#
#   content :if => false do
#
#     if brand
#       campaigns = brand.campaigns_convenio_redeban
#       if campaigns && campaigns.count > 0
#         panel 'Subir archivo' do
#           semantic_form_for 'redeban_files', :url => brand_admin_update_certificates_load_url, :builder => ActiveAdmin::FormBuilder do |f|
#             f.inputs do
#               f.input :archivo, :as => :file, :require => true
#             end
#             f.actions
#           end
#         end
#
#         panel 'Convenios' do
#           table_for campaigns do
#             column :id
#             column :type do |campaign|
#               if campaign.type.nil? || campaign.type.blank?
#                 'Campaign'
#               else
#                 campaign.type
#               end
#             end
#             column :state do |campaign|
#               if campaign.type.nil? || campaign.type.blank?
#                 campaign.campaign_state.name
#               else
#                 campaign.enabled
#               end
#             end
#             column :name
#             column :convenio_redeban
#           end
#         end
#       else
#         para "Marca no tiene un convenio con redeban.", class: 'status_tag error'
#       end
#     else
#       para "Marca no existe.", class: 'status_tag error'
#     end
#
#   end
#
#   page_action :load, method: :post do
#
#     errors = []
#     @brand = current_admin_user.belongs_to_brand
#     if params[:redeban_files].present? && params[:redeban_files][:archivo].present?
#       file = params[:redeban_files][:archivo]
#       certificates = UpdateCertificatesRedeban.new @brand, current_admin_user
#       if certificates.error_message.count == 0
#         certificates.load file
#       end
#       errors.concat( certificates.error_message )
#     else
#       errors.push "El campo archivo es obligatorio."
#     end
#     if errors.count > 0
#       flash[:error] = errors.join ', '
#       redirect_to brand_admin_update_certificates_path, notice: 'Se actualizaron certificados.'
#     else
#       redirect_to brand_admin_update_certificates_path, notice: 'Se actualizaron certificados.'
#     end
#   end
#
#   controller do
#
#     def initialize
#       @brand = nil
#     end
#
#     def index
#       @brand = current_admin_user.belongs_to_brand
#     end
#
#   end
#
# end