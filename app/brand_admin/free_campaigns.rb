# ActiveAdmin.register FreeCampaign, :namespace=>:brand_admin do
#
#   actions :index
#
#   menu :label => "Campañas Gratis"
#
#   filter :office, :label => "Oficina"
#
#   scope_to :current_admin_user, :association_method => :brand_free_campaigns
#
#   index :title=>"Campañas", :download_links => [:csv, :xml, :json, :pdf] do
#     column :id
#     column :office
#     column :cost
#     column :valid_for_days
#     column :gift_codes_sent do |free_campaign|
#       free_campaign.gift_codes.count
#     end
#     column :created_at
#   end
#
#   controller do
#     def index
#       index! do |format|
#         format.pdf{
#           render 'index.pdf.prawn', :layout => false
#         }
#       end
#     end
#   end
# end
