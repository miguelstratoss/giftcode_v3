ActiveAdmin.register GiftCode, :namespace=>:brand_admin do

  actions :index, :show, :xml

  filter :from_email, :label => "Comprador por"
  filter :to_email, :label => "Enviado a"
  filter :city, :label => "Ciudad"
  filter :office, :label => "Oficinas"  , :collection => proc { Office.where(:brand_id => current_admin_user.brand.id) }
  filter :gift_code_state, :label => "Estado"
  filter :created_at, :label => "Creado"
  filter :updated_at, :label => "Modificado"
  filter :identification_number, :label => "Numero identificación"

  scope_to :current_admin_user, :association_method => :gift_codes

  show do |gift_code|
    attributes_table do
      row ("Tipo") { gift_code.type }
      row :identification_number
      row ("Oficina") do
        if gift_code.office.present?
          link_to gift_code.office.office_name, brand_admin_office_path(gift_code.office.id)
        else
          "N/A"
        end
      end
      row ("Campaña") do
        if !gift_code.is_a_free_gift_code?
          link_to gift_code.gift_card.campaign.name, brand_admin_campaign_path(gift_code.gift_card.campaign.id)
        else
          link_to "Campaña gratis #"+gift_code.free_campaign.id.to_s, admin_free_campaign_path(gift_code.free_campaign)
        end
      end
      row :gift_card do
        if !gift_code.is_a_free_gift_code?
          columns :class => "horizontal_columna" do
            column do b "Costo:" end
            column do gift_code.gift_card.cost end
            column do b "Valido por:" end
            column do gift_code.gift_card.valid_for_days end
          end
        else
          "N/A"
        end
      end
      row ("Estado") { gift_code.gift_code_state }
      row ("Comprador") do
        if !gift_code.is_a_free_gift_code?
          columns :class => "horizontal_columna" do
            column do b "Nombre:" end
            column do gift_code.from_first_name + " " + gift_code.from_last_name end
            column do b "e-Mail:" end
            column do gift_code.from_email end
          end
        else
          "N/A"
        end
      end
      row "Enviado a:" do
          columns :class => "horizontal_columna" do
            column do b "Nombre:" end
            column do gift_code.to_first_name + " " + gift_code.to_last_name end
            column do b "e-Mail:" end
            column do gift_code.to_email end
            column do b "Celular:" end
            column do gift_code.to_mobile_phone end
          end
      end

      row :base_iva do
        if !gift_code.is_a_free_gift_code?
          gift_code.base_iva
        else
          "N/A"
        end
      end
      row :iva do
        if !gift_code.is_a_free_gift_code?
          gift_code.iva
        else
          "N/A"
        end
      end
      row ("Costo") { gift_code.cost }
      row :available_balance
      row :valid_for_days
      row :to_city
      row "e-Mail al Comprador" do
        if !gift_code.is_a_free_gift_code?
          gift_code.email_sent_to_buyer
        else
          "N/A"
        end
      end
      row ("e-Mail al beneficiario") { gift_code.email_sent_to_receiver }
      row "SMS Enviado" do
        if !gift_code.is_a_free_gift_code?
          gift_code.sms_sent_to_receiver
        else
         "N/A"
        end
      end
      row "Usuario" do
        if gift_code.admin_user.nil?
          "N/A"
        else
          gift_code.admin_user.email
        end
      end
      row ("Creado") { gift_code.created_at }
      row ("Modifico") { gift_code.updated_at }
    end
    panel 'Third party transactions' do
      table_for( gift_code.third_party_transactions ) do
        column('id', :sortable => :id)
        column 'transaction_value'
        column 'new_balance'
        column 'office'
        column 'tp_transaction_type' do |third_party_transaction|
          third_party_transaction.tp_transaction_type.name
        end
        column 'tp_transaction_state' do |third_party_transaction|
          third_party_transaction.tp_transaction_state.name
        end
        column('approbation_number') do |third_party_transaction|
           if third_party_transaction.movements_gift_code
             third_party_transaction.movements_gift_code.id
           end
        end
        column 'created_at'
      end
    end
    # active_admin_comments
  end

  # index :download_links => [ :csv, :xml, :json, :pdf ] do
  index :download_links => false do
    column :id
    column "Tipo",:type, :sortable => 'gift_codes.gift_code_state_id'
    column :identification_number
    column "Comprado por", :from_email, :sortable => 'gift_codes.from_email'  do |gift_code|
      if !gift_code.is_a_free_gift_code?
        gift_code.from_email
      else
        "N/A"
      end
    end
    column "Enviado a",:to_email
    column "Valor",:cost
    column "Estado", :gift_code_state, :sortable => 'gift_codes.gift_code_state_id'
    column "Creado", :created_at
    default_actions
  end

  csv do
    column :id
    column :type
    column ('form_name') do |gift_code|
      if !gift_code.is_a_free_gift_code?
        gift_code.from_first_name + ' ' + gift_code.from_last_name
      else
        "N/A"
      end
    end
    column ('from_email') do |gift_code|
      if !gift_code.is_a_free_gift_code?
        gift_code.from_email
      else
        "N/A"
      end
    end
    column ('to_name') { |gift_code| gift_code.to_first_name + ' ' + gift_code.to_last_name }
    column :to_email
    column :base_iva
    column :iva
    column :cost
    column :available_balance
    column ('email_send_to_buyer') do |gift_code|
      if !gift_code.is_a_free_gift_code?
        gift_code.email_sent_to_buyer
      else
        "N/A"
      end
    end
    column ('sms_send_to_receiver')  do |gift_code|
      if !gift_code.is_a_free_gift_code?
        gift_code.sms_sent_to_receiver
      else
        "N/A"
      end
    end
    column ('admin_user') do |gift_code|
      if gift_code.admin_user.nil?
        "N/A"
      else
        gift_code.admin_user.email
      end
    end
    column :email_sent_to_receiver
    column ('gift_code_state') {|gift_code| gift_code.gift_code_state.name}
    column :created_at
  end

  controller do
    def index
      index! do |format|
        format.pdf{
          render 'index.pdf.prawn', :layout => false
        }
      end
    end
  end

  scope "Todos" do |gift_code| gift_code end
  scope "Web" do |gift_code| gift_code.all_web end
  scope "Oficina" do |gift_code| gift_code.all_offices end
  scope "Gratis" do |gift_code| gift_code.all_free end

end
