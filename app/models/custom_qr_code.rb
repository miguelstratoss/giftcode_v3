# == Schema Information
#
# Table name: custom_qr_codes
#
#  id             :integer          not null, primary key
#  image          :string(255)
#  content        :string(255)
#  size           :integer
#  binary_qr_code :binary
#  created_by     :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class CustomQrCode < ActiveRecord::Base

  attr_accessor :image_width, :image_height

  attr_accessible :image, :content, :size, :binary_qr_code, :created_by

  validates :image, :presence => true
  validates :content, :presence => true
  validates :size, :presence => true, :numericality => { :greater_than_or_equal_to => 1, :less_than_or_equal_to => 10 }
  validates :created_by, :presence => true
  #validates :binary_qr_code, :presence => true
  validate :validate_number_of_characters, :validate_image_size

  after_validation :generate_binary_qr_code

  belongs_to :admin_user, :class_name => 'AdminUser', :foreign_key => 'created_by'

  mount_uploader :image, CustomQrCodeUploader

  def self.qr_code_sizes
    sizeArray = Array.new(10, 0)
    (0..9).each do |i|
      sizeArray[i] = Hash.new
      sizeArray[i][:description] = 'Size '+(i+1).to_s+' - Maximum '+GCQRCode::MAXIMUM_CHARACTERS_BY_SIZE_MODULES[i+1].to_s+' characters'+' - PNG image size: '+(GCQRCode::SIZE_MODULES[i+1]*4).to_s+'x'+(GCQRCode::SIZE_MODULES[i+1]*4).to_s+'px'
      sizeArray[i][:id] = i+1
    end
    sizeArray
  end

  def initialize_default_values
    self.size = 4
  end

  # custom validation for length of content according to the maximum number of characters allowed per size
  def validate_number_of_characters
    if self.size.present? && self.content.present? && self.content.size > GCQRCode::MAXIMUM_CHARACTERS_BY_SIZE_MODULES[self.size]
      errors.add :content, "For the size "+self.size.to_s+', maximum '+GCQRCode::MAXIMUM_CHARACTERS_BY_SIZE_MODULES[self.size].to_s+' characters can be entered as content. Currently there are '+self.content.size.to_s+' characters.'
    end
  end

  # custom validation for image width & height with specific dimensions
  def validate_image_size
    if self.image.present? && self.image_width.present? && self.image_height.present?
      if self.image_width != GCQRCode::SIZE_MODULES[self.size]*4 || self.image_height != GCQRCode::SIZE_MODULES[self.size]*4
        errors.add :image, "valid size for the size selected: "+(GCQRCode::SIZE_MODULES[self.size]*4).to_s+"x"+(GCQRCode::SIZE_MODULES[self.size]*4).to_s+" pixels exact!"
      end
    end
  end

  def generate_binary_qr_code
    puts "ERROR COUNT: "+self.errors.count.to_s
    puts "ERROR MESSAGES: "+self.errors.to_a.to_s
    if self.errors.count == 0
      if Rails.env.production?
        url = self.image.current_path.to_s
      else
        url = self.image.current_path.to_s
      end
      qr_code = GCQRCode.new(self.content, url, self.size, "h", false)
      png = qr_code.generate_inline_qr_code_image_color_with_white_border
      binary_string = png.to_blob
      self.binary_qr_code = binary_string
      puts "BINARY_STRING: "+self.binary_qr_code.to_s
    end
  end

end
