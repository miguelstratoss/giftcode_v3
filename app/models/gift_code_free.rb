# encoding: UTF-8
# == Schema Information
#
# Table name: gift_codes
#
#  id                     :integer          not null, primary key
#  from_user_id           :integer
#  to_user_id             :integer
#  to_city_id             :integer
#  gift_card_id           :integer
#  cost                   :decimal(10, 2)
#  to_mobile_phone        :string(255)
#  to_email               :string(255)
#  valid_for_days         :integer
#  title_message          :string(255)
#  body_message           :text
#  binary_qr_code_image   :binary
#  available_balance      :decimal(10, 2)
#  view_id                :string(255)
#  token                  :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  gift_code_state_id     :integer
#  iva                    :decimal(10, 2)   default(16.0)
#  base_iva               :decimal(10, 2)
#  ref_sale               :string(255)
#  email_sent_to_buyer    :boolean
#  email_sent_to_receiver :boolean
#  sms_sent_to_receiver   :boolean
#  free_campaign_id       :integer
#  admin_user_id          :integer
#  redemption_pin         :string(255)
#  from_first_name        :string(255)
#  from_last_name         :string(255)
#  from_email             :string(255)
#  to_first_name          :string(255)
#  to_last_name           :string(255)
#  office_id              :integer
#  send_notifications_now :boolean
#  send_date_time         :datetime
#  decrypt                :integer
#  campaign_point_id      :integer
#  type                   :string(255)
#  points_used            :integer
#

class GiftCodeFree < GiftCode

  require 'barby'
  require 'barby/barcode/code_128'
  require 'barby/outputter/rmagick_outputter'

  attr_accessible :cost, :to_first_name,:to_last_name,:from_first_name, :from_last_name, :from_email, :to_mobile_phone, :to_email, :valid_for_days, :title_message, :body_message, :to_email_confirmation, :to_user_id, :to_city_id, :gift_card_id, :gift_code_state_id, :iva, :base_iva, :ref_sale, :email_sent_to_buyer, :email_sent_to_receiver, :sms_sent_to_receiver,  :free_campaign_id, :admin_user_id, :redemption_pin, :from_user, :send_notifications_now, :send_date_time, :decrypt, :to_address, :to_state, :to_city, :to_country, :to_postal_code

  validates :cost, :presence => true, :numericality => { :greater_than_or_equal_to => 0}
  validates :to_first_name, :presence => true, :length => { :maximum => 50 }
  validates :to_last_name, :presence => true, :length => { :maximum => 50 }
  validates :to_country, :presence => true, :length => { :maximum => 50 }
  validates :to_address, :presence => true, :length => { :maximum => 50 }
  validates :to_state, :presence => true, :length => { :maximum => 50 }
  validates :to_city, :presence => true, :length => { :maximum => 50 }
  validates :to_mobile_phone, :presence => true
  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :to_email, :presence => true, :confirmation => true, :length => { :maximum => 50 }, :format => { :with => email_regex }
  validates_confirmation_of :to_email
  validates :body_message, :presence => true, :length => { :maximum => 400 }
  validates :from_user_id, :presence => true
  validates :from_first_name, :presence => true, :length => { :maximum => 50 }
  validates :from_last_name, :presence => true, :length => { :maximum => 50 }
  validates :from_email, :presence => true, :length => { :maximum => 50 } , :format => { :with => email_regex }
  validates :to_user_id, :presence => true
  # validates :to_city_id, :presence => true
  validates :gift_card_id, :presence => true, :unless => 'is_free?'
  validates :gift_code_state_id, :presence => true
  validates :iva, :presence => true, :numericality => { :greater_than_or_equal_to => 0}, :unless => 'is_a_free_gift_code?'
  validates :base_iva, :presence => true, :numericality => { :greater_than_or_equal_to => 0}, :unless => 'is_a_free_gift_code?'
  validates :ref_sale, :presence => true, :uniqueness => true, :unless => 'is_a_free_gift_code?'
  validates :free_campaign_id, :presence => true, :unless => '!is_a_free_gift_code?'
  validates :admin_user_id, :presence => true, :unless => '!is_a_free_gift_code?'
  validates :send_date_time, :presence => { :message => 'Fecha de envio incorrecta. (debe ser una hora superior a la hora actual.)' },  if: :date_valid_send?
  validates :redemption_pin, :uniqueness => true, if: :validate_unique_pin

  def validate_unique_pin
    self.redemption_pin != nil
  end

  belongs_to :from_user, :class_name => 'User' , :foreign_key => 'from_user_id', :autosave => true
  belongs_to :to_user, :class_name => 'User', :foreign_key => 'to_user_id'
  # belongs_to :to_city, :class_name => 'City', :foreign_key => 'to_city_id'
  belongs_to :gift_card
  belongs_to :gift_code_state
  belongs_to :free_campaign
  belongs_to :admin_user
  belongs_to :office


  has_one :redeban_code, foreign_key: 'hash2', primary_key: 'redemption_pin'

  has_many :transactions, :dependent => :destroy
  has_many :payment_answers, :dependent => :destroy
  has_many :payment_confirmations, :dependent => :destroy
  has_many :downloads, :dependent => :destroy
  has_many :third_party_transactions
  has_many :request_third_party_entities


  ## Estos campos son si el giftcode fue generado en free_campaign
  #


  after_initialize :initialize_default_values

  default_scope :order => 'gift_codes.created_at DESC'

  scope :in_generated_and_payment_pending_state, where("gift_code_state_id = #{GiftCodeState::STATES[:generated_and_payment_pending]} OR gift_code_state_id = #{GiftCodeState::STATES[:generated_office_pending]} OR gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_pending]}")
  scope :in_all_accepted_state, where("gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:free_from_an_office]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]}")
  scope :in_payment_accepted_state, where("gift_code_state_id = :state", :state => GiftCodeState::STATES[:payment_accepted])
  scope :in_payment_rejected_state, where("gift_code_state_id = #{GiftCodeState::STATES[:payment_rejected]} OR gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_rejected]}")

  scope :all_web,
        where(
         "gift_code_state_id = #{GiftCodeState::STATES[:generated_and_payment_pending]} OR
         gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]} OR
         gift_code_state_id = #{GiftCodeState::STATES[:payment_rejected]}"
        )

  scope :all_offices,
        where(
         "gift_code_state_id = #{GiftCodeState::STATES[:generated_office_pending]} OR
         gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]}"
        )
  scope :all_free,
        where(
         "gift_code_state_id = #{GiftCodeState::STATES[:free_from_an_office]}"
        )
  scope :all_web_external,
        where(
         "gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]} OR
         gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]} OR
         gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]}"
        )

  scope :_email_sent_to_buyer,
        where(
        "(
            gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]} OR
            gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]} OR
            gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]}
        ) AND email_sent_to_buyer = #{true}"
        )

  scope :_email_sent_to_receiver,
        where(
            "(
            gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]} OR
            gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]} OR
            gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]}
        ) AND email_sent_to_receiver = #{true}"
        )

  # --

  # Constants //CONTANTES UTILIZADAS EN VERSIONES ANTERIORES
  PAYMENT_RESULT_MESSAGE = ['El GiftCode no existe.', 'El pago se realizo correctamente.', 'El GiftCode es de otra marca.', 'El GiftCode no tiene saldo disponible.']

  KGS_PER_GIFT = 10
  GIFTS_PER_TREE = 25

  NUMBER_OF_CHARACTERS_OF_CONTENT_QR_CODE = 32

  NUMBER_OF_CHARACTERS_OF_REF_SALE = 20

  # NUMBER_OF_CHARACTERS_OF_REDEMPTION_PIN = 16
  NUMBER_OF_CHARACTERS_OF_REDEMPTION_PIN = 16
  ## fin Constants //CONTANTES UTILIZADAS EN VERSIONES ANTERIORES

  def date_valid_send?
    unless self.send_notifications_now
      sdt = self.send_date_time || Time.zone.now
      if sdt >= ( Time.zone.now + 1.hour )
        return false
      else
        self.send_date_time = nil
        return true
      end
    else
      return false
    end
  end

  def initialize_default_values
    if new_record?
      self.gift_code_state_id = GiftCodeState::STATES[:generated_and_payment_pending]
    else
      self.gift_code_state_id ||= GiftCodeState::STATES[:generated_and_payment_pending]
    end
  end

  def generate_unique_values
    rdm_view_id = UUIDTools::UUID.random_create
    self.view_id = rdm_view_id.hexdigest

    rdm_token = UUIDTools::UUID.random_create
    s_token = rdm_token.hexdigest
    self.token= s_token[s_token.length-8, s_token.length]

    generate_redemption_pin

  end

  def campaign
    if self.type != 'PUNTOS'
      self.gift_card.campaign
    elsif self.type == 'PUNTOS'
      self.campaign_point
    end
  end

  def generate_redemption_pin
    if self.campaign.convenio_redeban?
      self.redemption_pin = nil
      get_redeban_code campaign
      self.decrypt = 1
    else
      bin = self.campaign.country.bin
      generate_pin_random
      while GiftCode.find_by_redemption_pin(Digest::SHA3.hexdigest(self.redemption_pin))
        generate_pin_random
      end
      egc = EncryptGiftCode.create code: self.redemption_pin
      self.redemption_pin = egc.hash2
      self.decrypt = 2
    end

  end


  def generate_qr_code_image
    # if self.type != 'PUNTOS'
    #   url = self.gift_card.campaign.brand.qr_code_image.thumb132.current_path.to_s
    #   (url = self.gift_card.campaign.brand.qr_code_image.thumb132.to_s) if url.blank?
    # elsif self.type == 'PUNTOS'
    #   url = self.campaign_point.brand.qr_code_image.thumb132.current_path.to_s
    #   (url = self.campaign_point.brand.qr_code_image.thumb132.to_s) if url.blank?
    # end

    # if Rails.env.development?
    #   url =  Rails.root.to_s + '/app/assets/images/brand_132x132.png'
    # end

    url =  Rails.root.to_s + '/app/assets/images/fondo_qr.png'

    #generate QRcode INLINE and save the png image as a binary file in the database
    # qr_code = GCQRCode.new(redemption_pin_formatted(''), url, 4, "h", true)
    # qr_code = GCQRCode.new(redemption_pin_formatted(''), url, 3, "m", false)
    # png = qr_code.generate_inline_qr_code_image_color_with_white_border
    # binary_string = png.to_blob
    qr = RQRCode::QRCode.new( redemption_pin_formatted(''), :size =>2, :level => :q )
    png = qr.to_img
    self.binary_qr_code_image = png.resize(90, 90).to_blob
  end

  def barcode_image
    barcode = Barby::Code128B.new(redemption_pin_formatted(''))
    outputter = Barby::RmagickOutputter.new(barcode)
    # outputter = Barby::CairoOutputter.new(barcode)
    # ChunkyPNG::Image.from_blob()
    # outputter = Barby::PngOutputter.new(barcode)
    outputter.height = 70
    outputter.to_png
  end

  def generate_free_qr_code_image
    #Giftcode generation
    if Rails.env.production?
      url = self.free_campaign.office.brand.qr_code_image.thumb132.current_path.to_s
    else
      url = self.free_campaign.office.brand.qr_code_image.thumb132.current_path.to_s
    end
    #logger.debug "****************URL of the image: "+url
    #logger.debug "****************URL of the image: "+@gift_card.campaign.brand.qr_code_image.thumb132.current_path.to_s
    #generate QRcode INLINE and save the png image as a binary file in the database
    #documento el siguiente campo por que no funciona, la aternativa es la siguiente linea
    qr_code = GCQRCode.new(self.redemption_pin, url, 4, "h", true)
    #qr_code = RQRCode::QRCode.new(self.content_qr_code, url, 4, "h", true)
    #no funciona la siguiente  linea al quitaar GCQRC
    png = qr_code.generate_inline_qr_code_image_color_with_white_border
    #png = qr_code.generate_inline_qr_code_image_color
    #png = qr_code.generate_inline_qr_code_image_bw
    #agrego linea para generar imagen
    #png = qr_code .to_img
    binary_string = png.to_blob
    self.binary_qr_code_image = binary_string
  end

  def calculate_base_iva
    #The round is to up
    self.base_iva = (self.cost / (1+self.iva/100)).round(2)
  end

  def generate_ref_sale
    rdm_content = UUIDTools::UUID.random_create
    self.ref_sale = rdm_content.hexdigest[0, NUMBER_OF_CHARACTERS_OF_REF_SALE].upcase
  end

  def string_for_sign
    if !GlobalConstants::process_payments_in_production_mode?
      GlobalConstants::PAGOS_ONLINE_TEST_SIGN_KEY+"~"+GlobalConstants::PAGOS_ONLINE_TEST_USER_ID+"~"+self.ref_sale+"~"+("%0.2f"%self.cost).to_s+"~"+self.gift_card.campaign.currency.short_name
    else
      GlobalConstants::PAGOS_ONLINE_PRODUCTION_SIGN_KEY+"~"+GlobalConstants::PAGOS_ONLINE_PRODUCTION_USER_ID+"~"+self.ref_sale+"~"+("%0.2f"%self.cost).to_s+"~"+self.gift_card.campaign.currency.short_name
    end
  end

  def valid_until
    if !self.campaign.convenio_redeban?
      if self.send_date_time
        self.send_date_time + self.valid_for_days.days+1
      else
        self.created_at + self.valid_for_days.days+1
      end
    else
      # redeban_code = RedebanCode.find_by_hash2 self.redemption_pin
      if self.redeban_code
        date = self.redeban_code.expiration_date
        Time.zone.parse("20#{date[2, 2]}-#{date[0, 2]}-01 00:00")
      end
    end
  end

  def process_successful_payment( resend_notifications = false )
    if self.gift_code_state_id != GiftCodeState::STATES[:free_from_an_office ]
      payment = true
      case self.gift_code_state_id
        when GiftCodeState::STATES[:generated_and_payment_pending]
          self.gift_code_state_id = GiftCodeState::STATES[:payment_accepted]
        when GiftCodeState::STATES[:generated_office_pending ]
          self.gift_code_state_id = GiftCodeState::STATES[:generated_office_confirmed]
        when GiftCodeState::STATES[:generated_external_web_pending ]
          self.gift_code_state_id = GiftCodeState::STATES[:generated_external_web_payment_accepted]
        when GiftCodeState::STATES[:free_points]
          self.gift_code_state_id = GiftCodeState::STATES[:free_points]
        else
          payment = false
      end
      if payment
        # Generate New Values (content_qr_code, view_id, token)
        self.generate_unique_values
        # Generate binary qr code
        self.generate_qr_code_image
        # Set available balance
        self.available_balance = self.cost
        #Save gift code state before notifications
        self.save
      end
      if ( payment && self.redemption_pin != nil && !self.redemption_pin.blank? ) || resend_notifications
        #se envia emails de notificacion
        if self.gift_code_state_id == GiftCodeState::STATES[:free_points]
          campaign = self.campaign_point
        else
          campaign = self.gift_card.campaign
        end
        if campaign.send_mail
          send_email
        end
        if campaign.send_sms
          send_sms
        end
      elsif payment && ( self.redemption_pin == nil || self.redemption_pin.blank? )
        #El giftcode no tiene pin valido
        begin
          email = GiftCodeNotifier.error_pin self
          email.deliver
        rescue
          puts "Error grave que no se pudo notificar del giftcode:"
          puts self.id.to_s
        end
      end
    end
  end

  def process_unsuccessful_payment( resend_notifications = false )
    if self.gift_code_state_id != GiftCodeState::STATES[:free_from_an_office ]
      case self.gift_code_state_id
        when GiftCodeState::STATES[:generated_and_payment_pending]
          self.gift_code_state_id = GiftCodeState::STATES[:payment_rejected]
        when GiftCodeState::STATES[:generated_office_pending ]
          #no tiene estado se deja asi
        when GiftCodeState::STATES[:generated_external_web_pending ]
          self.gift_code_state_id = GiftCodeState::STATES[:generated_external_web_payment_rejected]
      end
      self.save
    end
  end

  def complete_to_name
    complete_name = (self.to_first_name ||= "") + " " + (self.to_last_name ||= "")
    complete_name == "" ? self.to_email : complete_name
  end

  def is_free?
    is_a_free_gift_code? || self.type == 'PUNTOS'
  end

  def is_a_free_gift_code?
    self.gift_code_state_id == GiftCodeState::STATES[:free_from_an_office]
  end

  #agregado por miguel segura
  #devuelve el tipo de giftcode
  def type
    case self.gift_code_state_id
      when GiftCodeState::STATES[:free_from_an_office]
        "GRATIS"
      when GiftCodeState::STATES[:payment_accepted],  GiftCodeState::STATES[:payment_rejected], GiftCodeState::STATES[:generated_and_payment_pending]
        "WEB"
      when GiftCodeState::STATES[:generated_office_confirmed], GiftCodeState::STATES[:generated_office_pending]
        "OFICINA"

      when GiftCodeState::STATES[:generated_external_web_payment_accepted], GiftCodeState::STATES[:generated_external_web_pending]
        "PAGINA EXTERNA"
      when GiftCodeState::STATES[:free_points]
        "PUNTOS"
    end
  end

  def to_s
    self.id.to_s
  end

  def redemption_pin_formatted( chart = nil)
    if chart.nil?
     chart = '-'
    end
    begin
      case self.decrypt
        when 1
          pin = RedebanCode.find_by_hash2 self.redemption_pin
        when 2
          pin = EncryptGiftCode.find_by_hash2 self.redemption_pin
        else
          pin = nil
      end
    rescue
      pin=nil
    end
    begin
      if pin
        decrypt = Encryptor.decrypt( Base64.decode64(pin.code), :key => GlobalConstants::SECRET_KEY, :iv => Base64.decode64(pin.iv), :salt => pin.salt)
        ( decrypt.scan /.{1,4}/).join(chart)
      else
        'None'
      end
    rescue
      'None'
    end

  end


  def ref_sale_formatted
    self.ref_sale.present? ? (self.ref_sale.upcase.scan /.{4}/).join('-') : 'None'
  end

  def calculate_iva
    (self.base_iva * (self.iva/100)).round(2)
  end

  # 0 no redimido
  # 1 redimido completo
  # 2 redimido parcial
  # 3 bono vencido
  # 99 error desconocido, puede ser porque el bono no tiene pago aceptado
  def redeemed_state
    begin
      if self.valid_until.past?
        { code:  3, message: 'El bono ha vencido.' }
      else
        if self.cost == self.available_balance
          { code:  0, message: 'El bono no se ha redimido.' }
        else
          if self.available_balance == 0
            { code:  1, message: 'El bono ya fue redimido.' }
          else
            { code:  2, message: 'Bono redimido con saldo.' }
          end
        end
      end
    rescue
      { code:  99, message: 'Error desconocido.' }
    end
  end

  def to_name_capitalize
    name = []
    self.to_first_name.split(' ').each{ |s| name.push s.capitalize }
    self.to_last_name.split(' ').each{ |s| name.push s.capitalize }
    name.join(' ')
  end

  def from_name_capitalize
    name = []
    self.from_first_name.split(' ').each{ |s| name.push s.capitalize }
    self.from_last_name.split(' ').each{ |s| name.push s.capitalize }
    name.join(' ')
  end

  private
    def get_redeban_code campaign      #
      # campaign.batches.where( cost: self.cost ).joins(:redeban_codes).where(:)
      # redeban_code = RedebanCode.where(:enabled=>true, :already_used => nil).last
      #la siguiente consulata no esta optimizada en cualquiera de los dos casos, por que falta determina que batches tiene certificados para ser usuados.
      redeban_code = nil
      if campaign.type == 'CampaignPoint'
        rc = RedebanCode.joins(:batch).where(batches: { id: campaign.batches.map{ |b| b.id }}, redeban_codes: { state_id: RedebanCode::STATES[:habilitado], already_used: [false, nil]}).order( 'redeban_codes.created_at ASC').first
        if rc
          redeban_code = RedebanCode.find rc.id
        end
      else
        #si la campaña no es de puntos se debe filtar batch por giftcard que tiene el valor del certificado
        # batches = campaign.batches.where( gift_card_id: self.gift_card_id )
        # batches = campaign.re
        # rc = RedebanCode.joins(:batch).where(batches: { id: batches.map{ |b| b.id }}, redeban_codes: { state_id: RedebanCode::STATES[:habilitado], already_used: [false, nil]}).order( 'redeban_codes.created_at ASC').first
        redeban_code = campaign.redeban_codes.where(state_id: RedebanCode::STATES[:habilitado], already_used: [false, nil], charge_state_id: RedebanCode::CHARGE_STATE[:sin_saldo]).shuffle.first
      end

      if redeban_code
        self.redemption_pin = redeban_code.hash2
        redeban_code.update_attributes already_used: true, gift_card_id: self.gift_card_id, charge_state_id: RedebanCode::CHARGE_STATE[:enviar_redeban]
      end

    end

    def send_email
      #Send email to receiver
      begin
        if self.send_notifications_now
          if self.gift_code_state_id == GiftCodeState::STATES[:generated_external_web_payment_accepted]
            GiftCodeNotifier.receiver_gift_code_whitelabel(self).deliver
          else
            GiftCodeNotifier.receiver_gift_code(self).deliver
          end
          self.email_sent_to_receiver=true
        end
      rescue => e
        logger.warn "Unable to foo, will ignore: #{e}"
        self.email_sent_to_receiver=false
      ensure
        self.save
      end
      #Send email to buyer
      begin
        GiftCodeNotifier.buyer_gift_code(self).deliver
        self.email_sent_to_buyer=true
      rescue
        self.email_sent_to_buyer=false
      ensure
        self.save
      end
    end


    def send_sms
      # #El mensaje esta incorrecto. REVISAR CUANDO ESTO FUNCIONE.
      # message = self.from_first_name[0..18] + " te envio un GiftCode de #{"%0.0f" % self.cost} al correo #{self.to_email}"
      # begin
      #   smsManager = SMSManager.new(self.to_mobile_phone, message)
      #   smsManager.send_sms
      #   self.sms_sent_to_receiver=true
      # rescue
      #   self.sms_sent_to_receiver=false
      # ensure
      #   self.save
      # end
      self.sms_sent_to_receiver=false
      self.save
    end

    def generate_pin_random
      s_token = UUIDTools::UUID.random_create.to_i.to_s
      self.redemption_pin = s_token[s_token.length-NUMBER_OF_CHARACTERS_OF_REDEMPTION_PIN, s_token.length].to_s
    end



end
