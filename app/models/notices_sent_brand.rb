class NoticesSentBrand < ActiveRecord::Base

  TYPE = { :bonos_redeban_menor_100 => 1, :bonos_redeban_agotandose_rapido => 2 }
  TYPE_ID = { 1 => 'Bonos redeban < 100', 2 => 'Bonos redeban agotandose forma rapida' }

  attr_accessible :campaign_id, :details, :type_notification

  validates :campaign_id, presence: true
  validates :type_notification, presence: true
  validates :details, presence: true

  belongs_to :campaign

  serialize :details, Array

  after_create :send_notices

  def send_notices
    if self.type_notification.present?
      case self.type_notification
        when NoticesSentBrand::TYPE[:bonos_redeban_menor_100]
          GiftCodeNotifier.delay.warning_redeban_codes_less_100( self.campaign.brand )
          # self.delay.send_notification_1
        when NoticesSentBrand::TYPE[:bonos_redeban_agotandose_rapido]
          GiftCodeNotifier.delay.warning_redeban_codes_being_rapidly_depleted( self.campaign.brand )
          # self.delay.send_notification_2
      end
    end
  end

  # 'Bonos redeban < 100'
  def send_notification_1

  end

  # 'Bonos redeban agotandose forma rapida'
  def send_notification_2

  end

end
