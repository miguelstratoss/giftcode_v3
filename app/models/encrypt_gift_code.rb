# == Schema Information
#
# Table name: encrypt_gift_codes
#
#  id         :integer          not null, primary key
#  salt       :string(255)
#  iv         :string(255)
#  hash2      :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EncryptGiftCode < ActiveRecord::Base
  attr_accessible :code, :hash2

  before_save :encryptor

  validates_uniqueness_of :code
  validates_presence_of   :code

  private
  def encryptor
    self.salt = Time.now.to_i.to_s
    self.iv   = Base64.encode64(  OpenSSL::Cipher::Cipher.new('aes-256-cbc').random_iv ).encode('utf-8')
    self.hash2 = Digest::SHA3.hexdigest(code)
    self.code = Base64.encode64( Encryptor.encrypt( code , :key => GlobalConstants::SECRET_KEY, :iv => Base64.decode64(self.iv) , :salt => self.salt ) ).encode('utf-8')
  end

end
