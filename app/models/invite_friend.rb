# == Schema Information
#
# Table name: invite_friends
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  friend_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  fbk_post_id :string(255)
#

class InviteFriend < ActiveRecord::Base
  attr_accessible :friend_id, :user_id, :fbk_post_id

  validate :friend_id, require: true
  validate :user_id, require: true

  belongs_to :user
  belongs_to :friend, :class_name => 'User', :foreign_key => 'friend_id'

  scope :ordered_desc_by_creation_date, order('invite_friends.created_at DESC')

  def used?
    u = nil
    u = User.find_by_fbk_user_id(self.friend.fbk_user_id) if self.friend.present?
    if !u.nil? && self.user.present?
      if u.referral_id == self.user.fbk_user_id
        'true'
      else
        'false'
      end
      'false'
    else
      'false'
    end
  end
end
