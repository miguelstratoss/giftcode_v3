# == Schema Information
#
# Table name: campaigns
#
#  id                        :integer          not null, primary key
#  brand_id                  :integer
#  name                      :string(255)
#  message                   :string(255)
#  send_mail                 :boolean
#  send_sms                  :boolean
#  country_id                :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  currency_id               :integer
#  campaign_state_id         :integer
#  iva                       :decimal(10, 2)   default(16.0)
#  sales_code                :string(255)
#  cost_admin                :decimal(10, 2)
#  iva_cost_admin            :decimal(10, 2)
#  begin_date                :date
#  end_date                  :date
#  cost                      :decimal(10, 2)
#  points_required           :integer
#  number_of_free_gift_codes :integer
#  enabled                   :boolean
#  type                      :string(255)
#  observations              :string(255)
#  convenio_redeban          :integer
#

class Campaign < ActiveRecord::Base

  include ActionView::Helpers::NumberHelper

  attr_accessible :name, :message, :send_mail, :send_sms, :country_id, :brand_id, :currency_id, :campaign_state_id, :iva, :sales_code, :cost_admin, :iva_cost_admin, :convenio, :redeban, :show_qr, :show_barcode, :office_redeem_bono

  validates :name, :presence => true, :length => { :maximum => 50 }
  validates :message, :presence => true, :length => { :maximum => 255 }
  validates :brand_id, :presence => true
  validates :country_id, :presence => true
  validates :currency_id, :presence => true
  validates :campaign_state_id, :presence => true
  validates :iva, :presence => true, :numericality => { :greater_than_or_equal_to => 0}
  validates :cost_admin, :presence => true, :numericality => { :greater_than_or_equal_to => 0}
  validates :iva_cost_admin, :presence => true, :numericality => { :greater_than_or_equal_to => 0}
  validate  :convenio_redeban_batches

  validates_uniqueness_of :campaign_state_id, :scope => [:country_id, :brand_id], :if => :invalid?, :message => "Solo puede haber una campaña activa por pais"

  belongs_to :brand
  belongs_to :country
  belongs_to :currency
  belongs_to :campaign_state
  has_many :gift_cards
  has_many :brand_codes
  # has_many :gift_cards_enable_for_selling_true, :conditions => { :enable_for_selling => true }, :class_name => "GiftCard", :order => 'cost ASC'
  has_many :gift_codes, :through => :gift_cards
  has_many :batches
  has_many :redeban_codes, :autosave => true
  has_many :notices_sent_brands

  after_initialize :initialize_default_vales

  before_create :generate_sales_code

  before_destroy :validate_campaign

  def convenio_redeban_batches
    unless self.new_record?
      if self.convenio_changed? && self.batches.count > 0
        errors.add :convenio, 'La campaña tiene lotes asociados.'
      end
    end
  end

  def validate_campaign
    return true if self.batches.count == 0
    errors.add :convenio, 'La campaña tiene lotes asociados.'
    false
  end

  #miguel segura --
  def cost_admin_current
    number_to_currency( self.cost_admin || 0 , :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
  end

  def calculate_iva_current
    number_to_currency(( ( self.cost_admin || 0 ) * ( (self.iva_cost_admin || 0 )/100)).round(2), :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
  end

  def cost_total_admin
    ( ( self.cost_admin || 0 ) + (( self.cost_admin || 0 ) * ( (self.iva_cost_admin || 0 )/100)).round(2) )
  end

  def cost_total_admin_current
    number_to_currency( ( ( self.cost_admin || 0 ) + (( self.cost_admin || 0 ) * ( (self.iva_cost_admin || 0 )/100)).round(2) ), :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
  end

  def cities
    City.joins(:offices).joins(:state).where( "offices.brand_id = #{self.brand_id} AND offices.enable_for_selling = #{true} AND states.country_id = #{self.country_id}" ).uniq("cities.id")
  end

  def generate_sales_code
    if self.sales_code.nil?
      rdm_content= UUIDTools::UUID.random_create
      self.sales_code= rdm_content.hexdigest
    end
  end

  def invalid?
    campaign_state_id == CampaignState::STATES[:enable]
  end

  def initialize_default_vales
    self.send_mail ||= true
    self.send_sms ||= true
    self.country ||= Country.find_by_name( "Colombia" )
  end

  def gift_code_total ( type = "all" )
    c = 0
    query = {}
    case type
      when "office"
        query = { :gift_code_state_id => GiftCodeState::STATES[:generated_office_confirmed] }
      when "web"
        query = { :gift_code_state_id => GiftCodeState::STATES[:payment_accepted] }
      else
        query = { }
    end
    self.gift_cards.collect do |gc|
      c = c + gc.gift_codes.where( query ).count
    end
    return c
  end

  #miguel segura --
  #agrupa ventas por fecha de giftcodes relizadas por internet y officina
  def time_sales_group
    self.gift_codes.where("gift_codes.gift_code_state_id=#{GiftCodeState::STATES[:payment_accepted]} OR gift_codes.gift_code_state_id=#{GiftCodeState::STATES[:generated_office_confirmed]}").reorder("date(gift_codes.created_at)").count(group:"date(gift_codes.created_at)")
  end
  #--

  def calculate_iva_cost_admin
    (self.cost_admin * (self.iva_cost_admin/100)).round(2)
  end

  def convenio_redeban?
    !self.convenio.blank? && !self.convenio.nil? && self.redeban
  end

  def gift_cards_enable_for_selling
    if convenio_redeban?
      # batches = self.batches.joins( :redeban_codes ).where( redeban_codes: { state_id: RedebanCode::STATES[:habilitado] , already_used: [false, nil ] } ).uniq
      # ids_gift_card = []
      # batches.each do |batch|
      #   ids_gift_card.push batch.gift_card_id
      # end
      # ids_gift_card = ids_gift_card.uniq
      # if ids_gift_card
      #   self.gift_cards.where( :id => ids_gift_card ).order( 'cost ASC' )
      # else
      #   []
      # end
      if self.redeban_codes( state_id: RedebanCode::STATES[:habilitado] , already_used: [false, nil ], :charge_state_id => RedebanCode::CHARGE_STATE[:sin_saldo]  ).count > 0
        self.gift_cards.where( :enable_for_selling => true ).order( 'cost ASC' )
      else
        []
      end
    else
      self.gift_cards.where( :enable_for_selling => true ).order( 'cost ASC' )
    end
  end

end

