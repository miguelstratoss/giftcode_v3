# == Schema Information
#
# Table name: batches
#
#  id            :integer          not null, primary key
#  campaign_id   :integer
#  created_by    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  file          :string(255)
#  fecha_emision :date
#  gift_card_id  :integer
#

class Batch < ActiveRecord::Base

  attr_accessible :created_by, :campaign_id, :file, :fecha_emision, :gift_card_id

  validates :created_by, :presence => true
  validates :campaign_id, :presence => true
  validates :file, :presence => true, :uniqueness => {case_sensitive: false}
  validates :fecha_emision, :presence => true
  validate  :convenio_redeban


  belongs_to :campaign
  delegate :brand, to: :campaign
  belongs_to :gift_card
  belongs_to :created, :class_name => 'AdminUser', :foreign_key => 'created_by'
  has_many :redeban_codes, :autosave => true

  def convenio_redeban
    if self.campaign_id.present?
      unless self.campaign.convenio_redeban?
        errors.add :campaign_id, 'Batch : La campaña no tiene convenio con redeban.'
      end
    end
  end

  def total_redeban_codes
    self.redeban_codes.count
  end

  def used_redeban_codes
    self.redeban_codes.where(:already_used => true).count
  end

  def available_redeban_codes
    self.redeban_codes.where(:state_id => RedebanCode::STATES[:habilitado], :already_used => [false, nil]).count
  end

  def enabled_redeban_codes
    self.redeban_codes.where(:state_id => RedebanCode::STATES[:habilitado]).count
  end

  def con_saldos_redeban_codes
    self.redeban_codes.where(:state_id => RedebanCode::STATES[:habilitado], :charge_state_id => RedebanCode::CHARGE_STATE[:con_saldo] ).count
  end

  def sin_saldos_redeban_codes
    self.redeban_codes.where(:state_id => RedebanCode::STATES[:habilitado], :charge_state_id => RedebanCode::CHARGE_STATE[:sin_saldo]).count
  end


  def value
    if self.campaign.type == 'CampaignPoint'
      self.campaign.cost
    else
      if gift_card
        self.gift_card.cost
      else
        0
      end
    end
  end

end
