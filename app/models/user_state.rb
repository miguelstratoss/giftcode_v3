# == Schema Information
#
# Table name: user_states
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserState < ActiveRecord::Base

  STATES = { :registered => 1, :non_registered => 2, :removed => 2 }

  attr_accessible :name

  validates :name, :presence => true, :length => { :maximum => 50 }

  has_many :users

end
