# == Schema Information
#
# Table name: offices
#
#  id                  :integer          not null, primary key
#  office_name         :string(255)
#  office_address      :string(255)
#  nit                 :string(255)
#  contact_name        :string(255)
#  contact_email       :string(255)
#  brand_id            :integer
#  city_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  enable_for_selling  :boolean          default(TRUE)
#  phone               :string(255)
#  mobile_phone        :string(255)
#  bank_account_number :string(255)
#  bank_name           :string(255)
#  bank_account_type   :string(255)
#  local_company_name  :string(255)
#  schedule            :string(255)
#

class Office < ActiveRecord::Base
  attr_accessible :office_name, :office_address, :nit, :contact_name, :contact_email, :brand_id, :city_id,
      :enable_for_selling, :phone, :mobile_phone, :bank_account_number, :bank_name, :bank_account_type,
      :local_company_name,:schedule, :ip

  validates :office_name, :presence => true, :length => { :maximum => 50 }
  validates :brand_id, :presence => true
  validates :city_id, :presence => true
  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :contact_email, :presence => true, :format => { :with => email_regex }

  belongs_to :brand
  belongs_to :city
  has_many :admin_users
  # has_many :transactions, :class_name => 'Transaction', :foreign_key => 'created_at_office_id'
  has_many :third_party_transactions
  has_many :gift_codes
  has_many :free_campaigns

  def to_s
    self.brand.name + ' ('+ self.city.complete_name+') - '+self.office_name
  end

  #agregado por miguel segura
  #agrupa ventas por fecha de giftcodes relizadas por internet y oficina
  def time_sales_group
    self.gift_codes.where("gift_codes.gift_code_state_id=#{GiftCodeState::STATES[:payment_accepted]} OR gift_codes.gift_code_state_id=#{GiftCodeState::STATES[:generated_office_confirmed]}").reorder("date(gift_codes.created_at)").count(group:"date(gift_codes.created_at)")
  end
  #agrupa bonos redemidos por fecha
  # def time_transactions_group
  #   self.transactions.where("1=1").reorder("date(transactions.created_at)").count(group:"date(transactions.created_at)")
  # end
  #giftcodes vendido hoy
  def gift_codes_today
    self.gift_codes.where( "gift_codes.created_at >= '#{Time.zone.today}' AND gift_codes.gift_code_state_id=#{GiftCodeState::STATES[:generated_office_confirmed]}" )
  end
  #giftcodes vendido en la semana
  def gift_codes_week
    self.gift_codes.where( "gift_codes.created_at >= '#{1.week.ago }' AND gift_codes.gift_code_state_id=#{GiftCodeState::STATES[:generated_office_confirmed]}" )
  end
  #giftcodes vendido en el mes
  def gift_codes_month
    self.gift_codes.where( "gift_codes.created_at >= '#{1.month.ago }' AND gift_codes.gift_code_state_id=#{GiftCodeState::STATES[:generated_office_confirmed]}" )
  end
  #giftcodes vendido en el año
  def gift_codes_year
    self.gift_codes.where( "gift_codes.created_at >= '#{1.year.ago }' AND gift_codes.gift_code_state_id=#{GiftCodeState::STATES[:generated_office_confirmed]}" )
  end

  #redimidos hoy
  def transactions_today
    self.transactions.where( "transactions.created_at >= '#{Time.zone.today}'" )
  end
  #giftcodes vendido en la semana
  def transactions_week
    self.transactions.where( "transactions.created_at >= '#{1.week.ago }'" )
  end
  #giftcodes vendido en el mes
  def transactions_month
    self.transactions.where( "transactions.created_at >= '#{1.month.ago }'" )
  end
  #giftcodes vendido en el año
  def transactions_year
    self.transactions.where( "transactions.created_at >= '#{1.year.ago }'" )
  end

end
