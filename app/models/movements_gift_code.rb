class MovementsGiftCode < ActiveRecord::Base
  attr_accessible :brand_id, :campaign_id, :gift_card_id, :gift_code_id, :identification_number, :third_party_transactions_id

  belongs_to :brand
  belongs_to :campaign
  belongs_to :gift_card
  belongs_to :gift_code
  belongs_to :third_party_transaction


end
