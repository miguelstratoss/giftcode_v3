# == Schema Information
#
# Table name: recommendations
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Recommendation < ActiveRecord::Base
  attr_accessible :description

  validates :description, :presence => true, :length => { :maximum => 200 }

  belongs_to :user

  scope :ordered_desc_by_creation_date, order('created_at DESC')
end
