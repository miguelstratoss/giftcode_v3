class RedebanDailyBalance < ActiveRecord::Base
  attr_accessible :consecutivo, :estado, :file_name, :redeban_code_id, :saldo_inicial, :valor_saldo
  belongs_to :redeban_code
end
