class RedebanMovement < ActiveRecord::Base
  attr_accessible :brand_id, :line, :admin_user_id

  validates :brand_id,      presence: true
  validates :admin_user_id, presence: true
  validates :file,          presence: true

  belongs_to :brand
  belongs_to :admin_user

end
