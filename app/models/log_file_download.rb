class LogFileDownload < ActiveRecord::Base
  attr_accessible :admin_user_id, :name

  validates :admin_user_id, :presence => true
  validates :name,  :presence => true

  belongs_to :admin_user
  has_many  :redeban_codes
end
