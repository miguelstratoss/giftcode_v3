# == Schema Information
#
# Table name: gift_codes
#
#  id                     :integer          not null, primary key
#  from_user_id           :integer
#  to_user_id             :integer
#  to_city_id             :integer
#  gift_card_id           :integer
#  cost                   :decimal(10, 2)
#  to_mobile_phone        :string(255)
#  to_email               :string(255)
#  valid_for_days         :integer
#  title_message          :string(255)
#  body_message           :text
#  binary_qr_code_image   :binary
#  available_balance      :decimal(10, 2)
#  view_id                :string(255)
#  token                  :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  gift_code_state_id     :integer
#  iva                    :decimal(10, 2)   default(16.0)
#  base_iva               :decimal(10, 2)
#  ref_sale               :string(255)
#  email_sent_to_buyer    :boolean
#  email_sent_to_receiver :boolean
#  sms_sent_to_receiver   :boolean
#  free_campaign_id       :integer
#  admin_user_id          :integer
#  redemption_pin         :string(255)
#  from_first_name        :string(255)
#  from_last_name         :string(255)
#  from_email             :string(255)
#  to_first_name          :string(255)
#  to_last_name           :string(255)
#  office_id              :integer
#  send_notifications_now :boolean
#  send_date_time         :datetime
#  decrypt                :integer
#  campaign_point_id      :integer
#  type                   :string(255)
#  points_used            :integer
#

class PointGiftCode < GiftCode

  attr_accessible :campaign_point_id, :points_used

  validates :campaign_point_id, :presence => true
  validates :points_used, :presence => true

  belongs_to :campaign_point

end
