# == Schema Information
#
# Table name: third_party_entities
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  secret         :string(255)
#  incocredito_id :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  configuration  :text
#

class ThirdPartyEntity < ActiveRecord::Base
  attr_accessible :incocredito_id, :name, :secret, :configuration

  validates :incocredito_id, :presence => true
  validates :name, :presence => true

  validates_uniqueness_of :incocredito_id
  validates_uniqueness_of :secret
  validates_uniqueness_of :name

  has_many :brand_codes
  has_many :request_third_party_entities

  THIRD_PARTY_STATES = { :success => 1, :wrong_sign => 2, :giftcard_not_found => 3, :wrong_credentials_brand => 4, :wrong_credentials_commerce_id => 5, :no_funds => 6, :insufficient_funds => 7, :value_format_error => 8, :could_not_save => 9, :confirmation_failed => 10}
  TYPES = { :funds_request => 1, :redemption => 2, :confirmation => 3, :reverse_transaction => 4}
  ENTITY = { :redeban => 'Redeban', :giftcode => 'Giftcode', :credibanco => 'CredibanCo' }

  before_create :generate_secret_key
  before_create :default_values

  def generate_secret_key
    rdm_content= UUIDTools::UUID.random_create
    self.secret= rdm_content.hexdigest
  end

  def default_values
    self.configuration ||= '{}'
  end
end
