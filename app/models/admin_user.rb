# == Schema Information
#
# Table name: admin_users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  password_salt          :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  failed_attempts        :integer          default(0)
#  unlock_token           :string(255)
#  locked_at              :datetime
#  authentication_token   :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  role_id                :integer
#  brand_id               :integer
#  office_id              :integer
#

class AdminUser < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable,  :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :role_id, :brand_id, :office_id

  validates :role_id, :presence => true
  validates :office_id, :presence =>  { :if => :presence_office? }
  validates :brand_id, :presence => true, :if => :presence_brand?

  def prueba?
   #errors.add(:office_id, "message")
  end

  belongs_to :role
  #For brand admin users
  belongs_to :brand
  #For brand office admin users
  belongs_to :office

  has_many :custom_qr_codes, :class_name => 'CustomQrCode', :foreign_key => 'created_by'
  has_many :admin_control_access_alerts
  has_many :transactions, :class_name => 'Transaction', :foreign_key => 'created_by_admin_user_id'
  has_many :gift_codes

  #MIGUEL SEGURA --
  scope :all_office_users, where( :role_id => Role::STATES[:admin_brand_office])
  scope :all_brand_admin, where(:role_id => Role::STATES[:admin_brand ])
  #--

  #BEGIN BRAND AND OFFICE ADMINS
  #miguel segura
  #vuelve el campo office_id obligatorio si el rol es administrador de oficina
  def presence_office?
    ( self.role_id == Role::STATES[:admin_brand_office] )? true : false
  end
  #miguel segura
  #vuelve el campo brand_id obligatorio si el rol es administrador de marca
  def presence_brand?
    ( self.role_id == Role::STATES[:admin_brand] )? true : false
  end

  #This method is used by offices.rb.old file in the BrandAdmin and OfficeAdmin namespaces
  def brand_offices
    if self.role.id == Role::STATES[:admin_brand]
      self.brand.offices
    elsif self.role.id == Role::STATES[:admin_brand_office]
      self.office.brand.offices
    end
  end

  #This method is used by gift_cards.rb file in the BrandAdmin namespace
  def campaigns
    if self.role.id == Role::STATES[:admin_brand]
      return brand_campaigns
    end
  end

  #This method is used by campaigns.rb file in the BrandAdmin and OfficeAdmin namespaces
  def brand_campaigns
    if self.role.id == Role::STATES[:admin_brand]
      self.brand.campaigns
    elsif self.role.id == Role::STATES[:admin_brand_office]
      self.office.brand.campaigns
    end
  end

  def belongs_to_brand
    if self.role.id == Role::STATES[:admin_brand]
      self.brand
    elsif self.role.id == Role::STATES[:admin_brand_office]
      self.office.brand
    end
  end

  #miguel segura --
  def brand_free_campaigns
    if self.role.id == Role::STATES[:admin_brand]
      self.brand.free_campaigns
    elsif self.role.id == Role::STATES[:admin_brand_office]
      self.office.brand.free_campaigns
    end
  end
  #--

  #This method is used by campaigns.rb file in the BrandAdmin and OfficeAdmin namespaces
  def gift_prueba
    if self.role.id == Role::STATES[:admin_brand]
      self.brand.campaigns
    elsif self.role.id == Role::STATES[:admin_brand_office]
      self.office.brand.campaigns
    end
  end

  #This method is used by transactions.rb.old.old file in the BrandAdmin namespace
  def brand_transactions
    #Transaction.joins(:gift_code => {:gift_card => :campaign})
    #Transaction.joins(:gift_code => {:gift_card => {:campaign => :brand}})
    #Transaction.joins(:gift_code => {:gift_card => {:campaign => :brand}}).where('brands.id = :brand_id', :brand_id => 2)
    Transaction.joins(:gift_code => {:gift_card => {:campaign => :brand}}).where('brands.id = :brand_id', :brand_id => self.brand.id)
  end

  def brand_gift_cards
    GiftCard.joins(:campaign => :brand).where('brands.id = :brand_id', :brand_id => self.brand.id)
  end

  #This method is used by gift_codes.rb.old file in the BrandAdmin namespace
  def gift_cards
    if self.role.id == Role::STATES[:admin_brand]
      return brand_gift_cards
    end
  end

  def gift_codes
    if self.role.id == Role::STATES[:admin_brand_office]
      GiftCode.where("gift_codes.gift_code_state_id in (#{GiftCodeState::STATES[:payment_accepted]},  #{GiftCodeState::STATES[:free_from_an_office]}, #{GiftCodeState::STATES[:generated_office_confirmed]}, #{GiftCodeState::STATES[:generated_external_web_payment_accepted]}, #{GiftCodeState::STATES[:free_points]}) AND gift_codes.email_sent_to_receiver = #{true}").where("gift_codes.id IN (#{gift_codes_paid.to_sql}) OR gift_codes.id IN (#{gift_codes_free.to_sql})")
    elsif self.role.id == Role::STATES[:admin_brand]
      GiftCode.where("gift_codes.gift_code_state_id in (#{GiftCodeState::STATES[:payment_accepted]},  #{GiftCodeState::STATES[:free_from_an_office]}, #{GiftCodeState::STATES[:generated_office_confirmed]}, #{GiftCodeState::STATES[:generated_external_web_payment_accepted]}, #{GiftCodeState::STATES[:free_points]}) AND gift_codes.email_sent_to_receiver = #{true}").where("gift_codes.id IN (#{gift_codes_paid.to_sql}) OR gift_codes.id IN (#{gift_codes_free.to_sql})")
    end
  end

  def gift_codes_paid
    if self.role.id == Role::STATES[:admin_brand_office]
      #Paid GiftCodes of the brands in the country of the office admin
      GiftCode.joins(:gift_card => { :campaign => :brand }).where('brands.id = :brand_id AND campaigns.country_id = :country_id', :brand_id => self.office.brand.id, :country_id => self.office.city.state.country.id).select("gift_codes.id")
    elsif self.role.id == Role::STATES[:admin_brand]
      GiftCode.joins(:gift_card => { :campaign => :brand }).where('brands.id = :brand_id', :brand_id => self.brand.id ).select("gift_codes.id")
    end
  end
  def gift_codes_free
    if self.role.id == Role::STATES[:admin_brand_office]
      #Free GiftCodes of the office in the office of the office admin
      GiftCode.joins(:free_campaign => :office).where('offices.id = :office_id', :office_id => self.office.id).select("gift_codes.id")
    elsif self.role.id == Role::STATES[:admin_brand]
      GiftCode.joins(:free_campaign => { :office => :brand }).where('brands.id = :brand_id', :brand_id => self.brand.id).select("gift_codes.id")
    end
  end

  def office_transactions
    if self.role.id == Role::STATES[:admin_brand_office]
      self.office.transactions
    end
  end

  def brand_third_party_transactions
    if self.role.id == Role::STATES[:admin_brand]
      ThirdPartyTransaction.joins(:gift_code => {:gift_card => {:campaign => :brand}}).where('brands.id = :brand_id', :brand_id => self.brand.id)
    end
  end

  def office_third_party_transactions
    if self.role.id == Role::STATES[:admin_brand_office]
      self.office.third_party_transactions
    end
  end
  #END BRAND AND OFFICE ADMINS

  #MIGUEL SEGURA --
  #trae los usuarios de la marca
  def brand_users
    if self.role.id == Role::STATES[:admin_brand]
       AdminUser.where("admin_users.id IN (#{brand_office_join_user.to_sql}) OR admin_users.id IN (#{brand_join_user.to_sql})")
    end
  end
  def brand_office_join_user
    if self.role.id == Role::STATES[:admin_brand]
      AdminUser.joins(:office => :brand ).where( 'brands.id = :brand_id', :brand_id =>self.brand.id).select("admin_users.id")
    end
  end
  def brand_join_user
    if self.role.id == Role::STATES[:admin_brand]
      AdminUser.joins(:brand).where( 'brands.id = :brand_id', :brand_id =>self.brand.id).select("admin_users.id")
    end
  end
  #--
  def to_s
    self.email
  end

end
