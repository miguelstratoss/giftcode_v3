# == Schema Information
#
# Table name: third_party_registrations
#
#  id            :integer          not null, primary key
#  received_sign :string(255)
#  received_date :datetime
#  brand_code_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class ThirdPartyRegistration < ActiveRecord::Base
  attr_accessible :brand_code_id, :received_date, :received_sign

  validates_presence_of :brand_code_id
  #validates_presence_of :received_date, :if => :validation_received_date
  validates_presence_of :received_sign

  validates_uniqueness_of :received_sign

  belongs_to :brand_code

  def validation_received_date
    if self.received_date.strftime('%m/%d/%Y') != Time.now.strftime('%m/%d/%Y')
      errors.add(:received_date, 'Fecha erronea')
      false
    else
      puts self.received_date
      self.received_date = Time.now
      true
    end
  end
end
