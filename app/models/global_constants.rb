# encoding: UTF-8
module GlobalConstants

  #For videos
  USE_VIMEO_PLAYER = false

  #COST DE ADMINISTRACION.
  COST_ADMIN = 2000
  IVA_COST_ADMIN = 320
  #FOR PAGOS ONLINE

  #Production or others environments
  PAGOS_ONLINE_PRODUCTION_ANSWER_URL = "http://www.giftcode.co/answer"
  PAGOS_ONLINE_PRODUCTION_CONFIRMATION_URL = "http://www.giftcode.co/confirmation"
  PAGOS_ONLINE_PRODUCTION_GATEWAY_URL = "https://gatewaylap.pagosonline.net/ppp-web-gateway/"
  PAGOS_ONLINE_PRODUCTION_SIGN_KEY = "41pripkh705ej2gpdas12nj4d8"
  PAGOS_ONLINE_PRODUCTION_USER_ID = "500169"
  PAGOS_ONLINE_COLOMBIA_ACCOUNT_ID = "500409"

  #Staging environment
  PAGOS_ONLINE_TEST_ANSWER_URL = "http://demo.giftcode.co/answer"
  PAGOS_ONLINE_TEST_CONFIRMATION_URL = "http://demo.giftcode.co/confirmation"
  PAGOS_ONLINE_TEST_USER_ID = "1"
  PAGOS_ONLINE_TEST_SIGN_KEY = "012345678901"
  PAGOS_ONLINE_TEST_GATEWAY_URL = "https://stg.gatewaylap.pagosonline.net/ppp-web-gateway/"

  #FOR REDEBAN PAYMENTS

  #Production or others environments
  REDEBAN_PRODUCTION_SIGN_KEY = "EekuKiemie4bee2erah1"
  REDEBAN_TEST_SIGN_KEY = "EekuKiemie4bee2erah1"

  PREFIX_FOR_LOGS = "(giftcode_log) "

  #Metatags for the home page
  # Maximum 90 characters
  HOME_TITLE = "El regalo ideal lo puedes enviar con GiftCode"
  # Maximum 200 characters
  HOME_DESCRIPTION = "GiftCode: La manera más fácil y rápida para enviar bonus de regalo y contribuir con el medio ambiente. Envía bonus de regalo a tus familiares y amigos en menos de 2 minutos."
  # LinkedIn Today (July 2012) requires images to be at least 150 x 80 pixels (Ideal 200x150 for all social networks). If you use the image size required by LinkedIn Today, the image is scaled down to work for Facebook.
  HOME_IMAGE_URL = "https://giftcodeprov2.s3.amazonaws.com/static/thumbnail200x150.png"
  # HOME_IMAGE_IRL = "https://giftcodeprov2.s3.amazonaws.com/static/giftcode-el-regalo-ideal-1200x630.png"
  YO_QUIERO_IMAGE_URL = "http://greencode.co/images/yo_quiero5.jpg"
  HOME_IMAGE_IRL = "http://greencode.co/images/yo_quiero4.jpg"#"http://greencode.co/images/logo_fbk_1200x630.png"#"https://giftcodeprov2.s3.amazonaws.com/static/giftcode-el-regalo-ideal-1200x630.png"

  #HOME_IMAGE_IRL = "http://greencode.co/Logo-post-en-facebook.png"
  HOME_URL = "http://www.giftcode.co"

  #For different metatags of the home page
  # Maximum 90 characters for title
  # Maximum 200 characters for description
  HOME_META_TAG_TYPE_IDENTIFIER = 'f'
  HOME_META_TAGS_BY_DEFAULT = { :id => "default", :title => HOME_TITLE, :description => HOME_DESCRIPTION, :image_url => HOME_IMAGE_URL, :url => HOME_URL}
  HOME_META_TAGS_FOR_WELCOME_EMAIL = { :id => "invite_friends", :title => "Los invito a conocer GiftCode, El Regalo Ideal", :description => "¿Sabías que con GiftCode puedes enviar bonus de regalo a tus familiares y amigos en cuestión de minutos desde cualquier lugar y dispositivo? Únete a la comunidad verde GiftCode.", :image_url => HOME_IMAGE_URL, :url => HOME_URL}
  HOME_META_TAGS_FOR_BUYER_OF_A_GIFT_CODE = { :id => "buyer_gift_code", :title => "Acabo de enviar un GiftCode en menos de 2 minutos", :description => "Acabo de vivir la experiencia de enviar un GiftCode y fue rápido y seguro, los invito a conocer GiftCode. Bonos de regalos amigables con el planeta.", :image_url => HOME_IMAGE_URL, :url => HOME_URL}

  HOME_META_TAGS_FOR_BUYER_AFTER_FIRST_DOWNLOAD = { :id => "buyer_first_download", :title => "Acaban de recibir el GiftCode que envíe hace un momento", :description => "GiftCode me acaba de notificar que el GiftCode que envíe hace un rato ya lo recibieron. ¿Y tú que esperas para comenzar a enviar y recibir GiftCodes?", :image_url => HOME_IMAGE_URL, :url => HOME_URL}

  HOME_META_TAGS_FOR_RECEIVER_OF_A_GIFT_CODE = { :id => "rec_gift_code", :title => "Me sorprendieron con un GiftCode", :description => "Acabo de recibir un GiftCode (bono de regalo) directamente en mi celular a través de un SMS, realmente me sorprendieron, los invito a conocer GiftCode. Bonos de regalo amigables con el planeta.", :image_url => HOME_IMAGE_URL, :url => HOME_URL}
  HOME_META_TAGS_FOR_RECEIVER_AFTER_A_TRANSACTION = { :id => "tran_for_receiver_gift_code", :title => "Acabo de reclamar un GiftCode", :description => "Pude reclamar un GiftCode (bono de regalo) de manera fácil y rápida, los invito a conocer GiftCode. Bonos de regalos amigables con el planeta.", :image_url => HOME_IMAGE_URL, :url => HOME_URL}
  HOME_META_TAGS_FOR_GREEN_GIFT_CODE_GOAL = { :id => "green_gift_code", :title => "Con GiftCode ayudas a reforestar el mundo", :description => "Enviando GiftCodes (bonus de regalo) puedes ayudar con la siembra de un millón de árboles en Puerto Gaitán – Colombia. GiftCode: Bonos de regalo amigables con el planeta.", :image_url => HOME_IMAGE_URL, :url => HOME_URL}

  SECRET_KEY = '6f9840075b5b14fae264910863440a16'

  TWITTER_RELATED_ACCOUNTS = '_GreenCode,TicketCode,alexgreencode'

  def self.get_meta_tags_for_home(identifier)
    meta_tags = nil
    if identifier.present?
      if identifier == GlobalConstants::HOME_META_TAGS_FOR_WELCOME_EMAIL[:id]
        meta_tags = {:title => GlobalConstants::HOME_META_TAGS_FOR_WELCOME_EMAIL[:title], :description => GlobalConstants::HOME_META_TAGS_FOR_WELCOME_EMAIL[:description].gsub(/\s\s+/,' '), :image_url => GlobalConstants::HOME_META_TAGS_FOR_WELCOME_EMAIL[:image_url], :url => GlobalConstants::HOME_META_TAGS_FOR_WELCOME_EMAIL[:url]+"/?"+GlobalConstants::HOME_META_TAG_TYPE_IDENTIFIER+"="+GlobalConstants::HOME_META_TAGS_FOR_WELCOME_EMAIL[:id] }

      elsif identifier == GlobalConstants::HOME_META_TAGS_FOR_BUYER_OF_A_GIFT_CODE[:id]
        meta_tags = {:title => GlobalConstants::HOME_META_TAGS_FOR_BUYER_OF_A_GIFT_CODE[:title], :description => GlobalConstants::HOME_META_TAGS_FOR_BUYER_OF_A_GIFT_CODE[:description].gsub(/\s\s+/,' '), :image_url => GlobalConstants::HOME_META_TAGS_FOR_BUYER_OF_A_GIFT_CODE[:image_url], :url => GlobalConstants::HOME_META_TAGS_FOR_BUYER_OF_A_GIFT_CODE[:url]+"/?"+GlobalConstants::HOME_META_TAG_TYPE_IDENTIFIER+"="+GlobalConstants::HOME_META_TAGS_FOR_BUYER_OF_A_GIFT_CODE[:id] }

      elsif identifier == GlobalConstants::HOME_META_TAGS_FOR_BUYER_AFTER_FIRST_DOWNLOAD[:id]
        meta_tags = {:title => GlobalConstants::HOME_META_TAGS_FOR_BUYER_AFTER_FIRST_DOWNLOAD[:title], :description => GlobalConstants::HOME_META_TAGS_FOR_BUYER_AFTER_FIRST_DOWNLOAD[:description].gsub(/\s\s+/,' '), :image_url => GlobalConstants::HOME_META_TAGS_FOR_BUYER_AFTER_FIRST_DOWNLOAD[:image_url], :url => GlobalConstants::HOME_META_TAGS_FOR_BUYER_AFTER_FIRST_DOWNLOAD[:url]+"/?"+GlobalConstants::HOME_META_TAG_TYPE_IDENTIFIER+"="+GlobalConstants::HOME_META_TAGS_FOR_BUYER_AFTER_FIRST_DOWNLOAD[:id] }

      elsif identifier == GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_OF_A_GIFT_CODE[:id]
        meta_tags = {:title => GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_OF_A_GIFT_CODE[:title], :description => GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_OF_A_GIFT_CODE[:description].gsub(/\s\s+/,' '), :image_url => GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_OF_A_GIFT_CODE[:image_url], :url => GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_OF_A_GIFT_CODE[:url]+"/?"+GlobalConstants::HOME_META_TAG_TYPE_IDENTIFIER+"="+GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_OF_A_GIFT_CODE[:id] }

      elsif identifier == GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_AFTER_A_TRANSACTION[:id]
        meta_tags = {:title => GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_AFTER_A_TRANSACTION[:title], :description => GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_AFTER_A_TRANSACTION[:description].gsub(/\s\s+/,' '), :image_url => GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_AFTER_A_TRANSACTION[:image_url], :url => GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_AFTER_A_TRANSACTION[:url]+"/?"+GlobalConstants::HOME_META_TAG_TYPE_IDENTIFIER+"="+GlobalConstants::HOME_META_TAGS_FOR_RECEIVER_AFTER_A_TRANSACTION[:id] }

      elsif identifier == GlobalConstants::HOME_META_TAGS_FOR_GREEN_GIFT_CODE_GOAL[:id]
        meta_tags = {:title => GlobalConstants::HOME_META_TAGS_FOR_GREEN_GIFT_CODE_GOAL[:title], :description => GlobalConstants::HOME_META_TAGS_FOR_GREEN_GIFT_CODE_GOAL[:description].gsub(/\s\s+/,' '), :image_url => GlobalConstants::HOME_META_TAGS_FOR_GREEN_GIFT_CODE_GOAL[:image_url], :url => GlobalConstants::HOME_META_TAGS_FOR_GREEN_GIFT_CODE_GOAL[:url]+"/?"+GlobalConstants::HOME_META_TAG_TYPE_IDENTIFIER+"="+GlobalConstants::HOME_META_TAGS_FOR_GREEN_GIFT_CODE_GOAL[:id] }
      end
    end

    if meta_tags.nil?
      meta_tags = {:title => GlobalConstants::HOME_META_TAGS_BY_DEFAULT[:title], :description => GlobalConstants::HOME_META_TAGS_BY_DEFAULT[:description].gsub(/\s\s+/,' '), :image_url => GlobalConstants::HOME_META_TAGS_BY_DEFAULT[:image_url], :url => GlobalConstants::HOME_META_TAGS_BY_DEFAULT[:url] }
    end

    return meta_tags
  end

  def self.process_payments_in_production_mode?
    if Rails.env.staging? || Rails.env.development?
      false
    else
      true
    end
  end

  #GLOBAL VARIABLES FOR MIXPANEL

  def self.use_mix_panel?
    (Rails.env.production? || Rails.env.staging? || Rails.env.prestaging?) && true
    #Comment the above line and uncomment the following line for doing mixpanel texting in the development environment
    #(Rails.env.production? || Rails.env.staging? || Rails.env.prestaging? || Rails.env.development?) && true
  end

  def self.token_for_mixpanel
    if Rails.env.production?
      "bfde9b17a073a764dcb7d1483107cdf7"
    elsif Rails.env.staging?
      "bbbaea6b2a63cd51ee3fe63603ffce75"
    elsif Rails.env.prestaging?
      # "bfde9b17a073a764dcb7d1483107cdf7"
      "ee610a6fee3a37db24719db53d54ed61"
    elsif Rails.env.development?
      "2e84b2ea8650c738a177a7ee8fa4b371"
      # "bfde9b17a073a764dcb7d1483107cdf7"
    end
  end

  def self.host
    if Rails.env.production?
      'http://www.giftcode.co'
    elsif Rails.env.staging?
      'http://demo.giftcode.co'
    elsif Rails.env.prestaging?
      'http://prestaging.giftcode.co'
    elsif Rails.env.development?
      'http://0.0.0.0:4000'
    elsif Rails.env.c9?
      'https://giftcodev3-miguelseguray-1.c9.io'
    end
  end

  def self.env
    if Rails.env.staging?
      'DEMO: '
    elsif Rails.env.prestaging?
      'PRESTAGING: '
    elsif Rails.env.development?
      'DEVELOPMENT: '
    elsif Rails.env.c9msegura?
      'CLOUD9-MSEGURA'
    elsif Rails.env.production?
      ''
    end
  end

  def self.facebook
    if Rails.env.production?
      '264825790280070'
    elsif Rails.env.staging?
      '314105948686173'
    elsif Rails.env.prestaging?
      '247447088742117'
    elsif Rails.env.development?
      '350149361715844'
    elsif Rails.env.c9msegura?
      '419442598257898'
    end
  end

  #for version
  def self.full_version?
    Setting.find_by_name('Show Full Version').value == 'true'
  end

  def self.I18n_locale
    :es
  end

  def self.connect_web_service
    if Rails.env.development?
      'https://192.168.0.105/giftcodev3_gateway/data/web_service'
    else
      'https://162.243.38.82/giftcodev3_gateway/data/web_service'
    end
  end

  def self.connect_amazon
      if Rails.env.production?
        'https://54.84.177.229/pasarela_credibanco/set_data.php'
      else
        if Rails.env.development?
          'http://localhost/pasarela_credibanco/set_data_test.php'
        else
          'https://54.84.177.229/pasarela_credibanco/set_data_test.php'
        end
      end
  end

end