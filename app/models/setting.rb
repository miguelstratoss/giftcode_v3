# == Schema Information
#
# Table name: settings
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  value      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Setting < ActiveRecord::Base
  attr_accessible :name, :value

  validates :name, :presence => true, :uniqueness => { :case_sensitive => false }, :length => { :maximum => 50 }
  validates :value, :presence => true, :length => { :maximum => 50 }
end
