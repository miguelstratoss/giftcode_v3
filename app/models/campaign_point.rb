# == Schema Information
#
# Table name: campaigns
#
#  id                        :integer          not null, primary key
#  brand_id                  :integer
#  name                      :string(255)
#  message                   :string(255)
#  send_mail                 :boolean
#  send_sms                  :boolean
#  country_id                :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  currency_id               :integer
#  campaign_state_id         :integer
#  iva                       :decimal(10, 2)   default(16.0)
#  sales_code                :string(255)
#  cost_admin                :decimal(10, 2)
#  iva_cost_admin            :decimal(10, 2)
#  begin_date                :date
#  end_date                  :date
#  cost                      :decimal(10, 2)
#  points_required           :integer
#  number_of_free_gift_codes :integer
#  enabled                   :boolean
#  type                      :string(255)
#  observations              :string(255)
#  convenio_redeban          :integer
#

class CampaignPoint < Campaign

  attr_accessible :begin_date, :end_date, :cost, :points_required, :number_of_free_gift_codes, :enabled, :observations

  validates :begin_date, :presence => true
  validates :end_date, :presence => true
  validates :cost, :presence => true
  validates :points_required, :presence => true
  validates :number_of_free_gift_codes, :presence => true

  belongs_to :brand
  has_many :point_gift_codes

  def available_gift_codes
    if self.convenio_redeban?
      self.batches.joins( :redeban_codes ).where( redeban_codes: { state_id: RedebanCode::STATES[:habilitado], already_used: [false, nil ] } ).count
    else
      self.number_of_free_gift_codes - self.point_gift_codes.count
    end
  end

  def cost_current
    number_to_currency( self.cost, :separator => ',', :delimiter => '.', :precision => 0, :locale => :en)
  end

  def number_days_missing
    d = DateTime.parse(Time.zone.now.to_s)
    d2 = self.end_date
    (d2-d).to_i
  end

  # def self.number_campaign_enabled
  #   CampaignPoint.where( "enabled = :enabled and end_date >= :today", { enabled: true, today: Time.zone.now }  ).joins("LEFT JOIN gift_codes ON gift_codes.type = 'PointGiftCode' AND gift_codes.campaign_point_id = campaigns.id").group("campaigns.id").having("count(campaigns.id) < campaigns.number_of_free_gift_codes").length
  # end
  #
  # def self.campaign_enabled
  #   CampaignPoint.where( "enabled = :enabled and end_date >= :today", { enabled: true, today: Time.zone.now }  ).joins("LEFT JOIN gift_codes ON gift_codes.type = 'PointGiftCode' AND gift_codes.campaign_point_id = campaigns.id").group("campaigns.id").having("count(campaigns.id) < campaigns.number_of_free_gift_codes")
  # end

  def self.enable_for_selling
    campaign_points = CampaignPoint.where( "enabled = :enabled and end_date >= :today", { enabled: true, today: Time.zone.now }  ).where(:convenio => nil ).joins("LEFT JOIN gift_codes ON gift_codes.type = 'PointGiftCode' AND gift_codes.campaign_point_id = campaigns.id").group("campaigns.id").having("count(campaigns.id) < campaigns.number_of_free_gift_codes")
    #cr = convenio_redeban
    #cp = campaign_point
    cpcr = CampaignPoint.where( "enabled = :enabled and end_date >= :today", { enabled: true, today: Time.zone.now }  ).where('convenio != null')
    cpcr.each do |campaign|
      if campaign.batches.joins( :redeban_codes ).where( redeban_codes: { state_id: RedebanCode::STATES[:habilitado], already_used: [false, nil ] } ).uniq.count > 0
        campaign_points.push campaign
      end
    end
    campaign_points
  end

end
