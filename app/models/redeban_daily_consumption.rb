# == Schema Information
#
# Table name: redeban_daily_consumptions
#
#  id                :integer          not null, primary key
#  redeban_code_id   :integer
#  file_name         :string(255)
#  valor_de_consumo  :decimal(, )
#  fecha_vencimiento :date
#  estado            :integer
#  valor_del_saldo   :decimal(, )
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  error             :string(255)
#

class RedebanDailyConsumption < ActiveRecord::Base
  attr_accessible :estado, :fecha_vencimiento, :file_name, :valor_de_consumo, :valor_del_saldo

  serialize :error, Array

  belongs_to :redeban_code
  
end
