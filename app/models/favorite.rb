# == Schema Information
#
# Table name: favorites
#
#  id         :integer          not null, primary key
#  brand_id   :integer
#  user_id    :integer
#  enabled    :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Favorite < ActiveRecord::Base
  attr_accessible :enabled, :brand_id

  validates_uniqueness_of :brand_id, :scope => :user_id
  validates_presence_of :brand
  validates_associated :brand
  validates_presence_of :user
  validates_associated :user

  belongs_to :brand
  belongs_to :user
  has_many :categories, :through => :brand

  scope :ordered_desc_by_creation_date, order('favorites.created_at DESC')

  def self.user_favorite_number
    self.joins(:brand=>:brand_categories).where('favorites.enabled = true AND brands.enable_for_selling = true').group('brand_categories.brand_id').count.count
  end

  def brand_name
    self.brand.name
  end

end
