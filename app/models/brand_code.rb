# == Schema Information
#
# Table name: brand_codes
#
#  id                    :integer          not null, primary key
#  third_party_entity_id :integer
#  campaign_id           :integer
#  code                  :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  active_for_payment    :boolean
#  terminal              :string(255)
#  type_terminal         :string(255)
#

class BrandCode < ActiveRecord::Base
  attr_accessible :code, :third_party_entity_id, :campaign_id, :active_for_payment, :terminal, :type_terminal, :generated_by_gift_code

  belongs_to :campaign
  belongs_to :third_party_entity
  has_many  :third_party_transactions

  delegate :brand, :to => :campaign, :allow_nil => true

  validates :campaign_id, :presence => true
  validates :third_party_entity_id, :presence => true
  validates :code, :presence => true


  validates_uniqueness_of :campaign_id, :scope => [:third_party_entity_id]
  validates_uniqueness_of :active_for_payment, :scope => [:campaign_id], :if => :true_unique?, :message => "Solo puede haber un medio de pago activo por campaña"

  def true_unique?
    result = BrandCode.where( :campaign_id => self.campaign_id, :active_for_payment => true ).count
    if self.active_for_payment == false && result == 1
      false
    elsif result > 0
      true
    else
      false
    end
  end

  def to_s
    self.campaign.brand.name + ': ' + self.third_party_entity.name
  end
end
