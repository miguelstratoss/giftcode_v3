# == Schema Information
#
# Table name: transactions
#
#  id                       :integer          not null, primary key
#  gift_code_id             :integer
#  cost                     :decimal(10, 2)
#  terminal_id              :string(255)
#  consecutive              :string(255)
#  terminal_date            :datetime
#  state                    :string(255)
#  authorization_number     :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  created_at_office_id     :integer
#  created_by_admin_user_id :integer
#

class Transaction < ActiveRecord::Base

  attr_accessible :cost, :terminal_id, :consecutive, :terminal_date,
                  :state, :authorization_number, :content_qr_code, :created_at_office_id,
                  :created_by_admin_user_id, :cost_confirmation, :available_balance

  attr_accessor :content_qr_code, :cost_confirmation, :available_balance

  validates :gift_code_id, :presence => true
  validates :cost,:presence => true, :numericality => { :greater_than_or_equal_to => 0}, :confirmation => true
  validates :terminal_id, :presence => true
  validates :consecutive, :presence => true
  validates :terminal_date, :presence => true
  validates :state, :presence => true
  validates :authorization_number, :presence => true
  validates :created_at_office_id, :presence => true
  validates :created_by_admin_user_id, :presence => true

  belongs_to :gift_code
  belongs_to :created_at_office, :class_name => 'Office', :foreign_key => 'created_at_office_id'
  belongs_to :created_by_admin_user, :class_name => 'AdminUser', :foreign_key => 'created_by_admin_user_id'

  scope :ordered_by_created_at_desc, order('transactions.created_at DESC')
  scope :created_today, -> { where( :created_at => (Time.zone.now.beginning_of_day..Time.zone.now) )}
  scope :created_yesterday, -> { where( :created_at => (Time.zone.now.yesterday.beginning_of_day..Time.zone.now.yesterday.end_of_day) )}
  scope :created_this_week, -> { where( :created_at => (Time.zone.now.beginning_of_week..Time.zone.now.end_of_week) )}
  scope :created_last_week, -> { where( :created_at => (1.week.ago.beginning_of_week..1.week.ago.end_of_week) )}
  scope :created_this_month, -> { where( :created_at => (Time.zone.now.beginning_of_month..Time.zone.now.end_of_month) )}
  scope :created_last_month, -> { where( :created_at => (1.month.ago.beginning_of_month..1.month.ago.end_of_month) )}

  scope :paid, joins(:gift_code).where('gift_codes.gift_code_state_id = :gift_code_state_id', :gift_code_state_id => GiftCodeState::STATES[:payment_accepted])
  scope :free, joins(:gift_code).where('gift_codes.gift_code_state_id = :gift_code_state_id', :gift_code_state_id => GiftCodeState::STATES[:free_from_an_office])

end
