# == Schema Information
#
# Table name: admin_control_access_alerts
#
#  id                    :integer          not null, primary key
#  admin_user_id         :integer
#  alert_for_access_path :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class AdminControlAccessAlert < ActiveRecord::Base
  attr_accessible :alert_for_access_path

  validates :admin_user_id, :presence => true

  belongs_to :admin_user

  scope :ordered_by_created_at_asc, order('admin_control_access_alerts.created_at DESC')
end
