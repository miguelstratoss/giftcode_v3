# == Schema Information
#
# Table name: third_party_transactions
#
#  id                      :integer          not null, primary key
#  third_party_entity_id   :integer
#  brand_code_id           :integer
#  transaction_value       :float
#  transaction_currency    :string(255)
#  new_balance             :float
#  received_hash           :string(1000)
#  dir_ip                  :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  tp_transaction_type_id  :integer
#  tp_transaction_state_id :integer
#  related_transaction_id  :integer
#  gift_code_id            :integer
#  office_id               :integer
#  terminal_id             :integer
#  reversed                :boolean
#  confirmed               :boolean
#  dir_ip_client           :string(255)
#

class ThirdPartyTransaction < ActiveRecord::Base

  attr_accessible :brand_code_id, :confirmed, :dir_ip, :dir_ip_client, :gift_code_id, :tp_transaction_state_id, :tp_transaction_type_id, :new_balance, :received_hash, :related_transaction_id, :reversed, :third_party_entity_id, :transaction_currency, :transaction_value, :office_id, :terminal_id, :input_type_id, :approbation_number, :created_at

  belongs_to :tp_transaction_state
  belongs_to :tp_transaction_type
  belongs_to :brand_code
  belongs_to :third_party_entity
  belongs_to :gift_code
  belongs_to :office
  belongs_to :input_type

  has_one :movements_gift_code

  delegate :campaign, :to => :brand_code, :allow_nil => true
  delegate :brand, :to => :brand_code, :allow_nil => true

  scope :marketplace, where("third_party_transactions.tp_transaction_type_id = :state", :state => TpTransactionType::TYPES[:marketplace_request])
  scope :funds, where("third_party_transactions.tp_transaction_type_id = :state", :state => TpTransactionType::TYPES[:funds_request])
  scope :redemption, where("third_party_transactions.tp_transaction_type_id = :state", :state => TpTransactionType::TYPES[:redemption])
  scope :pending_redemption, where("third_party_transactions.tp_transaction_type_id = :state", :state => TpTransactionType::TYPES[:pending_redemption])
  scope :confirm_redemption, where("third_party_transactions.tp_transaction_type_id = :state", :state => TpTransactionType::TYPES[:confirm_redemption])
  scope :reverse_redemption, where("third_party_transactions.tp_transaction_type_id = :state", :state => TpTransactionType::TYPES[:reverse_redemption])
  scope :all

  before_create :save_settings
  after_save :save_movement

  def save_settings
    if self.gift_code_id
      self.brand_id = self.gift_code.brand_id
      self.campaign_id = self.gift_code.campaign_id
      self.gift_card_id = self.gift_code.gift_card_id
    end
  end

  def save_movement
    if self.tp_transaction_type_id == TpTransactionType::TYPES[:redemption] && self.tp_transaction_state_id == TpTransactionState::STATES[:success]
      activated = MovementsGiftCode.new
      activated.brand_id = self.brand_id
      activated.campaign_id = self.campaign_id
      activated.gift_code_id = self.gift_code_id
      activated.gift_card_id = self.gift_card_id
      activated.third_party_transaction_id = self.id
      activated.save
    end
  end

end
