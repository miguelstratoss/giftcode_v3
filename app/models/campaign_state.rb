# == Schema Information
#
# Table name: campaign_states
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CampaignState < ActiveRecord::Base
  attr_accessible :name

  validates :name, :presence => true, :uniqueness => { :case_sensitive => false }, :length => { :maximum => 50 }

  STATES = { :enable => 1, :disabled => 2 }

  has_many :campaigns
end
