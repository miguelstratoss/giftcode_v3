# == Schema Information
#
# Table name: gift_cards
#
#  id                 :integer          not null, primary key
#  campaign_id        :integer
#  cost               :decimal(10, 2)
#  image              :string(255)
#  valid_for_days     :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  enable_for_selling :boolean          default(TRUE)
#

class GiftCard < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  attr_accessible :cost, :valid_for_days, :campaign_id, :enable_for_selling, :convenio

  validates :cost, :presence => true, :numericality => { :only_integer =>true,
                                :greater_than_or_equal_to => 0}

  validates :valid_for_days, :presence => true, :numericality => { :only_integer =>true,
                                :greater_than_or_equal_to => 0}

  validates :campaign_id, :presence => true

  belongs_to :campaign

  has_many :gift_codes
  has_many :batches

  # mount_uploader :image, GiftCardUploader

  scope :enabled_for_selling, where("enable_for_selling = :state", :state => true)
  scope :ordered_by_cost, order('gift_cards.cost ASC')

  def to_s
    self.cost
  end

  #miguel segura --
  def cost_current
    number_to_currency(self.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
  end

  def cost_plus_cost_total_admin_current
    number_to_currency(self.cost + self.campaign.cost_total_admin, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
  end
  #--

end
