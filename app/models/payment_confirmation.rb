# == Schema Information
#
# Table name: payment_confirmations
#
#  id                  :integer          not null, primary key
#  gift_code_id        :integer
#  pol_state           :integer
#  cost                :decimal(10, 2)
#  iva                 :decimal(10, 2)
#  currency_short_name :string(255)
#  payment_type_id     :integer
#  params              :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  final_result        :string(255)
#

class PaymentConfirmation < ActiveRecord::Base
  attr_accessible :gift_code_id, :pol_state, :cost, :iva, :currency_short_name, :payment_type_id, :params, :final_result

  belongs_to :gift_code

  scope :ordered_by_created_at_desc, order('payment_confirmations.created_at DESC')
end
