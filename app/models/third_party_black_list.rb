# == Schema Information
#
# Table name: third_party_black_lists
#
#  id         :integer          not null, primary key
#  dir_ip     :string(255)
#  blocked    :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ThirdPartyBlackList < ActiveRecord::Base
  attr_accessible :blocked, :dir_ip

  validates :dir_ip, :presence => true, :length => { :maximum => 40 }
end
