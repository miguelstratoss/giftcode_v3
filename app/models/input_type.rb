class InputType < ActiveRecord::Base
  attr_accessible :name
  has_many :ThirdPartyTransaction
  TYPES = { :manual => 1, :qr_reader => 2 }
end
