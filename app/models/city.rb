# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  state_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class City < ActiveRecord::Base
  attr_accessible :name

  validates :name, :presence => true, :length => { :maximum => 50 }

  belongs_to :state
  has_many :offices
  has_many :brands, :through => :offices
  has_many :searches



  def complete_name
    name + ", "+state.country.name
  end

  def self.all_cities_for_search
    self.all.map { |k| "\"#{k.complete_name}\"" }.join(",")
  end

  def offices_enabled_for_a_brand(brand)
    Office.joins(:brand, :city).where('brands.id = :brand_id and cities.id = :city_id and offices.enable_for_selling = true', :brand_id => brand.id, :city_id => self.id).uniq
  end
end
