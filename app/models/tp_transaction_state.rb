# == Schema Information
#
# Table name: tp_transaction_states
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TpTransactionState < ActiveRecord::Base

  STATES = { :success => 1, :wrong_sign => 2, :giftcard_not_found => 3, :wrong_credentials_brand => 4, :wrong_credentials_commerce_id => 5, :no_funds => 6, :insufficient_funds => 7, :value_format_error => 8, :could_not_save => 9, :target_transaction_not_found => 10, :dir_ip_blocked => 11, :giftcard_expired => 12, :ip_client_blocked => 13, :gitcard_not_belongs_to_brand => 14, :unknown_error => 99 }

  attr_accessible :id, :name

  validates :name, :presence => true, :length => { :maximum => 50 }

  has_many :third_party_transactions

end
