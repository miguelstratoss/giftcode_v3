# == Schema Information
#
# Table name: friendship_states
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FriendshipState < ActiveRecord::Base

  STATES = { :without_relation => 1, :invited => 2, :friend => 3, :deleted => 4 }

  attr_accessible :name

  validates :name, :presence => true, :length => { :maximum => 50 }

  has_many :friendships
end
