# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  position   :integer
#  enabled    :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  _class     :string(255)
#

class Category < ActiveRecord::Base

  attr_accessible :name, :enabled, :position, :_class

  validates :_class, :presence => true
  validates :name, :presence => true
  validates_uniqueness_of :name
  validates_uniqueness_of :position

  has_many :brand_categories
  has_many :brands, :through => :brand_categories, :order => "name ASC", :conditions => 'brands.enable_for_selling = true'
  has_many :favorites, :through => :brands, :conditions => { 'favorites.enabled' => true }

end
