# == Schema Information
#
# Table name: facebook_walls
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  fbk_post_id :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class FacebookWall < ActiveRecord::Base
  attr_accessible :fbk_post_id, :user_id

  validate :user_id, require: true
  validate :fbk_post_id, require: true

  belongs_to :user

end
