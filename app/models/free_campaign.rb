# == Schema Information
#
# Table name: free_campaigns
#
#  id             :integer          not null, primary key
#  office_id      :integer
#  admin_user_id  :integer
#  valid_for_days :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  body_message   :text
#  cost           :decimal(10, 2)
#  currency_id    :integer
#

class FreeCampaign < ActiveRecord::Base
  attr_accessible :office_id, :admin_user_id, :cost, :valid_for_days, :body_message, :currency_id

  validates :office_id, :presence => true
  validates :admin_user_id, :presence => true
  validates :cost, :presence => true, :numericality => { :greater_than_or_equal_to => 0}
  validates :valid_for_days, :presence => true, :numericality => { :only_integer =>true, :greater_than_or_equal_to => 0}
  validates :currency_id, :presence => true
  validates :body_message, :presence => true, :length => { :maximum => 400 }

  belongs_to :admin_user
  belongs_to :office
  belongs_to :currency
  has_many :gift_codes

  def to_s
    self.office.office_name+' ('+self.office.brand.name+')'
  end

  def brand
    office.brand
  end
end
