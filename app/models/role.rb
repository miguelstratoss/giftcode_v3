# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Role < ActiveRecord::Base

  attr_accessible :name

  validates :name, :presence => true, :uniqueness => true, :length => { :maximum => 50 }

  has_many :admin_users

  STATES = { :admin_brand => 1, :admin_gift_code => 2, :admin_brand_office => 3}

end
