# == Schema Information
#
# Table name: brands
#
#  id                    :integer          not null, primary key
#  name                  :string(255)
#  contact_name          :string(255)
#  contact_email         :string(255)
#  sn_twitter_name       :string(255)
#  nit                   :string(255)
#  description           :text
#  url_website           :string(255)
#  url_facebook          :string(255)
#  url_twitter           :string(255)
#  url_youtube           :string(255)
#  url_linkedin          :string(255)
#  image                 :string(255)
#  qr_code_image         :string(255)
#  manager_id            :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  short_description     :string(255)
#  large_image           :string(255)
#  logo2_image           :string(255)
#  qr_code_example_image :string(255)
#  enable_for_selling    :boolean          default(FALSE)
#  favorite_priority     :integer          default(1)
#  image_email           :string(255)
#  image_white_label     :string(255)
#  yo_quiero_facebook    :integer
#

class Brand < ActiveRecord::Base
  attr_accessible :name, :contact_name, :contact_email, :sn_twitter_name, :nit, :description,
                  :url_website, :url_facebook, :url_twitter, :url_youtube, :url_linkedin, :image,
                  :image_white_label, :image_email, :qr_code_image, :image_yo_quiero, :manager_id, :short_description, :large_image,
                  :enable_for_selling, :favorite_priority, :trade_name#, :image_email_bin,, :identifier, :original_filename, :content_type, :size, :data#, :logo2_image,:qr_code_example_image


  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :name, :presence => true, :length => { :maximum => 50 }, :uniqueness => {case_sensitive: false}
  validates :contact_email, :length => { :maximum => 50 }, :format => { :with => email_regex }, :allow_blank => true, :allow_nil => true
  validates :sn_twitter_name, :length => { :maximum => 50 }
  validates :nit, :length => { :maximum => 30 }
  validates :description, :length => { :maximum => 2500 }
  validates :url_website,:length => { :maximum => 255 }
  validates :url_facebook, :length => { :maximum => 255 }
  validates :url_twitter, :length => { :maximum => 255 }
  validates :url_youtube, :length => { :maximum => 255 }
  validates :url_linkedin, :length => { :maximum => 255 }
  validates :short_description, :length => { :maximum => 100 }, :allow_blank => true, :allow_nil => true

  mount_uploader :image, BrandUploader
  mount_uploader :image_white_label, BrandImageWhiteLabelUploader
  mount_uploader :image_email, BrandImageEmailUploader
  # mount_uploader :image_email_bin, BrandImageEmailBinUploader
  mount_uploader :qr_code_image, BrandQrCodeImageUploader
  mount_uploader :large_image, BrandLargeImageUploader
  mount_uploader :image_yo_quiero, BrandImageYoQuieroUploader

  has_many :campaigns, :autosave => true
  has_many :campaign_points
  has_many :gift_cards, :through => :campaigns
  has_many :offices
  has_many :gift_codes, :through => :offices
  has_many :free_campaigns, :through => :offices
  #has_many :cities, :through => :offices
  has_and_belongs_to_many :users
  has_one :admin_user
  #belongs_to :manager, :class_name => 'Brand', :foreign_key => 'manager_id'
  has_many :slider_images
  has_many :brand_categories
  has_many :categories, :through => :brand_categories
  has_many :favorites
  has_many :batches, :through => :campaigns
  has_many :redeban_codes, :through => :batches



  scope :ordered_by_favorite_priority_asc, order('brands.favorite_priority ASC')
  scope :ordered_by_name_asc, order('brands.name ASC')

  scope :enabled_for_selling, where("brands.enable_for_selling = :state", :state => true)
  scope :not_enabled_for_selling, where("brands.enable_for_selling = :state", :state => false)

  #miguel segura
  #carga las ciudades de la marcar sin repetir ciudad
  def cities(country_id)
     City.joins(:offices => [:brand]).joins(:state => [:country]).where('brands.id = :brand_id AND offices.enable_for_selling = :enable AND countries.id = :country_id', :brand_id => self.id, :enable => true, :country_id => country_id ).select("cities.name, cities.id").uniq("cities.id")
  end
  #

  def gift_code_number
    GiftCode.joins(:gift_card => {:campaign => :brand}).where('brands.id = :brand_id', :brand_id => self.id).count
  end

  def self.brands_enabled_for_sending_a_gift_code_to_a_city(city)
    #The brands that are shown for sending a gift_code to a city must complete the following requirements:
    #The brand must be enabled_for_selling
    #There must be a campaign enabled in the country of the city
    #The brand must have at least one office enabled_for_selling in the city
    Brand.joins(:campaigns, :offices).where("brands.enable_for_selling = :brand_state and campaigns.campaign_state_id = :campaign_state
                                  and campaigns.country_id = :country_id and offices.enable_for_selling = :office_state
                                  and offices.city_id = :city_id",
                                  :brand_state => true, :campaign_state => CampaignState::STATES[:enable],
                                  :country_id => city.state.country.id, :office_state => true, :city_id => city.id).uniq
  end

  def self.brands_enabled_for_sending_a_gift_code( country_id )
      Brand
      .joins(:campaigns, :gift_cards, :offices)
      .where(
          "
          brands.enable_for_selling = :brand_state and
          campaigns.campaign_state_id = :campaign_state and
          campaigns.country_id = :country_id and
          offices.enable_for_selling = :office_state and
          gift_cards.enable_for_selling = :enabled_for_selling
          ",
          :brand_state => true,
          :campaign_state => CampaignState::STATES[:enable],
          :country_id => country_id,
          :office_state => true,
          :enabled_for_selling => true
      ).uniq
  end

  def self.brands_enabled_for_sending_point_gift_code( country_id )
    Brand
    .joins(:campaign_points)
    .where(
        "
        brands.enable_for_selling = :brand_state and
        campaigns.country_id = :country_id and
        campaigns.enabled = :enabled
        ",
        :brand_state => true,
        :country_id => country_id,
        :enabled => true
    ).uniq
  end

  def self.brand_enable_for_selling( brand_name, country_id )
    Brand
    .joins(:campaigns, :gift_cards, :offices)
    .where(
        "
        brands.name = :brand_name and
        brands.enable_for_selling = :brand_state and
        campaigns.campaign_state_id = :campaign_state and
        campaigns.country_id = :country_id and
        offices.enable_for_selling = :office_state and
        gift_cards.enable_for_selling = :enabled_for_selling
        ",
        :brand_name => brand_name,
        :brand_state => true,
        :campaign_state => CampaignState::STATES[:enable],
        :country_id => country_id,
        :office_state => true,
        :enabled_for_selling => true
    ).first
  end

  #indica si la marca tiene una campaña en el pais y esta activo
  def self.brand_campaign_country?( brand_id, country_id )
    Brand.joins(:campaigns).where("brands.id = #{brand_id} and campaigns.country_id = #{country_id} and campaigns.campaign_state_id = #{CampaignState::STATES[:enable] }").count >= 1 ? true : false
  end
  #--

  def image_for_mobile
    self.image.url(:thumb180).to_s
  end

  def offices_address_comma_separated
    self.offices.collect{ |o| o.office_name + ' ' + o.office_address }.to_sentence(locale: GlobalConstants.I18n_locale )
  end

  def favorite(current_user)
    current_user.favorites.exists? :enabled => true, :brand_id => self.id
  end

  def campaign_enable_for_selling
    begin
      campaign = self.campaigns.where(:campaign_state_id => CampaignState::STATES[:enable]).first
      brand_codes = campaign.brand_codes.find_by_active_for_payment true
      if campaign.gift_cards_enable_for_selling.count > 0 && brand_codes
        campaign
      else
        nil
      end
    rescue
      nil
    end
  end

  def campaigns_convenio_redeban
    self.campaigns.where( 'convenio  IS NOT NULL' ).where( 'redeban', true)
  end

  def exists_convenio_redeban? convenio
    self.campaigns.exists? convenio: convenio, redeban: true
  end

end
