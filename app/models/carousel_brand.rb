# == Schema Information
#
# Table name: carousel_brands
#
#  id         :integer          not null, primary key
#  brand_id   :integer
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CarouselBrand < ActiveRecord::Base
  attr_accessible :brand_id, :position

  validates_presence_of :brand_id
  validates_uniqueness_of :brand_id
  validates_presence_of :position

  belongs_to :brand


end
