class CredibancoFranquiciaPrefix < ActiveRecord::Base

  attr_accessible :bin, :credibanco_franquicia_id, :enabled

  validates :bin, presence: true, uniqueness: true
  validates :credibanco_franquicia_id, presence: true

  belongs_to :credibanco_franquicia

end
