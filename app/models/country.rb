# == Schema Information
#
# Table name: countries
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  bin        :string(255)
#

class Country < ActiveRecord::Base

  attr_accessible :name, :bin

  validates :name, :presence => true, :length => { :maximum => 50 }

  has_many :states
  has_many :cities, :through => :states
  has_many :offices, :through => :cities

  has_many :campaigns

end
