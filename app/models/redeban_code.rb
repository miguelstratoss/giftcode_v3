# == Schema Information
#
# Table name: redeban_codes
#
#  id              :integer          not null, primary key
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  already_used    :boolean
#  batch_id        :integer
#  hash2           :string(255)
#  expiration_date :string(255)
#  code            :string(255)
#  iv              :string(255)
#  salt            :string(255)
#  state_id        :integer
#

class RedebanCode < ActiveRecord::Base

  #
  # Codificación estado del CERTIFICADO ( suministrado por redeban - documento : 'Manual Operativo Generico.docx' )
  #
  # Estado CERTIFICADO	Código
  # Bloqueado	            01
  # Suspendido	          02
  # Desafiliado	          03
  # Vencido	              04
  # Consumido	            05
  # Redimido	            06
  # Habilitado	          07
  # No Habilitado	        08
  #


  STATES = { :bloqueado => 1, :suspendido => 2, :desafiliado => 3, :vencido => 4, :consumido => 5, :redimido => 6, :habilitado => 7, :no_habilitado => 8 }
  STATES_ID = { 1 => 'Bloqueado', 2 => 'Suspendido', 3 => 'Desafiliado', 4 => 'Vencido', 5 => 'Consumido', 6 => 'Redimido',  7 => 'Habilitado', 8 => 'No Habilitado' }
  CHARGE_STATE = { :sin_saldo => 1, :con_saldo => 2, :enviar_redeban => 3, :se_envio_redeban => 4 }
  CHARGE_STATE_ID = { 1 => 'Sin saldo', 2 => 'Con saldo', 3 => 'Enviar a redeban', 4 => 'Se envio a redeban' }
  LOG_ERROR_UPDATE_FILE = { :primero_debe_actualizarse_la_fecha => 0, :bono_ya_habilitado => 1, :bono_bloqueado => 2, :bono_suspendido => 4, :bono_desafiliado => 5, :bono_consumido => 6, :no_se_puede_Reversar => 7, :campo_inválido => 10, :fecha_inválida => 11, :bono_no_existe => 12, :bono_no_pertenece_al_convenio => 13, :cupo_a_reversar_es_mayor_que_el_saldo_actual => 14, :cupo_a_reversar_no_equivale_al_saldo => 15 }
  LOG_ERROR_UPDATE_FILE_ID = { 0 => 'Primero debe actualizarse la fecha', 1 => 'Bono Ya Habilitado', 2 => 'Bono Bloqueado', 4 => 'Bono Suspendido', 5 => 'Bono Desafiliado', 6 => 'Bono Consumido', 7 => 'No se puede Reversar', 10 => 'Campo Inválido', 11 => 'Fecha Inválida', 12 => 'Bono No Existe', 13 => 'El Bono no pertenece al Convenio que se está actualizando', 14 => 'El cupo a reversar es mayor que el saldo actual del bono', 15 => 'El cupo a reversar no equivale al saldo del bono' }
  # LOG_STATE_UPDATE = { :cupo => 1, :fecha => 2, :cupo_fecha => 3, :bloqueo => 4, :habilitacion => 5, :suspender => 6, :desafiliar => 7 }
  # LOG_STATE_UPDATE_ID = { 1 => 'Cupo', 2 => 'Fecha', 3 => 'Cupo y Fecha', 4 => 'Bloqueo', 5 => 'Habilitación', 6 => 'Suspender',  7 => 'Desafiliar' }

  attr_accessible :batch_id,  :state_id, :already_used, :expiration_date, :hash2, :gift_card_id, :charge_state_id, :campaign_id, :log_file_download_id, :log_novelty, :log_value, :log_update_date, :log_consecutive

  validates_presence_of   :batch_id
  validates_uniqueness_of :hash2
  validates_presence_of   :hash2
  validates_presence_of   :state_id
  validates_presence_of   :charge_state_id

  belongs_to :batch
  # delegate :campaign, to: :batch
  belongs_to :campaign
  belongs_to :gift_card
  belongs_to :log_file_download
  has_many :redeban_detailed_movement_certificates, :autosave => true
  has_many :redeban_daily_balances, autosave: true

  has_one :gift_code, foreign_key: 'redemption_pin', primary_key: 'hash2'

  scope :bloqueado, -> { where( state_id: STATES[:bloqueado] ) }
  scope :suspendido, -> { where( state_id: STATES[:suspendido] ) }
  scope :desafiliado, -> { where( state_id: STATES[:desafiliado] ) }
  scope :vencido, -> { where( state_id: STATES[:vencido] ) }
  scope :consumido, -> { where( state_id: STATES[:consumido] ) }
  scope :redimido, -> { where( state_id: STATES[:redimido] ) }
  scope :habilitado, -> { where( state_id: STATES[:habilitado] ) }
  scope :no_habilitado, -> { where( state_id: STATES[:no_habilitado] ) }

  # after_update :notification_brand

  def encryptor(pin)
    if self.new_record?
      unless pin.blank? && !pin.nil?
        self.hash2 = Digest::SHA3.hexdigest(pin)
        self.salt = Time.now.to_i.to_s
        self.iv   = Base64.encode64(  OpenSSL::Cipher::Cipher.new('aes-256-cbc').random_iv ).encode('utf-8')
        self.code = Base64.encode64( Encryptor.encrypt( pin , :key => GlobalConstants::SECRET_KEY, :iv => Base64.decode64(self.iv) , :salt => self.salt ) ).encode('utf-8')
      else
        self.hash2 = nil
      end
    end
  end

  def notification_brand
    campaign = self.campaign
    if campaign.present?
      count = campaign.redeban_codes.where( :already_used =>  true ).count
      send_notification_day = campaign.notices_sent_brands.where( :type_notification => NoticesSentBrand::TYPE[:bonos_redeban_menor_100]).where( "created_at <= ?", Time.zone.now.beginning_of_day ).exists?
      if count < 100 && send_notification_day
        notice = campaign.notices_sent_brands.new
        notice.type_notification = NoticesSentBrand::TYPE[:bonos_redeban_menor_100]
        notice.details = {
            redeban_codes: count
        }
        notice.save
      end
      count = campaign.redeban_codes.where( :already_used =>  true ).where( :updated_at, Time.zone.now..(Time.zone.now + 59.minutes ) ).count
      send_notification_day = campaign.notices_sent_brands.where( :type_notification => NoticesSentBrand::TYPE[:bonos_redeban_agotandose_rapido]).where( "created_at <= ?", Time.zone.now.beginning_of_day ).exists?
      if count > 5 && send_notification_day
        notice = campaign.notices_sent_brands.new
        notice.type_notification = NoticesSentBrand::TYPE[:bonos_redeban_agotandose_rapido]
        notice.details = {
            redeban_codes: count
        }
        notice.save
      end
    end
  end

end
