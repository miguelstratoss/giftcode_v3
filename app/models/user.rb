# encoding: UTF-8
# == Schema Information
#
# Table name: users
#
#  id                      :integer          not null, primary key
#  first_name              :string(255)
#  last_name               :string(255)
#  email                   :string(255)
#  time_zone               :string(255)
#  city_id                 :integer
#  language_id             :integer
#  sex_id                  :integer
#  birthday_date           :date
#  mailing_address         :string(255)
#  mobile_phone            :string(255)
#  office_phone            :string(255)
#  image                   :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  encrypted_password      :string(255)      default(""), not null
#  reset_password_token    :string(255)
#  reset_password_sent_at  :datetime
#  remember_created_at     :datetime
#  sign_in_count           :integer          default(0)
#  current_sign_in_at      :datetime
#  last_sign_in_at         :datetime
#  current_sign_in_ip      :string(255)
#  last_sign_in_ip         :string(255)
#  password_salt           :string(255)
#  confirmation_token      :string(255)
#  confirmed_at            :datetime
#  confirmation_sent_at    :datetime
#  unconfirmed_email       :string(255)
#  failed_attempts         :integer          default(0)
#  unlock_token            :string(255)
#  locked_at               :datetime
#  authentication_token    :string(255)
#  facebook_token          :string(255)
#  facebook_expires_at     :datetime
#  facebook_expires        :boolean
#  show_introduction       :boolean          default(FALSE)
#  sms_authorization       :boolean          default(TRUE)
#  identification          :string(255)
#  user_state_id           :integer          default(1)
#  birthday                :date
#  fbk_user_id             :integer
#  last_fbk_sync_at        :datetime
#  fbk_location_name       :string(255)
#  fbk_timezone            :string(255)
#  fbk_locale              :string(255)
#  referral_id             :integer
#  date_referral           :datetime
#  registration_date       :datetime
#  utm_source              :string(255)
#  utm_medium              :string(255)
#  birthday_confirmation   :boolean
#  city_confirmation       :boolean
#  data_email_confirmation :boolean
#  email_fbk_or_temp       :string(255)
#  token_validate_email    :string(255)
#  email_fbk               :string(255)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and
  # , :registerable, :recoverable, :rememberable, :validatable
  devise :database_authenticatable, :omniauthable, :trackable

  # Setup accessible (or protected) attributes for your model
    attr_accessible :first_name, :last_name, :email, :email_fbk, :email_fbk_or_temp, :time_zone, :image, :remote_image_url, :sex_id, :language_id, :city_id,
                  :mobile_phone, :office_phone, :birthday_date, :mailing_address,
                  :password, :password_confirmation, :remember_me,
                  :facebook_token, :facebook_expires_at, :facebook_expires, :show_introduction, :sms_authorization,
                  :identification, :birthday, :fbk_user_id, :user_state_id, :fbk_location_name, :fbk_locale, :fbk_timezone,
                  :referral_id, :date_referral, :registration_date, :utm_source, :utm_medium,
                  :data_email_confirmation
  #:category_ids

  validates :first_name, :length => { :maximum => 50 }
  validates :last_name, :length => { :maximum => 50 }
  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, :format => { :with => email_regex }, :allow_blank => true, :uniqueness => { :case_sensitive => false }
  validates :email_fbk_or_temp, :format => { :with => email_regex }, :allow_blank => true
  validates :email_fbk, :format => { :with => email_regex }, :allow_blank => true
  validates :mailing_address, :length => { :maximum => 255 }
  validates :mobile_phone, :length => { :maximum => 50 }
  validates :office_phone, :length => { :maximum => 50 }
  validates :user_state_id, :presence => true
  #validates :fbk_user_id, :uniqueness => true

  after_initialize :initialize_default_values

  belongs_to :sex
  belongs_to :language
  belongs_to :city
  belongs_to :referred_by, :class_name => 'User', :foreign_key => 'id'
  has_many :sent_gifts, :class_name => 'GiftCode', :foreign_key => 'from_user_id'
  #has_one :brand_managed, :class_name => 'Brand', :foreign_key => 'manager_id'
  has_many :received_gifts, :class_name => 'GiftCode', :foreign_key => 'to_user_id'
  #has_and_belongs_to_many :categories
  # has_and_belongs_to_many :brands
  has_many :recommendations, :dependent => :destroy
  # has_many :searches, :dependent => :destroy
  has_many :downloads, :dependent => :destroy
  has_many :friendships, :class_name => 'Friendship', :foreign_key => 'user_id', :dependent => :destroy
  #has_many :friends, :through => :friendships, :conditions => "approved = true", :source => :user
  has_many :friends, :through => :friendships, :source => :friend
  has_many :favorites, :dependent => :destroy
  has_many :brands, :through => :favorites, :conditions => { 'favorites.enabled' => true }
  has_many :brand_categories, :through => :brands
  has_many :categories, :through => :brand_categories
  has_many :reverse_friendships, :class_name => 'Friendship', :foreign_key => 'friend_id'
  has_many :invite_friends
  has_many :referrals, :class_name => 'User', :foreign_key => 'referral_id', :primary_key =>  'fbk_user_id', :conditions => 'users.referral_id is not null'
  has_many :logins
  has_many :facebook_walls
  has_many :gift_codes_received, :class_name => 'GiftCode', :foreign_key => 'to_user_id', :conditions => "gift_codes.gift_code_state_id in (#{GiftCodeState::STATES[:payment_accepted]},  #{GiftCodeState::STATES[:free_from_an_office]}, #{GiftCodeState::STATES[:generated_office_confirmed]}, #{GiftCodeState::STATES[:generated_external_web_payment_accepted]}, #{GiftCodeState::STATES[:free_points]}) AND gift_codes.email_sent_to_receiver = #{true}"
  has_many :gift_codes_sent, :class_name => 'GiftCode', :foreign_key => 'from_user_id', :conditions => "gift_codes.gift_code_state_id in (#{GiftCodeState::STATES[:payment_accepted]}, #{GiftCodeState::STATES[:free_from_an_office]}, #{GiftCodeState::STATES[:generated_office_confirmed]}, #{GiftCodeState::STATES[:generated_external_web_payment_accepted]}, #{GiftCodeState::STATES[:free_points]})"
  has_many :gift_codes_outstanding_web, :class_name => 'GiftCode', :foreign_key => 'from_user_id', :conditions => "gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_and_payment_pending]}"
  has_many :point_gift_codes_received, :class_name => 'PointGiftCode', :foreign_key => 'to_user_id'
  has_many :point_gift_codes_sent, :class_name => 'PointGiftCode', :foreign_key => 'from_user_id'
  has_many :notification_users


  belongs_to :user_state

  mount_uploader :image, UserUploader

  scope :ordered_desc_by_creation_date, order('users.created_at DESC')
  scope :ordered_asc_by_first_name, order('users.first_name ASC')
  scope :registered, where("users.user_state_id = :state", :state => UserState::STATES[:registered])
  scope :non_registered, where("users.user_state_id = :state", :state => UserState::STATES[:non_registered])
  scope :referred, where("users.referral_id IS NOT NULL")
  scope :referred_of_yesterday, -> { where( :date_referral => (Time.zone.now.yesterday.beginning_of_day..Time.zone.now.yesterday.end_of_day))}
  scope :without_invite_friends, where("id NOT IN (SELECT user_id FROM invite_friends)")
  scope :without_favorites, where("id NOT IN (SELECT user_id FROM favorites)")
  scope :registered_today, -> { where( :registration_date => (Time.zone.now.beginning_of_day..Time.zone.now) )}

  def initialize_default_values
    if new_record?
      self.birthday_date ||= Date.today
      self.language ||= Language.find_by_name("Español")
      self.city ||= City.find_by_name("Bogotá")
      self.time_zone ||= "Bogota"
    end
  end

  def complete_name
    complete_name = (self.first_name ||= "") + " " + (self.last_name ||= "")
    complete_name == "" ? self.email : complete_name
  end

  def info_for_select
    complete_name + ' ('+email+')'
  end

  def to_s
    email
  end

  def received_gifts_v1
    GiftCode.where(:to_email => self.email )
  end

  def is_a_user_for_demo?
    #if Rails.env.staging?
    #  self.email == 'a.vasquez@greencode.com.co' || self.email == 'mjvc007@hotmail.com'
    #else
    #  false
    #end
    true
  end

  def received_gifts_for_showing
    GiftCode.where(:to_email => self.email ).in_all_accepted_state
  end

  def number_of_received_gifts_for_showing
    received_gifts_for_showing.count
  end

  def sent_gifts_for_showing
    #self.sent_gifts.in_payment_accepted_state
    self.sent_gifts.in_all_accepted_state
  end

  def number_of_sent_gifts_for_showing
    sent_gifts_for_showing.count
  end

  #def green_points_earned
  #  green_points.sum(:points_earned)
  #end

  def get_welcome_word
    if self.sex.present?
      if self.sex.id == Sex::SEXES[:male]
        "Bienvenido"
      else
        "Bienvenida"
      end
    else
      "Bienvenido(a)"
    end
  end

  # METHODS REQUIRED FOR FACEBOOK INTEGRATION

  def graph
    Koala::Facebook::API.new(self.facebook_token)
  end

  def get_fbk_info
    graph.get_object("me", {}, api_version: "v2.3")
  end

  def get_fbk_friends
    graph.get_connections("me", "friends",  "fields"=>"name,birthday,gender,link,picture.type(large),first_name,last_name,location,timezone,locale", api_version: "v2.3")
  end

  def update_fbk_friends
    if self.facebook_expires_at && self.facebook_expires_at >= Time.zone.now
      @profile = get_fbk_info
      @friends =  get_fbk_friends
      puts @friends.inspect
      # puts YAML::dump(@friends)
      created_and_registered = 0
      created_and_non_registered = 0
      non_created = 0
      valid_new_users = 0
      old_records_with_empty_fields_updated = 0

      @friends.each do |f|
        user = User.find_by_fbk_user_id(f['id'])
        #If the user have been created and registered
        if user.present? && user.user_state_id == UserState::STATES[:registered]
          @friendship = Friendship.find_or_initialize_by_user_id_and_friend_id(self.id, user.id)
          if @friendship.new_record?
            @friendship.friendship_state_id = FriendshipState::STATES[:friend]
            @friendship.save
            ## notificacion al amigo que ya esta en giftcode
            begin
              friends = []
              friends.push(self)
              email = GiftCodeNotifier.friends(user, friends )
              email.deliver
            rescue => e
              puts e.inspect
              puts 'Error: No se logro enviar correo al amigo "' + user.id.to_s + '" de ' + self.id.to_s
            end
            ##
          end
          created_and_registered = created_and_registered+1

        #If the user have been created and non-registered
        elsif user.present?
          @friendship = Friendship.find_or_initialize_by_user_id_and_friend_id(self.id, user.id)
          if @friendship.new_record?
            @friendship.friendship_state_id = FriendshipState::STATES[:without_relation]
            @friendship.save
          end
          created_and_non_registered=created_and_non_registered+1

        #If the user have not been created
        else
          birthday=nil
          begin
            birthday = f['birthday'].present? ? Date.strptime("{ #{f['birthday']} }", "{ %m/%d/%Y }") : nil
          rescue
            begin
              birthday = f['birthday'].present? ? Date.strptime("{ #{f['birthday']+'/1800'} }", "{ %m/%d/%Y }") : nil
            rescue
            end
          end


          sex_id = ( f['gender'] == "male") ? Sex.find_by_name( Sex::SEXES[:male] ).id : Sex.find_by_name( Sex::SEXES[:female] ).id


          location_name = f['location'].present? && f['location']['name'].present? ? f['location']['name'] : nil
          locale = f['locale'].present? ? f['locale'] : nil
          timezone = f['timezone'].present? ? f['timezone'] : nil

          user = User.new(:email => f['id']+'@facebook.com', :password => Devise.friendly_token[0,20],
                       :first_name => f['first_name']||="", :last_name => f['last_name']||="",
                       :sex_id => sex_id, :show_introduction => true,
                       :sms_authorization => true, :birthday => birthday, :fbk_user_id => f['id'],
                       :user_state_id => UserState::STATES[:non_registered], :fbk_location_name => location_name,
                       :fbk_locale => locale, :fbk_timezone => timezone)
          if user.valid?
            user.save
            valid_new_users = valid_new_users+1
          end
          @friendship = Friendship.find_or_initialize_by_user_id_and_friend_id(self.id, user.id)
          if @friendship.new_record?
            @friendship.friendship_state_id = FriendshipState::STATES[:without_relation]
            @friendship.save
          end
          non_created = non_created + 1;
        end

        if !user.new_record?
          #For update friends that was created initially without birthday, location_name, locale, timezone
          if user.birthday.nil? && f['birthday'].present?
            begin
              user.birthday = f['birthday'].present? ? Date.strptime("{ #{f['birthday']} }", "{ %m/%d/%Y }") : nil
            rescue
              begin
                user.birthday = f['birthday'].present? ? Date.strptime("{ #{f['birthday']+'/1800'} }", "{ %m/%d/%Y }") : nil
              rescue
              end
            end
          end
          if user.fbk_location_name.nil? && f['location'].present? && f['location']['name'].present?
            user.fbk_location_name = f['location']['name']
          end
          if user.fbk_locale.nil? && f['locale'].present?
            user.fbk_locale = f['locale']
          end
          if user.fbk_timezone.nil? && f['timezone'].present?
            user.fbk_timezone = f['timezone']
          end
          if user.changed?
            user.save
            old_records_with_empty_fields_updated=old_records_with_empty_fields_updated+1
          end
        end
      end

      puts "Already created and registered: "+created_and_registered.to_s
      puts "Already created and non-registered: "+created_and_non_registered.to_s
      puts "Old records with empty fields updated: "+old_records_with_empty_fields_updated.to_s
      puts ""
      puts "Not created: "+non_created.to_s
      puts "Valid new users: "+valid_new_users.to_s
    end
  end

  def verify_fbk_synchronization(force_update = false)
    if self.last_fbk_sync_at.nil? || self.last_fbk_sync_at < 1.months.ago || force_update
      self.delay.update_fbk_friends
      self.last_fbk_sync_at = Time.zone.now
      self.save
      true
    else
      false
    end
  end

  #Only for batch updates from rake task
  def update_user_profile_with_facebook_info(show_exceptions = false)
    puts "ID:"+self.id.to_s if self.id.present?
    puts "Token: "+self.facebook_token.last(6) if self.facebook_token.present?
    if self.facebook_token.present?
      puts "Facebook expires at: "+self.facebook_expires_at.to_s if self.facebook_expires_at.present?
      puts "Time now: "+Time.zone.now.to_s
      #if (self.facebook_expires_at && self.facebook_expires_at >= Time.zone.now)
        begin
          info = get_fbk_info
          self.fbk_user_id = info['id'] if info['id'].present? && self.fbk_user_id.nil?

          if info['birthday'].present? && self.birthday.nil?
            begin
              self.birthday = Date.strptime("{ #{info['birthday']} }", "{ %m/%d/%Y }")
            rescue
              self.birthday = Date.strptime("{ #{info['birthday']+'/1800'} }", "{ %m/%d/%Y }")
            end
          end

          if info['location'].present? && self.fbk_location_name.nil? && info['location']['name'].present?
            self.fbk_location_name = info['location']['name']
          end

          if info['timezone'].present? && self.fbk_timezone.nil?
            self.fbk_timezone = info['timezone']
          end

          if info['locale'].present? && self.fbk_locale.nil?
            self.fbk_locale = info['locale']
          end

          self.valid?
          puts "Error number: "+self.errors.size.to_s
          puts "Errors messages: "+self.errors.messages.to_s
          self.save if self.changed?
          return true
        rescue
          puts $!, $@ if show_exceptions
        end
      #end
    end
    false
  end

  #Only for manual validations
  def self.find_users_with_fbk_user_id_repeated
    User.select("distinct(fbk_user_id) as fbk_user_id, count(fbk_user_id) as times").group("fbk_user_id").having("count(fbk_user_id) >= 2")
  end
  def self.find_users_with_email_repeated
    User.select("distinct(email) as email, count(email) as times").group("email").having("count(email) >= 2")
  end

  #miguel segura
  #creacion usuario basico
  def self.user_new( first_name, last_name, email, sms = true)
    t_user = User.new
    t_user.email = email
    t_user.first_name = first_name
    t_user.last_name = last_name
    t_user.password = Devise.friendly_token[0,20]
    t_user.show_introduction = true
    t_user.sms_authorization = sms
    t_user.user_state_id = UserState::STATES[:non_registered]
    t_user.save
    t_user.email = 'unknown_' + t_user.id.to_s + '@giftcode.co'
    t_user.email_fbk_or_temp = email
    t_user.save
    return t_user
  end

  def self.email_by_day_referrals
    users = User.referred_of_yesterday.group(:referral_id).select(:referral_id)
    users.each do |u|
      user = User.find_by_fbk_user_id_and_user_state_id( u.referral_id, UserState::STATES[:registered] )
      if !user.nil?
        referrals = user.referrals.where("date_referral >= :start and date_referral<= :end", :start => Time.zone.now.yesterday.beginning_of_day, :end => Time.zone.now.yesterday.end_of_day).select([:first_name, :last_name])
        array = Array.new
        referrals.each do |r|
          array << r.name_capitalize
        end
        GiftCodeNotifier.referrals({:count => referrals.count ,:friends =>  array ,:user => user }).deliver
      end
    end
  end

  def self.with_a_minimum_number_of_invite_friends(minimum_number = 1)
    id_of_user_with_the_minimum_number_of_invite_friends = User.joins(:invite_friends).select("users.id").group('users.id, invite_friends.user_id').having('count(invite_friends.user_id) >= :minimum_number', :minimum_number => minimum_number).to_sql
    where("id IN (#{id_of_user_with_the_minimum_number_of_invite_friends})")
  end

  def self.with_a_minimum_number_of_favorites(minimum_number = 1)
    id_of_user_with_the_minimum_number_of_favorites = User.joins(:favorites).select("users.id").group('users.id, favorites.user_id').having('count(favorites.user_id) >= :minimum_number', :minimum_number => minimum_number).to_sql
    where("id IN (#{id_of_user_with_the_minimum_number_of_favorites})")
  end

  def image_for_mobile
    self.image.url(:thumb140).to_s
  end

  def total_points
    total_points_per_registration + total_point_per_favorite_brands + total_points_per_referals
  end

  def total_points_per_registration
    self.user_state_id == UserState::STATES[:registered] ? 30 : 0
  end

  def total_point_per_favorite_brands
    self.brands.count > 0 ? 70 : 0
  end

  def total_points_per_referals
    self.referrals.count * 40
  end

  def points_used
    self.point_gift_codes_sent.sum(:points_used)
  end

  def available_points
    total_points - points_used
  end

  def name_capitalize
    name = []
    self.first_name.split(' ').each{ |s| name.push s.capitalize }
    self.last_name.split(' ').each{ |s| name.push s.capitalize }
    name.join(' ')
  end

  def confirmation_data
    self.birthday_confirmation
  end


  def change_email(new_email)
    if new_email.present? && !new_email.blank?
      if self.email != new_email
        temp_email = self.email
        self.email_fbk_or_temp = self.email
        self.email = new_email
        if self.valid?
          token = UUIDTools::UUID.random_create
          self.token_validate_email = token.hexdigest
          self.email = temp_email
          self.email_fbk_or_temp = new_email
          if self.save
            email = GiftCodeNotifier.confirmation_email(self)
            email.deliver
            'change'
          else
            'unknown'
          end
        else
          unless self.valid?(:email)
            'email'
          else
            'unknown'
          end
        end
      else
        self.data_email_confirmation = true
        if self.save
          true
        else
          puts( self.errors.full_messages.inspect )
          false
        end
      end
    else
      puts 'No llego email'
      false
    end
  end

  def post_facebook_wall

    @link = "#{GlobalConstants::host}/?amigo=#{self.fbk_user_id}"
    puts @link

    begin
       if Rails.env.production?
        fbk = Facebook.new
        graph = fbk.conn(self.facebook_token)
        response = graph.put_wall_post(
            '#YoQuiero un Giftcode',
            {
                'name' => 'Únete y descubre la manera más sencilla de enviar y recibir bonos electrónicos de regalo.',
                'link' => @link,
                'caption' => 'GIFTCODE.',
                #'description' => 'Únete a giftcode, cuentale a tus amigos lo que verdaderamente quieres y gana bonos de regalo de tus marcas favoritas',
                'picture' => 'http://greencode.co/images/imagen_3.jpg'
            },
            self.fbk_user_id
        )
        #el retorno es id de usuario en facebook seguido de un guion y id de post ej 456123_45678987443132
        post_id = response['id'].split('_')
        puts response.inspect
        fw = FacebookWall.new(:user_id => self.id, :fbk_post_id => post_id[1] )
        fw.save
       end
    rescue
      puts "Error #{$!}"
    ensure
    end


  end

end

