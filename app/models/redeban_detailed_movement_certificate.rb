# == Schema Information
#
# Table name: redeban_detailed_movement_certificates
#
#  id                     :integer          not null, primary key
#  file_name              :string(255)
#  nombre_convenio        :string(255)
#  fecha_hora_movimiento  :datetime
#  descripcion_movimiento :string(255)
#  codigo_establecimiento :integer
#  codigo_terminal        :integer
#  tipo_transacion        :string(255)
#  valor_transaccion      :decimal(, )
#  num_referencia         :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  redeban_code_id        :integer
#  error                  :string(255)
#

class RedebanDetailedMovementCertificate < ActiveRecord::Base
  attr_accessible :codigo_establecimiento, :codigo_terminal, :descripcion_movimiento, :fecha_hora_movimiento, :file_name, :nombre_convenio, :num_referencia, :tipo_transacion, :valor_transaccion, :redeban_code_id, :error

  serialize :error, Array

  belongs_to :redeban_code

end
