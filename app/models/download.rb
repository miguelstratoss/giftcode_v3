# encoding: UTF-8
# == Schema Information
#
# Table name: downloads
#
#  id            :integer          not null, primary key
#  gift_code_id  :integer
#  user_id       :integer
#  token         :string(255)
#  view_id       :string(255)
#  download_type :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Download < ActiveRecord::Base
  attr_accessible :token, :view_id, :gift_code_id, :user_id, :download_type

  DOWNLOAD_TYPES = ["Vista SMS", "Imagen PNG Pública", "Imagen PNG Privada"]

  validates :download_type, :inclusion => {:in => Download::DOWNLOAD_TYPES}

  after_create :send_notification_of_first_download_to_buyer

  belongs_to :gift_code
  belongs_to :user

  default_scope :order => 'downloads.created_at DESC'

  def send_notification_of_first_download_to_buyer
    if self.gift_code_id.present? && Download.find_all_by_gift_code_id(self.gift_code_id).count == 1 && !self.gift_code.is_a_free_gift_code?
      GiftCodeNotifier.buyer_gift_code_after_first_download(self.gift_code).deliver
    end
  end
end
