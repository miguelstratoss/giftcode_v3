# == Schema Information
#
# Table name: currencies
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  symbol     :string(255)
#  short_name :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Currency < ActiveRecord::Base
  attr_accessible :name, :symbol, :short_name

  validates :name, :presence => true, :uniqueness => { :case_sensitive => false }, :length => { :maximum => 50 }
  validates :symbol, :presence => true, :length => { :maximum => 10 }
  validates :short_name, :presence => true, :length => { :minimum => 3, :maximum => 3 }

  has_many :campaigns
  has_many :free_campaigns

  def complete_name
    name + " (" + short_name + ")"
  end
end
