# == Schema Information
#
# Table name: gift_code_states
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GiftCodeState < ActiveRecord::Base

  STATES = { :generated_and_payment_pending => 1, :payment_accepted => 2, :payment_rejected => 3, :free_from_an_office => 4, :generated_office_pending => 5, :generated_office_confirmed => 6, :generated_external_web_pending => 7, :generated_external_web_payment_accepted => 8, :generated_external_web_payment_rejected => 9, :free_points => 10 }

  attr_accessible :name

  validates :name, :presence => true, :length => { :maximum => 50 }

  default_scope :order => 'gift_code_states.id ASC'

  has_many :gift_codes

  def self.array_successful
    [STATES[:payment_accepted], STATES[:free_from_an_office], STATES[:generated_office_confirmed], STATES[:generated_external_web_payment_accepted], STATES[:free_points] ]
  end

end
