# == Schema Information
#
# Table name: brand_categories
#
#  id          :integer          not null, primary key
#  brand_id    :integer
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class BrandCategory < ActiveRecord::Base

  attr_accessible :brand_id, :category_id


  validates :brand_id, :presence => true
  validates :category_id, :presence => true
  validates_uniqueness_of :category_id, :scope => :brand_id

  belongs_to :category
  belongs_to :brand

end
