# == Schema Information
#
# Table name: sexes
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Sex < ActiveRecord::Base

  attr_accessible :name

  validates :name, :presence => true, :uniqueness => { :case_sensitive => false }, :length => { :maximum => 50 }

  SEXES = { :male => 'Masculino', :female => 'Femenino', :other => 'Otro' }
end
