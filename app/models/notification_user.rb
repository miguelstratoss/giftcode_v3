# == Schema Information
#
# Table name: notification_users
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name_notification :string(255)
#

class NotificationUser < ActiveRecord::Base
  attr_accessible  :user_id, :name_notification

  validates_presence_of :name_notification
  validates_presence_of :user_id

  belongs_to :user

end
