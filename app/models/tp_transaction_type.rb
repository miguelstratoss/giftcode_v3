# == Schema Information
#
# Table name: tp_transaction_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TpTransactionType < ActiveRecord::Base

  TYPES = { :marketplace_request => 1, :funds_request => 2, :redemption => 3, :pending_redemption => 4, :confirm_redemption => 5, :reverse_redemption => 6 }

  attr_accessible :id, :name

  validates :name, :presence => true, :length => { :maximum => 50 }

  has_many :third_party_transactions

end
