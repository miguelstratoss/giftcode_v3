# == Schema Information
#
# Table name: request_third_party_entities
#
#  id                    :integer          not null, primary key
#  third_party_entity_id :integer
#  gift_code_id          :integer
#  data_sent             :text
#  data_received         :text
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  response_code         :string(255)
#  response_message      :string(255)
#  state                 :string(255)
#

class RequestThirdPartyEntity < ActiveRecord::Base
  attr_accessible :data_received, :data_sent, :gift_code_id, :third_party_entity_id, :response_code, :response_message, :state, :payment_type, :card_type, :card_franchise, :total_value, :approbation_number, :bin

  belongs_to :gift_code
  belongs_to :third_party_entity

end
