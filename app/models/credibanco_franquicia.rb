class CredibancoFranquicia < ActiveRecord::Base

  attr_accessible :codigo_identificacion, :length, :name, :ahorro, :corriente, :credito, :enabled, :identificacion

  validates :name, presence: true
  validates :codigo_identificacion, presence: true, uniqueness: true
  validates :identificacion, presence: true, uniqueness: true
  validates :length, presence: true

  has_many :credibanco_franquicia_prefixes

end
