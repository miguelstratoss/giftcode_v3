# == Schema Information
#
# Table name: friendships
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  friend_id           :integer
#  friendship_state_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Friendship < ActiveRecord::Base

  validates :user_id, :presence => true
  validates :friend_id, :presence => true
  validates :friendship_state_id, :presence => true

  belongs_to :friendship_state
  belongs_to :user
  belongs_to :friend, :class_name => 'User', :foreign_key => 'friend_id'

end
