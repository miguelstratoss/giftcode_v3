# == Schema Information
#
# Table name: logins
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Login < ActiveRecord::Base
  attr_accessible :user_id
  belongs_to :user
end
