//= require jquery
//= require jquery.ui.datepicker
//= require custom.modernizr
//= require foundation
//= require moment
//= require angular
//= require angular-ui
//= require app/main_external
//= require app/config/csrf
//= require app/controllers/third_party_ctrl
//= require app/directives/date
//= require app/directives/hour
//= require app/directives/datepicker

$(document).foundation();


