//= require jquery
//= require custom.modernizr
//= require foundation
//= require mobile
//= require ZeroClipboard.min.js
//= require jquery.ui.datepicker
//= require jquery.ui.autocomplete
//= require angular
//= require angular-resource
//= require angular-ui
//= require angular-file-upload.min
//= require angular-animate
//= require ng-infinite-scroll.min
//= require truncate
//= require moment
//= require app/main
//= require_tree ./app/directives/
//= require_tree ./app/factories/
//= require_tree ./app/controllers/
//= require_tree ./app/config/
//= require_tree ../../../vendor/assets/javascripts/jcarousel/
//= require wizValidation.min


//$(function() {
//    var jcarousel = $('.jcarousel');
//
//           jcarousel
//               .on('jcarousel:reload jcarousel:create', function () {
//                   var width = jcarousel.innerWidth();
//                   if (width >= 750) {
//                       width = width / 5;
//                   } else if (width >= 600) {
//                       width = width / 4;
//                   } else if (width >= 500) {
//                       width = width / 3;
//                   } else if (width >= 300) {
//                       width = width / 2;
//                   }else if (width >= 200) {
//                       width = width / 1;
//                   }
//
//                   jcarousel.jcarousel('items').css('width', width + 'px');
//               })
//               .jcarousel({
//                   wrap: 'circular'
//               });
//
//           $('.jcarousel-control-prev')
//               .jcarouselControl({
//                   target: '-=1'
//               });
//
//           $('.jcarousel-control-next')
//               .jcarouselControl({
//                   target: '+=1'
//               });
//});
