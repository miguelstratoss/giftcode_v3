_app.directive  "hour", [ () ->
  #require moment.js
  henceforward = (value, element )->
    actual = moment().add('hour', 1)
    send_date = $(element).attr('send_date')
    despues = moment(send_date, 'DD/MM/YYYY',true).hour(value)
    before = actual.isBefore( despues , 'hour' )
    if before
      return { type: 'henceforward', success: true }
    else
      if value==undefined #required=='false' || required==false
        return { type: 'henceforward', success: true }
      else
        return { type: 'henceforward', success: false }

  return {
    restrict: 'A',
    require: 'ngModel',
    link: (scope, element, attrs, ngModelCtrl ) ->
      ngModelCtrl.$parsers.unshift( (viewValue)->
#        validity = henceforward(viewValue, $(element).attr('send_date'),$(element).attr('required'))
        validity = henceforward(viewValue, element )
#        validity = henceforward(viewValue, $(element).attr('send_date'), attrs.hour )
#        validity = henceforward(viewValue, $(element).attr('send_date'), ngModelCtrl.$getValidity('required'))
        ngModelCtrl.$setValidity(validity.type, validity.success)
        return viewValue
      )

      ngModelCtrl.$formatters.unshift( (viewValue) ->
#        validity = henceforward(viewValue, $(element).attr('send_date'), $(element).attr('required') )
        validity = henceforward(viewValue, element )
#        validity = henceforward(viewValue, $(element).attr('send_date'), attrs.hour )
#        validity = henceforward(viewValue, $(element).attr('send_date'), ngModelCtrl.$getValidity('required') )
        ngModelCtrl.$setValidity(validity.type, validity.success);
        return viewValue
      )
  }
]
