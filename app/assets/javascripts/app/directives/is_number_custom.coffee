_app.directive 'isNumberCustom', [ () ->

  stringIsNumber = (s)->
    x = +s
    return x.toString() == s


  return {
    restrict: 'A',
    require: 'ngModel',
    link: (scope, element, attrs, ngModelCtrl ) ->
      ngModelCtrl.$parsers.unshift( (viewValue) ->
        if viewValue != ''
          ngModelCtrl.$setValidity(
            'isNumberCustom',
            stringIsNumber(viewValue)
          )
          return viewValue
      )
      ngModelCtrl.$formatters.unshift( (viewValue) ->
        if viewValue != ''
          ngModelCtrl.$setValidity(
            'isNumberCustom',
            stringIsNumber(viewValue)
          )
        return viewValue
      )
  }
]