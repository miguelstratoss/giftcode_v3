_app.directive 'whenScrolled', () ->
  (scope, element, attr) ->
    $(document).bind 'scroll', () ->
      scrollPercentage = (($(window).scrollTop() + $(window).height()) / $(document).height()) * 100
      if (scrollPercentage > 75 && !scope.in_progress && !scope.is_reached_end)
        scope.$apply(attr.whenScrolled)

