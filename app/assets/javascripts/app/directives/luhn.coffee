_app.directive 'luhn', [ () ->

  algorithm = ( value )->
    if (/[^0-9-\s]+/.test(value))
      return false

    nCheck = 0
    nDigit = 0
    bEven = false

    value = value.replace(/\D/g, "")

    for n in [ value.length - 1 ... -1]
      cDigit = value.charAt(n)
      nDigit = parseInt(cDigit, 10)

      if (bEven)
        if ((nDigit *= 2) > 9)
          nDigit -= 9

      nCheck += nDigit
      bEven = !bEven


    return (nCheck % 10) == 0;

  return {
    restrict: 'A',
    require: 'ngModel',
    link: (scope, element, attrs, ngModel) ->

      validate = ( value )->
        ngModel.$setValidity('luhn', algorithm( value ) )
        value

      ngModel.$parsers.push( validate );
      ngModel.$formatters.push( validate );
  }

]