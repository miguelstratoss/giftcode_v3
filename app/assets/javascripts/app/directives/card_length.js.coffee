_app.directive 'cardLength', [ () ->

  length = ( value, size )->
    value.length == size

  return {
  restrict: 'A',
  require: 'ngModel',
  link: ( scope, element, attrs, ngModel ) ->

    validate = ( value )->
      if value
        ngModel.$setValidity('cardLength', length( value, parseInt( $(element).attr('card-length') ) ) )
        value
      else
        ''

    ngModel.$parsers.push( validate );
    ngModel.$formatters.push( validate );
  }

]