_app.directive 'cardPrefix', [ () ->

  prefix = (x, value)->
    parseInt(x) == parseInt(value.slice(0, x.toString().length))

  exist = ( value, prefixes )->
    for x in prefixes
      if prefix x,value
        return true
    false

  return {
    restrict: 'A',
    require: 'ngModel',
    link: ( scope, element, attrs, ngModel ) ->

      validate = ( value )->
        if value
          ngModel.$setValidity('cardPrefix', exist( value, JSON.parse( $(element).attr('card-prefix') )  ) )
          value
        else
          ''

      ngModel.$parsers.push( validate );
      ngModel.$formatters.push( validate );
  }

]