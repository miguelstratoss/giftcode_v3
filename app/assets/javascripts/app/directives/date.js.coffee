_app.directive  "date", [ () ->

  format = (value)->
    if ( moment(value, 'DD/MM/YYYY', true).isValid() )
      return { type: 'date', success: true }
    else
      if value==undefined || value=='' #required=='false' || required==false
        return { type: 'date', success: true }
      else
        return { type: 'date', success: false }

  henceforward = (value)->
    before = moment().isBefore( moment( value,'DD/MM/YYYY') )
    same = moment().isSame( moment( value,'DD/MM/YYYY'), 'day' )
    if same || before
      return { type: 'henceforward', success: true }
    else
      if value==undefined || value=='' #required=='false' || required==false
        return { type: 'henceforward', success: true }
      else
        return { type: 'henceforward', success: false }


  return {
    restrict: 'A',
    require: 'ngModel',
    link: (scope, element, attrs, ngModelCtrl ) ->

      ngModelCtrl.$parsers.unshift( (viewValue = '') ->

        validity = format(viewValue)
        ngModelCtrl.$setValidity(validity.type, validity.success)

        validity = henceforward(viewValue)
        ngModelCtrl.$setValidity(validity.type, validity.success)

        return viewValue
      )

      ngModelCtrl.$formatters.unshift( (viewValue) ->

        validity = format(viewValue)
        ngModelCtrl.$setValidity(validity.type, validity.success)

        validity = henceforward(viewValue)
        ngModelCtrl.$setValidity(validity.type, validity.success)

        return viewValue
      )
  }
]
