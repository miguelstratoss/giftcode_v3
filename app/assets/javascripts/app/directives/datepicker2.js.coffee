_app.directive 'datepicker2', [ ()  ->

  $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: 'anterior',
    nextText: 'siguiente',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  }
  $.datepicker.setDefaults($.datepicker.regional['es'])
  return {
  restrict: 'A',
  require: 'ngModel',
  link: (scope, element, attrs, ngModelCtrl ) ->
    $( ()->
      element.datepicker({
        dateFormat:'yy-mm-dd',
        minDate: 0,
        minDate: moment("1900-01-01").toDate(),
        maxDate: moment("2000-12-31").toDate(),
        changeYear: true,
        changeMonth: true,
        yearRange: "-115:+0",
        onSelect: (date)->
          scope.$apply( () ->
            ngModelCtrl.$setViewValue(date);
          )
      })
    )
  }
]
