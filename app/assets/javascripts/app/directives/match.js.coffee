_app.directive  "match", [ '$parse',( ($parse) ->
  return {
    require: 'ngModel',
    link: (scope, elem, attrs, ctrl) ->
      scope.$watch ()->
        return $parse(attrs.match)(scope) == ctrl.$modelValue
      , (currentValue)->
        ctrl.$setValidity('mismatch', currentValue )
  }
)
]