_app.directive 'autocompleteCity', [ () ->
  return (scope, element, attrs, ctrl) ->
    element.autocomplete({
      minLength:1,
      source: (request, response)->
        $.ajax(
          {
            url: "http://gd.geobytes.com/AutoCompleteCity", #'/search_cities_fbk',#
            dataType: "jsonp",
            data: {
              q: request.term
            },
            success: ( data ) ->
              response( data )
          }
        )
    })
]