_app.directive  "isDate", [ () ->

  return {
  restrict: 'A',
  require: 'ngModel',
  link: (scope, element, attrs, ngModelCtrl ) ->

    ngModelCtrl.$parsers.unshift( (viewValue) ->
      if viewValue != ''
        ngModelCtrl.$setValidity(
          'isDate',
          moment(viewValue, 'YYYY-MM-DD', true).isValid()
        )
      return viewValue
    )

    ngModelCtrl.$formatters.unshift( (viewValue) ->
      if viewValue != ''
        ngModelCtrl.$setValidity(
          'isDate',
          moment(viewValue, 'YYYY-MM-DD', true).isValid()
        )
      return viewValue
    )

  }
]
