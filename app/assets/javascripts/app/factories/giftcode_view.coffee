_app.factory 'GiftcodeView', [ '$rootScope',( $rootScope ) ->

  _payment_credibanco = ( show )->
    $rootScope.$broadcast('GiftcodeView:payment_credibanco', show );

  return {
    payment_credibanco: _payment_credibanco
  }
]
