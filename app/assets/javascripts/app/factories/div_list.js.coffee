_app.factory 'divList', ['$location', '$rootScope', ($location, $rootScope) ->

  $rootScope.rootForm = null

  return {
    preload: ( div_id, li_id, list_length, position ) ->
      if $( div_id ) && $rootScope.rootForm == null
        $rootScope.rootForm = angular.element( $( div_id ) )

      top = $( li_id + position ).offset().top
      $(li_id + position).addClass('active')
      i =  position + 1
      run = true
      if i >= list_length
        i--
      while run && $( li_id + i ).offset().top == top
        if i >= list_length
          run = false
        i++
      i--

      $( li_id + i).after( $rootScope.rootForm )
      $( div_id ).css('display', 'block')
    ,
    open: ( li_id, position ) ->
      angular.element("body,html").animate({scrollTop: angular.element( li_id + position ).offset().top + angular.element( li_id + position ).height() }, "slow")
      return true
    ,
    close: ( div_id, li_id,  position, force = false )->
      unless position == 0
        unless $( li_id + position).length == 0
          if force
            $("body,html").animate( { scrollTop: $( li_id + position ).offset().top - 70 }, "toggle")
          $(div_id).css('display', 'none')
          $(li_id + position).removeClass('active')
  }
]