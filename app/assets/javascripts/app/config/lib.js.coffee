_app.run ['$rootScope','facebook',  ($rootScope, facebook) ->

  $rootScope.alert = false

  $rootScope.EMAIL_REGEXP = /^((?!\.)[a-z0-9._%+-]+(?!\.)\w)@[a-z0-9-\.]+\.[a-z.]{1,5}(?!\.)\w$/i
  $rootScope.DATE_FORMAT_SERVER = /^\d{4}-\d{2}-\d{2}$/

  $(document).foundation()
  $rootScope.client = new ZeroClipboard( document.getElementById("copy-button"), {
    moviePath: "/ZeroClipboard.swf"
  } )

  $("#alert").foundation('reveal', 'open')

  $rootScope.login_facebook = () ->
    facebook.login()
  $rootScope.login = () ->
    $('#modal_login').foundation('reveal', 'open')
  $rootScope.close_login = () ->
    $('#modal_login').foundation('reveal', 'close')
  $rootScope.close_contact = () ->
    $('#modal_contact').foundation('reveal', 'close')
  $rootScope.open_contact = () ->
    $rootScope.$apply $('#modal_contact').foundation('reveal', 'open')

  $rootScope.image = ''

  $(document).ready( ()->
    if $( window ).height() > 720
      $('.landing_column').height( $( window ).height() )
  )

]