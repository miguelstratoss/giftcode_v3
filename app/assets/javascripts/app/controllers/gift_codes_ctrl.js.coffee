_app.controller "GiftCodesCtrl",[ '$scope', '$http', 'divList', ($scope, $http, $divList ) ->
  $scope.gift_code_id = 0
  $scope.gift_codes = []
  $scope._gift_code = []

  $('.show-hide').removeClass('show-hide')

  $scope.load_gift_codes_received = ->
    $http( url: '/json/gift_codes', method: "GET")
    .success( (data) ->
        $scope.gift_codes = data
      )
    .error( (data) ->
        $scope.gift_codes = []
      )

  $scope.load_gift_codes_sent = ->
    $http( url: '/json/gift_codes_sent', method: "GET")
    .success( (data) ->
        $scope.gift_codes = data
      )
    .error( (data) ->
        $scope.gift_codes = []
      )

  $scope.load_gift_code = ( ref_sale, gift_code_id )->
    $scope.close()
    $scope.gift_code_id = gift_code_id
    $divList.preload "#gift_code", "#gift_code_", $scope.gift_codes.length, $scope.gift_code_id
    $http( url: '/json/gift_code', method: "POST", data: { ref_sale: ref_sale } )
    .success( (data) ->
      $scope._gift_code = data
      $divList.open "#gift_code_", $scope.gift_code_id
    )
    .error( (data) ->
      $scope._gift_code = []
    )

  $scope.load_gift_code_sent = ( ref_sale, gift_code_id )->
    $scope.close()
    $scope.gift_code_id = gift_code_id
    $divList.preload "#gift_code", "#gift_code_", $scope.gift_codes.length, $scope.gift_code_id
    $http( url: '/json/gift_code_sent', method: "POST", data: { ref_sale: ref_sale } )
    .success( (data) ->
        $scope._gift_code = data
        $divList.open "#gift_code_", $scope.gift_code_id
      )
    .error( (data) ->
        $scope._gift_code = []
      )

  $scope.close = () ->
    $divList.close "#gift_code", "#gift_code_", $scope.gift_code_id
    $scope._gift_code = []
]