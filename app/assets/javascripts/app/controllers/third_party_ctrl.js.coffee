_app.controller 'ThirdPartyCtrl', [ '$scope', '$http', ($scope, $http)->

    $scope.button = ''
    $scope.ref_sale = ''
    $scope.total_value = ''

    $scope.send=(model, form)->
       $scope.submitted = true
       $scope.datetime = moment( model.send_date, 'DD/MM/YYYY',true).hour( model.send_time ).format('DD/MM/YYYY h:mm a')
       if form.$valid
         $scope.preload = true
         $http(
           {
             method : 'POST',
             url : '/embebed_giftcode_create',
             data :{
               'gift_code': {
                 'from_first_name': model.from_first_name,
                 'from_last_name': model.from_last_name,
                 'from_email': model.from_email,
                 'to_first_name': model.to_first_name,
                 'to_last_name': model.to_last_name ,
                 'to_mobile_phone': model .to_mobile_phone ,
                 'to_email': model.to_email,
                 'to_email_confirmation': model.to_email_confirmation,
                 'send_notifications_now': model.send_notifications_now,
                 'send_date_time': moment( model.send_date, 'DD/MM/YYYY',true).hour( model.send_time ).format(),
                 'to_city_id': model.to_city_id,
                 'gift_card_id': model.gift_card_id,
                 'body_message': model.body_message
               }
             }
           }
         )
         .success(
             (data) ->
               if data.flash.type == 'success'
                 $scope.button = data.button
                 $scope.ref_sale = data.ref_sale
                 $scope._gift_code = data.gift_code
                 $scope.preload = false
                 $("#confirmation").foundation('reveal', 'open');
               else
                 $scope.success=false
                 $scope.error=true
                 $scope.preload = false
                 $scope.button = ''
                 $scope.ref_sale = ''

              window.location = "#message"
           )
         .error(
             (data)->
               $scope.success=false
               $scope.error=true
               $scope.preload = false
               window.location = "#message"
           )
       else
         $scope.success=false
         $scope.error=true
         window.location = "#message"


    $scope.close = () ->
      $('#confirmation').foundation('reveal', 'close')


    $scope.$watch 'giftcode.send_date', () ->
      $scope.giftcode.send_time = ""
      $('#send_time').trigger('change', true)


    $scope.calculate_cost = ( element ) ->
      if undefined != element
        for key, value of $scope.total
          if ''+value.id == ''+element
            $scope.total_value = value.total
      else
        $scope.total_value = ''


    $scope.onclick_location = ( url ) ->
      window.location.href = url



]