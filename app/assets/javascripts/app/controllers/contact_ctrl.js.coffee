_app.controller "ContactCtrl",[ '$scope', '$http', ($scope, $http ) ->

  $scope.success = false
  $scope.alert = false
  $scope.preload = false
  $scope.mail_sent=false
  $scope.msg_sending = "Enviar ahora"

  $scope.send = (form,model) ->

    console.log(model)
    if form.$valid
      $scope.msg_sending = "Enviando ..."
      $scope.preload = true
      $http.post( $scope.url , model)
      .success( (data) ->
          if data.flash.type == "success"
            $scope.msg_sending = "Mensaje Enviado"
            $scope.success = true
            $scope.alert = false
            form.email.$dirty = false
            form.message.$dirty = false
            $scope.contact = []
            $('#modal_contact').foundation('reveal', 'close')
          else
            $scope.alert = true
            $scope.success = false
            $scope.msg_sending = "Error Enviando"
          $(window).scrollTop( $( '#contact' ).offset().top )
          $scope.preload = false
      )
      .error( (data) ->
          $scope.alert = true
          $scope.success = false
          $scope.preload = false
          $scope.msg_sending = "Error Enviando"
          $(window).scrollTop( $( '#contact' ).offset().top )
      )
    else
      $scope.submitted = true

  $scope.cancel = () ->
    $scope.submitted = false
    $scope.contact = { name: '', email: '', phone: '', company: '', message: '' }

]