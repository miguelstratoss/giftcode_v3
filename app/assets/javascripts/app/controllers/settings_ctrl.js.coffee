_app.controller "SettingsCtrl",[ '$scope', '$http', '$upload', ($scope, $http, $upload ) ->

  $scope.file = []
  $scope.submitted = false
  $scope.success = false
  $scope.alert = false
  $scope.preload = false
  $scope.update = false

  $('.show-hide').removeClass('show-hide')

  $scope.onFileSelect = ($files) ->
    $scope.file = $files[0]

  $scope.save = ( perfil, form ) ->
    $scope.submitted = true
    if form.$valid
      $scope.preload = true
      $scope.submitted = true
      $(window).scrollTop( $( '#form_for' ).offset().top )
      $upload.upload(
        {
          method: 'PUT'
          url: $scope.url,
          data: perfil,
          file: $scope.file
        }
      ).success( (data) ->
        $scope.message_text = data.flash.text
        $scope.message(data.flash.type)
        $scope.image = data.profile.image.thumb140.url
        console.log $scope.image
        $scope.update = true
        if data.redirect_to != ''
          window.location = data.redirect_to 
      ).error( (data,  status) ->
        $scope.message('error')
      )


  $scope.cancel = ->
    location.reload()

  $scope.message = (type) ->
    if type == 'success'
      #$location.hash('success-box')
      #$anchorScroll(true)
      $scope.success = true
      $scope.alert = false
    else
      #$location.hash('alert-box')
      #$anchorScroll(true)
      $scope.alert = true
      $scope.success = false
    $(window).scrollTop( $( '#settings' ).offset().top )
    $scope.preload = false
]