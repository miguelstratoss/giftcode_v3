_app.controller 'CredibancoGatewayCtrl' , [ '$scope', '$http', 'GiftcodeView', ($scope, $http, GiftcodeView ) ->

  $scope.form_payment_sending = false
  $scope.preload = false

  $scope.tipos_cuenta = {
    "03": "Ahorros",
    "04": "Corriente",
    "01": "Crédito"
  }

  $scope.franquicia = {
    "VISA":       "Visa",
    "DINERS":     "Diners Club",
    "AMEX":       "American Express",
    "CREDENCIAL": "Credencial",
    "MASTERCARD": "Master Card"
  }

  $scope.$on( 'GiftcodeView:payment_credibanco',
    ( even, response )->
      $scope.data_bono = response
  )

  $scope.payment_sent = ( form, payment )->
    $scope.preload = true
    $scope.form_payment_sending = true
    if form.$valid
      initDFP($scope.gift_code.fingerprint)
      payment.fingerprint = $scope.gift_code.fingerprint
      setTimeout(()->
        $http({
          method : 'PUT',
          url : '/payments/' + $scope.ref_sale,
          data :{
            'payment': payment
          }
        })
        .success(
          (data) ->
            console.log data
#            if data == "true" || data == true
            window.location.href = '/payments/confirmation/' + $scope.ref_sale
#            else
            console.log data
        )
        .error(
          (data) ->
            $scope.preload = false
            console.log data
            alert('Error desconocido, intentalo de nuevo.')
            $scope.close( true )
        )
      ,3000)
    else
      console.log 'formulario con errores'
      $scope.preload = false


  $scope.close = ( data ) ->
    GiftcodeView.payment_credibanco( true )

]