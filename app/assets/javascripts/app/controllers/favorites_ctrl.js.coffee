_app.controller "FavoritesCtrl",[ '$scope', '$http', '$filter','divList', 'facebook', ( $scope, $http, $filter, $divList, facebook ) ->

  $scope.favorites = []
  $scope.count = []
  $scope.class = ""
  $scope.color = "color:green"
  $scope.result = []
  $scope.categories = []
  $scope.favorites = []
  $scope.alert = false

  $('.show-hide').removeClass('show-hide')


  $scope.load_favorites = ->
    if $scope.type != 'friend'
      url = '/json/marcas-favoritas'
    else
      url = '/json/marcas-favoritas-amigo/' + $scope.friend

    $http( url: url, method: "GET")
    .success( (data) ->
        $scope.favorites = data
      )
    .error( (data) ->
      )

  $scope.active = ( _class, i ) ->
    $divList.preload( "#list_brands_" + i , "#category_", 12, i )
    $divList.open "#category_", i
    $scope.class = _class

  $scope.close = ->
    $scope.class = ""


  $scope.update = (brand_id)->
    $http( url: '/mi-marca-favorita/'+brand_id+'/actualizar', method: "PUT")
    .success( (data) ->
        for i in [0...$scope.count.length]
          $scope.count[i] = 0
        for favorite in data.count_favorites then do (favorite) =>
          $scope.count[favorite.id] = favorite.count
        for key of data.favorites
          $scope.favorites[key]= data.favorites[key] || false
    )
    .error( (data) ->

     )

  $scope.click = (brand_id)->
    $scope.favorites[brand_id] = !$scope.favorites[brand_id]
    $scope.update(brand_id)

  $scope.$watch 'search', ( _new, _old) ->
    if $scope.type != 'friend'
      $scope.result = []
      for brand in $filter('filter')($scope.brands,{name:_new}) then do (brand) =>
        $scope.result[brand.id] = true


  $scope.buscar = () ->
    $scope.not_found = false
    if this.search
      categories = $filter('filter')($scope.categories, this.search )
      if categories.length > 0 && this.search.length > 0
        $scope.active categories[0]._class, categories[0].position
      else
        $scope.close()
        $scope.not_found = true
        $http( url: '/favorites/suggest', method: "POST", data:  { name: this.search } )


  $scope.next = () ->
    count = 0
    for key of $scope.favorites
      if $scope.favorites[key]
        count++
    if count > 0
      $scope.preload = true
      $(window).scrollTop( $( '#favorites' ).offset().top )
      $http( url: '/compartir-en-facebook', method: "POST").success(
        (data)->
#          window.location = "/puntos"
          window.location = "/regalar-bono"
      ).error(
        (data)->
          window.location = "/regalar-bono"
#          window.location = "/puntos"
      )
    else
      $(window).scrollTop( $( '#favorites' ).offset().top )
      $scope.alert = true

  $scope.show_confirmation_email = true
  $scope.error_confirmation_email = false
  $scope.change_confirmation_email = false
  $scope.sent_data_email = false
  $scope.confirmation_email = (form) ->
    $scope.show_confirmation_email = true
    $scope.error_confirmation_email = false
    $scope.sent_data_email = true
    if form.$valid
      $http( url: '/confirmation_email', method: "PUT", data: { email: $scope.email })
      .success( (data) ->
        $scope.sent_data_email = false
        if data == true || data == 'true'
          $scope.show_confirmation_email = false
          $scope.error_confirmation_email = false
        else
          if data == 'change'
           $scope.change_confirmation_email = true
           $scope.show_confirmation_email = false
           $scope.error_confirmation_email = false
           $scope.msg_confirmation_email = 'Recibirá un correo electrónico con instrucciones sobre como validar el nuevo correo electrónico.'
          else
            $scope.error_confirmation_email = true
            $(window).scrollTop( $( '#error_confirmation' ).offset().top )
            if data == 'email'
              $scope.msg_confirmation_email = 'Correo electronico ya esta en uso o invalido.'
            else
              $scope.msg_confirmation_email = 'Error desconocido al validar email.'
      )
      .error( (data) ->
        $scope.error_confirmation_email = true
        $scope.sent_data_email = false
        $(window).scrollTop( $( '#error_confirmation' ).offset().top )
        $scope.msg_confirmation_email = 'Error en confirmación del email, por favor intentar mas tarde.'
      )

  $scope.show_confirmation_birthday = true
  $scope.error_confirmation_birthday = false
  $scope.confirmation_birthday = (form) ->
    if form.$valid
      console.log $scope.birthday
      $http( url: '/confirmation_birthday', method: "PUT", data: { birthday: $scope.birthday })
      .success( (data) ->
        $scope.sent_data_email = false
        if data == true || data == 'true'
          $scope.show_confirmation_birthday = false
          $scope.error_confirmation_birthday = false
        else
          $scope.error_confirmation_birthday = true
          $(window).scrollTop( $( '#error_confirmation' ).offset().top )
          if data == 'birthday'
            $scope.msg_confirmation_birthday = 'Fecha incorrecta.'
          else
            $scope.msg_confirmation_birthday = 'Error desconocido al validar fecha.'
      )
      .error( (data) ->
        $scope.error_confirmation_birthday = true
        $scope.sent_data_birthday = false
        $(window).scrollTop( $( '#error_confirmation' ).offset().top )
        $scope.msg_confirmation_birthday = 'Error en confirmación de fecha, por favor intentar mas tarde.'
      )


  $scope.show_confirmation_city = true
  $scope.error_confirmation_city = false
  $scope.confirmation_city = (form) ->
    if form.$valid
      $http( url: '/confirmation_city', method: "PUT", data: { city: $scope.city })
      .success( (data) ->
        $scope.sent_data_city = false
        if data == true || data == 'true'
          $scope.show_confirmation_city = false
          $scope.error_confirmation_city = false
        else
          $scope.error_confirmation_city = true
          $(window).scrollTop( $( '#error_confirmation' ).offset().top )
          if data == 'city'
            $scope.msg_confirmation_city = 'Campo incorrecta.'
          else
            $scope.msg_confirmation_city = 'Error desconocido al validar campo.'
      )
      .error( (data) ->
        $scope.error_confirmation_city = true
        $scope.sent_data_city = false
        $(window).scrollTop( $( '#error_confirmation' ).offset().top )
        $scope.msg_confirmation_city = 'Error en nombre de la ciudad, por favor intentar mas tarde.'
      )

]

