_app.controller "PointsCtrl",[ '$scope', '$http', 'divList', ($scope, $http, $divList ) ->

  $scope.there_campaigns = false
  $scope.campaigns = []
  $scope.backup_campaigns = []
  $scope.form_campaign = []
  $scope.friends = []
  $scope.my = []
  $scope.id = 0
  $scope.numero = 0
  $scope.data = []
  $scope.search = []
  $scope.preload_modal = false
  $scope.payment_disable = false

  $scope.load_campaigns_points = ->
    $scope.there_campaigns = false
    $scope.campaigns = []
    $http( url: '/services/points/campaigns', method: "GET")
    .success( (data) ->
      $('.show-hide').removeClass('show-hide')
      $scope.campaigns = data
      if $($scope.campaigns).length > 0
        $scope.there_campaigns = true
        $scope.backup_campaigns = $scope.campaigns
      else

    )
    .error( (data) ->

    )

  $scope.load_friends = ->
    $http( url: '/json/amigos', method: "GET")
    .success( (data) ->
      $scope.friends = data
    )
    .error( (data) ->
      $scope.friends = []
    )

  $scope.form = ( params )->
    $scope.close()
    if $scope.points >= params.campaign.points_required
      $scope.id = params.id
      $divList.preload "#form", "#campaign_", $scope.campaigns.length, $scope.id
      $scope.form_campaign = params.campaign
      $divList.open "#campaign_", $scope.id
    else
      console.log $scope.points
      console.log params.campaign.points_required


  $scope.close = ( force = false ) ->
    $divList.close "#form", "#campaign_", $scope.id, force
    $scope.reset_gift_code()
    $scope.data.to_first_name.$dirty = false #brands#friends
    $scope.data.to_last_name.$dirty = false #brands#friends
    $scope.data.to_mobile_phone.$dirty = false #brands#friends
    $scope.data.to_email.$dirty = false #brands#friends
    $scope.data.to_email_confirmation.$dirty = false #brands#friends
    $scope.data.body_message.$dirty = false #brands#friends
    $scope.data.send_date.$dirty = false #brands#friends
    $scope.data.send_time.$dirty = false #brands#friends
    $scope.alert = false
    $('#modal_gift_code').foundation('reveal', 'close')


  $scope.reset_gift_code =  ->
    $scope.gift_code.to_user_id = ""
    $scope.gift_code.to_first_name = ""
    $scope.gift_code.to_last_name = ""
    $scope.gift_code.to_mobile_phone = ""
    $scope.gift_code.to_email = ""
    $scope.gift_code.to_email_confirmation = ""
    $scope.gift_code.body_message = ""
    $scope.gift_code.cost_current = ""
    $scope.gift_code.currency_short_name = ""
    $scope.gift_code.total_current = ""
    $scope.gift_code.send_date = ""
    $scope.gift_code.send_time = ""
    $scope.gift_code.send_notifications_now = true
    $scope.search.complete_name = ""


  $scope.load_friend = ( friend )->
    $scope.search.complete_name = friend.complete_name
    $scope.gift_code.to_first_name = friend.first_name
    $scope.gift_code.to_last_name = friend.last_name
    $scope.gift_code.to_email = friend.email
    $scope.gift_code.to_email_confirmation = friend.email
    $scope.gift_code.to_user_id = friend.id
    $scope.gift_code.to_mobile_phone = friend.mobile_phone


  $scope.next = ( form ) ->
    if form.$valid
      $('#modal_gift_code').foundation('reveal', 'open')
    else
      $scope.submitted = true

  $scope.end_up = ()  ->
    $http({
      method : 'POST',
      url : '/services/points/campaigns/' + $scope.form_campaign.id + '/gift_codes/create',
      data :{
        'gift_code': {
          'to_first_name': $scope.gift_code.to_first_name,
          'to_last_name': $scope.gift_code.to_last_name ,
          'to_mobile_phone': $scope.gift_code.to_mobile_phone ,
          'to_email': $scope.gift_code.to_email,
          'to_email_confirmation': $scope.gift_code.to_email_confirmation,
          'send_notifications_now': $scope.gift_code.send_notifications_now,
          'send_date_time': moment($scope.gift_code.send_date, 'DD/MM/YYYY',true).hour($scope.gift_code.send_time).format(),
#          'to_city_id': $scope.gift_code.to_city_id,
          'body_message': $scope.gift_code.body_message
        }
      }
    })
    .success( (data) ->
      if data.flash.type == "success"
        window.location = "/puntos/giftcode/" + data.ref_sale
      else
        $scope.close()
        $scope.message = data.flash.text
        $scope.alert = true
        $("body,html").animate( { scrollTop: 400 }, "slow" )
        $scope.preload_modal = false
        $scope.payment_disable = false

    )
    .error( (data) ->
      $scope.close()
      $scope.message = "Error con el servidor, por favor intentalo mas tarde."
      $("body,html").animate( { scrollTop: 400 }, "slow" )
      $scope.alert = true
      $scope.preload_modal = false
      $scope.payment_disable = false
    )

  $scope.refresh_form = ->
    $scope.reset_gift_code()
    $scope.select_hour()

  $scope.select_city = ->
#    $('#to_city_id').trigger('change', true)

  $scope.select_hour = ->
#    $('#send_time').trigger('change', true)

  $scope.$watch 'gift_code.send_date', () ->
    $scope.gift_code.send_time = ""
#    $scope.select_hour()

  $scope.close_confirmation = () ->
    $('#modal_gift_code').foundation('reveal', 'close')

  $scope.open_pop_up = () ->
    $("#points_pop_up").foundation('reveal', 'open')

  $scope.close_pop_up = () ->
    $("#points_pop_up").foundation('reveal', 'close')

  $scope.load_my_info = ()->
    $scope.gift_code.to_first_name = $scope.my.first_name
    $scope.gift_code.to_last_name = $scope.my.last_name
    $scope.gift_code.to_email = $scope.my.email
    $scope.gift_code.to_email_confirmation = $scope.my.email
    $scope.gift_code.to_mobile_phone = $scope.my.mobile_phone

  $scope.client.on( 'load', ( client ) ->
    client.on( "complete",  (client, args)->
      $("#copy-text").animate({ opacity:'0.5'},100)
      $("#copy-text").animate({ opacity:'1'},100)
    )
  )

]