# #ActiveAdmin::Dashboards.build do
ActiveAdmin.register_page 'Dashboard', :namespace => :office_admin do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  controller do

    def index
      @pin = pin_format params[:redemption_pin]
      @currency = Currency.all
      @enabled = init
      if @enabled && request.post?
        if params[:redimir]
          @redemption = executeRedemption( @pin, params[:value], params[:currency], request.remote_ip )
          if @redemption[:status_id] == '1'
            flash[:success] = @redemption[:status_id].to_s + ' - ' + @redemption[:message]
            redirect_to office_admin_third_party_transaction_path @redemption[:giftcode_transaction_id].to_i
          else
            flash[:error] = @redemption[:status_id].to_s + ' - ' + @redemption[:message]
          end
        end
        if params[:redemption_pin]
          @data = fundsRequest @pin, request.remote_ip
          if @data[:status_id] == '3'
            @enabled = false
            @message = @data[:message]
          end
        end
      end
    end

    def executeRedemption( pin, value, currency, ip)
      key= "#{@brand}|0|#{pin}|#{value}|#{currency}|#{@commerce_id}|#{@transDate}||||#{@secret}"
      firm = Digest::MD5.hexdigest(key)
      tp = ThirdParty.new
      return tp.redemption({ commerceId: @commerce_id, brandCode: @brand, termId: '', pin: pin, transactionId: 0, value: value, currency: currency, transDate: @transDate, firm: firm, office: @office_id }, ip)
    end

    def fundsRequest(pin = nil, ip )
      if pin
        key = "#{@brand}|#{pin.to_s}|#{@commerce_id}|#{@transDate}||||#{@secret}"
        firm = Digest::MD5.hexdigest(key)
        tp = ThirdParty.new
        data = tp.get_balance( { commerceId: @commerce_id, brandCode: @brand, pin: pin, transDate: @transDate, firm: firm, office: @office_id }, ip )
        data[:pin] = pin
        return data
      end
    end

    def init
      office = current_admin_user.office
      campaigns = Campaign.exists? brand_id:office.brand_id, office_redeem_bono: true
      @redeem = false
      retornar = false
      if campaigns
        if @pin
          gift_code  = GiftCode.where(brand_id: current_admin_user.brand_id ).where("(gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]} ) AND gift_codes.redemption_pin='#{Digest::SHA3.hexdigest(@pin)}'").first
          if gift_code &&  gift_code.campaign.office_redeem_bono == true
            brand_code = gift_code.campaign.brand_codes.includes(:third_party_entity).find_by_active_for_payment true
            tpe = brand_code.third_party_entity
            @secret = tpe.secret
            @commerce_id = tpe.incocredito_id
            @brand = brand_code.code
            @office_id = office.id
            @transDate = Time.zone.now.strftime("%Y%m%d%H%M%S")
            params[:gift_code_id] = gift_code.id
          else
            flash[:warning] = 'Bono invalido'
            params[:redemption_pin] = nil
            puts '[warning] Bono invalido'
          end
        end
        @redeem = true
        retornar = true
      else
        flash[:warning] = 'No se encontraron campañas activas para redención desde la oficina.'
        puts '[warning] No se encontraron campañas activas para redención desde la oficina.'
      end
      return retornar
    end

    def pin_format( pin = nil )
      if pin
        _pin = pin.gsub('-','')
        _pin = _pin.gsub(' ','')
        return _pin
      else
        return pin
      end
    end
  end


  content :title => proc{ I18n.t("active_admin.dashboard") } do

    unless params[:redemption_pin] && request.post?
      render 'gift_code_third_party_transaction'
    end


    if params[:redemption_pin] && request.post?
      gift_code = GiftCode.find_by_id params[:gift_code_id]
      panel 'Datos del giftcode: [' + gift_code.id.to_s + "] " + gift_code.identification_number.to_s do
        render 'gift_code'
        render 'form_redimir'
      end
    end

    if params[:redemption_pin] && request.post?

      panel "Transacciones del Bono" do
        div do
          table_for ThirdPartyTransaction.where( tp_transaction_type_id: TpTransactionType::TYPES[:redemption], tp_transaction_state_id: TpTransactionState::STATES[:success], gift_code_id: gift_code.id ).order("created_at DESC") do |t|
            t.column :id
            t.column :transaction_value do |third_party_transaction|
              number_to_currency(third_party_transaction.transaction_value, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
            end
            t.column :new_balance do |third_party_transaction|
              number_to_currency(third_party_transaction.new_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
            end
            t.column('approbation_number') do |third_party_transaction|
              if third_party_transaction.movements_gift_code
                third_party_transaction.movements_gift_code.id
              end
            end
            t.column :office
            t.column :created_at
          end
        end
      end
    else
      panel "Transacciones Recientes en Esta Sede de la Marca" do
        div do
          table_for current_admin_user.office_third_party_transactions.where(tp_transaction_type_id: TpTransactionType::TYPES[:redemption], tp_transaction_state_id: TpTransactionState::STATES[:success] ).order("created_at DESC").limit(30)do |t|
            t.column :id
            t.column :gift_code_id
            t.column 'identification number' do |tpt|
              (tpt.gift_code_id) ? tpt.gift_code.identification_number : ''
            end
            t.column :transaction_value do |third_party_transaction|
              number_to_currency(third_party_transaction.transaction_value, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
            end
            t.column :new_balance do |third_party_transaction|
              number_to_currency(third_party_transaction.new_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
            end
            t.column('approbation number') do |third_party_transaction|
              if third_party_transaction.movements_gift_code
                third_party_transaction.movements_gift_code.id
              end
            end
            t.column :created_at
          end
        end
      end
    end

    panel "Soporte/Ayuda" do
      ul do
            li "Para soporte o ayuda puede comunicarse al email contacto@greencode.com.co"
      end
    end

  end

end
