ActiveAdmin.register ThirdPartyTransaction, :namespace=>:office_admin do

  config.clear_action_items!
  actions :index, :show, :new, :create

  scope_to :current_admin_user, :association_method => :office_third_party_transactions

  # filter :brand_code_campaign_name, :label => 'Campaign', :as => :string ,:collection => proc { ThirdPartyTransaction.campaign.where( :name => Thread.current[:campaign_name] ).active }
  filter :tp_transaction_state
  filter :tp_transaction_type
  # filter :third_party_entity_id
  filter :gift_card_id
  # filter :transaction_value
  # filter :transaction_currency
  # filter :new_balance
  # filter :received_hash
  # filter :reversed
  # filter :confirmed
  # filter :office_id
  # filter :related_transaction_id
  filter :created_at
  # filter :updated_at

  controller do

    before_filter :only => :index do
      @per_page = 30
    end

    def new
      if params[:ref_sale].present?
        @gift_code = GiftCode.find_by_ref_sale(params[:ref_sale])
        @transaction = ThirdPartyTransaction.new
      else
        flash[:error]="Debe ingresar primero el contenido del QR Code o el PIN del GiftCode."
        redirect_to office_admin_dashboard_path
      end
    end

    def create
      transDate = Time.zone.now.strftime("%Y%m%d%H%M%S")
    end

  end

  form :partial => "form"

  index( download_links: false ) do
    column :id
    column :gift_card_id
    column :gift_code_id
    column 'identification_number',  sortable: 'third_party_transactions.gift_code_id' do |tpt|
      (tpt.gift_code_id) ? tpt.gift_code.identification_number : ''
    end
    column :transaction_value,  sortable: 'third_party_transactions.transaction_value' do |third_party_transaction|
      number_to_currency(third_party_transaction.transaction_value, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
    end
    # column :transaction_currency
    column :new_balance, sortable: 'third_party_transaction.new_balance'  do |third_party_transaction|
      number_to_currency(third_party_transaction.new_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)
    end
    column 'approbation_number' do |third_party_transaction|
      if third_party_transaction.movements_gift_code
        third_party_transaction.movements_gift_code.id
      end
    end
    column :tp_transaction_type_id, :sortable => 'third_party_transactions.tp_transaction_type_id' do |tpt|
      (tpt.tp_transaction_type_id) ? tpt.tp_transaction_type.name : ''
    end
    column :tp_transaction_state_id, :sortable => 'third_party_transactions.tp_transaction_state_id' do |tpt|
      (tpt.tp_transaction_state_id) ? tpt.tp_transaction_state.name : ''
    end
    column :created_at
  end

  show do |third_party_transaction|
    div class: 'panel' do
      h3 'Transacción'
      div class: 'attributes_table' do
        table do
          tr do
            th 'Numero de aprobación'
            td third_party_transaction.movements_gift_code.id, style: "background-color: #2f761d;color: aliceblue; font-size: 100px; padding:40px 10px;text-align:center;"
          end
          tr do
            th 'Valor transacción'
            td number_to_currency(third_party_transaction.transaction_value, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),  style: "font-size: 60px; padding:40px 10px;text-align:right;"
          end
          tr do
            th 'Nuevo balance'
            td number_to_currency(third_party_transaction.new_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en),  style: "font-size: 60px; padding:40px 10px;text-align:right;"
          end
          tr do
            th 'Fecha'
            td I18n.l( third_party_transaction.created_at, :format => :admin_short),  style: "font-size: 20px; padding:20px 10px;text-align:right;"
          end
        end
      end
    end
  end
  action_item only: [:show] do
    button_to( 'Atras', office_admin_root_path )
  end
end