module PaymentsHelper

  def button_pay(gift_code)
    campaign = gift_code.gift_card.campaign
    @brand_codes = campaign.brand_codes.find_by_active_for_payment true
    bt = "No tiene medio de pago"
    if !@brand_codes.nil?
      case @brand_codes.third_party_entity.name.to_s
        when ThirdPartyEntity::ENTITY[:redeban]
          bt = redeban(gift_code)
      end
    end
    return bt
  end

  private
    def redeban(gift_code)
      if @brand_codes.terminal && @brand_codes.code && @brand_codes.type_terminal
        return "<a class='button pre continue' href='#{ payments_path(:ref_sale => gift_code.ref_sale)}'>Pagar</a>".html_safe
      else
        return "No se obtiene información de la campaña para la entidad"
      end
    end
end