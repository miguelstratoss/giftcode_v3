module SettingsHelper
  def is_selected_as_settings_tab (tab)
    session[:settings_tab] and session[:settings_tab] == tab
  end
end
