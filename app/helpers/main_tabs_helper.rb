module MainTabsHelper
  def is_selected_as_my_gifts_tab (tab)
    session[:my_gifts_tab] and session[:my_gifts_tab] == tab
  end
end
