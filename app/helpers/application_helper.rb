# encoding: UTF-8
module ApplicationHelper
  def title
    base_title = "GiftCode"
    if @title.nil?
      base_title
    else
      "#{@title} | GiftCode"
    end
  end

  def keywords
    base_keywords = "GiftCode, bono de regalo, regalos virtuales, bono de regalo electrónico, qr code, giftcards en colombia"
    if @keywords.nil?
      base_keywords
    else
      "#{@keywords}"
    end
  end

  def description
    base_description = "¿No sabes qué regalar? GiftCode es la manera más fácil y segura de sorprender a tus amigos con bonus de regalo electrónicos de sus marcas favoritas."
    if @description.nil?
      base_description
    else
      "#{@description}"
    end
  end

  def logo
    image_tag("new_design_2/gift.png", :alt => "Sample App", :class => "round")
  end

  def is_selected_as_main_tab (tab)
    session[:main_tab] and session[:main_tab] == tab
  end

  def normalize_text(text)
    text.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n,'').to_s
  end

  def gift_code_received_link_with_popover(gift_code, placement = "right")
    link_to 'Ver más', "#", :class => 'btn', :rel => "popover", :tabindex => "-1", :title => 'GiftCode Recibido',  "data-content" => content_for_gift_code_received_for_popover(gift_code), "data-placement" => placement
  end

  def content_for_gift_code_received_for_popover(gift_code)
    "Enviado por: <strong> #{gift_code.from_user.complete_name}</strong><br>"+
    "Mensaje: <strong> <br>#{gift_code.body_message}</strong><br>"+
    "Recibido en el email: <strong> #{gift_code.from_user.email}</strong><br>"+
    "Recibido en el teléfono: <strong> #{gift_code.to_mobile_phone}</strong><br>"+
    "Valor: <strong>#{number_to_currency(gift_code.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)}</strong><br>"+
    "Saldo disponible: <strong> #{gift_code.currency.short_name} #{number_to_currency(gift_code.available_balance, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)}</strong><br>"+
    "Recibido el: <strong> #{ l gift_code.created_at, :format => :short }</strong><br>"+
    "Válido hasta: <strong> #{ l gift_code.valid_until, :format => :short }</strong><br>"+
    "A ser reclamado en la ciudad de: <br><strong> #{gift_code.to_city.complete_name}</strong><br>"+
    "PIN: <strong>#{gift_code.redemption_pin_formatted}</strong><br>"
  end

  def gift_code_sent_link_with_popover(gift_code, placement = "right")
    link_to 'Ver más', "#", :class => 'btn', :rel => "popover", :tabindex => "-1", :title => 'GiftCode Enviado',  "data-content" => content_for_gift_code_sent_for_popover(gift_code), "data-placement" => placement
  end

  def content_for_gift_code_sent_for_popover(gift_code)
    "Enviado a: <strong> #{gift_code.to_user_name}</strong><br>"+
    "Mensaje: <strong> <br>#{gift_code.body_message}</strong><br>"+
    "Enviado al email: <strong> #{gift_code.to_email}</strong><br>"+
    "Enviado al teléfono: <strong> #{gift_code.to_mobile_phone}</strong><br>"+
    "Valor: <strong> #{gift_code.currency.short_name} #{number_to_currency(gift_code.cost, :separator => ",", :delimiter => ".", :precision => 0, :locale => :en)}</strong><br>"+
    "Enviado el: <strong> #{ l gift_code.created_at, :format => :short }</strong><br>"+
    "Válido hasta: <strong> #{ l gift_code.valid_until, :format => :short }</strong><br>"+
    "Enviado a la ciudad de: <br><strong> #{gift_code.to_city.complete_name}</strong><br>"
  end
end
