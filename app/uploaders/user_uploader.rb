# encoding: utf-8

class UserUploader < CarrierWave::Uploader::Base
  include Sprockets::Helpers::RailsHelper
  include Sprockets::Helpers::IsolatedHelper

  # Include RMagick or MiniMagick support:
  #include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  # storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  def default_url
    # https://graph.facebook.com/{{friend.fbk_user_id}}/picture?width=195&height=195
    # asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
    case version_name.to_s
      when 'thumb30'
        "https://graph.facebook.com/#{model.fbk_user_id}/picture?width=30&height=30"
      when 'thumb40'
        "https://graph.facebook.com/#{model.fbk_user_id}/picture?width=40&height=40"
      when 'thumb50'
        "https://graph.facebook.com/#{model.fbk_user_id}/picture?width=50&height=50"
      when 'thumb60'
        "https://graph.facebook.com/#{model.fbk_user_id}/picture?width=60&height=60"
      when 'thumb80'
        "https://graph.facebook.com/#{model.fbk_user_id}/picture?width=80&height=80"
      when 'thumb140'
        "https://graph.facebook.com/#{model.fbk_user_id}/picture?width=140&height=140"
      when 'thumb195'
        "https://graph.facebook.com/#{model.fbk_user_id}/picture?width=195&height=195"
      else
        "https://graph.facebook.com/#{model.fbk_user_id}/picture"
    end

  end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :scale => [50, 50]
  # end


  version :thumb30 do
    process :resize_to_fit => [30, 30]
  end

  version :thumb40 do
    process :resize_to_fit => [40, 40]
  end

  version :thumb50 do
    process :resize_to_fit => [50, 50]
  end

  version :thumb60 do
    process :resize_to_fit => [60, 60]
  end

  version :thumb80 do
    process :resize_to_fit => [80, 80]
  end

  version :thumb140 do
    process :resize_to_fit => [140, 140]
  end

  version :thumb195 do
    process :resize_to_fit => [195, 195]
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end
