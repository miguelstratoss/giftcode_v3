require 'savon'

class SMSManager
  attr_reader :service_url, :mobile_phone, :message, :token

  def initialize( mobile_phone, message)
    @service_url = "http://sendmt.123celular.com/gatewaymts/gatewaysendMTv2.php?wsdl"
    @token = "marvasg"
    @mobile_phone = mobile_phone
    @message = message
  end

  def send_sms
    client = Savon::Client.new @service_url

    #client.wsdl.soap_actions

    response = client.request :send_mt do
      soap.body = { :MSISDN1 => mobile_phone, :texto1 => message, :cliente1 => token }
    end

    #response.to_hash
  end
end