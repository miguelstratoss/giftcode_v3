class BrandServices


  def get_brand_payments (giftcode_commerce_id, incocredito, passphrase, init_date, end_date, dir_ip)
    if !ThirdPartyBlackList.find_by_blocked_and_dir_ip(true, dir_ip)
      entity = ThirdPartyEntity.find_by_incocredito_id_and_secret(giftcode_commerce_id, passphrase)
      if entity
        brand_code = BrandCode.find_by_code_and_third_party_entity_id(incocredito, entity.id)
        if brand_code
          brand = brand_code.campaign.brand.name
          end_date = end_date + ' 23:59:59'
          request = RequestThirdPartyEntity.find_by_sql("SELECT gift_code_id, created_at, payment_type, card_type, card_franchise, total_value, approbation_number, bin
          FROM request_third_party_entities
          WHERE state = 'Aprobada' and created_at between '" +init_date+ "' and '" +end_date+ "' and approbation_number is not null")
          if request
            LogRequestBrandPayment.create(:incocredito => incocredito, :init_date => init_date, :end_date => end_date, :dir_ip => dir_ip, :state => 'success')
            return request
          else
            LogRequestBrandPayment.create(:incocredito => incocredito, :init_date => init_date, :end_date => end_date, :dir_ip => dir_ip, :state => 'no_data_was_found')
            return { :status_id => 2, :status => "no_data_was_found", :message => "No se encontraron registros para el rango de fechas" }
          end
        else
          LogRequestBrandPayment.create(:incocredito => incocredito, :init_date => init_date, :end_date => end_date, :dir_ip => dir_ip, :state => 'wrong_credentials')
          verify_black_list(dir_ip)
          return { :status_id => 3, :status => "no_brand_found", :message => "La marca no se encontró" }
        end
      else
        LogRequestBrandPayment.create(:incocredito => incocredito, :init_date => init_date, :end_date => end_date, :dir_ip => dir_ip, :state => 'wrong_credentials')
        verify_black_list(dir_ip)
        return { :status_id => 4, :status => "wrong_credentials", :message => "Credenciales inválidas, contacte con el adinistrador" }
      end
    else
      LogRequestBrandPayment.create(:incocredito => incocredito, :init_date => init_date, :end_date => end_date, :dir_ip => dir_ip, :state => 'ip_has_been_bloqued')
      return { :status_id => 5, :status => "ip_has_been_bloqued", :message => "Su dirección IP ha sido bloqueada, Contacte con el administrador " }
    end
  end



  private
  # Controla el uso de la fuerza bruta para decifrar las credenciales de autenticación
  def verify_black_list(dir_ip)
    failed_transactions = LogRequestBrandPayment.where("created_at BETWEEN ? AND ? AND dir_ip = ? AND state = 'wrong_credentials' ", Time.now - 1.hours, Time.now, dir_ip).count
    if failed_transactions > 30
      black_list = ThirdPartyBlackList.find_by_dir_ip(dir_ip)
      if black_list
        black_list.blocked=true
      else
        black_list = ThirdPartyBlackList.new
        black_list.dir_ip=dir_ip
        black_list.blocked=true
      end
      black_list.save
      false
    else
      true
    end
  end
end