require "rqrcode_png"
require "fog"

class GCQRCode

  SIZE_MODULES = [0, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57]
  MAXIMUM_CHARACTERS_BY_SIZE_MODULES = [0, 7, 14, 24, 34, 44, 58, 64, 84, 98, 119]
  NUMBER_OF_WHITE_MODULES_FOR_BORDER = 3

  PIXELS_PER_MODULE = 4
  NUMBER_OF_PIXELS_TO_AVOID_IMAGE_DEGRADATION = 2

  attr_reader :qr_code_size, :image_size, :ecc_level, :content, :modules, :qr_code_image_url, :from_thumb_image#, :main_path, :complete_qr_file_path

  # Method used to manipulate QR codes in memory
  # content -> String. The content (String) to be incorporated into the QR code
  # qr_code_image_url -> String. The URL of the image brand (This image should be of the same size of @image_size)
  # qr_code_size -> Integer. The size (version) of the QR code. Version 1 has 21 modules, Version 2 has 25 modules, etc. See SIZE_MODULES constant
  # qr_code_ecc_level -> String. The error correction rate of the QR code ('l', 'm', 'q', 'h')
  # from_thumb_image -> If the QR code will be created from a thumb version of an image
  def initialize( content, qr_code_image_url, qr_code_size, qr_code_ecc_level, from_thumb_image)
    #Begin: parameters that may be changed
    @content = content
    @qr_code_image_url = qr_code_image_url
    @qr_code_size = qr_code_size
    @ecc_level = qr_code_ecc_level
    @from_thumb_image = from_thumb_image
    #End: parameters that may be changed

    @modules = SIZE_MODULES[@qr_code_size]
    @image_size = @modules*PIXELS_PER_MODULE
    #@main_path = "./public/images/qr_codes/"
    #@complete_qr_file_path = @main_path + file_name.to_s + ".png"
  end

  def generate_inline_qr_code_image_bw
    qr = RQRCode::QRCode.new( @content, :size => @qr_code_size, :level => @ecc_level )
    png = qr.to_img # returns an instance of ChunkyPNG
    png = png.resize(@image_size, @image_size)
    #png.save( @complete_qr_file_path )
    png
  end


  def generate_inline_qr_code_image_color
    qr = RQRCode::QRCode.new( @content, :size => @qr_code_size, :level => @ecc_level )
    if Rails.env.production? && @from_thumb_image
      connection = Fog::Storage.new({
                                        :provider                 => 'AWS',
                                        :aws_secret_access_key    => 'gKyc40wHWLU2nUqpmBNJuhsTAc/JwvMzsNBcicqd',
                                        :aws_access_key_id        => 'AKIAJUXFDIGW65P5VUBQ'
                                    })
      directory = connection.directories.get('giftcodeprov2')
      file = directory.files.get( @qr_code_image_url )
      image = ChunkyPNG::Image.from_blob( file.body )
    elsif Rails.env.staging? && @from_thumb_image
      connection = Fog::Storage.new({
                                        :provider                 => 'AWS',
                                        :aws_secret_access_key    => 'gKyc40wHWLU2nUqpmBNJuhsTAc/JwvMzsNBcicqd',
                                        :aws_access_key_id        => 'AKIAJUXFDIGW65P5VUBQ'
                                    })
      directory = connection.directories.get('giftcodeprov2staging')
      file = directory.files.get( @qr_code_image_url )
      image = ChunkyPNG::Image.from_blob( file.body )
    elsif Rails.env.prestaging? && @from_thumb_image
      connection = Fog::Storage.new({
                                        :provider                 => 'AWS',
                                        :aws_secret_access_key    => 'gKyc40wHWLU2nUqpmBNJuhsTAc/JwvMzsNBcicqd',
                                        :aws_access_key_id        => 'AKIAJUXFDIGW65P5VUBQ'
                                    })
      directory = connection.directories.get('giftcodeprov2prestaging')
      file = directory.files.get( @qr_code_image_url )
      image = ChunkyPNG::Image.from_blob( file.body )
    else
      image = ChunkyPNG::Image.from_file( @qr_code_image_url )
    end



    white_color = ChunkyPNG::Color.rgb(255, 255, 255)

    qr.modules.each_index do |x|
      qr.modules.each_index do |y|
        if !qr.dark?(x,y)
          xx = y*PIXELS_PER_MODULE
          yy = x*PIXELS_PER_MODULE

          #To put all MANDATORY white pixels
          ((xx+NUMBER_OF_PIXELS_TO_AVOID_IMAGE_DEGRADATION)..(xx+PIXELS_PER_MODULE-1)).each do |sub_xx|
            ((yy+NUMBER_OF_PIXELS_TO_AVOID_IMAGE_DEGRADATION)..(yy+PIXELS_PER_MODULE-1)).each do |sub_yy|
              #puts "SubX:"+sub_xx.to_s+" SubY:"+sub_yy.to_s
              image[sub_xx,sub_yy] = white_color
            end
          end

          if( !@fill_border || (x<8 && y<8) || (x<8 && y>=(@modules-8)) || (x>=(@modules-8) && y<8 ) )

            (0..(NUMBER_OF_PIXELS_TO_AVOID_IMAGE_DEGRADATION-1)).each do |pixel_without_degradation|
              #For every column without blank
              ((yy+pixel_without_degradation)..(yy+PIXELS_PER_MODULE-1)).each do |sub_yy|
                #puts "X:"+xx.to_s+" SubY:"+sub_yy.to_s
                image[xx+pixel_without_degradation,sub_yy] = white_color
              end
            end

            (0..(NUMBER_OF_PIXELS_TO_AVOID_IMAGE_DEGRADATION-1)).each do |pixel_without_degradation|
              #For every row without blank
              ((xx+0)..(xx+PIXELS_PER_MODULE-1)).each do |sub_xx|
                #puts "SubX:"+sub_xx.to_s+" Y:"+yy.to_s
                image[sub_xx,yy+pixel_without_degradation] = white_color
              end
            end

          end
        end
      end
    end

    # qr.modules.each_index do |x|
    #   qr.modules.each_index do |y|
    #     if !qr.dark?(x,y)
    #       xx = y*4
    #       yy = x*4
    #       image[xx+1,yy+1] = white_color
    #       image[xx+1,yy+2] = white_color
    #       image[xx+1,yy+3] = white_color
    #       image[xx+2,yy+1] = white_color
    #       image[xx+2,yy+2] = white_color
    #       image[xx+2,yy+3] = white_color
    #       image[xx+3,yy+1] = white_color
    #       image[xx+3,yy+2] = white_color
    #       image[xx+3,yy+3] = white_color
    #       if( (x<8 && y<8) || (x<8 && y>=(@modules-8)) || (x>=(@modules-8) && y<8 ) )
    #         image[xx,yy] = white_color
    #         image[xx,yy+1] = white_color
    #         image[xx,yy+2] = white_color
    #         image[xx,yy+3] = white_color
    #         image[xx+1,yy] = white_color
    #         image[xx+2,yy] = white_color
    #         image[xx+3,yy] = white_color
    #       end
    #     end
    #   end
    # end
    image
  end

  def generate_inline_qr_code_image_color_with_white_border
    image = generate_inline_qr_code_image_color

    #Begin the addition of white spaces
    total_white_pixel_number = NUMBER_OF_WHITE_MODULES_FOR_BORDER*4*2
    side_white_pixel_number = total_white_pixel_number/2

    width_with_spaces = image.dimension.width + total_white_pixel_number
    height_with_spaces = image.dimension.height + total_white_pixel_number

    image_with_spaces = ChunkyPNG::Image.new(width_with_spaces, height_with_spaces, ChunkyPNG::Color::TRANSPARENT)

    (0..width_with_spaces-1).each do |x|
      (0..height_with_spaces-1).each do |y|
        if x<side_white_pixel_number || y<side_white_pixel_number || x>=(width_with_spaces-side_white_pixel_number) || y>=(height_with_spaces-side_white_pixel_number)
          image_with_spaces[x,y] = ChunkyPNG::Color.rgb(255, 255, 255)
        else
          image_with_spaces[x,y] = image[x-side_white_pixel_number, y-side_white_pixel_number]
        end
      end
    end
    #End the addition of white spaces
    image_with_spaces
  end

end