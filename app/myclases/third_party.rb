class ThirdParty

  def redemption ( params = Array.new, remote_ip )
    transaction = ThirdPartyTransaction.new
    transaction.received_hash=params.inspect.to_s.gsub(', "controller"=>"third_party", "action"=>"redemption"','')  # Registro del Hash
    transaction.tp_transaction_type_id = TpTransactionType::TYPES[:redemption]  # Registra tipo de transacción
    transaction.dir_ip = remote_ip.to_s # Registra dirección IP del server
    transaction.dir_ip_client = params[:ipClient] # Registra dirección IP del cliente
    transaction.tp_transaction_state_id = TpTransactionState::STATES[:unknown_error]
    transaction.office_id = params[:office]
    transaction.terminal_id = params[:termId]
    transaction.input_type_id = params[:inputType]

    if verify_black_list (transaction.dir_ip)
      if verify_black_list_client (transaction.dir_ip_client)
        third=ThirdPartyEntity.find_by_incocredito_id(params[:commerceId])
        if third
          transaction.third_party_entity_id = third.id   # Registra id de tercero
          brand_code=BrandCode.find_by_third_party_entity_id_and_code(third.id,params[:brandCode])
          if brand_code
            transaction.brand_code_id = brand_code.id  # Registra código de la marca del tercero
            campaign_id = brand_code.campaign_id
            if params[:pin]
              pin=params[:pin]
            end
            if params[:giftcodeNumber]
              pin=params[:giftcodeNumber]
            end
            @gift_code3 = GiftCode.where("(gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]} ) AND gift_codes.redemption_pin='#{Digest::SHA3.hexdigest(pin)}'").first
            unless @gift_code3
              @gift_code3 = PointGiftCode.where("gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:free_points]} AND gift_codes.redemption_pin='#{Digest::SHA3.hexdigest(pin)}'").first
            end
            if @gift_code3
              @gift_code = GiftCode.joins(:gift_card).where("(gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]} ) AND gift_cards.campaign_id = #{campaign_id} AND gift_codes.redemption_pin='#{Digest::SHA3.hexdigest(pin)}'").first
              unless @gift_code
                @gift_code = PointGiftCode.where("gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:free_points]} AND gift_codes.redemption_pin='#{Digest::SHA3.hexdigest(pin)}'").first
              end
              if @gift_code
                transaction.gift_code_id = @gift_code.id  # Registra id de giftcode
                key = brand_code.code.to_s+'|'+params[:transactionId].to_s+'|'+pin.to_s+'|'+params[:value].to_s+'|'+params[:currency].to_s+'|'+third.incocredito_id.to_s+'|'+params[:transDate].to_s+'||||'+third.secret.to_s
                firm = Digest::MD5.hexdigest(key)
                registration = ThirdPartyRegistration.new
                registration.brand_code = brand_code
                registration.received_date = params[:transDate]
                registration.received_sign = params[:firm]
                if firm == params[:firm] && registration.save
                  if @gift_code.valid_until.past?
                    transaction.tp_transaction_state_id=TpTransactionState::STATES[:giftcard_expired]
                    json = { :status_id => TpTransactionState::STATES[:giftcard_expired].to_s, :status => "giftcard_expired", :message => "El bono ha expirado", :expiration_date =>  @gift_code.valid_until.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                  else
                    if @gift_code.available_balance.to_i == 0
                      transaction.tp_transaction_state_id=TpTransactionState::STATES[:no_funds]
                      json = { :status_id => TpTransactionState::STATES[:no_funds].to_s, :status => "no funds", :message => "El saldo del bono es igual a cero", :balance => @gift_code.available_balance.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                    else
                      transaction.transaction_value = params[:value].to_f   # Registra valor de la transacción
                      transaction.transaction_currency = params[:currency].to_s   # Registra Moneda Utilizada
                      if Float(params[:value])
                        new_balance = (@gift_code.available_balance.to_f - params[:value].to_f).round(2)
                        if new_balance < 0
                          transaction.tp_transaction_state_id=TpTransactionState::STATES[:insufficient_funds]
                          json = { :status_id => TpTransactionState::STATES[:insufficient_funds].to_s, :status => "insufficient funds", :message => "El saldo del bono es insuficiente, cambie el valor a redimir", :balance => @gift_code.available_balance.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                        else
                          @gift_code2 = GiftCode.find(@gift_code.id)
                          @gift_code2.available_balance = new_balance
                          if @gift_code2.save
                            transaction.new_balance = new_balance    # Registra Nuevo Saldo
                            transaction.tp_transaction_state_id=TpTransactionState::STATES[:success]
                            count = ThirdPartyTransaction.where('created_at >= ?',Time.zone.now.beginning_of_day).where(:brand_code_id => brand_code.id).count
                            transaction.approbation_number = count+1
                          else
                            transaction.tp_transaction_state_id=TpTransactionState::STATES[:could_not_save]
                            json = { :status_id => TpTransactionState::STATES[:could_not_save].to_s, :status => "could_not_save", :message => "La transaccion no pudo ser procesada- contacte con el administrador", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                          end
                        end
                      else
                        transaction.tp_transaction_state_id=TpTransactionState::STATES[:value_format_error]
                        json = { :status_id => TpTransactionState::STATES[:value_format_error].to_s, :status => "value format error", :message => "El valor debe ser num&eacute;rico (999999999.99)", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                      end
                    end
                  end
                else
                  transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_sign]
                  json = { :status_id => TpTransactionState::STATES[:wrong_sign].to_s, :status => "wrong sign", :message => "La firma emitida no coincide - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                end
              else
                # transaction.tp_transaction_state_id=TpTransactionState::STATES[:giftcard_not_found]
                # json = { :status_id => TpTransactionState::STATES[:giftcard_not_found].to_s, :status => "giftcard not found", :message => "El bono no se encontro en el sistema giftcode", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                transaction.tp_transaction_state_id=TpTransactionState::STATES[:gitcard_not_belongs_to_brand]
                json = { :status_id => TpTransactionState::STATES[:gitcard_not_belongs_to_brand].to_s, :status => "gitcard_not_belongs_to_brand", :message => "El bono no pertenece a la marca", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
              end
            else
              # transaction.tp_transaction_state_id=TpTransactionState::STATES[:gitcard_not_belongs_to_brand]
              # json = { :status_id => TpTransactionState::STATES[:gitcard_not_belongs_to_brand].to_s, :status => "gitcard_not_belongs_to_brand", :message => "El bono no pertenece a la marca", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }

              transaction.tp_transaction_state_id=TpTransactionState::STATES[:giftcard_not_found]
              json = { :status_id => TpTransactionState::STATES[:giftcard_not_found].to_s, :status => "giftcard not found", :message => "El bono no se encontro en el sistema giftcode", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
            end
          else
            transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_brand]
            json = { :status_id => TpTransactionState::STATES[:wrong_credentials_brand].to_s, :status => "wrong credentials - brand", :message => "Codigo de marca invalida - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
          end
        else
          transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_commerce_id]
          json = { :status_id => TpTransactionState::STATES[:wrong_credentials_commerce_id].to_s, :status => "wrong credentials - commerce_id", :message => "Codigo de comercio invalido - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
        end
      else
        transaction.tp_transaction_state_id=TpTransactionState::STATES[:ip_client_blocked]
        json = { :status_id => TpTransactionState::STATES[:ip_client_blocked].to_s, :status => "ip_client_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
      end
    else
      transaction.tp_transaction_state_id=TpTransactionState::STATES[:dir_ip_blocked]
      json = { :status_id => TpTransactionState::STATES[:dir_ip_blocked].to_s, :status => "ip_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
    end

  ensure
    if transaction.save
      if transaction.tp_transaction_state_id == TpTransactionState::STATES[:success]
        json = { :status_id => TpTransactionState::STATES[:success].to_s, :status => "success", :message => "Transacción Exitosa", :new_balance => new_balance.to_s, :giftcode_transaction_id => transaction.id, :approbation_number => transaction.approbation_number , :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
      end
    end
    return json
  end

  # Confirmation Needed
  def pending_redemption( params = Array.new, remote_ip )
    transaction = ThirdPartyTransaction.new
    transaction.received_hash=params.inspect.to_s.gsub(', "controller"=>"third_party", "action"=>"pending_redemption"','')  # Registro del Hash
    puts transaction.received_hash
    transaction.tp_transaction_type_id = TpTransactionType::TYPES[:pending_redemption]  # Registra tipo de transacción
    transaction.dir_ip = remote_ip.to_s # Registra dirección IP del cliente
    transaction.tp_transaction_state_id = TpTransactionState::STATES[:unknown_error]
    transaction.terminal_id = params[:termId]
    transaction.input_type_id = params[:inputType]
    if verify_black_list (transaction.dir_ip)
      if verify_black_list_client (transaction.dir_ip_client)
        third=ThirdPartyEntity.find_by_incocredito_id(params[:commerceId])
        if third
          transaction.third_party_entity_id = third.id   # Registra id de tercero
          brand_code=BrandCode.find_by_third_party_entity_id_and_code(third.id,params[:brandCode])
          if brand_code
            transaction.brand_code_id = brand_code.id  # Registra código de la marca del tercero
            campaign_id = brand_code.campaign_id
            if params[:pin]
              pin=params[:pin]
            end
            if params[:giftcodeNumber]
              pin=params[:giftcodeNumber]
            end
            @gift_code = GiftCode.joins(:gift_card).where("(gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]}  OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]}) AND gift_cards.campaign_id = #{campaign_id} AND gift_codes.redemption_pin='#{Digest::SHA3.hexdigest(pin)}'").first
            unless @gift_code
              @gift_code = PointGiftCode.where("gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:free_points]} AND gift_codes.redemption_pin='#{Digest::SHA3.hexdigest(pin)}'").first
            end
            if @gift_code
              transaction.gift_code_id = @gift_code.id  # Registra id de giftcode
              key = brand_code.code.to_s+'|'+params[:transactionId].to_s+'|'+pin.to_s+'|'+params[:value].to_s+'|'+params[:currency].to_s+'|'+third.incocredito_id.to_s+'|'+params[:transDate].to_s+'||||'+third.secret.to_s
              firm = Digest::MD5.hexdigest(key)
              registration = ThirdPartyRegistration.new
              registration.brand_code = brand_code
              registration.received_date = params[:transDate]
              registration.received_sign = params[:firm]
              if firm == params[:firm] && registration.save
                if @gift_code.valid_until.past?
                  transaction.tp_transaction_state_id=TpTransactionState::STATES[:giftcard_expired]
                  json = { :status_id => TpTransactionState::STATES[:giftcard_expired].to_s, :status => "giftcard_expired", :message => "El bono ha expirado", :expiration_date =>  @gift_code.valid_until.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                else
                  if @gift_code.available_balance.to_i == 0
                    transaction.tp_transaction_state_id=TpTransactionState::STATES[:no_funds]
                    json = { :status_id => TpTransactionState::STATES[:no_funds].to_s, :status => "no funds", :message => "Sin Fondos, el saldo del bono es igual a cero", :balance => @gift_code.available_balance.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                  else
                    transaction.transaction_value = params[:value].to_f   # Registra valor de la transacción
                    transaction.transaction_currency = params[:currency].to_s   # Registra Moneda Utilizada
                    if Float(params[:value])
                      new_balance = (@gift_code.available_balance.to_f - params[:value].to_f).round(2)
                      if new_balance < 0
                        transaction.tp_transaction_state_id=TpTransactionState::STATES[:insufficient_funds]
                        json = { :status_id => TpTransactionState::STATES[:insufficient_funds].to_s, :status => "insufficient funds", :message => "El saldo del bono es insuficiente, cambie el valor a redimir", :balance => @gift_code.available_balance.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                      else
                        transaction.tp_transaction_state_id=TpTransactionState::STATES[:success]
                      end
                    else
                      transaction.tp_transaction_state_id=TpTransactionState::STATES[:value_format_error]
                      json = { :status_id => TpTransactionState::STATES[:value_format_error].to_s, :status => "value format error", :message => "El valor debe ser num&eacute;rico (999999999.99)", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                    end
                  end
                end
              else
                transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_sign]
                json = { :status_id => TpTransactionState::STATES[:wrong_sign].to_s, :status => "wrong sign", :message => "La firma emitida no coincide - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
              end
            else
              transaction.tp_transaction_state_id=TpTransactionState::STATES[:giftcard_not_found]
              json = { :status_id => TpTransactionState::STATES[:giftcard_not_found].to_s, :status => "giftcard not found", :message => "El bono no se encontro en el sistema giftcode.", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
            end
          else
            transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_brand]
            json = { :status_id => TpTransactionState::STATES[:wrong_credentials_brand].to_s, :status => "wrong credentials - brand", :message => "Codigo de marca invalida - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
          end
        else
          transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_commerce_id]
          json = { :status_id => TpTransactionState::STATES[:wrong_credentials_commerce_id].to_s, :status => "wrong credentials - commerce_id", :message => "Codigo de comercio invalido - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
        end
      else
        transaction.tp_transaction_state_id=TpTransactionState::STATES[:ip_client_blocked]
        json = { :status_id => TpTransactionState::STATES[:ip_client_blocked].to_s, :status => "ip_client_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
      end
    else
      transaction.tp_transaction_state_id=TpTransactionState::STATES[:dir_ip_blocked]
      json = { :status_id => TpTransactionState::STATES[:dir_ip_blocked].to_s, :status => "ip_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
    end

  ensure
    if transaction.save
      if transaction.tp_transaction_state_id == TpTransactionState::STATES[:success]
        json = { :status_id => TpTransactionState::STATES[:success].to_s, :status => "success", :message => "Transacci&oacute;n Exitosa", :new_balance => new_balance.to_s, :giftcode_transaction_id => transaction.id, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
      end
    end
    return json
  end


  # Confirm a pending redemption
  def confirm_redemption( params = Array.new, remote_ip)
    transaction = ThirdPartyTransaction.new
    transaction.received_hash=params.inspect.to_s.gsub(', "controller"=>"third_party", "action"=>"confirm_redemption"','')  # Registro del Hash
    puts transaction.received_hash
    transaction.tp_transaction_type_id = TpTransactionType::TYPES[:confirm_redemption]  # Registra tipo de transacción
    transaction.dir_ip = remote_ip.to_s # Registra dirección IP del cliente
    transaction.tp_transaction_state_id = TpTransactionState::STATES[:unknown_error]  # Incializa el estado de la transacción
    transaction.terminal_id = params[:termId]
    transaction.related_transaction_id =  params[:targetTransaction]
    if verify_black_list (transaction.dir_ip)
      if verify_black_list_client (transaction.dir_ip_client)
        third=ThirdPartyEntity.find_by_incocredito_id(params[:commerceId])
        if third
          transaction.third_party_entity_id = third.id   # Registra id de tercero
          brand_code=BrandCode.find_by_third_party_entity_id_and_code(third.id,params[:brandCode])
          if brand_code
            transaction.brand_code_id = brand_code.id  # Registra código de la marca del tercero
            key = brand_code.code.to_s+'|'+params[:transactionId].to_s+'||||'+third.incocredito_id.to_s+'|'+params[:transDate].to_s+'|'+params[:targetTransaction].to_s+'|||'+third.secret.to_s
            firm = Digest::MD5.hexdigest(key)
            registration = ThirdPartyRegistration.new
            registration.brand_code = brand_code
            registration.received_date = params[:transDate]
            registration.received_sign = params[:firm]
            if firm == params[:firm] && registration.save
              target_transaction = ThirdPartyTransaction.find_by_id_and_brand_code_id_and_third_party_entity_id_and_tp_transaction_type_id_and_confirmed(params[:targetTransaction], brand_code.id, third.id, TpTransactionType::TYPES[:pending_redemption],nil)
              if target_transaction
                @gift_code2 = GiftCode.find(target_transaction.gift_code_id)
                transaction.gift_code_id = target_transaction.gift_code_id
                transaction.transaction_value = target_transaction.transaction_value.to_f   # Registra valor de la transacción
                transaction.transaction_currency = target_transaction.transaction_currency.to_s   # Registra Moneda Utilizada
                transaction.transaction_value = target_transaction.transaction_value
                if @gift_code2.available_balance.to_i == 0
                  transaction.tp_transaction_state_id=TpTransactionState::STATES[:no_funds]
                  json = { :status_id => TpTransactionState::STATES[:no_funds].to_s, :status => "no funds", :message => "El saldo del bono es igual a cero", :balance => @gift_code2.available_balance.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                else
                  if Float(target_transaction.transaction_value)
                    new_balance = @gift_code2.available_balance.to_f - target_transaction.transaction_value
                    if new_balance < 0
                      transaction.tp_transaction_state_id=TpTransactionState::STATES[:insufficient_funds]
                      json = { :status_id => TpTransactionState::STATES[:insufficient_funds].to_s, :status => "insufficient funds", :message => "El saldo del bono es insuficiente, cambie el valor a redimir", :balance => @gift_code2.available_balance.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                    else
                      @gift_code2.available_balance = new_balance # Asigna Nuevo Saldo
                      if @gift_code2.save
                        transaction.tp_transaction_state_id=TpTransactionState::STATES[:success]
                        target_transaction.confirmed = true
                        target_transaction.save
                        transaction.new_balance = new_balance    # Registra Nuevo Saldo
                        count = ThirdPartyTransaction.where('created_at >= ?',Time.zone.now.beginning_of_day).where(:brand_code_id => brand_code.id).count
                        transaction.approbation_number = count+1
                        json = { :status_id => TpTransactionState::STATES[:success].to_s, :status => "success", :message => "Confirmaci&oacute;n de Redenci&oacute;n Exitosa", :new_balance => @gift_code2.available_balance.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                      else
                        transaction.tp_transaction_state_id=TpTransactionState::STATES[:could_not_save]
                        json = { :status_id => TpTransactionState::STATES[:could_not_save].to_s, :status => "could_not_save", :message => "La Transacci&oacute;n no pudo ser procesada - contacte con el administrador", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                      end
                    end
                  else
                      transaction.tp_transaction_state_id=TpTransactionState::STATES[:value_format_error]
                      json = { :status_id => TpTransactionState::STATES[:value_format_error].to_s, :status => "value format error", :message => "El valor debe ser num&eacute;rico (999999999.99)", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                  end
                end
              else
                transaction.tp_transaction_state_id=TpTransactionState::STATES[:target_transaction_not_found]
                json = { :status_id => TpTransactionState::STATES[:target_transaction_not_found].to_s, :status => "target_transaction_not_found", :message => "La Transacci&oacute;n no fue encontrada o ya fue reversada", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
              end
            else
              transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_sign]
              json = { :status_id => TpTransactionState::STATES[:wrong_sign].to_s, :status => "wrong sign", :message => "La firma emitida no coincide - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
            end

          else
            transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_brand]
            json = { :status_id => TpTransactionState::STATES[:wrong_credentials_brand].to_s, :status => "wrong credentials - brand", :message => "Codigo de marca invalida - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
          end
        else
          transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_commerce_id]
          json = { :status_id => TpTransactionState::STATES[:wrong_credentials_commerce_id].to_s, :status => "wrong credentials - commerce_id", :message => "Codigo de comercio invalido - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
        end
      else
        transaction.tp_transaction_state_id=TpTransactionState::STATES[:ip_client_blocked]
        json = { :status_id => TpTransactionState::STATES[:ip_client_blocked].to_s, :status => "ip_client_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
      end
    else
      transaction.tp_transaction_state_id=TpTransactionState::STATES[:dir_ip_blocked]
      json = { :status_id => TpTransactionState::STATES[:dir_ip_blocked].to_s, :status => "ip_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
    end
  ensure
    if transaction.save
      if transaction.tp_transaction_state_id == TpTransactionState::STATES[:success]
        json = { :status_id => TpTransactionState::STATES[:success].to_s, :status => "success", :message => "Transacci&oacute;n Exitosa", :new_balance => transaction.new_balance, :giftcode_transaction_id => transaction.id, :approbation_number => transaction.approbation_number, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
      end
    end
    return json
  end

  # Reverse redemptions
  def reverse_redemption( params = Array.new, remote_ip )
    transaction = ThirdPartyTransaction.new
    transaction.received_hash=params.inspect.to_s.gsub(', "controller"=>"third_party", "action"=>"reverse_redemption"','')  # Registro del Hash
    transaction.tp_transaction_type_id = TpTransactionType::TYPES[:reverse_redemption]  # Registra tipo de transacción
    transaction.dir_ip = remote_ip.to_s # Registra dirección IP del cliente request.env["HTTP_X_CLUSTER_CLIENT_IP"]
    transaction.tp_transaction_state_id = TpTransactionState::STATES[:unknown_error]  # Incializa el estado de la transacción
    transaction.terminal_id = params[:termId]
    transaction.related_transaction_id =  params[:targetTransaction]
    if verify_black_list (transaction.dir_ip)
      if verify_black_list_client (transaction.dir_ip_client)
        third=ThirdPartyEntity.find_by_incocredito_id(params[:commerceId])
        if third
          transaction.third_party_entity_id = third.id   # Registra id de tercero
          brand_code=BrandCode.find_by_third_party_entity_id_and_code(third.id,params[:brandCode])
          if brand_code
            transaction.brand_code_id = brand_code.id  # Registra código de la marca del tercero
            campaign_id = brand_code.campaign_id
            #@gift_code = GiftCode.joins(:gift_card).where("(gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]} ) AND gift_cards.campaign_id = #{campaign_id} AND gift_codes.redemption_pin='#{pin}'").first
            key = brand_code.code.to_s+'|'+params[:transactionId].to_s+'||||'+third.incocredito_id.to_s+'|'+params[:transDate].to_s+'|'+params[:targetTransaction].to_s+'|||'+third.secret.to_s
            firm = Digest::MD5.hexdigest(key)
            registration = ThirdPartyRegistration.new
            registration.brand_code = brand_code
            registration.received_date = params[:transDate]
            registration.received_sign = params[:firm]
            if firm == params[:firm] && registration.save
              target_transaction = ThirdPartyTransaction.find_by_id_and_brand_code_id_and_third_party_entity_id_and_tp_transaction_type_id_and_reversed(params[:targetTransaction], brand_code.id, third.id, [TpTransactionType::TYPES[:redemption], TpTransactionType::TYPES[:confirm_redemption]],nil)
              if target_transaction
                @gift_code2 = GiftCode.find(target_transaction.gift_code_id)
                transaction.gift_code_id = target_transaction.gift_code_id
                transaction.transaction_value = target_transaction.transaction_value.to_f   # Registra valor de la transacción
                transaction.transaction_currency = target_transaction.transaction_currency.to_s   # Registra Moneda Utilizada
                @gift_code2.available_balance += target_transaction.transaction_value
                transaction.transaction_value = target_transaction.transaction_value
                if @gift_code2.save
                  transaction.tp_transaction_state_id=TpTransactionState::STATES[:success]
                  target_transaction.reversed = true
                  target_transaction.save
                  transaction.new_balance = @gift_code2.available_balance    # Registra Nuevo Saldo
                  count = ThirdPartyTransaction.where('created_at >= ?',Time.zone.now.beginning_of_day).where(:brand_code_id => brand_code.id).count
                  transaction.approbation_number = count+1
                else
                  transaction.tp_transaction_state_id=TpTransactionState::STATES[:could_not_save]
                  json = { :status_id => TpTransactionState::STATES[:could_not_save].to_s, :status => "could_not_save", :message => "La Transacci&oacute;n no pudo ser procesada - contacte con el administrador", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                end
              else
                transaction.tp_transaction_state_id=TpTransactionState::STATES[:target_transaction_not_found]
                json = { :status_id => TpTransactionState::STATES[:target_transaction_not_found].to_s, :status => "target_transaction_not_found", :message => "La Transacci&oacute;n no fue encontrada o ya fue reversada", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
              end
            else
              transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_sign]
              json = { :status_id => TpTransactionState::STATES[:wrong_sign].to_s, :status => "wrong sign", :message => "La firma emitida no coincide - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
            end

          else
            transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_brand]
            json = { :status_id => TpTransactionState::STATES[:wrong_credentials_brand].to_s, :status => "wrong credentials - brand", :message => "Codigo de marca invalida - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
          end
        else
          transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_commerce_id]
          json = { :status_id => TpTransactionState::STATES[:wrong_credentials_commerce_id].to_s, :status => "wrong credentials - commerce_id", :message => "Codigo de comercio invalido - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
        end
      else
        transaction.tp_transaction_state_id=TpTransactionState::STATES[:ip_client_blocked]
        json = { :status_id => TpTransactionState::STATES[:ip_client_blocked].to_s, :status => "ip_client_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
      end
    else
      transaction.tp_transaction_state_id=TpTransactionState::STATES[:dir_ip_blocked]
      json = { :status_id => TpTransactionState::STATES[:dir_ip_blocked].to_s, :status => "ip_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
    end
  ensure
    if transaction.save
      if transaction.tp_transaction_state_id == TpTransactionState::STATES[:success]
        json = { :status_id => TpTransactionState::STATES[:success].to_s, :status => "success", :message => "Reversi&oacute;n Exitosa", :new_balance => transaction.new_balance, :rollback_transaction_id => transaction.id, :approbation_number => transaction.approbation_number, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
      end
    end
    return json
  end

  # funds Request
  def get_balance( params = Array.new, remote_ip)
    transaction = ThirdPartyTransaction.new
    transaction.received_hash=params.inspect.to_s.gsub(', "controller"=>"third_party", "action"=>"get_balance"','')  # Registro del Hash
    transaction.tp_transaction_type_id = TpTransactionType::TYPES[:funds_request]  # Registra tipo de transacción
    transaction.dir_ip = remote_ip.to_s # Registra dirección IP del cliente
    transaction.tp_transaction_state_id = TpTransactionState::STATES[:unknown_error]
    transaction.terminal_id = params[:termId]
    transaction.office_id = params[:office]
    transaction.input_type_id = params[:inputType]
    if verify_black_list (transaction.dir_ip)
      if verify_black_list_client (transaction.dir_ip_client)
        third=ThirdPartyEntity.find_by_incocredito_id(params[:commerceId])
        if third
          transaction.third_party_entity_id = third.id   # Registra id de tercero
          brand_code=BrandCode.find_by_third_party_entity_id_and_code(third.id,params[:brandCode])
          campaign_id = brand_code.campaign_id
          if brand_code
            transaction.brand_code_id = brand_code.id  # Registra código de la marca del tercero
            if params[:pin]
              pin=params[:pin]
            end
            if params[:giftcodeNumber]
              pin=params[:giftcodeNumber]
            end
            @gift_code = GiftCode.joins(:gift_card).where("(gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:payment_accepted]} OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_external_web_payment_accepted]}  OR gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:generated_office_confirmed]}) AND gift_cards.campaign_id = #{campaign_id} AND gift_codes.redemption_pin='#{Digest::SHA3.hexdigest(pin)}'").first
            unless @gift_code
              @gift_code = PointGiftCode.where("gift_codes.gift_code_state_id = #{GiftCodeState::STATES[:free_points]} AND gift_codes.redemption_pin='#{Digest::SHA3.hexdigest(pin)}'").first
            end
            if @gift_code
              transaction.gift_code_id = @gift_code.id
              key = brand_code.code.to_s+'|'+pin.to_s+'|'+third.incocredito_id.to_s+'|'+params[:transDate].to_s+'||||'+third.secret.to_s
              firm = Digest::MD5.hexdigest(key)
              registration = ThirdPartyRegistration.new
              registration.brand_code = brand_code
              registration.received_date = params[:transDate]
              registration.received_sign = params[:firm]
              if firm == params[:firm] && registration.save
                if @gift_code.valid_until.past?
                  transaction.tp_transaction_state_id=TpTransactionState::STATES[:giftcard_expired]
                  json = { :status_id => TpTransactionState::STATES[:giftcard_expired].to_s, :status => "giftcard_expired", :message => "El bono ha expirado", :expiration_date =>  @gift_code.valid_until.to_s, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                else
                  transaction.tp_transaction_state_id=TpTransactionState::STATES[:success]
                  transaction.new_balance = @gift_code.available_balance.to_s    # Registra Saldo Existente
                  transaction.transaction_currency = brand_code.campaign.currency.short_name
                  json = { :status_id => TpTransactionState::STATES[:success].to_s, :status => "success", :message => "Success!", :balance => @gift_code.available_balance.to_s, :short_name_currency => transaction.transaction_currency, :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
                end
              else
                transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_sign]
                json = { :status_id => TpTransactionState::STATES[:wrong_sign].to_s, :status => "wrong sign", :message => "La firma emitida no coincide - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
              end
            else
              transaction.tp_transaction_state_id=TpTransactionState::STATES[:giftcard_not_found]
              json = { :status_id => TpTransactionState::STATES[:giftcard_not_found].to_s, :status => "giftcard not found", :message => "El bono no se encontro en el sistema giftcode.", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
            end
          else
            transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_brand]
            json = { :status_id => TpTransactionState::STATES[:wrong_credentials_brand].to_s, :status => "wrong credentials - brand", :message => "Codigo de marca invalida - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
          end
        else
          transaction.tp_transaction_state_id=TpTransactionState::STATES[:wrong_credentials_commerce_id]
          json = { :status_id => TpTransactionState::STATES[:wrong_credentials_commerce_id].to_s, :status => "wrong credentials - commerce_id", :message => "Codigo de comercio invalido - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
        end
      else
        transaction.tp_transaction_state_id=TpTransactionState::STATES[:ip_client_blocked]
        json = { :status_id => TpTransactionState::STATES[:ip_client_blocked].to_s, :status => "ip_client_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
      end
    else
      transaction.tp_transaction_state_id=TpTransactionState::STATES[:dir_ip_blocked]
      json = { :status_id => TpTransactionState::STATES[:dir_ip_blocked].to_s, :status => "ip_blocked", :message => "Su dirección IP ha sido bloqueada - contacte al administrador del sistema", :echo_brand_code => params[:brandCode].to_s, :echo_term_id => params[:termId].to_s, :echo_trans_date => params[:transDate].to_s, :echo_transaction_id => params[:transactionId].to_s }
    end
  ensure
    transaction.save
    return json
  end

  private
    # Controla el uso de la fuerza bruta para decifrar las credenciales de autenticación
    def verify_black_list(dir_ip)
      black_list = ThirdPartyBlackList.find_by_blocked_and_dir_ip(true, dir_ip)
      if black_list
        false
      else
        failed_transactions = ThirdPartyTransaction.where('created_at BETWEEN ? AND ? AND dir_ip = ? AND tp_transaction_state_id in (5,4,2,10)', Time.now - 1.hours, Time.now, dir_ip).count
        if failed_transactions > 30
          black_list = ThirdPartyBlackList.find_by_dir_ip(dir_ip)
          if black_list
            black_list.blocked=true
          else
            black_list = ThirdPartyBlackList.new
            black_list.dir_ip=dir_ip
            black_list.blocked=true
          end
          black_list.save
          false
        else
          true
        end
      end
    end

    # Controla el uso de la fuerza bruta al tratar de decifrar PINES de giftcode
    def verify_black_list_client(dir_ip_client)
      #1 Bloquea ip despues de 10 intentos en las ultimas 2 horas
      #2 Desbloquea ip si tiene menos de 10 intentos en las ultimas 2 horas
      if dir_ip_client.nil? || dir_ip_client.blank?
        return true
      else
        black_list = ThirdPartyBlackList.find_by_dir_ip( dir_ip_client)
        failed_transactions = ThirdPartyTransaction.where('created_at BETWEEN ? AND ? AND dir_ip_client = ? AND tp_transaction_state_id in (3)', Time.now - 1.hours, Time.now, dir_ip_client).count
        puts failed_transactions
        ret=false
        if black_list && failed_transactions > 20
          black_list.blocked = true
          black_list.save
          ret = false
        end
        if !black_list && failed_transactions > 20
          black_list = ThirdPartyBlackList.new
          black_list.dir_ip_client=dir_ip_client
          black_list.blocked=true
          black_list.save
          ret = false
        end
        if !black_list && failed_transactions <=20
          ret = true
        end
        if black_list &&  failed_transactions <=20
          black_list.blocked = false
          black_list.save
          ret = true
        end
        return ret
      end
    end

end