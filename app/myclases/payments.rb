require 'rest-client'

class Payments

  include ActionView::Helpers::NumberHelper

  attr_accessor :message, :setup, :data, :gift_code, :entity, :campaign, :brand_codes, :request, :payment_rejected, :demo

  def initialize( params = {})
    @load = false
    @error = false
    @request = nil
    @params = params
    @demo = false
    begin
      @gift_code =  GiftCode.find_by_ref_sale_and_gift_code_state_id( params[:ref_sale], [ GiftCodeState::STATES[:generated_and_payment_pending], GiftCodeState::STATES[:generated_external_web_pending] ] )
      if @gift_code
        @campaign = @gift_code.gift_card.campaign
        @brand_codes = @campaign.brand_codes.find_by_active_for_payment true
        @entity = @brand_codes.third_party_entity
        @setup = eval @entity.configuration
        @load = true
      else
        @message = 'Error no existe giftcode.'
      end
    rescue
      @error = true
      puts 'ERROR-Payments'
      puts 'No se encontro campaña' unless @campaign
      puts 'No hay medio de pago activo para la campaña' unless @brand_codes
      puts 'No se encontro entidad de pago' unless @entity
      puts 'No hay configuracion para la entidad' unless @setup

      @message = 'Error en los datos, por favor comunicarse con el administrador.'
    end
  end

  def type
    _return = 'application'
    if @gift_code && ( ( @gift_code.gift_code_state_id == GiftCodeState::STATES[:generated_external_web_pending] ) || (@gift_code.gift_code_state_id == GiftCodeState::STATES[:generated_external_web_payment_accepted]) || (@gift_code.gift_code_state_id == GiftCodeState::STATES[:generated_external_web_payment_rejected]) )
      _return = 'external'
    elsif @entity && ( @entity.name == ThirdPartyEntity::ENTITY[:credibanco] || @entity.name == ThirdPartyEntity::ENTITY[:giftcode] )
      if @params['action'] == 'confirmation' || @params['action'] == 'demo'
        _return = 'application'
      else
        _return = 'ajax'
      end
    end
    _return
  end

  def start request
    if @load
      case @entity.name
        when ThirdPartyEntity::ENTITY[:redeban]
          return request_redeban
        when ThirdPartyEntity::ENTITY[:credibanco]
          if request.get?
            @render = 'gateway_credibanco'
            return true
          elsif request.put?

            return send_credibanco
          else
            @message = 'Error no existe solicitud de pago.'
          end
        when ThirdPartyEntity::ENTITY[:giftcode]
          if request.get?
            @render = 'gateway_giftcode'
            return true
          else
            @message = 'Error no existe solicitud de pago.'
          end
        else
          @message = 'Error no existe solicitud de pago para la entidad, por favor comunicarse con el administrador.'
      end
    end
    false
  end

  def finalize
    if @load
      # request = RequestThirdPartyEntity.find_by_gift_code_id @gift_code.id
      # find_by_gift_code_id_and_response_code_and_state
      case @entity.name
        when ThirdPartyEntity::ENTITY[:redeban]
          request = RequestThirdPartyEntity.find_by_gift_code_id_and_response_code_and_state @gift_code.id, "00","Recibida"
          if request
            data = eval request.data_received
            return consult_redeban data
          else
            @message = 'Error no se ha iniciado pago.'
          end
        when ThirdPartyEntity::ENTITY[:credibanco]
          @request = RequestThirdPartyEntity.find_by_gift_code_id_and_response_code_and_state @gift_code.id, "00","Aprobada"
          if @request
            @message = 'El giftcode esta con pago aceptado.'
            @gift_code.process_successful_payment
            return true
          else
            @message = 'Error no se ha iniciado pago.'
          end
        else
          @message = 'Error no existe solicitud para la entidad, por favor comunicarse con el administrador.'
      end
      # else
      #   @message = 'Error no se ha iniciado pago.'
      # end
    else
      unless @error
        @gift_code =  GiftCode.find_by_ref_sale_and_gift_code_state_id( @params[:ref_sale], [ GiftCodeState::STATES[:payment_accepted], GiftCodeState::STATES[:generated_external_web_payment_accepted] ] )
        if @gift_code
          @campaign = @gift_code.gift_card.campaign
          @brand_codes = @campaign.brand_codes.find_by_active_for_payment true
          @entity = @brand_codes.third_party_entity
          if @entity.name == ThirdPartyEntity::ENTITY[:credibanco]
            @request =  @gift_code.request_third_party_entities.find_by_response_code '00'
          end
          @message = 'El giftcode esta con pago aceptado.'
          return true
        else
          @gift_code =  GiftCode.find_by_ref_sale_and_gift_code_state_id( @params[:ref_sale], [ GiftCodeState::STATES[:payment_rejected], GiftCodeState::STATES[:generated_external_web_payment_rejected] ] )
          if @gift_code
            @campaign = @gift_code.gift_card.campaign
            @brand_codes = @campaign.brand_codes.find_by_active_for_payment true
            @entity = @brand_codes.third_party_entity
            if @entity.name == ThirdPartyEntity::ENTITY[:credibanco]
              @request =  @gift_code.request_third_party_entities.first
            end
            @payment_rejected = @gift_code.gift_code_state_id == GiftCodeState::STATES[:payment_rejected] || @gift_code.gift_code_state_id ==  GiftCodeState::STATES[:generated_external_web_payment_rejected]
            @message = 'El giftcode esta con pago rechazado.'
            return true
          else
            @message = 'Error no existe giftcode.'
          end
        end
      end
    end
    return false
  end

  def demo_finalize
    if Rails.env.development? || Rails.env.staging? || Rails.env.prestaging?
      @demo = true
      if @load
        @gift_code.process_successful_payment
        @message = 'Demo - Transacción se completó exitosamente.'
        return true
      else
        unless @error
          @gift_code =  GiftCode.find_by_ref_sale_and_gift_code_state_id( @params[:ref_sale], [ GiftCodeState::STATES[:payment_accepted], GiftCodeState::STATES[:generated_external_web_payment_accepted] ] )
          if @gift_code
            @campaign = @gift_code.gift_card.campaign
            @brand_codes = @campaign.brand_codes.find_by_active_for_payment true
            @entity = @brand_codes.third_party_entity
            @message = 'El giftcode esta con pago aceptado.'
            return true
          else
            @message = 'Error no existe giftcode.'
          end
        end
      end
    else
      @message = 'Error - Evento no esperado.'
    end
    return false
  end

  def render
    send = @render
    @render = ''
    return send
  end

  private
    def request_redeban
      if @brand_codes.terminal && @brand_codes.code && @brand_codes.type_terminal
        send = {
            credenciales: @setup[:credenciales],
            cabeceraSolicitud: {
                infoPuntoInteraccion: {
                    tipoTerminal: @brand_codes.type_terminal,
                    idTerminal: @brand_codes.terminal,
                    idAdquiriente: @brand_codes.code,
                    idTransaccionTerminal: @gift_code.id
                }
            },
            infoCompra: {
                numeroFactura: @gift_code.id,
                montoTotal: ( @gift_code.cost + @campaign.cost_admin + @campaign.calculate_iva_cost_admin ).round(2).to_s,
                infoImpuestos: {
                    :tipoImpuesto => 'IVA',
                    :monto => ( @gift_code.calculate_iva + @campaign.calculate_iva_cost_admin ).round(2).to_s
                },
                montoDetallado: {
                    :tipoMontoDetallado => 'BaseDevolucionIVA',
                    :monto => ( @gift_code.base_iva + @campaign.cost_admin ).round(2).to_s
                },
                infoComercio: {
                    informacionComercio: 'Bono de regalo',
                    informacionAdicional: 'Giftcode'
                }
            }
        }
        @data = connect_digital_ocean( @setup[:ws], 'IniciarTransaccionDeCompra', send )
        if !@data.empty?
          # save_request( @entity.id, @gift_code.id, send, @data, @data['infoRespuesta']['codRespuesta'], @data['infoRespuesta']['estado'], @data['infoRespuesta']['descRespuesta'] )
          save_request( @entity.id, @gift_code.id, send, @data, @data['infoRespuesta']['codRespuesta'], @data['infoRespuesta']['estado'], @data['infoRespuesta']['descRespuesta'] )
          if @data['infoRespuesta']['codRespuesta'] == '00'
            if @data['infoRespuesta']['estado'] == 'Recibida'
              @render = 'request_redeban'
              return true
            end
            @message = 'Error en el estado, por favor inténtalo de nuevo.'
          else
            @message = 'Error al procesar pago, por favor inténtalo de nuevo.'
          end
        end
      else
        @message = 'Error no se obtiene información de la campaña para la entidad'
      end
      return false
    end

    def consult_redeban(data )
      send = {
          credenciales: @setup[:credenciales],
          cabeceraSolicitud: {
              infoPuntoInteraccion: {
                  tipoTerminal: @brand_codes.type_terminal,
                  idTerminal: @brand_codes.terminal,
                  idAdquiriente: @brand_codes.code,
                  idTransaccionTerminal: @gift_code.id
              }
          },
          idTransaccion: data['infoTransaccionResp']['idTransaccionActual']
      }
      @data = connect_digital_ocean( @setup[:ws], 'ConsultarEstadoDePago', send )
      if !@data.empty?

        if @data['infoPago']
          save_request( @entity.id, @gift_code.id, send, @data, @data['infoRespuesta']['codRespuesta'], @data['infoRespuesta']['estado'], @data['infoRespuesta']['descRespuesta'], @data['infoPago']['tipoMedioDePago'], @data['infoPago']['franquicia'], @data['infoPago']['montoTotal'], @data['infoPago']['numeroAprobacion'], nil )
        else
          save_request( @entity.id, @gift_code.id, send, @data, @data['infoRespuesta']['codRespuesta'], @data['infoRespuesta']['estado'], @data['infoRespuesta']['descRespuesta'] )#, @data['infoPago']['tipoMedioDePago'], @data['infoPago']['franquicia'], @data['infoPago']['montoTotal'], @data['infoPago']['numeroAprobacion'], nil )
        end

        if @data['infoRespuesta']['codRespuesta'] != '00'
          @gift_code.process_unsuccessful_payment
          @message = 'El pago fue rechazado, ' + @data['infoRespuesta']['descRespuesta']
        else
          if  @data['infoRespuesta']['estado'] == 'Aprobada'
            @gift_code.process_successful_payment
            @message = 'Transacción se completó exitosamente.'
            return true
          else
            @message = 'No ha completado la transacción.'
          end
        end
      end
      false
    end

    def send_credibanco
      @params[:purchaseData] = {
          currencyCode: '170', #CODIGO DE LA MONEDA
          totalAmount: number_with_delimiter( ("%.2f" % ( @gift_code.cost + @campaign.cost_admin + @campaign.calculate_iva_cost_admin ).round(2)) , separator: '', delimiter:''),
          purchaseCode: @gift_code.id,
          terminalCode: @brand_codes.terminal,
          commerceId: @brand_codes.type_terminal,
          planId:'01',
          iva: number_with_delimiter( ("%.2f" %  ( @gift_code.calculate_iva + @campaign.calculate_iva_cost_admin ).round(2)), separator: '',delimiter:''),
          ivaReturn: number_with_delimiter( ("%.2f" % ( @gift_code.base_iva + @campaign.cost_admin  ).round(2)), separator: '',delimiter:''),
      }

      @params[:shippingData] = {
          names: @params['payment']['names'],
          last_names: @params['payment']['last_names'],
          addressData: {
            address: @params['payment']['address'],
            countryCode: @params['payment']['country'],
            state: @params['payment']['state'],
            city:  @params['payment']['city'],
            postalCode: @gift_code.to_postal_code || '',
            phoneNumber: @params['payment']['phone'],
            cellPhoneNumber: @params['payment']['cell'],
            email: @params['payment']['email']
          }
      }

      puts "shippingData"
      puts @params[:shippingData].inspect
      puts "abort"


      @params[:production] = Rails.env.production? ? 'true':'false'
      # puts @params
      # {"result":0,"errorCode":"00","errorMessage":"Aprobada","authorizationCode":"169162","authorizedAmount":"000000015232","fraudScore":-1,"additionalMessage":"","fraudFactors":" ","resultAR":-1,"errorCodeAR":" ","errorMessageAR":" ","authorizationCodeAR":" ","authorizedAmountAR":"0.00","fraudScoreAR":-1}
      puts @params.inspect
      @data = connect_amazon( @params )
      unless @data.empty?
        @params['payment']['card_number'] = @params['payment']['card_number'].split(//).last(4).join
        save_request( @entity.id, @gift_code.id, @params, @data, @data['errorCode'], @data['errorMessage'], "authorizationCode: #{@data['authorizationCode']}, authorizedAmount: #{@data['authorizedAmount']}", @params['payment']['type'], @params['payment']['franquicia'], @data['"authorizedAmount"'], @data['authorizationCode'], nil )
        # if @data['errorCode'] == '00'
        #   @render = 'request_credibanco'
        #   @gift_code.process_successful_payment
        #   @message = "true"
        # else
        #   @render = 'request_credibanco'
        #   @gift_code.process_unsuccessful_payment
        #   @message = 'El pago fue rechazado, ' + @data['errorMessage']
        # end
        @render = 'request_credibanco'
        @message = "true"
        unless @data['errorCode'] == '00'
          @gift_code.process_unsuccessful_payment
          @message = 'El pago fue rechazado, ' + @data['errorMessage']
        end
        return true
      else
        @message = "false"
      end
      false
    end

    def connect_digital_ocean( setup = nil, call = nil, params = nil)
      begin
        # response = RestClient.post( GlobalConstants::connect_web_service, { :setup => setup, :call => call, :params => params, :verify_ssl => false } )
        response = RestClient::Resource.new( GlobalConstants::connect_web_service, :verify_ssl => false).post({ :setup => setup, :call => call, :params => params, :verify_ssl => false } )
        data = JSON.parse response
        if data.empty?
          puts 'Error web services, no retorno datos'
          @message = 'Error en respuesta.'
          return {}
        end
        return data
      rescue Exception => e
        puts 'Error a conectarse a DigitalOcean' if response
        puts 'Error en la respuesta en la conexion con DigitalOcean' if data
        @message = 'Error de conexión.'
        puts e.message
        return {}
      end
    end

    def connect_amazon( params = nil )
      begin
        puts  GlobalConstants::connect_amazon
        response = RestClient::Resource.new(  GlobalConstants::connect_amazon,  :verify_ssl => false, ssl_version: :SSLv3 ).post( { :params => eval( params.to_s ) } , :content_type => 'application/hash' )
        data = JSON.parse response
        if data.empty?
          puts 'Error web services, no retorno datos'
          @message = 'Error en respuesta.'
          return {}
        end
        return data
      rescue Exception => e
        puts response.inspect if response
        puts 'Error a conectarse a Amazon' unless response
        puts 'Error en la respuesta en la conexion con Amazon' unless data
        @message = 'Error de conexión.'
        puts e.message
        return {}
      end
    end

    def save_request( entity_id = nil, gc_id = nil, params = {}, response = {}, response_code = nil, state = nil, description = nil, payment_type = nil, franchise = nil, total_value=nil, approbation_number=nil, bin=nil)
      RequestThirdPartyEntity.create(
          :third_party_entity_id => entity_id ,
          :gift_code_id => gc_id,
          :data_sent => params.to_s,
          :data_received => response.to_s,
          :response_code => response_code,
          :state => state,
          :response_message => description,
          :payment_type => payment_type,
          :card_type => payment_type,
          :card_franchise => franchise,
          :total_value => total_value,
          :approbation_number => approbation_number,
          :bin => bin
      )
    end

end