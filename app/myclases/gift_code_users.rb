class GiftCodeUsers

  def self.find( params = nil )
    data = params[:username].split(':')
    puts params.inspect
    return case data[0]
        when 'facebook'
          user = GiftCodeUsers.new
          puts params.inspect
          user.facebook params[:password]
        else
          nil
    end
  end


  def facebook( token = nil )
    graph = Koala::Facebook::API.new(token)
    error_graph = false
    begin
      profile = graph.get_object('me')
      puts profile
    rescue
      error_graph = true
    end
    unless error_graph
      user = User.find_by_fbk_user_id(profile['id'])
      if user.present?
        puts GlobalConstants::PREFIX_FOR_LOGS + "user_present: " + user.id.to_s
        #If the user has not been registered
        send_welcome_email = false
        if user.user_state_id == UserState::STATES[:non_registered]
          begin
            User.transaction do
              user.fbk_user_id = profile['id'].to_i
              user.user_state_id = UserState::STATES[:registered]
              user.reverse_friendships.update_all(:friendship_state_id => FriendshipState::STATES[:friend])
              if profile['email'].present? && !profile['email'].blank?
                user.email = profile['email']
              end
              user.email_fbk = profile['email']
              if data.birthday.present? && !user.birthday.present?
                begin
                  user.birthday = Date.strptime("{ #{birthday} }", "{ %m/%d/%Y }")
                rescue
                  user.birthday = Date.strptime("{ #{birthday+'/1800'} }", "{ %m/%d/%Y }")
                end
              end
              user.registration_date = Time.zone.now
              send_welcome_email = true
            end
          rescue ActiveRecord::Rollback
            puts "The process to register a non-registered user could not be executed"
          end
        end
        unless user.image.present?
          user.remote_image_url = "https://graph.facebook.com/#{ profile['id'].to_i }/picture?width=140&height=140"
        end

        user.fbk_location_name = profile['location_name'] if profile['location_name'].present?
        user.fbk_locale = profile['locale'] if profile['locale'].present?
        user.fbk_timezone = profile['timezone'] if profile['timezone'].present?

        #This information is very useful for future calling to Facebook API
        user.facebook_token = token
        puts GlobalConstants::PREFIX_FOR_LOGS + "user_present before save: " + user.id.to_s
        user.save
        puts GlobalConstants::PREFIX_FOR_LOGS + "user_present after save: " + user.id.to_s

        #If the user has not been registered
        if send_welcome_email == true
          message = GiftCodeNotifier.welcome(user)
          message.deliver
        end
        puts GlobalConstants::PREFIX_FOR_LOGS + "user_present before sync: " + user.id.to_s
        user.verify_fbk_synchronization
        puts GlobalConstants::PREFIX_FOR_LOGS + "user_present after sync: " + user.id.to_s
      else # Create a user with a stub password.
        birthday = nil
        if profile['birthday'].present?
          begin
            birthday = Date.strptime("{ #{profile['birthday']} }", "{ %m/%d/%Y }")
          rescue
            birthday = Date.strptime("{ #{profile['birthday']+'/1800'} }", "{ %m/%d/%Y }")
          end
        end
        #antes :remote_image_url http://prestaging.giftcode.co/regalar-bono#/_=_=> info.image||=""
        sex_id = ( profile['gender'] == "male") ? Sex.find( Sex::SEXES[:male] ).id : Sex.find( Sex::SEXES[:female] ).id
        if  profile['email'].present? && !profile['email'].blank?
          email = profile['email']
        else
          email = f['id']+'@facebook.com'
        end
        puts email
        user = User.create!(:email => email, :email_fbk => profile['email'], :password => Devise.friendly_token[0,20],
                            :first_name => profile['first_name']||="", :last_name => profile['last_name']||="",
                            :sex_id => sex_id, :facebook_token => token,:remote_image_url => "https://graph.facebook.com/#{ profile['id'].to_i }/picture?width=140&height=140", :show_introduction => true,
                            :sms_authorization => true, :birthday => birthday, :fbk_user_id => profile['id'].to_i,
                            :user_state_id => UserState::STATES[:registered],
                            :fbk_locale => profile['locale'], :fbk_timezone => profile['timezone'], :registration_date => Time.zone.now )

        message = GiftCodeNotifier.welcome(user)
        message.deliver
        user.verify_fbk_synchronization
      end
      user
    else
      puts "ERROR CON LA CONEXION CON FACEBOOK, POSIBLE ERROR ACCESS TOKEN VENCIDO."
      nil
    end
  end

end