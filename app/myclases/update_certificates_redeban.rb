class UpdateCertificatesRedeban

  attr_accessor :error_message

  def initialize brand, user
    @error_message = []
    @brand = brand
    @user = user
    if brand
      unless brand.campaigns_convenio_redeban.count
        @error_message.push 'La marca no tiene convenio con redeba.'
      end
    else
      @error_message.push 'No hay marca.'
    end
  end

  def load file
    if @brand
      @file = file
      if @file.respond_to?(:path)

        @txt = File.readlines( @file.path,:encoding => "ASCII")
        @numero_filas = @txt.length
        #identifica el tipo de archivo
        case @file.original_filename[0, 2]
          when 'SA'
            # movimientos_redeban
            saldos_diarios
          when 'MV'
            movimientos_redeban
            # movimientos_detallados
          when 'CD'
            movimientos_redeban
            # consumo_diario
          when 'LO'
            if @file.original_filename[2, 1] == 'G'
              log_resultado_actualizacion
            else
              @error_message.push 'Tipo de archivo desconocido.'
            end
          else
            @error_message.push 'Tipo de archivo desconocido.'
        end

      else
        @error_message.push 'No se logro leer el archivo.'
      end
    end
  end

  private

    def log_resultado_actualizacion
      # Lee el nombre del archivo
      #
      # LOG	      = 	fijo de 2 posiciones
      # XXXX      = 	nombre del convenio de 4 posiciones
      # 20060801  = 	fecha, de 8 posiciones
      # 01        =   consecutivo del convenio
      # .txt.     =   archivo de texto
      @convenio = @file.original_filename[3, 4]
      @error_message.push("No existe convenio: #{@convenio}") unless @brand.exists_convenio_redeban? @convenio
      @fecha = @file.original_filename[7,8]
      @consecutivo_convenio = @file.original_filename[15,2]
      if @error_message.count == 0
        begin
          RedebanCode.transaction do
            count = 0
            while count < @numero_filas  do
              error = []
              row = @txt[count].gsub(/\r/,'').gsub(/\n/,'')
              initial = 0
              positions = 0
              if count == 0 && row[initial,positions = 2] == '00' #Tipo Caracter 2 00 = Encabezado
                archivo_origen = row[initial += positions,positions = 25] #Archivo origen Caracter 25 Nombre del archivo de origen
                fecha_archivo = row[initial += positions,positions = 8].to_date #Fecha Archivo Numérico 8 Fecha del Archivo Origen formato AAAAMMDD
                consecutivo = row[initial += positions,positions = 2]#Consecutivo Numérico 2 Consecutivo del Archivo Origen
                indicador = row[initial += positions,positions = 1] #Indicador Caracter 1 A = Actualización , R = Reverso
                fecha_proceso = row[initial += positions,positions = 14].to_date#Fecha y Hora Proceso Numérico 14 Fecha y Hora de proceso del Archivo Origen formato AAAAMMDDHHmmss
                proceso = row[initial += positions,positions = 10]#No. Reg Proc. Numérico 10 Número de Registros Procesados
                num_reg_exitosos = row[initial += positions,positions = 10] #No. Reg Exitosos Numérico 10 Número de Registros Procesados Exitosamente
                num_reg_rechazados = row[initial += positions,positions = 10] #No. Reg Rechazados Numérico 10 Número de Registros Rechazados
                #Filler Caracter 18 Relleno
                log = RedebanLog.create! name: @file.original_filename, source_file: archivo_origen, date_source_file: fecha_archivo, consecutive: consecutivo, type_act: indicador, date_process: fecha_proceso, number_records: proceso, successful: num_reg_exitosos, rejected: num_reg_rechazados
              elsif row[initial,positions = 2] == '01' #Tipo Caracter 2 01 = Procesados Exitosos
                novedad = row[initial += positions,positions = 2]  #Novedad Caracter 2 Novedad, Ver A.1.4
                bono = row[initial += positions,positions = 13]  #No. Bono Numérico 13 Número de Bono
                valor = row[initial += positions,positions = 12] #Valor Numérico 12 Valor del Movimiento, se define con 10 enteros y dos decimales
                fecha_actualizado = row[initial += positions,positions = 8] #Fecha Numérico 8 Fecha a ser actualizada formato AAAAMMDD
                consecutivo_exitoso = row[initial += positions,positions = 12] #Consecutivo Numérico 12 Consecutivo de Movimiento asignado por la aplicación
                #Filler Carácter 51 Relleno
                rc = RedebanCode.find_by_hash2 Digest::SHA3.hexdigest(bono)
                rc.update_attributes! log_novelty: novedad.to_i, log_value: valor[0, 10].to_i + "0.#{valor[10,2].to_s}".to_f, log_update_date: fecha_actualizado.to_date, log_consecutive: consecutivo_exitoso.to_i
              elsif row[initial,positions = 2] == '02' #Tipo Carácter 2 02 = Procesados rechazados
                novedad = row[initial += positions,positions = 2] #Novedad Carácter 2 Novedad, Ver A.1.4
                bono = row[initial += positions,positions = 13] #No. Bono Numérico 13 Número de Bono
                codigo_error = row[initial += positions,positions = 2]#Código Error Numérico 2 Error por el cual no fue procesado el registro, ver B.3.4
                #Filler Carácter 81 Relleno
                rc = RedebanCode.find_by_redemption_pin! Digest::SHA3.hexdigest(bono)
                rc.update_attributes! log_novelty: novedad.to_i, log_error_update_file_id: codigo_error
              end
              count = count + 1
            end
          end
        rescue ActiveRecord::Rollback
          @error_message.push "Error al actualizaron certificados"
        end
      end
    end

    def movimientos_redeban
      file = @file.original_filename
      count = 0
      while count < @numero_filas
        redeban_movement = RedebanMovement.new
        redeban_movement.brand_id = @brand.id
        redeban_movement.line = @txt[count].to_s
        redeban_movement.admin_user_id = @user.id
        redeban_movement.file = file
        redeban_movement.number_line = count
        redeban_movement.save
        count = count + 1
      end
    end

    def saldos_diarios
      # Lee el nombre del archivo
      #
      # SA		    = 	fijo de 2 posiciones
      # XXXX	    = 	nombre del convenio de 4 posiciones
      # 20060801 	= 	fecha, de 8 posiciones
      # 01		    =	  consecutivo del convenio
      # .txt    	=	  archivo de texto
      @convenio = @file.original_filename[2, 4]
      @error_message.push("No existe convenio: #{@convenio}") unless @brand.exists_convenio_redeban? @convenio
      @fecha = @file.original_filename[6,8]
      # @consecutivo_convenio = @file.original_filename[14,2] /este campo no se encuentra en el nombre del archivo


      count = 0
      while @error_message.count == 0 && count < @numero_filas  do
        row = @txt[count].gsub(/\r/,'').gsub(/\n/,'')
        fields = row.split(',')
        if count == 0
          # Encabezado
          # Tipo                  - Carácter - 2	- 01 = Encabezado
          # Convenio              - Numérico - 4	- Código de Convenio
          # Fecha                 - Numérico - 8  - Fecha del archivo formato AAAAMMDD
          # Consecutivo           -	Numérico - 2  - Consecutivo de archivo ( empieza en 01)
          # No. Registros detalle -	Numérico - 6  - Número de registros de Detalle por el convenio
          # Filler                - Carácter - 17 - Relleno con espacios.
          @error_message.push('Encabezado - Tipo')  unless fields[0].to_s == '01'
          @error_message.push('Encabezado - Convenio')  unless fields[1].to_i == @convenio.to_i
          @error_message.push('Encabezado - Fecha')  unless fields[2].to_i == @fecha.to_i
          # @error_message.push('Encabezado - Consecutivo')  unless fields[3].to_i == @consecutivo_convenio.to_i
          @consecutivo_convenio = fields[3].to_i
          @error_message.push('Encabezado - No. Registros detalle')  unless fields[4].to_i == (@numero_filas - 1)

        else
          # Registros
          # 0.Tipo	                    - Carácter - 2	- 02 = registro Detalle
          # 1.No. BONO                  -	Numérico - 13 -	Número de CERTIFICADO
          # 2.Saldo inicial del BONO  	- Numérico - 12 -	Saldo de creación del  CERTIFICADO , se define con 10 enteros y dos decimales
          # 3.Fecha Vencimiento	        - Numérico - 8  - Fecha de vencimiento del CERTIFICADO  formato AAAAMMDD
          # 4.Estado                    -	Numérico - 2	- Estado del CERTIFICADO, Campo Codificado
          # 5.Valor del Saldo	          - Numérico - 12	- Valor del Saldo actual del CERTIFICADO, se define con 10 enteros y dos decimales
          @error_message.push("Error registro[#{count + 1}] - Tipo") unless fields[0].to_s == '02'
          certificate = @brand.redeban_codes.find_by_hash2 Digest::SHA3.hexdigest(fields[1].to_s)
          if certificate
            inicial = fields[2].to_s
            value_inicial = inicial[0,inicial.length - 2 ].to_i + "0.#{inicial[-2,2].to_s}".to_f
            @error_message.push("Registro[#{count + 1}] - Saldo inicial") unless certificate.batch.value == value_inicial
            @error_message.push("Registro[#{count + 1}] - Estado no existe (#{fields[4].to_i})") unless RedebanCode::STATES_ID[ fields[4].to_i ]
            saldo = fields[5].to_s
            value_saldo = saldo[0,saldo.length - 2 ].to_i + "0.#{saldo[-2,2].to_s}".to_f
            begin
              registro = certificate.redeban_daily_balances.build
              # registro.redeban_code_id = certificate.id
              registro.file_name = @file.original_filename
              registro.consecutivo = @consecutivo_convenio
              registro.saldo_inicial = value_inicial
              registro.estado = fields[4].to_i
              registro.valor_saldo = value_saldo
              registro.save!
            rescue
              @error_message.push("Registro[#{count + 1}] - No se guardo registro.")
            end

          else
            @error_message.push("Registro[#{count + 1}] - No existe No. Bono")
          end
        end
        count = count + 1
      end


    end


    def movimientos_detallados
      # Lee el nombre del archivo
      #
      # MV	      = 	fijo de 2 posiciones
      # XXXX      = 	nombre del convenio de 4 posiciones
      # 20060801  = 	fecha, de 8 posiciones
      # 01        =   consecutivo del convenio
      # .txt.     =   archivo de texto
      @convenio = @file.original_filename[2, 4]
      @error_message.push("No existe convenio: #{@convenio}") unless @brand.exists_convenio_redeban? @convenio
      @fecha = @file.original_filename[6,8]
      @consecutivo_convenio = @file.original_filename[14,2]
      if @error_message.count == 0
        RedebanDetailedMovementCertificate.transaction do
          begin
            count = 0
            while count < @numero_filas  do
              error = []
              row = @txt[count].gsub(/\r/,'').gsub(/\n/,'')
              # Registros
              # Nombre Convenio       -	Carácter  -	17  -	Nombre registrado del Convenio
              initial = 0
              positions = 0
              nombre_convenio = row[initial,positions = 16]  # la documentacion dice que hasta el numero 17 pero en pruebas llegaba 16
              # Código convenio       -	Numérico  -	4   -	Código de Convenio asignado al Comercio
              convenio = row[initial += positions,positions = 4].to_i
              error.push("Registro[#{count + 1}] - Convenio")  unless convenio == @convenio.to_i
              # No.Bono               -	Numérico  -	13  -	Número de CERTIFICADO
              certificate = @brand.redeban_codes.find_by_hash2 Digest::SHA3.hexdigest(row[initial += positions,positions = 13].to_s)
              # Valor inicial bono    -	Numérico  - 12  -	Valor del Saldo Inicial del CERTIFICADO, se define con 10 enteros y dos decimales
              saldo = row[initial += positions,positions = 12].to_s
              value = saldo[0,10 ].to_i + "0.#{saldo[10,2].to_s}".to_f
              error.push("Registro[#{count + 1}] - Valor inicial") unless certificate.batch.value == value

              # 20140901141626
              # Fecha Mvto.           - Numérico  -	8   -	Fecha de movimiento del CERTIFICADO  formato AAAAMMDD
              # Hora Mvto.            -	Numérico  -	6   -	Hora de movimiento del CERTIFICADO
              # fecha_mvto = row[initial += positions,positions = 8]
              # hora_mvto = row[initial += positions,positions = 6]
              fecha_hora_mvto = row[initial += positions,positions = 14]

              # Descripción establec. -	Carácter  -	17  -	Descripción del Establecimiento
              desc_establec = row[initial += positions,positions = 17]
              # Código establ.        -	Numérico  -	10  -	Código del Establecimiento
              codigo_establ = row[initial += positions,positions = 10]
              # Código Terminal       -	Numérico	- 8	  - Código de Terminal donde se hizo el movimiento
              codigo_terminal = row[initial += positions,positions = 8]
              # Tipo Transacción	    - Carácter	- 12  -	Descripción del movimiento realizado ( COMPRA, HABILITACION, CONSULTA)
              tipo_transaccion = row[initial += positions,positions = 12]
              # Valor Transacción     -	Numérico  -	12  -	Valor del  movimiento , se define con 10 enteros y dos decimales
              saldo = row[initial += positions,positions = 12]
              valor_transaccion = saldo[0,saldo.length - 2 ].to_i + "0.#{saldo[-2,2].to_s}".to_f
              # Numero Referencia     -	Numérico  -	12  -	Numero único del movimiento
              numero_referencia = row[initial += positions,positions = 12]

              if certificate
                movimiento = certificate.redeban_detailed_movement_certificates.build
              else
                error.push("Registro[#{count + 1}] - No existe No. Bono")
                movimiento = RedebanDetailedMovementCertificate.new
              end
              movimiento.update_attributes(
                  file_name: @file.original_filename,
                  nombre_convenio: nombre_convenio,
                  fecha_hora_movimiento: fecha_hora_mvto.to_time,
                  descripcion_movimiento: desc_establec,
                  codigo_establecimiento: codigo_establ,
                  codigo_terminal: codigo_terminal,
                  tipo_transacion: tipo_transaccion,
                  valor_transaccion: valor_transaccion,
                  num_referencia: numero_referencia,
                  error: error
              )
              count = count + 1
              @error_message.concat error
            end
          rescue ActiveRecord::Rollback
            @error_message.push "Error al guardar movimiento detallado del certificado #{count}"
          end
        end
      end
    end

    def consumo_diario
      # Este archivo es generado a diario, automáticamente por redeban tomando los datos entre las 00 hrs y las 23:59 hrs
      #
      # Lee el nombre del archivo
      #
      # CD		= 	fijo de 2 posiciones
      # XXXX	= 	nombre del convenio de 4 posiciones
      # 20060801 	= 	fecha, de 8 posiciones
      # 01		=	consecutivo del convenio
      # .txt. 	=	archivo de texto

      @convenio = @file.original_filename[2, 4]
      @error_message.push("No existe convenio: #{@convenio}") unless @brand.exists_convenio_redeban? @convenio
      @fecha = @file.original_filename[6,8]
      @consecutivo_convenio = @file.original_filename[14,2]

      count = 0
      RedebanDailyConsumption.transaction
        while count < @numero_filas  do
          row = @txt[count].gsub(/\r/,'').gsub(/\n/,'')
          fields = row.split(',')
          if count == 0
            # Encabezado
            # Tipo                    - Caracter  -	2   -	01 = Encabezado
            # Convenio                - Numérico  -	4   - Código de Convenio
            # Fecha                   - Numérico  -	8   - Fecha del archivo formato AAAAMMDD
            # Valor                   - Numérico  -	12  -	Valor total de los consumos por el convenio en el día, se define con 10 enteros y dos decimales
            # No. Registros detalle   - Numérico  -	6   - Número de registros de Detalle por el convenio
            @error_message.push('Encabezado - Tipo')  unless fields[0].to_s == '01'
            @error_message.push('Encabezado - Convenio')  unless fields[1].to_i == @convenio.to_i
            @error_message.push('Encabezado - Fecha')  unless fields[2].to_i == @fecha.to_i
            @error_message.push('Encabezado - Consecutivo')  unless fields[3].to_i == @consecutivo_convenio.to_i
            @error_message.push('Encabezado - No. Registros detalle')  unless fields[4].to_i == (@numero_filas - 1)
          else
            # Tipo                - Carácter  -	2   -	02 = registro Detalle
            # No. Bono	          - Numérico	- 13	- Número de CERTIFICADO
            # Valor del Consumo	  - Numérico  -	12  -	Valor del consumo del CERTIFICADO en el día, se define con 10 enteros y dos decimales
            # Fecha Vencimiento   -	Numérico  -	8   -	Fecha de vencimiento del CERTIFICADO formato AAAAMMDD
            # Estado	            - Numérico  -	2   -	Estado del CERTIFICADO, Campo Codificado
            # Valor del Saldo	    - Numérico	- 12  -	Valor del Saldo del CERTIFICADO, se define con 10 enteros y dos decimales


            @error_message.push("Error registro[#{count + 1}] - Tipo") unless fields[0].to_s == '02'
            certificate = @brand.redeban_codes.find_by_hash2 Digest::SHA3.hexdigest(fields[1].to_s)
            if certificate
              # Valor del consumo
              saldo = fields[2].to_s
              valor_consumo = saldo[0,saldo.length - 2 ].to_i + "0.#{saldo[-2,2].to_s}".to_f

              fecha_vencimiento = fields[3].to_date

              @error_message.push("Registro[#{count + 1}] - Estado") unless RedebanCode::STATES_ID[ fields[4].to_i ]

              # Valor del saldo
              saldo = fields[5].to_s
              valor_saldo = saldo[0,saldo.length - 2 ].to_i + "0.#{saldo[-2,2].to_s}".to_f

            else
              @error_message.push("Registro[#{count + 1}] - No existe No. Bono")
            end
            movimiento = RedebanDailyConsumption.new
            movimiento.update_attribute(
                file_name: @file.original_filename,
                valor_de_consumo: valor_consumo,
                fecha_vencimeinto:fecha_vencimiento,
                estado: fields[4].to_i,
                valor_del_saldo: valor_saldo,
                error:  @error_message
            )

            # if fields[4].to_i == 05 || fields[4].to_i == 06
            #   pin = fields[1].to_s
            #   tpe = ThirdPartyEntity.find_by_name ThirdPartyEntity::ENTITY[:giftcode]
            #   brand = certificate.gift_code.campaign.brand_codes.find_by_third_party_entity_id tpe.id
            #   transDate = Time.zone.now.strftime("%Y%m%d%H%M%S")
            #   key= "#{brand}|0|#{pin}|#{valor_consumo}|COP|#{tpe.incocredito_id}|#{transDate}||||#{tpe.secret}"
            #   firm = Digest::MD5.hexdigest(key)
            #   tp = ThirdParty.new
            #   tp.redemption({ commerceId: tpe.incocredito_id, brandCode: brand, termId: '', pin: pin, transactionId: 0, value: valor_consumo, currency: 'CO', transDate: transDate, firm: firm }, request.remote_ip)
            # end

          end

          count = count + 1

        end

      end

    end
