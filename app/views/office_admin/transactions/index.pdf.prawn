data = @transactions.collect {|t|
    [ t.id,
      t.gift_code.to_email,
      t.cost,
      t.authorization_number,
      t.created_at.to_s,
      t.updated_at.to_s
    ]
}
data.unshift [ 'id','to email','cost','authorization','created at', 'update at']

prawn_document(:page_layout => :landscape) do |pdf|
    pdf.table(data) do
        style(row(0), :background_color => '559C43')
    end
end