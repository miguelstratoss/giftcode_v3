data = @campaigns.collect {|c|
    [ c.id,
      c.name,
      c.country.name,
      c.gift_cards.count,
      c.currency.short_name,
      c.campaign_state.name,
      c.created_at.to_s
    ]
}
data.unshift [ 'id','campaing','country', 'giftcards', 'currency', 'state', 'created at']

prawn_document(:page_layout => :landscape) do |pdf|
    pdf.table(data) do
        style(row(0), :background_color => '559C43')
    end
end