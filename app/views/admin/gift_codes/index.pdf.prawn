data = @gift_codes.collect {|gc|
    [ gc.id,
      gc.type,
      (!gc.is_a_free_gift_code? ? gc.from_email : "N/A" ),
      gc.to_email,
      gc.iva,
      gc.cost,
      gc.gift_code_state.name
    ]
}
data.unshift [ 'id','type','from_email','to_email','iva', 'cost', 'state']
prawn_document(:page_layout => :landscape) do |pdf|
    pdf.table(data) do
        style(row(0), :background_color => '559C43')
    end
end