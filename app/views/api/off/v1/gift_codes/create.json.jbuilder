json.extract! @gift_code, :send_notifications_now, :from_first_name, :from_last_name, :from_email, :to_first_name, :to_last_name, :to_mobile_phone, :to_email, :gift_card_id, :body_message, :ref_sale, :to_address, :to_state, :to_city
json.entity @entity.name
json.cost_current  @gift_code.gift_card.cost_current