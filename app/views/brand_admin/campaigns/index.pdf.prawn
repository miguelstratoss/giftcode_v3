data = @campaigns.collect {|c|
    [ c.id,
      c.brand.name,
      c.name,
      c.country.name,
      c.send_mail.to_s,
      c.send_sms.to_s,
      c.currency.short_name,
      c.campaign_state.name,
      c.gift_code_total( "office"),
      c.gift_code_total( "web"),
      c.created_at.to_s
    ]
}
data.unshift [ 'id','brand','campaing','country', 'send mail', 'send sms', 'currency', 'state', 'giftcode office', 'giftcode web', 'created at']

prawn_document(:page_layout => :landscape) do |pdf|
    pdf.table(data) do
        style(row(0), :background_color => '559C43')
    end
end