data = @admin_users.collect {|u|
    [ u.id,
      u.email,
      u.office.to_s,
      u.created_at.to_s
    ]
}
data.unshift [ 'id','email','office','created at']

prawn_document(:page_layout => :landscape) do |pdf|
    pdf.table(data) do
        style(row(0), :background_color => '559C43')
    end
end