xml.gift_codes :type => :array do
   @gift_codes.each do |gift_code|
     xml.gift_code do
       xml.tag!('id', gift_code.id)
       xml.tag!('type', gift_code.type)
       xml.tag!('form_name',
          if !gift_code.is_a_free_gift_code?
            gift_code.from_first_name + ' ' + gift_code.from_last_name
          else
            "N/A"
          end
       )
       xml.tag!('from_email',
        if !gift_code.is_a_free_gift_code?
          gift_code.from_email
        else
          "N/A"
        end
       )
       xml.tag!('to_name', gift_code.to_first_name + ' ' + gift_code.to_last_name )
       xml.tag!('to_email', gift_code.to_email )
       xml.tag!('base_iva', gift_code.base_iva )
       xml.tag!('iva', gift_code.iva )
       xml.tag!('cost', gift_code.cost )
       xml.tag!('available_balance', gift_code.available_balance )
       xml.tag!('currency', gift_code.currency.complete_name )
       xml.tag!('email_send_to_buyer',
        if !gift_code.is_a_free_gift_code?
          gift_code.email_sent_to_buyer
        else
          "N/A"
        end
       )
       xml.tag!('sms_send_to_receiver',
        if !gift_code.is_a_free_gift_code?
          gift_code.sms_sent_to_receiver
        else
          "N/A"
        end
       )
       xml.tag!('admin_user',
        if gift_code.admin_user.nil?
          "N/A"
        else
          gift_code.admin_user.email
        end
       )
       xml.tag!('email_sent_to_receiver', gift_code.email_sent_to_receiver)
       xml.tag!('gift_code_state', gift_code.gift_code_state.name)
       xml.tag!('created_at', gift_code.created_at)
     end
   end
end