json.array! @gift_codes do |gift_code|
    json.id  gift_code.id
    json.type gift_code.type
    json.form_name (
        if !gift_code.is_a_free_gift_code?
            gift_code.from_first_name + ' ' + gift_code.from_last_name
        else
            "N/A"
        end
    )
    json.form_email (
        if !gift_code.is_a_free_gift_code?
            gift_code.from_email
        else
            "N/A"
        end
    )
    json.to_name ( gift_code.to_first_name + ' ' + gift_code.to_last_name )
    json.to_email ( gift_code.to_email )
    json.base_iva ( gift_code.base_iva )
    json.iva ( gift_code.iva )
    json.cost (gift_code.cost )
    json.available_balance (gift_code.available_balance )
    json.email_send_to_buyer (
        if !gift_code.is_a_free_gift_code?
            gift_code.email_sent_to_buyer
        else
            "N/A"
        end
    )
    json.sms_send_to_receiver(
        if !gift_code.is_a_free_gift_code?
            gift_code.sms_sent_to_receiver
        else
            "N/A"
        end
    )
    json.admin_user (
        if gift_code.admin_user.nil?
            "N/A"
        else
            gift_code.admin_user.email
        end
    )
    json.email_sent_to_receiver (gift_code.email_sent_to_receiver)
    json.gift_code_state (gift_code.gift_code_state.name)
    json.created_at (gift_code.created_at)
end
