data = @gift_codes.collect {|gc|
    [ gc.id,
      gc.type,
      (!gc.is_a_free_gift_code? ? gc.from_email : "N/A" ),
      gc.to_email,
      gc.iva,
      gc.cost,
      gc.gift_code_state.name,
      gc.created_at.to_s
    ]
}
data.unshift [ 'id','type','from email','to email', 'iva', 'cost', 'state', 'created at']

prawn_document(:page_layout => :landscape) do |pdf|
    pdf.table(data) do
        style(row(0), :background_color => '559C43')
    end
end