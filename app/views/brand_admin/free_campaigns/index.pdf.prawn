data = @free_campaigns.collect {|fc|
    [ fc.id,
      fc.office.office_name,
      fc.cost.to_s,
      fc.valid_for_days,
      fc.gift_codes.count,
      fc.created_at.to_s
    ]
}
data.unshift [ 'id','office','cost','valid for days', 'giftcodes sent', 'created at']

prawn_document(:page_layout => :landscape) do |pdf|
    pdf.table(data) do
        style(row(0), :background_color => '559C43')
    end
end