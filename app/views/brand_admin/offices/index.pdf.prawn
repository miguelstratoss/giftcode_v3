data = @offices.collect {|o|
    [ o.id,
      o.office_name,
      o.office_address,
      o.city.complete_name,
      o.city.created_at.to_s
    ]
}
data.unshift [ 'id','office','address','city', 'created at']

prawn_document(:page_layout => :landscape) do |pdf|
    pdf.table(data) do
        style(row(0), :background_color => '559C43')
    end
end