class MixpanelJob
  @queue = :mixpanel

  def self.perform(json)
    mixpanel = Mixpanel::Consumer.new
    mixpanel.send(*JSON.load(json))
  end

end