# encoding: UTF-8
class GiftCodeNotifier < ActionMailer::Base
  #This is very important for SendGrid
  default :from => "Giftcode <giftcode@giftcode.co>"

  def buyer_gift_code(gift_code)
    @gift_code = gift_code
    if @gift_code.is_a_free_gift_code?
      @brand = @gift_code.free_campaign.office.brand
    else
      if @gift_code.type == "PUNTOS"
        @brand = @gift_code.campaign_point.brand
      else
        @brand = @gift_code.gift_card.campaign.brand
      end
    end

    mail :to => gift_code.from_email, :subject => GlobalConstants::env.to_s + 'Enviaste un bono de regalo de ' +@brand.name
  end

  def buyer_gift_code_send_pending(gift_code)
    @gift_code = gift_code
    if @gift_code.is_a_free_gift_code?
      @brand = @gift_code.free_campaign.office.brand
    else
      if @gift_code.type == "PUNTOS"
        @brand = @gift_code.campaign_point.brand
      else
        @brand = @gift_code.gift_card.campaign.brand
      end
    end
    mail :to => gift_code.from_email, :subject => GlobalConstants::env.to_s + 'Enviaste un bono de regalo de ' +@brand.name
  end

  def buyer_gift_code_whitelabel(gift_code)
    @gift_code = gift_code
    if @gift_code.is_a_free_gift_code?
      @brand = @gift_code.free_campaign.office.brand
    else
      if @gift_code.type == "PUNTOS"
        @brand = @gift_code.campaign_point.brand
      else
        @brand = @gift_code.gift_card.campaign.brand
      end
    end
    mail :to => gift_code.from_email, :subject => GlobalConstants::env.to_s + 'Enviaste un bono de regalo de ' +@brand.name
  end

  def buyer_gift_code_whitelabel_send_pending(gift_code)
    @gift_code = gift_code
    if @gift_code.is_a_free_gift_code?
      @brand = @gift_code.free_campaign.office.brand
    else
      if @gift_code.type == "PUNTOS"
        @brand = @gift_code.campaign_point.brand
      else
        @brand = @gift_code.gift_card.campaign.brand
      end
    end
    mail :to => gift_code.from_email, :subject => GlobalConstants::env.to_s + 'Enviaste un bono de regalo de ' +@brand.name
  end

  def receiver_gift_code(gift_code)
    @gift_code = gift_code
    if @gift_code.is_a_free_gift_code?
      @brand = @gift_code.free_campaign.office.brand
      @campaign =  @gift_code.free_campaign
    else
      if @gift_code.type == "PUNTOS"
        @brand = @gift_code.campaign_point.brand
        @campaign =  @gift_code.campaign_point
      else
        @brand = @gift_code.gift_card.campaign.brand
        @campaign =  @gift_code.gift_card.campaign
      end
    end
    unless @gift_code.binary_qr_code_image
      @qr_code = ChunkyPNG::Image.from_blob(File.read(Rails.root.to_s + '/app/assets/images/icono_grande.png')).to_data_url
    else
      @qr_code = ChunkyPNG::Image.from_blob(@gift_code.binary_qr_code_image).to_data_url
    end
    unless Rails.env.development?
      if @brand.image_email_url(:thumb180).to_s != "/assets/brand_195x195_round.png"
        @image_brand = ChunkyPNG::Image.from_blob(open( @brand.image_email_url(:thumb180).to_s ){|f| f.read }).to_data_url
      end
    end
    @barcode = ChunkyPNG::Image.from_blob(@gift_code.barcode_image).to_data_url
    kit = IMGKit.new( '<html><body style="background: #F0F0E0;">' + render_to_string(:partial => 'gift_codes/show_version_email') + '</body></html>', quality: 30, width: 600)
    kit.stylesheets << Rails.root.join("app/assets/stylesheets/", "application-email.css").to_s
    kit.stylesheets << Rails.root.join("app/assets/stylesheets/", "style-email.css.1.old").to_s
    blob = kit.to_png
    attachments.inline['gift_code.png'] = blob
    attachments['gift_code.png'] = blob
    mail :to => gift_code.to_email, :subject => GlobalConstants::env.to_s + 'Recibiste un bono de regalo de ' + @gift_code.from_first_name + ' ' + @gift_code.from_last_name
  end

  def receiver_gift_code_whitelabel(gift_code)
    @gift_code = gift_code
    if @gift_code.is_a_free_gift_code?
      @brand = @gift_code.free_campaign.office.brand
    else
      if @gift_code.type == "PUNTOS"
        @brand = @gift_code.campaign_point.brand
      else
        @brand = @gift_code.gift_card.campaign.brand
      end
    end
    ##@qr_code = ChunkyPNG::Image.from_blob(@gift_code.binary_qr_code_image).to_data_url
    kit = IMGKit.new( '<html><body style="background: #F0F0E0;">' + render_to_string(:partial => 'gift_codes/show_version_email') + '</body></html>', quality: 30, width: 600)
    kit.stylesheets << Rails.root.join("app/assets/stylesheets/", "application-email.css").to_s
    kit.stylesheets << Rails.root.join("app/assets/stylesheets/", "style-email.css.1.old").to_s
    blob = kit.to_png
    attachments.inline['gift_code.png'] = blob
    mail :to => gift_code.to_email, :subject => GlobalConstants::env.to_s + 'Recibiste un bono de regalo de ' +@brand.name
  end

  # def receiver_free_gift_code(gift_code)
  #   #!--NOTIFICACION SIN MODIFICACION DE DISEÑOS--!
  #   @gift_code = gift_code
  #   attachments.inline['qr_code.png'] = ChunkyPNG::Image.from_blob(@gift_code.binary_qr_code_image).to_blob
  #   #attachments["logo.png"] = File.read("./public/assets/logo_giftcode.png")
  #   #attachments["qr_code.png"] = File.read("./public/images/qr_codes/"+@gift_code.id.to_s+".png")
  #   #attachments.inline["qr_code.png"] = ChunkyPNG::Image.from_blob(gift_code.binary_qr_code_image).to_blob
  #   mail :to => gift_code.to_email, :subject => GlobalConstants::env.to_s + 'Recibiste un GiftCode'
  # end

  def receiver_gift_code_after_a_transaction(gift_code)
    @gift_code = gift_code
    if @gift_code.is_a_free_gift_code?
      @brand = @gift_code.free_campaign.office.brand
    else
      if @gift_code.type == "PUNTOS"
        @brand = @gift_code.campaign_point.brand
      else
        @brand = @gift_code.gift_card.campaign.brand
      end
    end
    mail :to => gift_code.to_email, :subject => GlobalConstants::env.to_s + 'Realizaste una compra con un GiftCode'
  end

  def welcome(user)
    @user = user
    @campaigns = CampaignPoint.where( "enabled = :enabled and end_date >= :today", { enabled: true, today: Time.zone.now }  ).joins("LEFT JOIN gift_codes ON gift_codes.type = 'PointGiftCode' AND gift_codes.campaign_point_id = campaigns.id").group("campaigns.id").having("count(campaigns.id) < campaigns.number_of_free_gift_codes").order("points_required ASC")
    mail :to => user.email, :subject => GlobalConstants::env.to_s + 'Bienvenid@ a Giftcode'
  end

  def contact_message(contact)
    @contact = contact
    mail :to => 'contacto@giftcode.co', :subject => GlobalConstants::env.to_s + 'Mensaje de contacto'
  end

  def referrals(data)
    @data = data
    subject = 'Tienes ' + @data[:count].to_s + ' ' + ( ( @data[:count] == 1 ) ? 'referido' : 'referidos').to_s
    mail :to => data[:user].email , :subject => GlobalConstants::env.to_s + subject
  end

  def redeem(user, campaigns )
    @user = user
    @campaigns = campaigns
    # CampaignPoint.where( "enabled = :enabled and end_date >= :today", { enabled: true, today: Time.zone.now }  ).joins("LEFT JOIN gift_codes ON gift_codes.type = 'PointGiftCode' AND gift_codes.campaign_point_id = campaigns.id").group("campaigns.id").having("count(campaigns.id) < campaigns.number_of_free_gift_codes").where("points_required <= ?", user.available_points ).order("points_required ASC").limit(3)
    mail :to => user.email , :subject => GlobalConstants::env.to_s + 'Ya puedes redimir giftcodes'
  end

  def friends(user,friends)
    @user = user
    @friends = []
    friends.each { |f| @friends.push(f.first_name + ' ' + f.last_name) }
    #"Las marcas favoritas de #{ ( friends.count > 1 ) ? 'tus amigos' : ('tu amigo ' + friends.first.first_name + ' ' + friends.first.last_name ) }"
    if @friends.length > 1
      subject = "Las marcas favoritas de tus amigos."
    else
      subject = "#{friends.first.first_name} #{friends.first.last_name} acaba de unirse a giftcode."
    end
      mail :to => user.email, :subject => GlobalConstants::env.to_s + subject
  end

  def favorite_brands(user)
    @user = user
    mail :to => user.email, :subject => GlobalConstants::env.to_s + 'Tus amigos no saben qué regalarte.'
  end

  def error_pin( giftcode )
    @giftcode =  giftcode
    mail :to => 'contacto@giftcode.co', :subject => GlobalConstants::env.to_s + 'Error, se genero un giftcode sin pin valido.'
  end

  def confirmation_email(user)
    @user = user
    mail :to => user.email_fbk_or_temp, :subject => GlobalConstants::env.to_s + 'Confirma tu correo.'
  end

  def warning_redeban_codes_less_100 brand
    @brand = brand
    mail :to => @brand.contact_email, :subject => GlobalConstants::env.to_s + @brand.name + ' se esta agotando tus bonos electronicos de regalo.'
  end

  def warning_redeban_codes_being_rapidly_depleted brand
    @brand = brand
    mail :to => @brand.contact_email, :subject => GlobalConstants::env.to_s + 'Se han vendido 5 giftcodes de ' + @brand.name + ' en la ultima hora.'
  end

  def error_bonos gift_code_id
    @gift_code = GiftCode.find_by_id gift_code_id
    if @gift_code.is_a_free_gift_code?
      @brand = @gift_code.free_campaign.office.brand
      @campaign =  @gift_code.free_campaign
    else
      if @gift_code.type == "PUNTOS"
        @brand = @gift_code.campaign_point.brand
        @campaign =  @gift_code.campaign_point
      else
        @brand = @gift_code.gift_card.campaign.brand
        @campaign =  @gift_code.gift_card.campaign
      end
    end
    unless @gift_code.binary_qr_code_image
      @qr_code = ChunkyPNG::Image.from_blob(File.read(Rails.root.to_s + '/app/assets/images/icono_grande.png')).to_data_url
    else
      @qr_code = ChunkyPNG::Image.from_blob(@gift_code.binary_qr_code_image).to_data_url
    end
    unless Rails.env.development?
      if @brand.image_email_url(:thumb180).to_s != "/assets/brand_195x195_round.png"
        @image_brand = ChunkyPNG::Image.from_blob(open( @brand.image_email_url(:thumb180).to_s ){|f| f.read }).to_data_url
      end
    end
    @barcode = ChunkyPNG::Image.from_blob(@gift_code.barcode_image).to_data_url
    kit = IMGKit.new( '<html><body style="background: #F0F0E0;">' + render_to_string(:partial => 'gift_codes/show_version_email') + '</body></html>', quality: 30, width: 600)
    kit.stylesheets << Rails.root.join("app/assets/stylesheets/", "application-email.css").to_s
    kit.stylesheets << Rails.root.join("app/assets/stylesheets/", "style-email.css.1.old").to_s
    blob = kit.to_png
    attachments.inline['gift_code.png'] = blob
    attachments['gift_code.png'] = blob
    mail :to => "m.segura@greencode.com.co", :subject => GlobalConstants::env.to_s + 'Recibiste un bono de regalo de ' + @gift_code.from_first_name + ' ' + @gift_code.from_last_name
  end

end
