ActiveAdmin.register InviteFriend do
  config.sort_order = "id_desc"
  menu :parent => 'Monitoring user'
  actions :index, :show

  filter :user_id, :label => 'user id', :as => :numeric
  filter :friend_id, :label => 'friend id', :as => :numeric
  filter :created_at
  filter :updated_at

  show do |invite_friend|
    attributes_table do
      row :id
      row :user
      row :created_at
      row :updated_at
    end
  end

  index do
    column :id
    column :user
    column :friend
    column :invitation_used do |invite_friend|
      invite_friend.used?
    end
    column :fbk_post_id
    column :created_at
    column :updated_at
    default_actions
  end
end
