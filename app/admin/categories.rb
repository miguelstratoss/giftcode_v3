ActiveAdmin.register Category do

  config.sort_order = "id_asc"
  menu :priority => 7
  actions :index, :show, :new, :create, :edit, :update

  form do |f|
    f.inputs do
      f.input :name
      f.input :position
      f.input :enabled
      f.input :_class
    end
    f.actions
  end

  show do |category|
    attributes_table do
      row :id
      row :name
      row :position
      row :enabled
      row :_class
      row :brand_number do
        category.brands.count
      end
    end

    panel "Brands Associated" do
      table_for(category.brand_categories) do
        column("brand id", :sortable => :id) { |brand_category| link_to "#{brand_category.brand.id}", admin_brand_path(brand_category.brand) }
        column("brand name") { |brand_category| brand_category.brand.name }
        column("association created at") { |brand_category| brand_category.created_at }
        column("association updated at") { |brand_category| brand_category.updated_at }
        column("") { |brand_category| link_to "View Brand", admin_brand_path(brand_category.brand)}
        column("") { |brand_category| link_to "Edit Association", edit_admin_category_brand_category_path(brand_category)}
      end
    end
  end

  index do
    column :id
    column :name
    column :position
    column :enabled
    column :_class
    column :brand_number do |category|
      category.brands.count
    end
    column :created_at
    column :updated_at
    default_actions
  end


  action_item only:[:show] do
    link_to "New BrandCategory", new_admin_category_brand_category_path(category)
  end

end
