ActiveAdmin.register CarouselBrand do

  menu :parent => "More options"

  index do
    column :id
    column :brand_id do |carousel_brand|
      carousel_brand.brand.name
    end
    column :position
    default_actions
  end


end