ActiveAdmin.register GiftCode do
  menu :parent => 'Monitoring user'
  actions :show, :index

  filter :from_user_id, :label => 'user id', :as => :numeric
  filter :to_user_id, :label => 'to user id', :as => :numeric
  filter :to_city
  filter :gift_code_state
  filter :free_campaign
  filter :admin_user
  filter :cost
  filter :to_user_name
  filter :to_mobile_phone
  filter :to_email
  filter :valid_for_days
  filter :title_message
  filter :body_message
  filter :content_qr_code
  filter :available_balance
  filter :token
  filter :view_id
  filter :created_at
  filter :updated_at
  filter :iva
  filter :base_iva
  filter :ref_sale
  filter :redemption_pin
  filter :send_notifications_now
  filter :send_date_time

  scope 'Web',:all_web
  scope 'Office', :all_offices
  scope 'Free', :all_free
  scope 'External', :all_web_external
  scope 'Pay Accepted', :in_all_accepted_state
  scope 'Pay Rejected',:in_payment_rejected_state
  scope 'e-sent Buyer',:_email_sent_to_buyer
  scope 'e-sent Receiver',:_email_sent_to_receiver

  show do |gift_code|
    attributes_table do
      row :id
      row :brand do
        if !gift_code.is_a_free_gift_code?
          link_to gift_code.gift_card.campaign.brand.name, admin_brand_path(gift_code.gift_card.campaign.brand)
        else
          link_to gift_code.free_campaign.office.brand.name, admin_brand_path(gift_code.free_campaign.office.brand)
        end
      end
      row :office do
        if !gift_code.office
          "NOT APPLY"
        elsif !gift_code.is_a_free_gift_code?
          "NOT APPLY"
        else
          link_to gift_code.free_campaign.office.office_name, admin_brand_office_path(gift_code.free_campaign.office.brand, gift_code.free_campaign.office)
        end
      end
      row :campaign do
        if !gift_code.is_a_free_gift_code?
          link_to gift_code.gift_card.campaign.name, admin_brand_campaign_path(gift_code.gift_card.campaign.brand, gift_code.gift_card.campaign)
        else
          link_to "Free Campaign #"+gift_code.free_campaign.id.to_s, admin_free_campaign_path(gift_code.free_campaign)
        end
      end
      row :gift_card do
        if !gift_code.is_a_free_gift_code?
          link_to gift_code.gift_card.id, admin_campaign_gift_card_path(gift_code.gift_card.campaign, gift_code.gift_card)
        else
          "NOT APPLY"
        end
      end
      row :gift_code_state
      row :from_user do
        if !gift_code.is_a_free_gift_code?
          link_to gift_code.from_email, admin_user_path(gift_code.from_user_id)
        else
          "NOT APPLY"
        end
      end
      row :to_user do
          link_to gift_code.to_email, admin_user_path(gift_code.to_user_id)
      end
      row "to_user_name" do
        if gift_code.to_first_name || gift_code.to_last_name
          gift_code.to_first_name + " " + gift_code.to_last_name
        else
          "BENEFICIARIO"
        end
      end
      row :to_mobile_phone do
        if !gift_code.is_a_free_gift_code?
          gift_code.to_mobile_phone
        else
          "NOT APPLY"
        end
      end
      row :to_email
      row :title_message
      row :body_message
      row :ref_sale do
        if !gift_code.is_a_free_gift_code?
          gift_code.ref_sale
        else
          "NOT APPLY"
        end
      end
      row :base_iva do
        if !gift_code.is_a_free_gift_code?
          gift_code.base_iva
        else
          "NOT APPLY"
        end
      end
      row :iva do
        if !gift_code.is_a_free_gift_code?
          gift_code.iva
        else
          "NOT APPLY"
        end
      end
      row :cost
      row :available_balance
      row :valid_for_days
      row :to_city
      row :email_sent_to_buyer do
        if !gift_code.is_a_free_gift_code?
          gift_code.email_sent_to_buyer
        else
          "NOT APPLY"
        end
      end
      row :email_sent_to_receiver
      row :sms_sent_to_receiver do
        if !gift_code.is_a_free_gift_code?
          gift_code.sms_sent_to_receiver
        else
          "NOT APPLY"
        end
      end
      row :sent_by_admin_user do
        if !gift_code.is_a_free_gift_code?
          "NOT APPLY"
        else
          link_to gift_code.admin_user.email, admin_admin_user_path(gift_code.admin_user) if gift_code.admin_user.present?
        end
      end
      row :send_notifications_now
      row :send_date_time
      row :created_at
      row :updated_at
    end
    panel "Transactions" do
      table_for(gift_code.transactions.ordered_by_created_at_desc) do
        column("id") { |transaction| link_to transaction.id, admin_transaction_path(transaction) }
        column("consecutive") { |transaction| transaction.consecutive }
        column("cost") { |transaction| number_to_currency(transaction.cost, :separator => ",", :delimiter => ".", :precision => 2, :locale => :en) }
        column("office") { |transaction| transaction.created_at_office if transaction.created_at_office.present? }
        column("created_at") { |transaction| I18n.l(transaction.created_at, :format => :long) }
        column("") { |transaction| link_to "View", admin_transaction_path(transaction)}
      end
    end

    panel "Third Party Transactions" do
      table_for(gift_code.third_party_transactions.order("created_at DESC")) do
        column :id do |tpt|
          link_to tpt.id, admin_third_party_transaction_path(tpt.id)
        end
        column :third_party_entity_id, :sortable => 'third_party_transactions.third_party_entity_id' do |tpt|
          (tpt.third_party_entity_id) ? link_to( tpt.third_party_entity.name ,  admin_third_party_entity_path(:id=>tpt.third_party_entity_id) ): ''
        end
        column :tp_transaction_type_id, :sortable => 'third_party_transactions.tp_transaction_type_id' do |tpt|
          (tpt.tp_transaction_type_id) ? tpt.tp_transaction_type.name : ''
        end
        column :tp_transaction_state_id, :sortable => 'third_party_transactions.tp_transaction_state_id' do |tpt|
          (tpt.tp_transaction_state_id) ? tpt.tp_transaction_state.name : ''
        end
        column :brand_code_id, :sortable => 'third_party_transactions.brand_code_id' do |tpt|
          (tpt.third_party_entity_id && tpt.brand_code_id) ? link_to( tpt.brand_code.to_s, admin_third_party_entity_brand_code_path(:third_party_entity_id=>tpt.third_party_entity_id, :id => tpt.brand_code_id) ): ''
        end
        column :new_balance
        column :dir_ip
        column :terminal_id
        column :created_at
        column :related_transaction_id
        column :reversed
        column :confirmed
        column :office_id, :sortable => 'third_party_transactions.office_id' do |tpt|
          (tpt.office_id) ? link_to( tpt.office, admin_brand_office_path(:brand_id => tpt.brand_code.brand,:id => tpt.office_id) ) : ''
        end
      end
    end

    panel "Request Third Party" do
      table_for(gift_code.request_third_party_entities.order("created_at DESC")) do
        column :id do |rtp|
          link_to rtp.id, admin_request_third_party_entity_path(rtp)
        end
        column :third_party_entity, :sortable => :third_party_entity_id
        column :response_code
        column :response_message
        column :state
      end
    end

    panel "Payment Answers" do
      table_for(gift_code.payment_answers.ordered_by_created_at_desc) do
        column("id") { |payment_answer| link_to payment_answer.id, admin_payment_answer_path(payment_answer) }
        column("pol_state") { |payment_answer| payment_answer.pol_state }
        column("final_result") { |payment_answer| payment_answer.final_result }
        column("cost") { |payment_answer| payment_answer.cost }
        column("iva") { |payment_answer| payment_answer.iva }
        column("created_at") { |payment_answer| I18n.l(payment_answer.created_at, :format => :long) }
        column("") { |payment_answer| link_to "View", admin_payment_answer_path(payment_answer)}
      end
    end
    panel "Payment Confirmations" do
      table_for(gift_code.payment_confirmations.ordered_by_created_at_desc) do
        column("id") { |payment_confirmation| link_to payment_confirmation.id, admin_payment_confirmation_path(payment_confirmation) }
        column("pol_state") { |payment_confirmation| payment_confirmation.pol_state }
        column("final_result") { |payment_confirmation| payment_confirmation.final_result }
        column("cost") { |payment_confirmation| payment_confirmation.cost }
        column("iva") { |payment_confirmation| payment_confirmation.iva }
        column("created_at") { |payment_confirmation| I18n.l(payment_confirmation.created_at, :format => :long) }
        column("") { |payment_confirmation| link_to "View", admin_payment_confirmation_path(payment_confirmation)}
      end
    end
    panel "Downloads" do
      table_for(gift_code.downloads) do
        column("id") { |download| download.id }
        column("user") { |download| link_to download.user.email, admin_user_path(download.user) if download.user.present? }
        column("download_type") { |download| download.download_type }
        column("created_at") { |download| I18n.l(download.created_at, :format => :long) }
        column("") { |download| link_to "View", admin_download_path(download)}
      end
    end
    # active_admin_comments
  end

  index :download_links => [:csv, :xml, :json, :pdf] do
    column :id
    column :type, :sortable => 'gift_codes.gift_code_state_id'
    column :from_user do |gift_code|
      if !gift_code.is_a_free_gift_code?
        link_to "#{gift_code.from_email}", admin_user_path(gift_code.from_user_id)
      else
        "N/A"
      end
    end
    column :to_user do |gift_code|
      link_to_if gift_code.to_user_id.present?,"#{gift_code.to_email}", admin_user_path(gift_code.to_user_id)
    end
    column :cost
    #column :download_number do |gift_code|
    #  gift_code.downloads.count
    #end
    column :gift_code_state
    column :email_sent_to_receiver
    column :created_at
    column :updated_at
    default_actions

  end

  controller do
    def index
      index! do |format|
        format.pdf{
          render 'index.pdf.prawn', :layout => false
        }
      end
    end
  end


  csv do
    column :id
    column :type
    column ('form_name') do |gift_code|
      if !gift_code.is_a_free_gift_code?
        gift_code.from_first_name + ' ' + gift_code.from_last_name
      else
        "N/A"
      end
    end
    column ('from_email') do |gift_code|
      if !gift_code.is_a_free_gift_code?
        gift_code.from_email
      else
        "N/A"
      end
    end
    column ('to_name') { |gift_code| gift_code.to_first_name + ' ' + gift_code.to_last_name }
    column :to_email
    column :base_iva
    column :iva
    column :cost
    column :available_balance
    column ('email_send_to_buyer') do |gift_code|
      if !gift_code.is_a_free_gift_code?
        gift_code.email_sent_to_buyer
      else
        "N/A"
      end
    end
    column ('sms_send_to_receiver')  do |gift_code|
      if !gift_code.is_a_free_gift_code?
        gift_code.sms_sent_to_receiver
      else
        "N/A"
      end
    end
    column ('admin_user') do |gift_code|
      if gift_code.admin_user.nil?
        "N/A"
      else
        gift_code.admin_user.email
      end
    end
    column :email_sent_to_receiver
    column ('gift_code_state') {|gift_code| gift_code.gift_code_state.name}
    column :created_at
  end

end