ActiveAdmin.register Delayed::Job do
  menu :parent => "More options"
  actions :index, :show

  index do
    column :id
    column :priority
    column :attempts
    column :last_error
    column :run_at
    column :locked_at
    column :failed_at
    column :locked_by
    column :queue
    column :created_at
    column :updated_at
    default_actions
  end
end
