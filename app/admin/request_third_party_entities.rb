ActiveAdmin.register RequestThirdPartyEntity do

  menu :parent => "More options"
  actions :show, :index

  filter :gift_code_id, :label => 'gift code id', :as => :numeric
  filter :third_party_entity
  filter :create_at
  filter :update_at
  filter :response_code
  filter :response_message
  filter :state

  index do
    column :id
    column :third_party_entity, :sortable => :third_party_entity_id
    column :gift_code, :sortable => :gift_code_id
    column :response_code
    column :response_message
    column :state
    default_actions
  end

end