ActiveAdmin.register AdminControlAccessAlert do

  config.sort_order = "id_asc"
  menu :parent => 'Monitoring user'
  actions :index, :show

  index do
    column :id
    column :admin_user
    column :alert_for_access_path
    column :created_at
    column :updated_at
    default_actions
  end
  
end
