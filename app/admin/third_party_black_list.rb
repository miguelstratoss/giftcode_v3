ActiveAdmin.register ThirdPartyBlackList do
  menu :parent => "More options"
  #config.sort_order = "id_asc"
  actions :index, :show, :new, :create, :edit, :update

  #scope :exclude_event,
  #      lambda  { |option|
  #        puts option.inspect
  #      }

  #search_method :exclude_event

  filter :blocked
  filter :dir_ip
  filter :created_at
  filter :updated_at

  index :download_links => false do
    column :id
    column :dir_ip
    column :blocked
    column :created_at
    column :updated_at
    default_actions
  end

end