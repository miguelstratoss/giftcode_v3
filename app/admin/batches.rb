ActiveAdmin.register Batch do

  actions :index, :show, :new, :create

  belongs_to :campaign, parent_class: Campaign

  filter :created_by
  filter :file
  filter :created_at
  filter :updated_at


  form do |f|

    f.inputs "Batch" do
      f.input :archivo, :as => :file, :require => true
    end

    # f.inputs "Datos adicionales" do
    #   f.semantic_fields_for :opcional do |j|
    #     j.inputs do
    #       # j.input :encabezado, :as=>:boolean, :required => false, :label => 'Leer encabezado'
    #       j.input :valor, :as => :number, :required => false, :label => 'Valor'
    #     end
    #   end
    # end

    f.buttons
  end

  controller do

    before_filter :valid

    def nombre filename
      @convenio = filename[0, 4] #código del convenio de 4 posiciones
      @full_message.push('Convenio es incorrecto') unless @campaign.convenio == @convenio
      @tipo = filename[4, 1] #tipo de convenio, de 1 posición
      @fecha_emision = filename[5, 8] #fecha, de 8 posiciones
      @consecutivo = filename[13,2] #consecutivo del convenio, de 2 posiciones
      @full_message.length > 0
    end

    def encabezado data, length
      # Tipo	Caracter	1	0 = Encabezado
      unless @convenio == data[1,4] # Convenio	Numérico	4	Código de Convenio
        @full_message.push 'encabezado::convenio'
      end
      unless @fecha_emision == data[5, 8] # Fecha	Numérico	8	Fecha de Emisión formato AAAAMMDD
        @full_message.push 'encabezado::fecha'
      end

      @valor = data[13, 10].to_i + "0.#{data[23,2].to_s}".to_f  # Valor	Numérico	12	Valor por el cual se emitieron los CERTIFICADOS, se define con 10 enteros y dos decimales
      campaign #funcion que valida la campaña

      @cantidad = data[25, 8].to_i # No.Registros	Numérico	8	Número de registros de los  CERTIFICADOS para magnetización
      unless @cantidad == length
        @full_message.push 'encabezado::cantidad'
      end
      # Filler	Caracter	51	Relleno con Espacios
      @full_message.length > 0
    end

    # def datos_adicionales
    #   if params[:batch] && params[:batch][:opcional] && params[:batch][:opcional][:valor] && !params[:batch][:opcional][:valor].blank? && !params[:batch][:opcional][:valor].nil?
    #     @valor = params[:batch][:opcional][:valor]
    #     campaign #funcion que valida la campaña
    #   else
    #     @full_message.push 'datos_opcionales::valor'
    #   end
    #   @full_message.length > 0
    # end

    def certificado registro, num
      begin
        text = registro.split('^')
        # Tipo	Caracter	1	% = Registro CERTIFICADO
        # Bandera	Caracter	1	B = Bandera de Inicio Track 1
        num_bono = text[0][2, 13] # No. Bono **	Numérico	13	Número de CERTIFICADO
        # Bandera	Caracter	1	^ = Bandera de Inicio para Nombre Tarjetahabiente
        # Nombre	Caracter	24	Nombre Tarjetahabiente (*)
        # Bandera	Caracter	1	^ = Bandera de Final Nombre Tarjetahabiente
        fecha_vencimiento = text[2][0, 4] # Fecha Vencimiento(4) y detalle	Numérico(9)	13	Fecha de vencimiento del CERTIFICADO, más detalle de la banda
        # Bandera	Caracter	1	? = Bandera de Final Track1
        # Bandera	Caracter	1	; = Bandera de Inicio Track2
        unless num_bono == text[2][15, 13] # No. 	Numérico	13	Número del CERTIFICADO
          @full_message.push 'certificado::num_bono'
        end
        # Bandera	Caracter	1	=  Separador Track 2
        unless fecha_vencimiento == text[2][29, 4] # Fecha Vencimiento(4) y detalle	Numérico(9)	13	Fecha de vencimiento del CERTIFICADO, mas detalle de la banda
          @full_message.push 'certificado::fecha_vencimiento_detalle'
        end
        # Bandera	Caracter	1	? = Bandera de Final Track2

        invalid = @full_message.length > 0
        unless invalid
          @bonos.push num: num_bono, fecha_vencimiento: fecha_vencimiento
        end
        invalid
      rescue
        @full_message.push "certificado[#{num}]::invalido"
        true
      end
    end

    def save
      Batch.transaction do
        begin
          batch = @campaign.batches.new created_by: current_admin_user.id, file: @file_data.original_filename, fecha_emision: @fecha_emision.to_time
          # unless @campaign.type == 'CampaignPoint'
          #   if @campaign.gift_cards.exists?( cost: @valor, enable_for_selling: true )
          #     batch.gift_card_id = @gift_card.id
          #   end
          # end
          if batch.save
            # , gift_card_id: @gift_card.id
            @bonos.each do |bono|
              rc = batch.redeban_codes.build expiration_date: bono[:fecha_vencimiento], state_id: RedebanCode::STATES[:habilitado], charge_state_id: RedebanCode::CHARGE_STATE[:sin_saldo], campaign_id: @campaign.id
              rc.encryptor bono[:num]
            end
            unless batch.save
              @full_message.push *batch.errors.full_messages
              batch.destroy
            end
          else
            @full_message.push *batch.errors.full_messages
          end
        rescue ActiveRecord::Rollback
          @full_message.push "Error al guardar batch."
          true
        end
      end
    end

    private
      def campaign
        if @campaign.type == "CampaignPoint"
          @full_message.push( 'Campaign no esta habilitada' ) unless( @campaign.enabled )
          @full_message.push( 'Batch(' + @valor.to_s + ') con valor diferente a la Campaign(' + @campaign.cost.to_s + ')'  ) unless( @campaign.cost == @valor )
        else
          # exist_gift_card = @campaign.gift_cards.exists?( cost: @valor, enable_for_selling: true )
          # @full_message.push( 'Campaign no tiene giftcard con valor ' + @valor.to_s ) unless( exist_gift_card || @valor.to_i == 0 )
          # if exist_gift_card
          #   @gift_card = @campaign.gift_cards.where( cost: @valor, enable_for_selling: true ).first
          #   if @gift_card
          #
          #   end
          # end
          @full_message.push( 'Campaign no esta habilitada' ) unless( @campaign.campaign_state_id == CampaignState::STATES[:enable] )
        end
      end

      def valid
        @campaign = Campaign.find params[:campaign_id]
        unless @campaign.convenio_redeban?
          flash[:error] = 'La campaign no tiene un convenio con redeban.'
          redirect_to admin_brand_campaign_path(@campaign.brand_id, @campaign.id )
        end
      end

  end

  member_action :create, :method => :post do
    @bonos = []
    @full_message = []
    error = false
    if params[:batch].present? && params[:batch][:archivo].present?
      @file_data = params[:batch][:archivo]
      if @file_data.respond_to?(:path)
        txt = File.readlines( @file_data.path,:encoding => "ASCII")
      else
        @full_message.push "Archivo no cargo."
        error = true
      end
      unless error
        error = nombre @file_data.original_filename
        cantidad_registros = txt.length
        count = 0
        while !error && count < cantidad_registros  do
          r = txt[count].gsub(/\r/,'').gsub(/\n/,'')
          # if count == 0 && params[:batch][:opcional][:encabezado].to_i == 1
          if count == 0 && r[0] != '%'
            error = encabezado r, (cantidad_registros - 1)
          else
            # if count == 0
            #   #Si el archivo no tiene encabezado
            #   error = datos_adicionales
            # # else
            # #   error = certificado r, count
            # end
            if !error
              error = certificado r, count
            end
          end
          count = count + 1
        end

        #si no hay errores a guardar
        unless error
          error = save
        end
        puts error
      end
    else
      @full_message.push "El campo archivo es obligatorio."
      error = true
    end
    if error
       puts @ful_messsage.inspect
       flash[:error] = @full_message.join ', '
       redirect_to new_admin_campaign_batch_path params[:campaign_id]
    else
       redirect_to admin_campaign_batches_path  params[:campaign_id]
    end
  end

  show do |batch|

    attributes_table do
      row :id
      row :created
      row :created_at
      row :updated_at
      row :file
      row :fecha_emision
    end

    panel "Certificados - Bonos" do
      columns do
        column do
          span 'Total'
        end
        column do
          span batch.total_redeban_codes
        end
      end
      columns do
        column do
          span 'Usados'
        end
        column do
          span batch.used_redeban_codes
        end
      end
      columns do
        column do
          span 'Disponibles'
        end
        column do
          span batch.available_redeban_codes
        end
      end
      columns do
        column do
          span 'Activos'
        end
        column do
          span batch.enabled_redeban_codes
        end
      end

      columns do
        column do
          span 'Con Saldo'
        end
        column do
          span batch.con_saldos_redeban_codes
        end
      end

      columns do
        column do
          span 'Sin Saldo'
        end
        column do
          span batch.sin_saldos_redeban_codes
        end
      end

      columns do
        column do
          span 'Valor'
        end
        column do
          span batch.value
        end
      end
    end

  end

  index do
    column :id
    column :created
    column :file
    column :fecha_emision
    default_actions
  end

  action_item only:[:index] do
      link_to 'Descargar ACT' , act_admin_brand_campaign_path(brand_id: campaign.brand_id, id: campaign.id)
  end


end