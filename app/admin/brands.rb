ActiveAdmin.register Brand do

  config.sort_order = "name_asc"
  menu :priority => 4
  actions :index, :show, :new, :create, :edit, :update

  filter :name
  filter :categories
  filter :contact_name
  filter :contact_email
  filter :nit
  filter :created_at
  filter :updated_at
  filter :enable_for_selling


  #For allowing the use of the UrlHelper
  controller do
    helper :url
  end

  form do |f|
    f.inputs "" do
      f.input :name, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :trade_name, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :short_description, :as => :text, :input_html => { :rows => 3, :maxlength => 100  }, :hint => "Up to 100 characters."
      f.input :description, :as => :text, :input_html => { :rows => 3, :maxlength => 2500  }, :hint => "Up to 2500 characters."
      f.input :contact_name, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :contact_email, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :sn_twitter_name, :label => "Twitter name", :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :nit, :input_html => { :maxlength => 30  }, :hint => "Up to 30 characters."
      f.input :image, :as => :file, :hint => "Image must be of 195x195 pixels."
      f.input :image_white_label, :as => :file, :hint => "Image must be of 175x55 pixels."
      f.input :image_email, :as => :file, :hint => "Image must be of 195x195 pixels."
      # f.input :image_email_bin, :as => :file, :hint => "Image must be of 195x195 pixels."
      f.input :qr_code_image, :as => :file, :hint => "Image must be of 132x132 pixels."
      f.input :large_image, :as => :file, :hint => "Image must be of 1700x221 pixels."
      f.input :image_yo_quiero, :as => :file, :hint => "Image must be of 195x195 pixels."
      f.input :url_website, :as => :url, :input_html => { :maxlength => 255  }
      f.input :url_facebook, :as => :url, :input_html => { :maxlength => 255  }
      f.input :url_twitter, :as => :url, :input_html => { :maxlength => 255  }
      f.input :url_youtube, :as => :url, :input_html => { :maxlength => 255  }
      f.input :url_linkedin, :as => :url, :input_html => { :maxlength => 255  }
      f.input :favorite_priority
      f.input :enable_for_selling
    end
    f.buttons
  end

  show do |brand|
    attributes_table do
      row :name
      row :trade_name
      row :description
      row :short_description
      row :contact_name
      row :contact_email
      row :nit
      row :sn_twitter_name
      row :image do
        image_tag(brand.image.url(:thumb195).to_s)
      end
      row :image_white_label do
        image_tag(brand.image_white_label.url.to_s)
      end
      row :image_email do
        image_tag(brand.image_email.url(:thumb195).to_s)
      end
      # row :image_email_bin do
      #   image_tag(brand.image_email_bin.url.to_s)
      # end
      row :qr_code_image do
        image_tag(brand.qr_code_image.url(:thumb132).to_s)
      end
      row :large_image do
        image_tag(brand.large_image.url.to_s)
      end
      row :image_yo_quiero do
        image_tag(brand.image_yo_quiero.url.to_s)
      end
      row :url_website do
        link_to brand.url_website, url_with_protocol( brand.url_website ), :target => "_blank"
      end
      row :url_facebook do
        link_to brand.url_facebook, url_with_protocol( brand.url_facebook ), :target => "_blank"
      end
      row :url_twitter do
        link_to brand.url_twitter, url_with_protocol( brand.url_twitter ), :target => "_blank"
      end
      row :url_youtube do
        link_to brand.url_youtube, url_with_protocol( brand.url_youtube ), :target => "_blank"
      end
      row :url_linkedin do
        link_to brand.url_linkedin, url_with_protocol( brand.url_linkedin ), :target => "_blank"
      end
      row :favorite_priority
      row :enable_for_selling
      row :categories_number do
        brand.categories.count
      end
      row :yo_quiero_facebook
      row :created_at
      row :updated_at
    end

    panel 'Campaigns' do
      table_for(brand.campaigns.where( type: ['',nil] ) ) do
        column('id', :sortable => :id) { |campaign| link_to "#{campaign.id}", admin_brand_campaign_path(brand, campaign) }
        column('state') { |campaign| campaign.campaign_state.name }
        column('name') { |campaign| campaign.name }
        column('giftcard number') { |campaign| campaign.gift_cards.count  }
        column('country') { |campaign| campaign.country.name }
        column('') { |campaign| link_to 'View', admin_brand_campaign_path(brand, campaign) }
        column('') { |campaign| link_to 'Edit', edit_admin_brand_campaign_path(brand, campaign) }
        column('') { |campaign| status_tag( 'RedebanCode', :ok ) if campaign.convenio_redeban? }
      end
    end

    panel 'CampaignsPoint' do
      table_for(brand.campaigns.where( type: CampaignPoint.name ) ) do
        column('id', :sortable => :id) { |campaign| link_to "#{campaign.id}", admin_brand_campaign_path(brand, campaign) }
        column('enabled') { |campaign| campaign.enabled }
        column('name') { |campaign| campaign.name }
        column('value') { |campaign | campaign.cost }
        column('country') { |campaign| campaign.country.name }
        column('') { |campaign| link_to 'View', admin_brand_campaign_point_path(brand, campaign) }
        column('') { |campaign| link_to 'Edit', edit_admin_brand_campaign_point_path(brand, campaign) }
        column('') { |campaign| status_tag( 'RedebanCode', :ok ) if campaign.convenio_redeban? }
      end
    end

    panel "Offices" do
      table_for(brand.offices) do
        column("id", :sortable => :id) { |office| link_to "#{office.id}", admin_brand_office_path(brand, office) }
        column("city name") { |office| office.city.name }
        column("office name") { |office| office.office_name }
        column("contact email") { |office| office.contact_email }
        column("enable for selling") { |office| office.enable_for_selling }
        column("") { |office| link_to "View", admin_brand_office_path(brand, office)}
        column("") { |office| link_to "Edit", edit_admin_brand_office_path(brand, office)}
      end
    end

    panel "Categories" do
      table_for(brand.categories) do
        column("id", :sortable => :id) { |category| link_to "#{category.id}", admin_category_path(category) }
        column("category name") { |category| category.name }
        column("") { |category| link_to "View", admin_category_path(category)}
      end
    end
  end

  index do
    column :id
    column :name
    column :image do |brand|
      image_tag(brand.image.url(:thumb120).to_s)
    end
    column :categories_number do |brand|
      brand.categories.count
    end
    column :campaign_number do |brand|
      brand.campaigns.count
    end
    column :office_number do |brand|
      brand.offices.count
    end
    column :favorite_users do |brand|
      brand.favorites.count
    end
    column :favorite_priority
    column :enable_for_selling
    column :yo_quiero_facebook
    column :created_at
    column :updated_at
    default_actions
  end

  action_item only:[:show] do
     link_to "Create New Campaign", new_admin_brand_campaign_path(brand)
  end

  action_item only:[:show] do
    link_to "Create New Campaign points", new_admin_brand_campaign_point_path(brand)
  end

  action_item only:[:show] do
      link_to "Create New Office", new_admin_brand_office_path(brand)
  end

  action_item only:[:show] do
    if brand.campaigns_convenio_redeban.count > 0
      link_to 'Update certificates redeban' , admin_updatecertificates_path(:brand_id => brand.id)
    end
  end

end
