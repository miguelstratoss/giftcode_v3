ActiveAdmin.register Recommendation do
  menu :parent => "More options"
  actions :index, :show

  filter :user_id, :label => 'user id', :as => :numeric
  filter :description
  filter :created_at
  filter :updated_at

  index do
    column :id
    column :description
    column :user
    column :created_at
    column :updated_at
    default_actions
  end

end
