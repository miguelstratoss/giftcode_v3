ActiveAdmin.register User do
  menu :priority => 1

  actions :show, :index, :edit, :update

  # filter :sex
  # filter :language
  # filter :city
  filter :referral_id, :label => 'referral id', :as => :numeric
  filter :user_state
  filter :first_name
  filter :last_name
  filter :email
  filter :time_zone
  filter :birthday
  filter :mailing_address
  filter :mobile_phone
  filter :office_phone
  filter :created_at
  filter :updated_at
  filter :show_introduction
  filter :identification
  filter :last_fbk_sync_at
  filter :fbk_location_name
  filter :fbk_time_zone
  filter :fbk_locale
  filter :date_referral
  filter :registration_date

  # scope :registered, :default => true do |users|
  #   users.registered
  # end
  # scope :non_registered do |users|
  #   users.non_registered
  # end
  # scope :reg_today do |users|
  #   users.registered_today
  # end
  # scope :referred do |users|
  #   users.referred
  # end
  # scope :with_invitations do |users|
  #   users.registered.with_a_minimum_number_of_invite_friends(1)
  # end
  # scope :without_invitations do |users|
  #   users.registered.without_invite_friends
  # end
  # scope :with_favorites do |users|
  #   users.registered.with_a_minimum_number_of_favorites(1)
  # end
  # scope :without_favorites do |users|
  #   users.registered.without_favorites
  # end

  form do |f|
    f.inputs "" do
      f.input :email, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :first_name, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :last_name, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :image
      # f.input :sex, :require => true
      # f.input :language, :require => true
      # f.input :city, :require => true
      f.input :time_zone
      f.input :birthday_date, :order => [:day, :month, :year]
      f.input :mailing_address, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :office_phone, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :mobile_phone, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :show_introduction
    end
    f.buttons
  end

  show do |user|
    attributes_table do
      row :id
      row :email
      row :first_name
      row :last_name
      row :image do
        image_tag(user.image.url.to_s)
      end
      row :image_30 do
        image_tag(user.image.url(:thumb30).to_s)
      end
      row :image_40 do
        image_tag(user.image.url(:thumb40).to_s)
      end
      row :image_50 do
        image_tag(user.image.url(:thumb50).to_s)
      end
      row :image_60 do
        image_tag(user.image.url(:thumb60).to_s)
      end
      row :image_80 do
        image_tag(user.image.url(:thumb80).to_s)
      end
      row :image_140 do
        image_tag(user.image.url(:thumb140).to_s)
      end
      row :favorite_brands do
        user.favorites.count
      end
      row :referral_id do
        user.referral_id
      end
      row :date_referral do
        user.date_referral
      end
      row :referrals_number do
        user.referrals.count
      end
      row :invitations_sent do
        user.invite_friends.count
      end
      row :total_fbk_friends do
        user.friends.count
      end
      row :number_of_fbk_friends_registered_in_giftcode do
        user.friends.registered.count
      end
      row :number_of_reverse_relation_ships do
        user.reverse_friendships.count
      end
      row :birthday
      row :fbk_location_name
      row :facebook_token do
        user.facebook_token.last(6) if user.facebook_token.present?
      end
      row :last_fbk_friends_sync do
        I18n.l(user.last_fbk_sync_at, :format => :long) if user.last_fbk_sync_at.present?
      end
      row :fbk_user_id
      row :fbk_expires_at do
        I18n.l(user.facebook_expires_at, :format => :long) if user.facebook_expires_at.present?
      end
      row :fbk_locale
      row :fbk_timezone
      row :user_state
      row :sign_in_count
      row :last_sign_in_at
      row :created_at
      row :registration_date
      row :utm_source
      row :utm_medium
      row :updated_at
    end
    attributes_table do
      row :mailing_address
      row :office_phone
      row :mobile_phone
      row :identification
      row :time_zone
      # row :city
      # row :language
      # row :sex
      row :show_introduction
      row :sms_authorization
      row :facebook_expires
      row :date_referral
    end
    panel "Favorite brands" do
      table_for(user.brands) do
        column("id", :sortable => :id) { |brand| link_to "#{brand.id}", admin_brand_path(brand) }
        column("name") { |brand| brand.name  }
        column("") { |brand| link_to "View", admin_brand_path(brand) }
      end
    end
    panel "Referrals" do
      table_for(user.referrals) do
        column("id", :sortable => :id) { |referral| link_to "#{referral.id}", admin_user_path(referral) }
        column("email") { |referral| link_to "#{referral.email}", admin_user_path(referral) }
        column("image") { |referral| image_tag(referral.image.url(:thumb40).to_s) }
        column("last sign in at") { |referral| I18n.l(referral.last_sign_in_at, :format => :long) if referral.last_sign_in_at.present? }
        column("created at") { |referral| I18n.l(referral.created_at, :format => :long) }
        column("") { |referral| link_to "View", admin_user_path(referral) }
      end
    end
    panel "Invitations Sent" do
      table_for(user.invite_friends) do
        column("id", :sortable => :id) { |invite_friend| link_to "#{invite_friend.id}", admin_invite_friend_path(invite_friend) }
        column("friend") { |invite_friend| link_to "#{invite_friend.friend.email}", admin_user_path(invite_friend.friend) if invite_friend.friend.present?}
        column("invitation used") { |invite_friend| invite_friend.used? }
        column("created at") { |invite_friend| I18n.l(invite_friend.created_at, :format => :long) }
        column("") { |invite_friend| link_to "View", admin_invite_friend_path(invite_friend) }
      end
    end
    # active_admin_comments
  end

  # :download_links => [ :csv, :xml, :json ]
  index do
    column :id do |user|
      link_to user.id, admin_user_path(user)
    end
    column :email
    column :image do |user|
      image_tag(user.image.url(:thumb40).to_s, width: 40)
    end
    column :first_name
    column :last_name
    column :registration_date
    # column :referral_id do |user|
    #   if user.referral_id
    #   #   u = User.find_by_fbk_user_id user.referral_id
    #     link_to user.referral_id, admin_user_path(user.referral_id)
    #   end
    # end
    # column :favorite_brands do |user|
    #   user.favorites.count
    # end
    # column :referrals_number do |user|
    #   user.referrals.count
    # end
    # column :invitations_sent do |user|
    #   user.invite_friends.count
    # end
    # column :total_fbk_friends do |user|
    #   user.friends.count
    # end
    # column :number_of_fbk_friends_registered_in_giftcode do |user|
    #   user.friends.registered.count
    # end
    # column :number_of_reverse_relation_ships do |user|
    #   user.reverse_friendships.count
    # end
    column :user_state
    default_actions
  end

  csv do
    column :id
    column :email
    column :first_name
    column :last_name
    column :registration_date
    column :birthday
    column :fbk_location_name
    column :last_fbk_friends_sync do |user|
      I18n.l(user.last_fbk_sync_at, :format => :long) if user.last_fbk_sync_at.present?
    end

    column :fbk_user_id
    column :user_state do |user|
      user.user_state.name
    end
    column :sign_in_count
    column :last_sign_in_at
    column :date_referral
    column :utm_source
    column :utm_medium
    column :created_at
    column :updated_at
  end

end
