ActiveAdmin.register BrandCode do

  # menu :parent => "More options"
  belongs_to :third_party_entity, parent_class: ThirdPartyEntity

  form do |f|
    f.inputs "" do
      f.input :campaign_id, :required => true, :as => :select, :collection => Campaign.all.map{|c| ["#{c.name}, #{c.brand.name}", c.id]}
      f.input :code, :required => true , label: 'Unique code'
      f.input :terminal
      f.input :type_terminal, label:'Type Terminal / Id Commerce'
      f.input :active_for_payment
    end
    f.actions
  end

  show do
    attributes_table do
      row :id
      row :third_party_entity
      row :campaign
      row 'Unique code' do
        resource.code
      end
      row :terminal
      row 'Type Terminal / Id Commerce' do
        resource.type_terminal
      end
      row :active_for_payment
      row :created_at
      row :updated_at
    end
  end

end
