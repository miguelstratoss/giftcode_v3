ActiveAdmin.register Favorite do
  menu :parent => 'Monitoring user'
  #menu :priority => 3
  actions :index, :show

  filter :user_id, :label => 'user id', :as => :numeric
  filter :brand
  filter :enabled
  filter :created_at
  filter :updated_at

  index do
    column :id do |favorite|
      link_to favorite.id, admin_favorite_path(favorite)
    end
    column :user
    column :brand
    column :enabled
    column :created_at
    column :updated_at
    default_actions
  end
end
