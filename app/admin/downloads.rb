ActiveAdmin.register Download do
  menu :parent => "More options"
  actions :index, :show

  filter :token
  filter :view_id
  filter :gift_code_id, :label => 'gift code id', :as => :numeric
  filter :user_id, :label => 'user id', :as => :numeric
  filter :download_type
  filter :created_at
  filter :updated_at

  index do
    column :id
    column :token
    column :view_id
    column :user
    column :gift_code do |download|
      link_to download.gift_code.id, admin_gift_code_path(download.gift_code) if download.gift_code.present?
    end
    column :download_type
    column :created_at
    column :updated_at
    default_actions
  end
end
