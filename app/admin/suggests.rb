ActiveAdmin.register Suggest do
  menu :parent => 'Monitoring user'

  actions :index

  filter :created_at

  index do
    column :id
    column :name
    column :user
    column :created_at
  end


end