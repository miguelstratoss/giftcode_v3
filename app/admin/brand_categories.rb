ActiveAdmin.register BrandCategory do

  belongs_to :category, parent_class: Category

  # menu false

  index do
    column :id
    column :brand
    column :category
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :brand
    end
    f.actions
  end


end
