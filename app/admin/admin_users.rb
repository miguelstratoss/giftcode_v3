ActiveAdmin.register AdminUser do
  menu :parent => "More options"

  form do |f|
    f.inputs "" do
      f.input :role, :required => true
      f.input :brand
      f.input :office
      f.input :email, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :password, :input_html => { :maxlength => 30  }, :hint => "Minimum 8 and maximum 30 characters."
      f.input :password_confirmation, :input_html => { :maxlength => 30  }, :hint => "Minimum 8 and maximum 30 characters."
    end
    f.buttons
  end

  show do |user|
    attributes_table do
      row :id
      row :role
      row :brand
      row :office do
        link_to user.office, admin_brand_office_path(user.office.brand, user.office) if user.office.present?
      end
      row :email
      row :reset_password_token
      row :reset_password_sent_at
      row :remember_created_at
      row :sign_in_count
      row :current_sign_in_at
      row :last_sign_in_at
      row :current_sign_in_ip
      row :last_sign_in_ip
      row :password_salt
      row :confirmation_token
      row :confirmed_at
      row :confirmation_sent_at
      row :unconfirmed_email
      row :failed_attempts
      row :unlock_token
      row :locked_at
      row :authentication_token
      row :created_at
      row :updated_at
    end
    panel "Admin Control Access Alerts" do
      table_for(user.admin_control_access_alerts.ordered_by_created_at_asc) do
        column("id", :sortable => :id) { |admin_control_access_alert| link_to "#{admin_control_access_alert.id}", admin_admin_control_access_alert_path(admin_control_access_alert) }
        column("alert_for_access_path") { |admin_control_access_alert| admin_control_access_alert.alert_for_access_path }
        column("created_at") { |admin_control_access_alert| I18n.l(admin_control_access_alert.created_at, :format => :long) }
        column("") { |admin_control_access_alert| link_to "View", admin_admin_control_access_alert_path(admin_control_access_alert) }
      end
    end
    # active_admin_comments
  end

  index do
    column :id
    column :email
    column :role
    column :brand
    column :office do |admin_user|
      link_to admin_user.office.office_name, admin_brand_office_path(admin_user.office.brand, admin_user.office) unless !admin_user.office.present?
    end
    column :number_of_control_access_alerts do |admin_user|
      admin_user.admin_control_access_alerts.count
    end
    column :last_sign_in_at
    column :created_at
    column :updated_at
    default_actions
  end

end
