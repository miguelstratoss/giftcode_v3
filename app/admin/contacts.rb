ActiveAdmin.register Contact do

  config.sort_order = "id_asc"
  menu :priority => 8
  actions :index, :show

  index do
    column :id
    column :name
    column :email
    column :phone
    column :company
    # column :issue
    column :created_at
    column :updated_at
    default_actions
  end
  
end
