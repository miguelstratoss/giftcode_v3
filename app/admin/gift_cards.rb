ActiveAdmin.register GiftCard, :as => "GiftCards" do


  belongs_to :campaign, parent_class: Campaign


  show title: 'GiftCard'  do |gift_card|
    attributes_table do
      row :id
      row :campaign_id
      row :cost
      row :valid_for_days
      row :convenio
      row :created_at
      row :updated_at
      row :enable_for_selling
    end
  end

  form do |f|
    f.inputs do
      f.input :cost, :required => true
      f.input :valid_for_days
      f.input :convenio
      f.input :enable_for_selling
    end
    f.buttons
  end

end