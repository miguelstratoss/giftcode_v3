# encoding: UTF-8
ActiveAdmin.register Transaction do
  menu :parent => "More options"

  actions :index, :show

  scope :all, :default => true do |transactions|
    transactions
  end
  scope :today do |transactions|
    transactions.created_today
  end
  scope :yesterday do |transactions|
    transactions.created_yesterday
  end
  scope :this_week do |transactions|
    transactions.created_this_week
  end
  scope :last_week do |transactions|
    transactions.created_last_week
  end
  scope :this_month do |transactions|
    transactions.created_this_month
  end
  scope :last_month do |transactions|
    transactions.created_last_month
  end

  filter :gift_code_id, :label => 'gift code id', :as => :numeric
  #filter :terminal_id, :label => 'Terminal'
  filter :cost
  filter :consecutive
  #filter :terminal_date, :label => 'Fecha en la terminal'
  #filter :authorization_number, :label => 'Numero de autorizacion'
  filter :created_at_office
  filter :created_by_admin_user
  filter :created_at

  controller do

    before_filter :only => :index do
      @per_page = 100
    end

  end

  show do |transaction|
    attributes_table do
      row :id do
        transaction.id
      end
      row :brand do
        link_to transaction.created_at_office.brand.name, admin_brand_path(transaction.created_at_office.brand)
      end
      row :created_at_office do
        link_to transaction.created_at_office, admin_brand_office_path(transaction.created_at_office.brand, transaction.created_at_office) if transaction.created_at_office.present?
      end
      row :gift_code_id do
        link_to transaction.gift_code.id, admin_gift_code_path(transaction.gift_code)
      end
      row :transaction_cost do
        number_to_currency(transaction.cost, :separator => ",", :delimiter => ".", :precision => 2, :locale => :en)
      end
      #row :terminal_id do
      #  transaction.terminal_id
      #end
      row :consecutive do
        transaction.consecutive
      end
      #row :terminal_date do
      #  I18n.l(transaction.terminal_date, :format => :long)
      #end
      row :authorization_number do
        transaction.authorization_number
      end
      row :transaction_state do
        transaction.state
      end
      row :created_by do
        link_to transaction.created_by_admin_user, admin_admin_user_path(transaction.created_by_admin_user) if transaction.created_by_admin_user.present?
      end
      row :created_at do
        I18n.l(transaction.created_at, :format => :long)
      end
      row :updated_at do
        I18n.l(transaction.updated_at, :format => :long)
      end
    end
    panel "Current GiftCode Information" do
      attributes_table_for transaction do
        row :gift_code__id do
          link_to transaction.gift_code.id, admin_gift_code_path(transaction.gift_code)
        end
        row :ref_sale do
          if !transaction.gift_code.is_a_free_gift_code?
            transaction.gift_code.ref_sale
          else
            "NOT APPLY"
          end
        end
        row :gift_code_state do
          if !transaction.gift_code.is_a_free_gift_code?
            status_tag(transaction.gift_code.gift_code_state.name, :ok)
          else
            status_tag(transaction.gift_code.gift_code_state.name+", SÓLO PUEDE SER RECLAMADO EN LA OFICINA: "+transaction.gift_code.free_campaign.office.office_name, :warning)
          end
        end
        row :to_username do
          transaction.gift_code.to_user_name
        end
        row :to_email do
          transaction.gift_code.to_email
        end
        row :to_mobile_phone do
          if !transaction.gift_code.is_a_free_gift_code?
            transaction.gift_code.to_mobile_phone
          else
            "NOT APPLY"
          end
        end
        row :to_city do
          transaction.gift_code.to_city.complete_name
        end
        row :currency do
          transaction.gift_code.currency.name
        end
        row :cost do
          number_to_currency(transaction.gift_code.cost, :separator => ",", :delimiter => ".", :precision => 2, :locale => :en)
        end
        row :available_balance do
          number_to_currency(transaction.gift_code.available_balance, :separator => ",", :delimiter => ".", :precision => 2, :locale => :en)
        end
        row :valid_until do
          transaction.gift_code.valid_until
        end
        row :created_at do
          I18n.l(transaction.gift_code.created_at, :format => :long)
        end
      end
    end
  end

  index do
    column :id
    column :created_at_office do |transaction|
      link_to transaction.created_at_office, admin_brand_office_path(transaction.created_at_office.brand, transaction.created_at_office) if transaction.created_at_office.present?
    end
    column :gift_code_id do |transaction|
      link_to transaction.gift_code.id, admin_gift_code_path(transaction.gift_code) if transaction.gift_code.present?
    end
    column :cost do |transaction|
      number_to_currency(transaction.cost, :separator => ",", :delimiter => ".", :precision => 2, :locale => :en)
    end
    column :gift_code_type do |transaction|
      if !transaction.gift_code.is_a_free_gift_code?
        status_tag("STANDARD", :ok)
      else
        status_tag("FREE", :warning)
      end
    end
    column :consecutive do |transaction|
      transaction.consecutive
    end
    column :created_by do |transaction|
      link_to transaction.created_by_admin_user, admin_admin_user_path(transaction.created_by_admin_user) if transaction.created_by_admin_user.present?
    end
    column :created_at do |transaction|
      I18n.l(transaction.created_at, :format => :long)
    end
    default_actions
  end

  sidebar :transaction_resume, :only => :index do
    attributes_table_for Transaction.new do
      row("Number of transactions") { transactions.limit(nil).offset(0).count }
      row("Total Cost of All Selected Transactions") { number_to_currency(transactions.limit(nil).offset(0).sum(:cost), :separator => ",", :delimiter => ".", :precision => 2, :locale => :en) }
      row(" ") { " " }
      row(" ") { " " }
      row("Paid transactions") { transactions.paid.limit(nil).offset(0).count }
      row("Total") { number_to_currency(transactions.paid.limit(nil).offset(0).sum(:cost), :separator => ",", :delimiter => ".", :precision => 2, :locale => :en) }
      row(" ") { " " }
      row(" ") { " " }
      row("Free Transactions") { transactions.free.limit(nil).offset(0).count }
      row("Total") { number_to_currency(transactions.free.limit(nil).offset(0).sum(:cost), :separator => ",", :delimiter => ".", :precision => 2, :locale => :en) }
    end
  end
  
end
