ActiveAdmin.register FacebookWall do
  menu :parent => 'Monitoring user'
  actions :index

  filter :user_id, :label => 'user id', :as => :numeric
  filter :created_at
  filter :updated_at

  index do
    column :id
    column :user
    column :fbk_post_id
    column :created_at
  end

end
