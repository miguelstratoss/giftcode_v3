ActiveAdmin.register Office do

  belongs_to :brand, parent_class: Brand

  form do |f|
    f.inputs "" do
      f.input :brand, :required => true
      f.input :city, :required => true
      f.input :office_name, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :office_address, :input_html => { :maxlength => 255  }, :hint => "Up to 255 characters."
      f.input :nit, :input_html => { :maxlength => 30  }, :hint => "Up to 30 characters."
      f.input :contact_name, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :contact_email, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :enable_for_selling
      f.input :phone
      f.input :mobile_phone
      f.input :bank_account_number
      f.input :bank_name
      f.input :bank_account_type
      f.input :local_company_name
      f.input :ip
      f.input :schedule, :as => :text, :input_html => { :rows => 3, :maxlength => 300  }
    end
    f.buttons
  end

  show do |office|
    attributes_table do
      row :id
      row :office_name
      row :office_address
      row :nit
      row :contact_name
      row :contact_email
      row :brand
      row :city
      row :enable_for_selling
      row :phone
      row :mobile_phone
      row :bank_account_number
      row :bank_name
      row :bank_account_type
      row :local_company_name
      row :schedule
      row :ip
      row :created_at
      row :updated_at
    end
    panel "Office Admin Users" do
      table_for(office.admin_users) do
        column("id") { |admin_user| link_to admin_user.id, admin_admin_user_path(admin_user) }
        column("email") { |admin_user| admin_user.email }
        column("created_at") { |admin_user| I18n.l(admin_user.created_at, :format => :long) }
        column("") { |admin_user| link_to "View", admin_admin_user_path(admin_user)}
      end
    end
    # panel "Last 100 Transactions" do
    #   table_for(office.transactions.limit(100).ordered_by_created_at_desc) do
    #     column("id") { |transaction| link_to transaction.id, admin_transaction_path(transaction) }
    #     column("consecutive") { |transaction| transaction.consecutive }
    #     column("cost") { |transaction| number_to_currency(transaction.cost, :separator => ",", :delimiter => ".", :precision => 2, :locale => :en) }
    #     column("created_at") { |transaction| I18n.l(transaction.created_at, :format => :long) }
    #     column("") { |transaction| link_to "View", admin_transaction_path(transaction)}
    #   end
    # end
    # active_admin_comments
  end
  
end
