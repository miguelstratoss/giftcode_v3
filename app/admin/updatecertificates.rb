ActiveAdmin.register_page 'UpdateCertificates' do

  menu false

  content :if => false do
    if brand
      campaigns = brand.campaigns_convenio_redeban
      if campaigns && campaigns.count > 0
        panel 'Subir archivo' do
          semantic_form_for 'redeban_files', :url => admin_updatecertificates_load_url(:brand_id => brand.id), :builder => ActiveAdmin::FormBuilder do |f|
            f.inputs do
              f.input :archivo, :as => :file, :require => true
            end
            f.actions
          end
        end

        panel 'Convenios' do
          table_for campaigns do
            column :id
            column :type do |campaign|
              if campaign.type.nil? || campaign.type.blank?
                'Campaign'
              else
                campaign.type
              end
            end
            column :state do |campaign|
              if campaign.type.nil? || campaign.type.blank?
                campaign.campaign_state.name
              else
                campaign.enabled
              end
            end
            column :name
            column :convenio
          end
        end
      else
        para "Marca no tiene un convenio con redeban.", class: 'status_tag error'
      end
    else
      para "Marca no existe.", class: 'status_tag error'
    end

  end

  page_action :load, method: :post do
    errors = []
    brand = Brand.where( id: params[:brand_id] )
    brand.present?
    if brand.present?
      @brand = brand.first
    end
    if params[:redeban_files].present? && params[:redeban_files][:archivo].present?
      file = params[:redeban_files][:archivo]
      certificates = UpdateCertificatesRedeban.new @brand, current_user
      if certificates.error_message.count == 0
        certificates.load file
      end
      errors.concat( certificates.error_message )
    else
      errors.push "El campo archivo es obligatorio."
    end
    if errors.count > 0
      flash[:error] = errors.join ', '
      redirect_to admin_updatecertificates_path(:brand_id => @brand.id), notice: 'Se actualizaron certificados.'
    else
      redirect_to admin_updatecertificates_path(:brand_id => @brand.id), notice: 'Se actualizaron certificados.'
    end
  end

  controller do

    def initialize
      @brand = nil
    end

    def index
      brand = Brand.where( id: params[:brand_id] )
      if brand.present?
        @brand = brand.first
      end
    end

  end

end