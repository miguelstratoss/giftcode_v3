ActiveAdmin.register_page 'Dashboard' do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do
  #   columns do
  #     column do
  #       panel 'Last 30 Users Registered' do
  #         table_for User.registered.ordered_desc_by_creation_date.limit(30) do |t|
  #           t.column("ID") { |user| link_to user.id, admin_user_path(user) }
  #           t.column("Image") { |user| image_tag(user.image.url(:thumb40).to_s, width: 40) }
  #           t.column("First Name") { |user| user.first_name }
  #           t.column("Last Name") { |user| user.last_name }
  #           t.column("Registration Date") { |user| I18n.l(user.registration_date, :format => :long) if user.registration_date.present? }
  #           t.column("Favorite Brands") { |user| user.favorites.count }
  #           t.column("Referral ID") { |user| user.referral_id }
  #           t.column("Referral Number") { |user| user.referrals.count }
  #           t.column("Invitations sent") { |user| user.invite_friends.count }
  #           t.column("Total Number of FBK Friends") { |user| user.friends.count }
  #           t.column("") { |user| link_to "View", admin_user_path(user) }
  #         end
  #       end
  #     end
  #   end
  #
  #   columns do
  #     column do
  #       panel 'Last 30 Users Reffered' do
  #         table_for User.referred.ordered_desc_by_creation_date.limit(30) do |t|
  #           t.column("ID") { |user| link_to user.id, admin_user_path(user) }
  #           t.column("Image") { |user| image_tag(user.image.url(:thumb40).to_s) }
  #           t.column("First Name") { |user| user.first_name }
  #           t.column("Last Name") { |user| user.last_name }
  #           t.column("Registration Date") { |user| I18n.l(user.registration_date, :format => :long) if user.registration_date.present? }
  #           t.column("Favorite Brands") { |user| user.favorites.count }
  #           t.column("Referral ID") { |user| user.referral_id }
  #           t.column("Referral Number") { |user| user.referrals.count }
  #           t.column("Invitations sent") { |user| user.invite_friends.count }
  #           t.column("Total Number of FBK Friends") { |user| user.friends.count }
  #           t.column("") { |user| link_to "View", admin_user_path(user) }
  #         end
  #       end
  #     end
  #   end
  #
  #   columns do
  #     column do
  #       panel 'Last 30 Brand Favorites' do
  #         table_for Favorite.where(:enabled => true).ordered_desc_by_creation_date.limit(30) do |t|
  #           t.column("ID") { |favorite| link_to favorite.id, admin_favorite_path(favorite) }
  #           t.column("User") do |favorite|
  #             if favorite.user
  #               link_to favorite.user.email, admin_user_path(favorite.user)
  #             else
  #               'N/A'
  #             end
  #           end
  #           t.column("Brand") { |favorite| link_to favorite.brand.name, admin_brand_path(favorite.brand) }
  #           t.column("Created at") { |favorite| I18n.l(favorite.created_at, :format => :long) }
  #           t.column("") { |favorite| link_to "View", admin_favorite_path(favorite) }
  #         end
  #       end
  #     end
  #   end
  #
  #   columns do
  #     column do
  #       panel 'Last 30 Invitations Sent' do
  #         table_for InviteFriend.ordered_desc_by_creation_date.limit(30) do |t|
  #           t.column("ID") { |invite_friend| link_to invite_friend.id, admin_invite_friend_path(invite_friend) }
  #           t.column("User") { |invite_friend| link_to invite_friend.user.email, admin_user_path(invite_friend.user) if invite_friend.user.present?  }
  #           t.column("Friend") { |invite_friend| link_to_if invite_friend.friend.email, admin_user_path(invite_friend.friend) if invite_friend.friend.present?  }
  #           t.column("Invitation Used") { |invite_friend| invite_friend.used? }
  #           t.column("Created at") { |invite_friend| I18n.l(invite_friend.created_at, :format => :long) }
  #           t.column("Used At") { |invite_friend| I18n.l(invite_friend.friend.date_referral, :format => :long) if invite_friend.friend.present? && invite_friend.friend.date_referral.present? && invite_friend.used? }
  #           t.column("") { |invite_friend| link_to "View", admin_invite_friend_path(invite_friend) }
  #         end
  #       end
  #     end
  #   end

    columns do
      column do
        panel "App server response time (ms)" do
          div do
            if Rails.env.prestaging?
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/9RkcmAhcurL" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            elsif Rails.env.staging?
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/fIS7ZQojp21" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            else
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/f3G29BVTavo" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            end
          end
        end
      end
      column do
        panel "Browser page load time (sec)" do
          div do
            if Rails.env.prestaging?
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/edRf0fZIKeZ" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            elsif Rails.env.staging?
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/7niJYRIkk6Y" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            else
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/RxsYk20NmC" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            end
          end
        end
      end
    end

    columns do
      column do
        panel "Error rate" do
          div do
            if Rails.env.prestaging?
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/btqclSp08hc" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            elsif Rails.env.staging?
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/8fiakmY20UQ" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            else
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/7IiWdS9GjAp" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            end
          end
        end
      end
      column do
        panel "Average memory usage per instance" do
          div do
            if Rails.env.prestaging?
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/2igWqjwnYZs" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            elsif Rails.env.staging?
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/fUExy0OBSGn" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            else
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/bNSInKb93Gl" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            end
          end
        end
      end
    end
  end
end
