ActiveAdmin.register Campaign do

  belongs_to :brand, parent_class: Brand

  form do |f|
    f.inputs "" do
      f.input :country, :required => true
      f.input :currency, :required => true
      f.input :campaign_state, :required => true
      f.input :name
      f.input :convenio
      f.input :redeban
      f.input :iva
      f.input :cost_admin
      f.input :iva_cost_admin
      f.input :message
      f.input :send_mail
      f.input :send_sms
      f.input :show_qr
      f.input :show_barcode
      f.input :office_redeem_bono
    end
    f.buttons
  end

  show do 
    attributes_table do
      row :id
      row :brand
      row :iva
      row :cost_admin
      row :iva_cost_admin
      row :name
      row :convenio
      row :redeban
      row :message
      row :send_mail
      row :send_sms
      row :show_qr
      row :show_barcode
      row :country
      row :currency
      row :campaign_state
      row :sales_code
      row :office_redeem_bono
      row :created_at
      row :updated_at
    end
    panel "GiftCards" do
      table_for(campaign.gift_cards) do
        column("id", :sortable => :id) { |gift_card| link_to "#{gift_card.id}", admin_campaign_gift_card_path(campaign, gift_card) }
        # column("image") { |gift_card| image_tag(gift_card.image.url(:thumb160).to_s) if gift_card.image.present? }
        column("cost") { |gift_card| gift_card.cost }
        column("convenio") { |gift_card| gift_card.convenio }
        column("gift codes sold") { |gift_card| gift_card.gift_codes.where( :gift_code_state_id => GiftCodeState.array_successful ).count }
        column("valid_for_days") { |gift_card| gift_card.valid_for_days }
        column("enable_for_selling") { |gift_card| gift_card.enable_for_selling }
        column("") { |gift_card| link_to "Edit", edit_admin_campaign_gift_card_path(campaign, gift_card)}
      end
    end
    # active_admin_comments
  end


  member_action :act, :method => :get do
    begin
      RedebanCode.transaction do
        time = Time.zone.now.strftime("%Y%m%d")
        output =  [ '01,' + resource.convenio.to_s + ',' + time + ',01,A' ]
        value = 0
        count = 0
        file_name = "ACT#{ resource.convenio.to_s + time }01.txt"
        where = resource.redeban_codes.includes(:gift_card).where( :state_id =>  RedebanCode::STATES[:habilitado], :charge_state_id => RedebanCode::CHARGE_STATE[:enviar_redeban] )
        unless where.blank?
          file = LogFileDownload.create!( name: file_name, admin_user_id: current_admin_user.id )
          where.each do |rc|
            decrypt = Encryptor.decrypt( Base64.decode64(rc.code), :key => GlobalConstants::SECRET_KEY, :iv => Base64.decode64(rc.iv), :salt => rc.salt)
            output << '02,' + decrypt + ',01,' + rc.gift_card.cost.to_i.to_s + '00,' + time
            value = value + rc.gift_card.cost
            count = count + 1
            rc.update_attributes! charge_state_id: RedebanCode::CHARGE_STATE[:se_envio_redeban], log_file_download_id: file.id
          end
          output <<  '03,' + count.to_s + ',0,0,0,0,0,0,' + value.to_i.to_s + '00'

          # key = GPGME::Data.new(File.open( Rails.root.join( 'key', 'Redeban_Multicolor_Bonos.asc') ) )
          # data = GPGME::Data.new(output.join("\n"))
          #
          # GPGME::Ctx.new do |ctx|
          #   ctx.import(key)
          #   crypto = GPGME::Crypto.new(:armor => true, :always_trust => true)
          #   e = crypto.encrypt(data, :recipients => 'seguridaddelared@redebanmulticolor.com.co')
          #   send_data e, :filename => file_name
          # end
          send_data output.join("\n"), :filename => file_name
        else
          puts '3'
          send_data 'no hay datos', :filename => 'empty.txt'
        end
      end
    rescue ActiveRecord::Rollback
      send_data "ERROR", :filename => "error.txt"
      puts "Error a crear archivo de ACT para redeban."
    end
  end




  action_item only:[:show] do
     link_to "Create New GiftCard", new_admin_campaign_gift_card_path(campaign)
  end

  action_item only:[:show] do
    if campaign.convenio_redeban?
      link_to( "Batches Redeban", admin_campaign_batches_path(campaign) )
    end
  end





end
