ActiveAdmin.register Currency do

  config.sort_order = "id_asc"
  menu :parent => "More options"
  actions :new, :create, :index, :show, :edit, :update, :destroy

  form do |f|
    f.inputs "" do
      f.input :name, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :symbol, :input_html => { :maxlength => 10  }, :hint => "Up to 10 characters."
      f.input :short_name, :input_html => { :maxlength => 3  }, :hint => "Use 3 characters ISO code."
    end
    f.buttons
  end
end
