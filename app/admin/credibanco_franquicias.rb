ActiveAdmin.register CredibancoFranquicia do

  menu :parent => 'Credibanco'

  show do
    attributes_table do
      row :id
      row :name
      row :codigo_identificacion
      row :length
      row :enabled
      row :created_at
      row :updated_at
    end

    panel "Prefix" do
      table_for(credibanco_franquicia.credibanco_franquicia_prefixes) do
        column('id', :sortable => :id) { |prefix| link_to "#{prefix.id}", admin_credibanco_franquicia_prefix_path(prefix.id) }
        column :bin
        column :enabled
        column('') do |prefix|
          link_to 'ver', admin_credibanco_franquicia_prefix_path(prefix.id)
        end
        column('') do |prefix|
          link_to 'editar', edit_admin_credibanco_franquicia_prefix_path(prefix.id)
        end
      end
    end
  end

end
