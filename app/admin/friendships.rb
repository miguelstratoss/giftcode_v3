ActiveAdmin.register Friendship do

  menu :parent => 'Monitoring user'
  #menu :priority => 2
  actions :index, :show

  filter :user_id, :label => 'user id', :as => :numeric
  filter :friend_id, :label => 'friend id', :as => :numeric
  filter :friendship_state
  filter :created_at
  filter :updated_at

  index do
    column :id do |friendship|
      link_to friendship.id, admin_friendship_path(friendship)
    end
    column :user
    column :friend
    column :friendship_state
    column :created_at
    column :updated_at
    default_actions
  end
  
end
