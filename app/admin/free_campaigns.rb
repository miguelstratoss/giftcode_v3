ActiveAdmin.register FreeCampaign do
  menu :parent => "More options"

  actions :new, :create, :show, :index, :edit, :update

  member_action :new_gift_code, :method => :get do
    @free_campaign = FreeCampaign.find(params[:id])
    @gift_code = @free_campaign.gift_codes.new
  end

  member_action :confirm_gift_code, :method => :get do
    @free_campaign = FreeCampaign.find(params[:id])
    if params[:gift_code] && params[:gift_code][:to_email] && params[:gift_code][:to_email].blank?
      flash[:error]="You must enter the email."
      redirect_to new_gift_code_admin_free_campaign_path
    else
      @email = params[:to_email].present? ? params[:to_email] : params[:gift_code][:to_email]
      @user = User.find_by_email(@email)
      if @user.nil?
        flash[:success]="There is not a registered user with the email: "+@email
      end
    end
  end

  member_action :create_gift_code, :method => :post do
    @free_campaign = FreeCampaign.find(params[:id])
    @user = User.find_by_email(params[:gift_code][:to_email])
    if params[:gift_code][:to_first_name].nil? || params[:gift_code][:to_last_name].nil? || params[:gift_code][:body_message].nil? || params[:gift_code][:to_first_name].blank? || params[:gift_code][:to_last_name].blank? || params[:gift_code][:body_message].blank?
      flash[:error]="You must enter the complete name and the message."
      redirect_to confirm_gift_code_admin_free_campaign_path(@free_campaign, :to_email => params[:gift_code][:to_email])
    else
      @gift_code = @free_campaign.gift_codes.new

      @gift_code.body_message=params[:gift_code][:body_message]
      @gift_code.to_first_name=params[:gift_code][:to_first_name]
      @gift_code.to_last_name=params[:gift_code][:to_last_name]
      @gift_code.to_email=params[:gift_code][:to_email]
      #End values from the form

      @gift_code.to_city = @free_campaign.office.city
      @gift_code.from_user = User.find_by_email('none@greencode.com.co')
      @gift_code.from_first_name = @gift_code.from_last_name = "none"
      @gift_code.from_email = "none@greencode.com.co"
      @gift_code.to_user = @user.present? ? @user : User::user_new( params[:gift_code][:to_first_name], params[:gift_code][:to_last_name], params[:gift_code][:to_email], false )
      @gift_code.cost = @free_campaign.cost
      @gift_code.valid_for_days = @free_campaign.valid_for_days
      @gift_code.gift_code_state_id = GiftCodeState::STATES[:free_from_an_office]
      @gift_code.office_id = @free_campaign.office.id
      @gift_code.generate_unique_values
      @gift_code.generate_free_qr_code_image
      @gift_code.available_balance = @gift_code.cost
      @gift_code.iva = nil
      @gift_code.admin_user = current_admin_user

      if @gift_code.save
        begin
          GiftCodeNotifier.receiver_free_gift_code(@gift_code).deliver
          @gift_code.email_sent_to_receiver=true
        rescue
          @gift_code.email_sent_to_receiver=false
        end
        @gift_code.save
        flash[:notice]='The GiftCode was sent successfully'
      else
        puts "ERRORES: "+ @gift_code.errors.to_hash.to_s
        flash[:error]='The GiftCode could not be sent'
      end
      redirect_to admin_free_campaign_path(@free_campaign)
    end
  end

  form do |f|
    f.inputs "" do
      f.input :admin_user, :required => true, :hint => "The admin who creates the free campaign. Select your admin email.", :input_html => { :disabled => (!f.object.new_record?) }
      f.input :office, :required => true, :input_html => { :disabled => (!f.object.new_record?) }
      f.input :currency, :required => true, :input_html => { :disabled => (!f.object.new_record?) }
      f.input :cost, :input_html => { :disabled => (!f.object.new_record?) }
      f.input :valid_for_days, :input_html => { :disabled => (!f.object.new_record?) }
      f.input :body_message, :as => :text, :input_html => { :rows => 2, :maxlength => 140  }, :hint => "Up to 140 characters."
    end
    f.buttons
  end

  show do |free_campaign|
    attributes_table do
      row :id
      row :brand do
        link_to free_campaign.office.brand.name, admin_brand_path(free_campaign.office.brand)
      end
      row :office do
        link_to free_campaign.office.office_name, admin_brand_office_path(free_campaign.office.brand, free_campaign.office)
      end
      row :currency
      row :body_message
      row :valid_for_days
      row :cost do
        number_to_currency(free_campaign.cost, :separator => ",", :delimiter => ".", :precision => 2, :locale => :en)
      end
      row :created_by do
        link_to free_campaign.admin_user.email, admin_admin_user_path(free_campaign.admin_user)
      end
      row :created_at
      row :updated_at
    end
    panel "GiftCodes" do
      table_for(free_campaign.gift_codes) do |t|
        column("ID") { |gift_code| gift_code.id }
        column("To email") { |gift_code| link_to_if gift_code.to_user.present?,"#{gift_code.to_email}", admin_user_path(gift_code.to_user) }
        column("Cost") { |gift_code| gift_code.cost }
        column("State") { |gift_code| gift_code.gift_code_state.name }
        column("Downloads") { |gift_code| gift_code.downloads.count }
        column("Transaction number") { |gift_code| gift_code.transactions.count }
        column("Sent by admin user") { |gift_code| link_to gift_code.admin_user.email, admin_admin_user_path(gift_code.admin_user) if gift_code.admin_user.present? }
        column("Created at") { |gift_code| I18n.l(gift_code.created_at, :format => :long) }
        column("") { |gift_code| link_to "View", admin_gift_code_path(gift_code) }
      end
    end
    # active_admin_comments
  end

  index do
    column :id
    column :brand do |free_campaign|
      link_to free_campaign.office.brand.name, admin_brand_path(free_campaign.office.brand)
    end
    column :office do |free_campaign|
      link_to free_campaign.office.office_name, admin_brand_office_path(free_campaign.office.brand, free_campaign.office)
    end
    column :currency do |free_campaign|
      free_campaign.currency.complete_name
    end
    column :cost do |free_campaign|
      number_to_currency(free_campaign.cost, :separator => ",", :delimiter => ".", :precision => 2, :locale => :en)
    end
    column :gift_codes_sent do |free_campaign|
      free_campaign.gift_codes.count
    end
    column :created_by do |free_campaign|
      link_to free_campaign.admin_user.email, admin_admin_user_path(free_campaign.admin_user)
    end
    column :created_at
    column :updated_at
    default_actions
  end

  action_item only:[:show] do
     link_to "Send Free GiftCode", new_gift_code_admin_free_campaign_path(free_campaign), :method => :get
  end
  
end
