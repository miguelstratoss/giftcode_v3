ActiveAdmin.register ThirdPartyEntity do

  menu :parent => "More options"
  config.sort_order = "id_asc"
  actions :index, :show, :new, :create, :edit, :update

  #For allowing the use of the UrlHelper
  controller do
    helper :url
  end

  form do |f|
    f.inputs "" do
      f.input :name, :input_html => { :maxlength => 50  }, :hint => "Up to 50 characters."
      f.input :incocredito_id, :input_html => { :maxlength => 12  }, :hint => "Up to 12 characters."
      f.input :configuration
    end
    f.actions
  end

  show do |third_party_entity|
    attributes_table do
      row :id
      row :name
      row :incocredito_id
      row :secret
      row :created_at
      row :updated_at
    end

    panel 'Brand Codes' do
      table_for(third_party_entity.brand_codes) do
        # column("id", :sortable => :id) { |brand_code| link_to "#{brand_code.id}", admin_third_party_entity_brand_code_path(third_party_entity, brand_code) }
        # column('brand') { |brand_code| brand_code.campaign.brand.name }
        column('brand') { |brand_code|  link_to brand_code.campaign.brand.name, admin_brand_path(brand_code.campaign.brand_id, brand_code.campaign_id)  }
        # column('campaign') { |brand_code| brand_code.campaign.name }
        column('campaign') { |brand_code| link_to brand_code.campaign.name, admin_brand_campaign_path(brand_code.campaign.brand_id, brand_code.campaign_id) }
        column('code') { |brand_code| brand_code.code }
        # column("") { |brand_code| link_to "View", admin_brand_code_path(brand_code)}
        # column("") { |brand_code| link_to "View", admin_third_party_entity_path(third_party_entity)}
        column('') { |brand_code| link_to 'View',  admin_third_party_entity_brand_code_path( third_party_entity, brand_code )}
        # column("") { |brand_code| link_to "Edit", edit_admin_brand_code_path(brand_code)}
        # column("") { |brand_code| link_to "Edit", edit_admin_third_party_entity_path(third_party_entity)}
        column('') { |brand_code| link_to 'Edit', edit_admin_third_party_entity_brand_code_path( third_party_entity, brand_code )}
      end
    end
  end

  index :download_links => false do
    column :id
    column :name
    column :created_at
    column :updated_at
    default_actions
  end

  action_item only:[:show] do
     link_to "Create Brand Code", new_admin_third_party_entity_brand_code_path(third_party_entity)
  end

end
