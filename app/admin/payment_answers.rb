ActiveAdmin.register PaymentAnswer do
  menu :parent => "More options"

  actions :index, :show

  filter :pol_state
  filter :cost
  filter :iva
  filter :currency_short_name
  filter :params
  filter :created_at
  filter :updated_at

  show do |payment_answer|
    attributes_table do
      row :id
      row :gift_code do
        link_to payment_answer.gift_code_id, admin_gift_code_path(payment_answer.gift_code) if payment_answer.gift_code.present?
      end
      row :pol_state
      row :cost
      row :iva
      row :currency_short_name
      row :payment_type_id
      row :params
      row :final_result
      row :created_at
      row :updated_at
    end
  end

  index do
    column :id
    column :gift_code_id do |payment_answer|
      link_to payment_answer.gift_code_id, admin_gift_code_path(payment_answer.gift_code) if payment_answer.gift_code.present?
    end
    column :pol_state
    column :cost
    column :iva
    column :currency_short_name
    column :payment_type_id
    column :final_result
    column :created_at
    column :updated_at
    default_actions
  end
  
end

