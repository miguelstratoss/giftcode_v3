# ActiveAdmin.register RedebanCode do
#
#   config.batch_actions = false
#
#   actions :index
#
#   belongs_to :campaign
#
#   filter :batch_id, :as => :numeric
#   filter :created_at
#   filter :updated_at
#   filter :already_used
#   filter :expiration_date
#
#   scope :all
#   scope :bloqueado
#   scope :suspendido
#   scope :desafiliado
#   scope :vencido
#   scope :consumido
#   scope :redimido
#   scope :habilitado
#   scope :no_habilitado
#
#   index :download_links => false do
#     column :id
#     column :state do |redeban_code|
#       RedebanCode::STATES_ID[redeban_code.state_id]
#     end
#     column :batch do |redeban_code|
#       link_to redeban_code.batch_id.to_s+'::'+redeban_code.batch.file, admin_campaign_batch_path(redeban_code.batch.campaign_id,redeban_code.batch_id)
#     end
#     column :already_used
#   end
#
#   form do |f|
#
#     f.inputs "Batch" do
#       f.input :archivo, :as => :file, :require => true
#     end
#
#     f.inputs "Datos adicionales" do
#       f.semantic_fields_for :opcional do |j|
#         j.inputs do
#           j.input :encabezado, :as=>:boolean, :required => false, :label => 'Leer encabezado'
#           j.input :valor, :as => :number, :required => false, :label => 'Valor'
#         end
#       end
#     end
#
#     f.buttons
#   end
#
#
#   action_item only:[:index, :new] do
#     link_to 'Show brand' , admin_brand_path(campaign.brand_id)
#   end
#
#   action_item only:[:index, :new] do
#     link_to 'Show campaign' , admin_brand_campaign_path(campaign.brand_id, campaign.id )
#   end
#
# end
