ActiveAdmin.register ThirdPartyRegistration do
  menu :parent => "More options"
  #config.sort_order = "id_asc"
  actions :index

  #scope :exclude_event,
  #      lambda  { |option|
  #        puts option.inspect
  #      }

  #search_method :exclude_event

  filter :brand_code_id
  filter :received_date
  filter :received_sign
  filter :created_at
  filter :updated_at

  index :download_links => false do
    column :id
    column ('brand') do |third_party_registration|
      third_party_registration.brand_code.campaign.brand.name + ', ' + third_party_registration.brand_code.campaign.name
    end
    column :received_date
    column :received_sign
    column :created_at
  end

end