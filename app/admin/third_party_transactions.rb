ActiveAdmin.register ThirdPartyTransaction do

  menu :parent => "More options"
  actions :show, :index

  before_filter :params_campaign

  scope :all, :default => true
  scope :marketplace do |tpt|
    tpt.marketplace
  end
  scope :funds do |tpt|
    tpt.funds
  end
  scope :redemption do |tpt|
    tpt.redemption
  end
  scope :pending_redemption do |tpt|
    tpt.pending_redemption
  end
  scope :confirm_redemption do |tpt|
    tpt.confirm_redemption
  end
  scope :reverse_redemption do |tpt|
    tpt.reverse_redemption
  end



  controller do
    def params_campaign
      Thread.current[:campaign_name] = params[:campaign_name]
      Thread.current[:brand_name] = params[:brand_name]
    end
  end

  filter :brand_code_campaign_name, :label => 'Campaign', :as => :string ,:collection => proc { ThirdPartyTransaction.campaign.where( :name => Thread.current[:campaign_name] ).active }
  filter :brand_code_campaign_brand_name, :label => 'Brand', :as => :string ,:collection => proc { ThirdPartyTransaction.brand.where( :name => Thread.current[:brand_name] ).active }
  filter :tp_transaction_state_id
  filter :tp_transaction_type_id
  filter :brand_code_id, :as => :numeric
  filter :third_party_entity_id
  filter :gift_code_id, :as => :numeric
  filter :transaction_value
  filter :transaction_currency
  filter :new_balance
  filter :received_hash
  filter :dir_ip
  filter :terminal_id
  filter :reversed
  filter :confirmed
  filter :office_id
  filter :related_transaction_id
  filter :created_at
  filter :updated_at

  show do
    attributes_table do
      row :id
      row :third_party_entity_id, :sortable => 'third_party_transactions.third_party_entity_id' do |tpt|
        (tpt.third_party_entity_id) ? link_to( tpt.third_party_entity.name ,  admin_third_party_entity_path(:id=>tpt.third_party_entity_id) ): ''
      end
      row :tp_transaction_type_id, :sortable => 'third_party_transactions.tp_transaction_type_id' do |tpt|
        (tpt.tp_transaction_type_id) ? tpt.tp_transaction_type.name : ''
      end
      row :tp_transaction_state_id, :sortable => 'third_party_transactions.tp_transaction_state_id' do |tpt|
        (tpt.tp_transaction_state_id) ? tpt.tp_transaction_state.name : ''
      end
      row :brand_code_id, :sortable => 'third_party_transactions.brand_code_id' do |tpt|
        (tpt.third_party_entity_id && tpt.brand_code_id) ? link_to( tpt.brand_code.to_s, admin_third_party_entity_brand_code_path(:third_party_entity_id=>tpt.third_party_entity_id, :id => tpt.brand_code_id) ): ''
      end
      row :gift_code_id, :sortable => 'third_party_transactions.gift_code_id' do |tpt|
        (tpt.gift_code_id) ? link_to( tpt.gift_code_id, admin_gift_code_path(:id => tpt.gift_code_id) ) : ''
      end
      row :transaction_value
      row :transaction_currency
      row :new_balance
      row :dir_ip
      row :terminal_id
      row :created_at
      row :related_transaction_id
      row :reversed
      row :confirmed
      row :office_id, :sortable => 'third_party_transactions.office_id' do |tpt|
        (tpt.office_id) ? link_to( tpt.office, admin_brand_office_path(:brand_id => tpt.brand_code.brand,:id => tpt.office_id) ) : ''
      end
      row :received_hash
    end
  end

  index do
    column :id
    column :third_party_entity_id, :sortable => 'third_party_transactions.third_party_entity_id' do |tpt|
      (tpt.third_party_entity_id) ? link_to( tpt.third_party_entity.name ,  admin_third_party_entity_path(:id=>tpt.third_party_entity_id) ): ''
    end
    column :tp_transaction_type_id, :sortable => 'third_party_transactions.tp_transaction_type_id' do |tpt|
      (tpt.tp_transaction_type_id) ? tpt.tp_transaction_type.name : ''
    end
    column :tp_transaction_state_id, :sortable => 'third_party_transactions.tp_transaction_state_id' do |tpt|
      (tpt.tp_transaction_state_id) ? tpt.tp_transaction_state.name : ''
    end
    column :brand_code_id, :sortable => 'third_party_transactions.brand_code_id' do |tpt|
      (tpt.third_party_entity_id && tpt.brand_code_id) ? link_to( tpt.brand_code.to_s, admin_third_party_entity_brand_code_path(:third_party_entity_id=>tpt.third_party_entity_id, :id => tpt.brand_code_id) ): ''
    end
    column :gift_code_id, :sortable => 'third_party_transactions.gift_code_id' do |tpt|
      (tpt.gift_code_id) ? link_to( tpt.gift_code_id, admin_gift_code_path(:id => tpt.gift_code_id) ) : ''
    end
    column :transaction_value
    column :transaction_currency
    column :new_balance
    column :dir_ip
    column :terminal_id
    column :created_at
    column :related_transaction_id
    column :reversed
    column :confirmed
    column :office_id, :sortable => 'third_party_transactions.office_id' do |tpt|
      (tpt.office_id) ? link_to( tpt.office, admin_brand_office_path(:brand_id => tpt.brand_code.brand,:id => tpt.office_id) ) : ''
    end
    column :received_hash
    default_actions
  end

end