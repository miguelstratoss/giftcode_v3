ActiveAdmin.register CampaignPoint do

  belongs_to :brand, parent_class: Brand

  form do |f|
    f.inputs "" do
      f.input :country, :required => true
      f.input :currency, :required => true
      f.input :cost
      f.input :name
      f.input :convenio
      f.input :redeban
      f.input :message
      f.input :send_mail
      f.input :send_sms
      f.input :begin_date
      f.input :end_date
      f.input :points_required
      f.input :number_of_free_gift_codes
      f.input :observations
      f.input :enabled
    end
    f.buttons
  end

  controller do
    def create
      params[:campaign_point].merge!(
          {
              campaign_state_id: CampaignState::STATES[:disabled],
              cost_admin: 0,
              iva_cost_admin: 0,
              iva: 0
          }
      )
      create!
    end
  end

  action_item only:[:show] do
    if campaign_point.convenio_redeban?
      link_to( 'Redeban', admin_campaign_batches_path(campaign_point) )
    end
  end

end