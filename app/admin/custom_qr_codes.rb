ActiveAdmin.register CustomQrCode do

  config.sort_order = "id_asc"
  menu :parent => "More options"
  actions :index, :show, :new, :create

  filter :admin_user
  filter :content
  filter :size
  filter :created_at
  filter :updated_at

  controller do

    before_filter :only => :index do
      @per_page = 10
    end

    def new
      @custom_qr_code = CustomQrCode.new
      @custom_qr_code.initialize_default_values
    end

    def create
      @custom_qr_code = CustomQrCode.new(params[:custom_qr_code])
      @custom_qr_code.admin_user = current_admin_user
      if @custom_qr_code.save
        flash[:notice] = "The custom qr code was successfully created."
        redirect_to admin_custom_qr_codes_path
        return
      end
      puts "Numero de errores"+@custom_qr_code.errors.to_a.to_s
      render 'new'
    end
  end

  form do |f|
    f.inputs "" do
      f.input :image, :as => :file
      f.input :size, :as => :select, :collection => Hash[CustomQrCode.qr_code_sizes.map{|qrcs| [qrcs[:description], qrcs[:id]]}], :hint => "Default size = 4."
      f.input :content
    end
    f.buttons
  end


  show do |custom_qr_code|
    attributes_table do
      row :id
      row :image do
        image_tag(custom_qr_code.image.url.to_s)
      end
      row :content
      row :size
      row :qr_code_image do
        raw "<img src='#{ChunkyPNG::Image.from_blob(custom_qr_code.binary_qr_code).to_data_url}'>"
      end
      row :admin_user
      row :created_at
      row :updated_at
    end
    # active_admin_comments
  end


  index do
    column :id do |custom_qr_code|
      link_to custom_qr_code.id, admin_custom_qr_code_path(custom_qr_code)
    end
    column :image do |custom_qr_code|
      image_tag(custom_qr_code.image.url.to_s)
    end
    column :qr_code_image do |custom_qr_code|
      raw "<img src='#{ChunkyPNG::Image.from_blob(custom_qr_code.binary_qr_code).to_data_url}'>"
    end
    column :content
    column :size
    column :admin_user
    column :created_at
    column :updated_at
    default_actions
  end
  
end
