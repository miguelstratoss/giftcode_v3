#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
sexM = Sex.create(:name => 'Masculino')
sexF = Sex.create(:name => 'Femenino')
sexO = Sex.create(:name => 'Otro')

#For categories
category1 = Category.create(:name => "Zapatos", :_class => "shoes",:position => 1, :enabled => true )
category2 = Category.create(:name => "Ropa", :_class => "cloath", :position => 2, :enabled => true)
category3 = Category.create(:name => "Música", :_class => "music", :position => 3, :enabled => true)
category4 = Category.create(:name => "Libros", :_class => "books", :position => 4, :enabled => true)
category5 = Category.create(:name => "Cine", :_class => "movies", :position => 5, :enabled => true)
category6 = Category.create(:name => "Restaurantes", :_class => "drinks", :position => 6, :enabled => true)
category7 = Category.create(:name => "Tecnología", :_class => "tech", :position => 7, :enabled => true)
category8 = Category.create(:name => "Belleza", :_class => "personalcare", :position => 8, :enabled => true)
category9 = Category.create(:name => "Accesorios", :_class => "accesories", :position => 9, :enabled => true)
category10 = Category.create(:name => "Deportes", :_class => "sports", :position => 10, :enabled => true)
category11 = Category.create(:name => "Tiendas por departamentos", :_class => "departaments", :position => 11, :enabled => true)
category12 = Category.create(:name => "Bebés", :_class => "babies", :position => 12, :enabled => true)

#For countries, states, and cities
colombia = Country.create(:name => 'Colombia', :bin => '8457')
usa = Country.create(:name => 'Estados Unidos')
state1 = Country.find_by_name("Colombia").states.create(:name => "Cundinamarca")
city1 = State.find_by_name("Cundinamarca").cities.create(:name => "Bogotá")
city1 = State.find_by_name("Cundinamarca").cities.create(:name => "Chía")
state2 = Country.find_by_name("Colombia").states.create(:name => "Antioquia")
city2 = State.find_by_name("Antioquia").cities.create(:name => "Medellín")
state3 = Country.find_by_name("Colombia").states.create(:name => "Atlántico")
city3 = State.find_by_name("Atlántico").cities.create(:name => "Barranquilla")
state4 = Country.find_by_name("Estados Unidos").states.create(:name => "California")
city4 = State.find_by_name("California").cities.create(:name => "San Francisco")


#For campaign state
campaign_state1 = CampaignState.create(:name => "Habilitada")
campaign_state2 = CampaignState.create(:name => "Deshabilitada")

#For languages
lanE = Language.create(:name => 'Español')

#For currencies
currency = Currency.create(:name => "Pesos colombianos", :symbol => "$", :short_name => "COP")

#For roles
role1 = Role.create(:name => 'Admin_Brand')
role2 = Role.create(:name => 'Admin_GiftCode')
role3 = Role.create(:name => 'Admin_Brand_Office')

#For admin users
admin_user1 = AdminUser.create(:email => 'm.segura@greencode.com.co', :password => '123456789', :password_confirmation => '123456789', :role_id => 2)

#For user states
us1 = UserState.create(:name => 'Registrado')
us2 = UserState.create(:name => 'NO Registrado')
us3 = UserState.create(:name => 'Eliminado')

#For friendship states
fs1 = FriendshipState.create(:name => 'Sin relacion')
fs2 = FriendshipState.create(:name => 'Invitado')
fs3 = FriendshipState.create(:name => 'Amigo')
fs4 = FriendshipState.create(:name => 'Eliminado')

#For giftcodes states
gift_code_state_1 = GiftCodeState.create(:id => '1',:name => 'Generado y Pendiente de Pago')
gift_code_state_2 = GiftCodeState.create(:id => '2',:name => 'Pago aceptado')
gift_code_state_3 = GiftCodeState.create(:id => '3',:name => 'Pago rechazado')
gift_code_state_4 = GiftCodeState.create(:id => '4',:name => 'Gratuito y exclusivo de una oficina')
gift_code_state_5 = GiftCodeState.create(:id => '5',:name => 'Generado Oficina pendiente')
gift_code_state_6 = GiftCodeState.create(:id => '6',:name => 'Generado Oficina confirmado')
gift_code_state_7 = GiftCodeState.create(:id => '7',:name => 'Generado pagina externa pendiente')
gift_code_state_8 = GiftCodeState.create(:id => '8',:name => 'Generado pagina externa pago aceptado')
gift_code_state_9 = GiftCodeState.create(:id => '9',:name => 'Generado pagina externa pago rechazado')
gift_code_state_10 = GiftCodeState.create(:id => '10',:name => 'Gratis puntos')

#For users
user1 = User.create(:first_name => 'none', :last_name => 'none', :email => 'none@greencode.com.co', :password => 'bhunjimkocftr876', :password_confirmation => 'bhunjimkocftr876')

#For issues
# issue = Issue.create(:name => 'Comercial')
# no existe servicio

s1 = Setting.create( :name => 'Show Full Version', :value => 'true')

#ThirdParty TransactionStates
TpTransactionState.create(:id => 1, :name => 'success')
TpTransactionState.create(:id => 2 ,:name => 'wrong_sign')
TpTransactionState.create(:id => 3, :name => 'giftcard_not_found')
TpTransactionState.create(:id => 4 ,:name => 'wrong_credentials_brand')
TpTransactionState.create(:id => 5 ,:name => 'wrong_credentials_commerce_id')
TpTransactionState.create(:id => 6 ,:name => 'no_funds')
TpTransactionState.create(:id => 7, :name => 'insufficient_funds')
TpTransactionState.create(:id => 8, :name => 'value_format_error')
TpTransactionState.create(:id => 9, :name => 'could_not_save')
TpTransactionState.create(:id => 10, :name => 'target_transaction_not_found')
TpTransactionState.create(:id => 11, :name => 'ip_blocked')
TpTransactionState.create(:id => 12, :name => 'giftcard_expired')
TpTransactionState.create(:id => 13, :name => 'ip_client_blocked')
TpTransactionState.create(:id => 14, :name => 'gitcard_not_belongs_to_brand')
TpTransactionState.create(:id => 99, :name => 'unknown_error')

#InputTypes
InputType.create(:id => 1, :name => 'Manual' )
InputType.create(:id => 2, :name => 'QR-Reader' )

#ThirdParty TransactionTypes
TpTransactionType.create(:id => 1, :name => 'marketplace_request' )
TpTransactionType.create(:id => 2, :name => 'funds_request' )
TpTransactionType.create(:id => 3, :name => 'redemption' )
TpTransactionType.create(:id => 4, :name => 'pending_redemption' )
TpTransactionType.create(:id => 5, :name => 'confirm_redemption' )
TpTransactionType.create(:id => 6, :name => 'reverse_redemption' )

#To create several brands
if Brand.find_by_name('Vélez').nil?
  brand = Brand.create(:name => 'Vélez', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@velez.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Spring Step').nil?
  brand = Brand.create(:name => 'Spring Step', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@springstep.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
  brand_category_2 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mario Hernández').nil?
  brand = Brand.create(:name => 'Mario Hernández', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@mariohernandez.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bosi').nil?
  brand = Brand.create(:name => 'Bosi', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@bosi.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Adidas').nil?
  brand = Brand.create(:name => 'Adidas', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@adidas.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
  brand_category_2 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Nike').nil?
  brand = Brand.create(:name => 'Nike', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@nike.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
  brand_category_2 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Arturo Calle').nil?
  brand = Brand.create(:name => 'Arturo Calle', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@arturocalle.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
  brand_category_2 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
  brand_category_3 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Armi').nil?
  brand = Brand.create(:name => 'Armi', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@armi.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Chevignon').nil?
  brand = Brand.create(:name => 'Chevignon', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@chevignon.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('FDS').nil?
  brand = Brand.create(:name => 'FDS', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@fds.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Gef').nil?
  brand = Brand.create(:name => 'Gef', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@gef.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Naf Naf').nil?
  brand = Brand.create(:name => 'Naf Naf', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@nafnaf.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Levi\'s').nil?
  brand = Brand.create(:name => 'Levi\'s', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@levis.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Studio F').nil?
  brand = Brand.create(:name => 'Studio F', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@studiof.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Tennis').nil?
  brand = Brand.create(:name => 'Tennis', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@tennis.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Zara').nil?
  brand = Brand.create(:name => 'Zara', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@tennis.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pilatos').nil?
  brand = Brand.create(:name => 'Pilatos', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@pilatos.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Tania').nil?
  brand = Brand.create(:name => 'Tania', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@tania.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Totto').nil?
  brand = Brand.create(:name => 'Totto', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@totto.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Tango Discos').nil?
  brand = Brand.create(:name => 'Tango Discos', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@tangodiscos.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Música').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Librería Nacional').nil?
  brand = Brand.create(:name => 'Librería Nacional', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@librerianacional.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Libros').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Panamericana').nil?
  brand = Brand.create(:name => 'Panamericana', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@panamericana.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Libros').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Cine Colombia').nil?
  brand = Brand.create(:name => 'Cine Colombia', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@cinecolombia.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Cinemark').nil?
  brand = Brand.create(:name => 'Cinemark', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@cinemark.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('El Corral').nil?
  brand = Brand.create(:name => 'El Corral', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@elcorral.com', :enable_for_selling => true )
  #brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Kokoriko').nil?
  brand = Brand.create(:name => 'Kokoriko', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@kokoriko.com', :enable_for_selling => true )
  #brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Crepes & Waffles').nil?
  brand = Brand.create(:name => 'Crepes & Waffles', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@crepesywaffles.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Hard Rock Café').nil?
  brand = Brand.create(:name => 'Hard Rock Café', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@hardrockcafe.com', :enable_for_selling => true )
  #brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bogotá Beer Company').nil?
  brand = Brand.create(:name => 'Bogotá Beer Company', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@bogotabeercompany.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mac Donald\'s').nil?
  brand = Brand.create(:name => 'Mac Donald\'s', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@macdonalds.com', :enable_for_selling => true )
  #brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('iShop').nil?
  brand = Brand.create(:name => 'iShop', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@ishop.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Ktronix').nil?
  brand = Brand.create(:name => 'Ktronix', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@ktronix.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Fedco').nil?
  brand = Brand.create(:name => 'Fedco', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@fedco.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('La Riviera').nil?
  brand = Brand.create(:name => 'La Riviera', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@lariviera.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Boot\'s \'N Bags').nil?
  brand = Brand.create(:name => 'Boot\'s \'N Bags', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@bootsnbags.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carolina Cruz').nil?
  brand = Brand.create(:name => 'Carolina Cruz', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@carolinacruz.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Éxito').nil?
  brand = Brand.create(:name => 'Éxito', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@exito.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pepe Ganga').nil?
  brand = Brand.create(:name => 'Pepe Ganga', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@pepeganga.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Falabella').nil?
  brand = Brand.create(:name => 'Falabella', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@falabella.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Baby Ganga').nil?
  brand = Brand.create(:name => 'Baby Ganga', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@babyganga.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Offcorss').nil?
  brand = Brand.create(:name => 'Offcorss', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@offcors.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end

#Siguiente lote de marcas
if Brand.find_by_name('Hush Puppies').nil?
  brand = Brand.create(:name => 'Hush Puppies', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Santorini').nil?
  brand = Brand.create(:name => 'Santorini', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Payless').nil?
  brand = Brand.create(:name => 'Payless', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Puma').nil?
  brand = Brand.create(:name => 'Puma', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Boot\'s \'N Bags').present?
  brand = Brand.find_by_name('Boot\'s \'N Bags')
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('ESPRIT').nil?
  brand = Brand.create(:name => 'ESPRIT', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Luber').nil?
  brand = Brand.create(:name => 'Luber', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('California Inn').nil?
  brand = Brand.create(:name => 'California Inn', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Diesel').nil?
  brand = Brand.create(:name => 'Diesel', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Tommy Hilfiger').nil?
  brand = Brand.create(:name => 'Tommy Hilfiger', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Zephir').nil?
  brand = Brand.create(:name => 'Zephir', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Groggy').nil?
  brand = Brand.create(:name => 'Groggy', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carlos Nieto').nil?
  brand = Brand.create(:name => 'Carlos Nieto', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Aeropostal').nil?
  brand = Brand.create(:name => 'Aeropostal', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bennetton').nil?
  brand = Brand.create(:name => 'Bennetton', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Forever 21').nil?
  brand = Brand.create(:name => 'Forever 21', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Girbaud').nil?
  brand = Brand.create(:name => 'Girbaud', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Seven & Seven').nil?
  brand = Brand.create(:name => 'Seven & Seven', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pronto').nil?
  brand = Brand.create(:name => 'Pronto', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Americanino').nil?
  brand = Brand.create(:name => 'Americanino', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Ralph Lauren').nil?
  brand = Brand.create(:name => 'Ralph Lauren', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Abril').nil?
  brand = Brand.create(:name => 'Abril', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Paul & Bear').nil?
  brand = Brand.create(:name => 'Paul & Bear', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Stradivarius').nil?
  brand = Brand.create(:name => 'Stradivarius', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bershka').nil?
  brand = Brand.create(:name => 'Bershka', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mario Hernández').present?
  brand = Brand.find_by_name('Mario Hernández')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Totto').present?
  brand = Brand.find_by_name('Totto')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Americanino').present?
  brand = Brand.find_by_name('Americanino')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Entertainment Store').nil?
  brand = Brand.create(:name => 'Entertainment Store', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Música').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Librería Lerner').nil?
  brand = Brand.create(:name => 'Librería Lerner', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@admin.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Libros').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Procinal').nil?
  brand = Brand.create(:name => 'Procinal', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Cinépolis').nil?
  brand = Brand.create(:name => 'Cinépolis', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)
end

if Brand.find_by_name('Juan Valdez').nil?
  brand = Brand.create(:name => 'Juan Valdez', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Don Jediondo').nil?
  brand = Brand.create(:name => 'Don Jediondo', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Buffalo Wings').nil?
  brand = Brand.create(:name => 'Buffalo Wings', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Wings').nil?
  brand = Brand.create(:name => 'Wings', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Burger King').nil?
  brand = Brand.create(:name => 'Burger King', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mr. Lee').nil?
  brand = Brand.create(:name => 'Mr. Lee', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Subway').nil?
  brand = Brand.create(:name => 'Subway', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Sandwich Qbano').nil?
  brand = Brand.create(:name => 'Sandwich Qbano', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carbón 100').nil?
  brand = Brand.create(:name => 'Carbón 100', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Frisby').nil?
  brand = Brand.create(:name => 'Frisby', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Dogger').nil?
  brand = Brand.create(:name => 'Dogger', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Andrés Exprés').nil?
  brand = Brand.create(:name => 'Andrés Exprés', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Taco Bell').nil?
  brand = Brand.create(:name => 'Taco Bell', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Papa John\'s').nil?
  brand = Brand.create(:name => 'Papa John\'s', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Archie\'s').nil?
  brand = Brand.create(:name => 'Archie\'s', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Jeno\'s Pizza').nil?
  brand = Brand.create(:name => 'Jeno\'s Pizza', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('El Corral').present?
  brand = Brand.find_by_name('El Corral')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Hard Rock Café').present?
  brand = Brand.find_by_name('Hard Rock Café')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Juan Valdez').present?
  brand = Brand.find_by_name('Juan Valdez')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Kokoriko').present?
  brand = Brand.find_by_name('Kokoriko')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mac Donald\'s').present?
  brand = Brand.find_by_name('Mac Donald\'s')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mac Center').nil?
  brand = Brand.create(:name => 'Mac Center', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Sony Store').nil?
  brand = Brand.create(:name => 'Sony Store  ', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Marlon Becerra').nil?
  brand = Brand.create(:name => 'Marlon Becerra', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Norberto Peluquería').nil?
  brand = Brand.create(:name => 'Norberto Peluquería', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bodytech').nil?
  brand = Brand.create(:name => 'Bodytech', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Spinning Center').nil?
  brand = Brand.create(:name => 'Spinning Center', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pylones').nil?
  brand = Brand.create(:name => 'Pylones', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Montblanc').nil?
  brand = Brand.create(:name => 'Montblanc', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Swatch').nil?
  brand = Brand.create(:name => 'Swatch', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Inkanta').nil?
  brand = Brand.create(:name => 'Inkanta', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Ice Watch').nil?
  brand = Brand.create(:name => 'Ice Watch', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Azulu').nil?
  brand = Brand.create(:name => 'Azulu', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mario Hernández').present?
  brand = Brand.find_by_name('Mario Hernández')
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Totto').present?
  brand = Brand.find_by_name('Totto')
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carolina Cruz').present?
  brand = Brand.find_by_name('Carolina Cruz')
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Freeport Sport').nil?
  brand = Brand.create(:name => 'Freeport Sport', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Speedo').nil?
  brand = Brand.create(:name => 'Speedo', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('The North Face').nil?
  brand = Brand.create(:name => 'The North Face', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Puma').present?
  brand = Brand.find_by_name('Puma')
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bodytech').present?
  brand = Brand.find_by_name('Bodytech')
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Spinning Center').present?
  brand = Brand.find_by_name('Spinning Center')
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('La Polar').nil?
  brand = Brand.create(:name => 'La Polar', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Ripley').nil?
  brand = Brand.create(:name => 'Ripley', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carulla').nil?
  brand = Brand.create(:name => 'Carulla', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Alkosto').nil?
  brand = Brand.create(:name => 'Alkosto', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Jumbo').nil?
  brand = Brand.create(:name => 'Jumbo', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end

if Brand.find_by_name('Travesuras').nil?
  brand = Brand.create(:name => 'Travesuras', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('EPK').nil?
  brand = Brand.create(:name => 'EPK', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Divercity').nil?
  brand = Brand.create(:name => 'Divercity', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pepe Ganga').present?
  brand = Brand.find_by_name('Pepe Ganga')
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end




# NUevas Marcas

if Brand.find_by_name('Balsámico').nil?
  brand = Brand.create(:name => 'Balsámico' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Beer-Pub').nil?
  brand = Brand.create(:name => 'Beer-Pub' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Burger Town').nil?
  brand = Brand.create(:name => 'Burger Town' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Cascabel').nil?
  brand = Brand.create(:name => 'Cascabel' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Cinabón').nil?
  brand = Brand.create(:name => 'Cinabón' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('El Trompo').nil?
  brand = Brand.create(:name => 'El Trompo' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Fridays - TGI').nil?
  brand = Brand.create(:name => 'Fridays - TGI' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Hooters').nil?
  brand = Brand.create(:name => 'Hooters' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Los Sánduches del Sr. Ostia').nil?
  brand = Brand.create(:name => 'Los Sánduches del Sr. Ostia' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mercado').nil?
  brand = Brand.create(:name => 'Mercado' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Myriam Camhi').nil?
  brand = Brand.create(:name => 'Myriam Camhi' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Oma').nil?
  brand = Brand.create(:name => 'Oma' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Presto').nil?
  brand = Brand.create(:name => 'Presto' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Randys').nil?
  brand = Brand.create(:name => 'Randys' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('The Khalifa').nil?
  brand = Brand.create(:name => 'The Khalifa' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Wok').nil?
  brand = Brand.create(:name => 'Wok' )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end


if Brand.find_by_name('Do iT').nil?
  brand = Brand.create(:name => 'Do iT', :sn_twitter_name => '@Doit_Accesorios' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Kevins').nil?
  brand = Brand.create(:name => 'Kevins', :sn_twitter_name => '@kevinsjoyeros' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Marruecos').nil?
  brand = Brand.create(:name => 'Marruecos' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mercedes Salazar').nil?
  brand = Brand.create(:name => 'Mercedes Salazar', :sn_twitter_name => '@mercedessalazar' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Multiópticas').nil?
  brand = Brand.create(:name => 'Multiópticas' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Opticentro').nil?
  brand = Brand.create(:name => 'Opticentro', :sn_twitter_name => '@Doit_Accesorios' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Paul Frank').nil?
  brand = Brand.create(:name => 'Paul Frank', :sn_twitter_name => '@paulfranktweets' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Poor Designer').nil?
  brand = Brand.create(:name => 'Poor Designer', :sn_twitter_name => '@poordesigner' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Tornamesa').nil?
  brand = Brand.create(:name => 'Tornamesa', :sn_twitter_name => '@latornamesa' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Victoria Secret').nil?
  brand = Brand.create(:name => 'Victoria Secret', :sn_twitter_name => '@victoriassecret' )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end




if Brand.find_by_name('Lego').nil?
  brand = Brand.create(:name => 'Lego')
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mother Care').nil?
  brand = Brand.create(:name => 'Mother Care', :sn_twitter_name => '@mothercarecol' )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end




if Brand.find_by_name('Caretas').nil?
  brand = Brand.create(:name => 'Caretas', :sn_twitter_name => '@Caretas_Col' )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Kolor Nails').nil?
  brand = Brand.create(:name => 'Kolor Nails', :sn_twitter_name => '@KolorNails' )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('La Barbería').nil?
  brand = Brand.create(:name => 'La Barbería', :sn_twitter_name => '@labarberia_bta' )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('We Love Nails').nil?
  brand = Brand.create(:name => 'We Love Nails', :sn_twitter_name => '@welovenailsbar' )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end




if Brand.find_by_name('New Balance').nil?
  brand = Brand.create(:name => 'New Balance', :sn_twitter_name => '@NewBalanceCO' )
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Runing Balboa').nil?
  brand = Brand.create(:name => 'Runing Balboa', :sn_twitter_name => '@runningbalboaco' )
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end




if Brand.find_by_name('La Música').nil?
  brand = Brand.create(:name => 'La Música' )
  brand_category_1 = Category.find_by_name('Música').brand_categories.create(:brand_id => brand.id)
end



if Brand.find_by_name('Celio').nil?
  brand = Brand.create(:name => 'Celio' )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Clonhadas').nil?
  brand = Brand.create(:name => 'Clonhadas',  :sn_twitter_name =>  '@Clonhadas1' )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Desigual').nil?
  brand = Brand.create(:name => 'Desigual',  :sn_twitter_name =>  '@Desigual' )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('ELA').nil?
  brand = Brand.create(:name => 'ELA' )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Kenzo').nil?
  brand = Brand.create(:name => 'Kenzo')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Koaj').nil?
  brand = Brand.create(:name => 'Koaj' )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Le Collezioni').nil?
  brand = Brand.create(:name => 'Le Collezioni' )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mango -MNG').nil?
  brand = Brand.create(:name => 'Mango -MNG')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('no Project').nil?
  brand = Brand.create(:name => 'no Project' )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pat Primo').nil?
  brand = Brand.create(:name => 'Pat Primo')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Polo Club').nil?
  brand = Brand.create(:name => 'Polo Club' )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Rosépistol').nil?
  brand = Brand.create(:name => 'Rosépistol')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Sportline').nil?
  brand = Brand.create(:name => 'Sportline')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Super Dry').nil?
  brand = Brand.create(:name => 'Super Dry')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Leonisa').nil?
  brand = Brand.create(:name => 'Leonisa')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bombalu').nil?
  brand = Brand.create(:name => 'Bombalu')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end



if Brand.find_by_name('I Shop').nil?
  brand = Brand.create(:name => 'I Shop', :sn_twitter_name => '@ishopcol')
  brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)
end



if Brand.find_by_name('Colsubsidio').nil?
  brand = Brand.create(:name => 'Colsubsidio')
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Home Sentry').nil?
  brand = Brand.create(:name => 'Home Sentry')
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end



if Brand.find_by_name('Brahma').nil?
  brand = Brand.create(:name => 'Brahma', :sn_twitter_name => '@BrahmaFootwear' )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Crocs').nil?
  brand = Brand.create(:name => 'Crocs', :sn_twitter_name => '@crocs' )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Opposite').nil?
  brand = Brand.create(:name => 'Opposite' )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('People Plays').nil?
  brand = Brand.create(:name => 'People Plays' )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Reinder').nil?
  brand = Brand.create(:name => 'Reinder' )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name("Replay's").nil?
  brand = Brand.create(:name => "Replay's" )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Sportline').nil?
  brand = Brand.create(:name => 'Sportline' )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Umbro').nil?
  brand = Brand.create(:name => 'Umbro' )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end


##########



#FRANQUICIAS SOPORTADAS
cbf1 = CredibancoFranquicia.create(:name => 'Visa', :identificacion => 'VISA', :codigo_identificacion => 90, :length => 16, :enabled => true, :ahorro => true, :corriente => true, :credito => true )
cbf1.credibanco_franquicia_prefixes.create(:bin => 4, :enabled => true)

cbf2 = CredibancoFranquicia.create(:name => 'Credencial', :identificacion => 'CREDENCIAL', :codigo_identificacion => 23, :length => 16, :enabled => true, :ahorro => false, :corriente => false, :credito => true )
cbf2.credibanco_franquicia_prefixes.create(:bin => 5406251, :enabled => true)
cbf2.credibanco_franquicia_prefixes.create(:bin => 5412038, :enabled => true)
cbf2.credibanco_franquicia_prefixes.create(:bin => 5412039, :enabled => true)
cbf2.credibanco_franquicia_prefixes.create(:bin => 5432032, :enabled => true)
cbf2.credibanco_franquicia_prefixes.create(:bin => 5491511, :enabled => true)

cbf3 =CredibancoFranquicia.create(:name => 'American Express', :identificacion => 'AMEX', :codigo_identificacion => 30, :length => 15, :enabled => true, :ahorro => false, :corriente => false, :credito => true )
cbf3.credibanco_franquicia_prefixes.create(:bin => 34, :enabled => true)
cbf3.credibanco_franquicia_prefixes.create(:bin => 37, :enabled => true)

cbf4 = CredibancoFranquicia.create(:name => 'Diners Club', :identificacion => 'DINERS', :codigo_identificacion => 34, :length => 14, :enabled => true, :ahorro => false, :corriente => false, :credito => true )
cbf4.credibanco_franquicia_prefixes.create(:bin => 36, :enabled => true)

cbf5 = CredibancoFranquicia.create(:name => 'Master Card', :identificacion => 'MASTERCARD', :codigo_identificacion => 91, :length => 16, :enabled => true, :ahorro => true, :corriente => true, :credito => true )
cbf5.credibanco_franquicia_prefixes.create(:bin => 51, :enabled => true)
cbf5.credibanco_franquicia_prefixes.create(:bin => 52, :enabled => true)
cbf5.credibanco_franquicia_prefixes.create(:bin => 53, :enabled => true)
cbf5.credibanco_franquicia_prefixes.create(:bin => 54, :enabled => true)
cbf5.credibanco_franquicia_prefixes.create(:bin => 55, :enabled => true)