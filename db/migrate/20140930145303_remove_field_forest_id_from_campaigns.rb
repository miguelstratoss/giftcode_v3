class RemoveFieldForestIdFromCampaigns < ActiveRecord::Migration
  def up
    remove_column :campaigns, :forest_id
  end

  def down
    add_column :campaigns, :forest_id, :integer
  end
end
