class RemoveFieldGeneratedByGiftCodeFromBrandCodes < ActiveRecord::Migration
  def up
    remove_column :brand_codes, :generated_by_gift_code
  end

  def down
    add_column :brand_codes, :generated_by_gift_code, :boolean
  end
end
