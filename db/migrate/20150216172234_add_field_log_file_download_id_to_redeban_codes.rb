class AddFieldLogFileDownloadIdToRedebanCodes < ActiveRecord::Migration
  def change
    add_column :redeban_codes, :log_file_download_id, :integer
  end
end
