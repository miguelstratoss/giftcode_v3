class AddFieldsSaltIvToRedebanCode < ActiveRecord::Migration
  def change
    add_column :redeban_codes, :salt, :string
    add_column :redeban_codes, :iv, :string
  end
end
