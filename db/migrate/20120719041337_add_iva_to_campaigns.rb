class AddIvaToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :iva, :decimal, :precision => 10, :scale => 2, :default => 16

  end
end
