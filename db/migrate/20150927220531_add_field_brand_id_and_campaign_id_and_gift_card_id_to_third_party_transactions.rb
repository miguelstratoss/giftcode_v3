class AddFieldBrandIdAndCampaignIdAndGiftCardIdToThirdPartyTransactions < ActiveRecord::Migration
  def change
    add_column :third_party_transactions, :brand_id, :integer
    add_column :third_party_transactions, :campaign_id, :integer
    add_column :third_party_transactions, :gift_card_id, :integer
  end
end
