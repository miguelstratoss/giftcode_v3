class AddFieldsConfirmationToUser < ActiveRecord::Migration
  def change
    add_column :users, :email_confirmation, :boolean
    add_column :users, :birthday_confirmation, :boolean
    add_column :users, :city_confirmation, :boolean
  end
end
