class DropSliderImagesTable < ActiveRecord::Migration
  def up
    drop_table :slider_images
  end

  def down
  end
end
