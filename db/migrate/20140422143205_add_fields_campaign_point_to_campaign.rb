class AddFieldsCampaignPointToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :begin_date, :date
    add_column :campaigns, :end_date, :date
    add_column :campaigns, :cost, :decimal, :precision => 10, :scale => 2
    add_column :campaigns, :points_required, :integer
    add_column :campaigns, :number_of_free_gift_codes, :integer
    add_column :campaigns, :enabled, :boolean
  end
end
