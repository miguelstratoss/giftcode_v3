class CreateTpTransactionStates < ActiveRecord::Migration
  def change
    create_table :tp_transaction_states do |t|
      t.integer :id
      t.string :name

      t.timestamps
    end
  end
end
