class ChangeDateReferralForUsers < ActiveRecord::Migration
  change_table :users do |t|
    t.change :date_referral, :datetime
  end
end
