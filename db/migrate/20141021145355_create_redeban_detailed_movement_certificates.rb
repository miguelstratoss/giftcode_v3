class CreateRedebanDetailedMovementCertificates < ActiveRecord::Migration
  def change
    create_table :redeban_detailed_movement_certificates do |t|
      t.string :file_name
      t.string :nombre_convenio
      t.datetime :fecha_hora_movimiento
      t.string :descripcion_movimiento
      t.integer :codigo_establecimiento
      t.integer :codigo_terminal
      t.string :tipo_transacion
      t.decimal :valor_transaccion
      t.integer :num_referencia

      t.timestamps
    end
  end
end
