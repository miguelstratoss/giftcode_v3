class DropRelationshipStates < ActiveRecord::Migration
  def up
    drop_table :relationship_states
  end

  def down
  end
end
