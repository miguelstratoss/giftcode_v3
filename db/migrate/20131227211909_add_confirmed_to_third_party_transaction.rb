class AddConfirmedToThirdPartyTransaction < ActiveRecord::Migration
  def change
    add_column :third_party_transactions, :confirmed, :boolean
  end
end
