class CreateCustomQrCodes < ActiveRecord::Migration
  def change
    create_table :custom_qr_codes do |t|
      t.string :image
      t.string :content
      t.integer :size
      t.binary :binary_qr_code
      t.integer :created_by

      t.timestamps
    end
  end
end
