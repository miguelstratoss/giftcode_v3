class RemoveFieldCurrencyIdFromGiftCode < ActiveRecord::Migration
  def up
    remove_column :gift_codes, :currency_id
  end

  def down
    add_column :gift_codes, :currency_id, :integer
  end
end
