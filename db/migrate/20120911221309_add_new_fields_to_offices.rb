class AddNewFieldsToOffices < ActiveRecord::Migration
  def change
    add_column :offices, :phone, :string

    add_column :offices, :mobile_phone, :string

    add_column :offices, :bank_account_number, :string

    add_column :offices, :bank_name, :string

    add_column :offices, :bank_account_type, :string

    add_column :offices, :local_company_name, :string

    add_column :offices, :schedule, :string

  end
end
