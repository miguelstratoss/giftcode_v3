class CreateRedebanDailyConsumptions < ActiveRecord::Migration
  def change
    create_table :redeban_daily_consumptions do |t|
      t.integer :redeban_code_id
      t.string :file_name
      t.decimal :valor_de_consumo
      t.date :fecha_vencimiento
      t.integer :estado
      t.decimal :valor_del_saldo

      t.timestamps
    end
  end
end
