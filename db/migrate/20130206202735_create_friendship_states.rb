class CreateFriendshipStates < ActiveRecord::Migration
  def change
    create_table :friendship_states do |t|
      t.string :name

      t.timestamps
    end
  end
end
