class CreateForestsSpecies < ActiveRecord::Migration
  def up
    create_table :forests_species, :id => false do |t|
      t.references :forest
      t.references :specie
      # t.timestamps
    end
  end

  def down
    drop_table :forests_species
  end
end
