class AddFromNameToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :from_first_name, :string
    add_column :gift_codes, :from_last_name, :string
    add_column :gift_codes, :from_email, :string
    add_column :gift_codes, :to_first_name, :string
    add_column :gift_codes, :to_last_name, :string
    remove_column :gift_codes, :to_user_name
  end
end
