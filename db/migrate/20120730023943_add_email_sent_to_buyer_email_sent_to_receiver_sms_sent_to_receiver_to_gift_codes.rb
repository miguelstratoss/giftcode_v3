class AddEmailSentToBuyerEmailSentToReceiverSmsSentToReceiverToGiftCodes < ActiveRecord::Migration
  def change
    add_column :gift_codes, :email_sent_to_buyer, :boolean

    add_column :gift_codes, :email_sent_to_receiver, :boolean

    add_column :gift_codes, :sms_sent_to_receiver, :boolean

  end
end
