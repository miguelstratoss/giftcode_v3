class AddAlreadyUsedToRedebanCode < ActiveRecord::Migration
  def change
    add_column :redeban_codes, :already_used, :boolean
  end
end
