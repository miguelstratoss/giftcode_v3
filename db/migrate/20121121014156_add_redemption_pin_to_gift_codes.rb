class AddRedemptionPinToGiftCodes < ActiveRecord::Migration
  def change
    add_column :gift_codes, :redemption_pin, :string
  end
end
