class AddFbkLocationNameAndFbkTimezoneAndFbkLocaleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fbk_location_name, :string

    add_column :users, :fbk_timezone, :string

    add_column :users, :fbk_locale, :string

  end
end
