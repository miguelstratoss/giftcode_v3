class CreateThirdPartyEntities < ActiveRecord::Migration
  def change
    create_table :third_party_entities do |t|
      t.string :name
      t.string :secret
      t.string :incocredito_id

      t.timestamps
    end
  end
end
