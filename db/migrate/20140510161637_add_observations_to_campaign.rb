class AddObservationsToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :observations, :string
  end
end
