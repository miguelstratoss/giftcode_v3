class AddOfficeIdToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :office_id, :integer
  end
end
