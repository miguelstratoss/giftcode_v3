class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name
      t.string :contact_name
      t.string :contact_email
      t.string :sn_twitter_name
      t.string :nit
      t.text :description
      t.string :url_website
      t.string :url_facebook
      t.string :url_twitter
      t.string :url_youtube
      t.string :url_linkedin
      t.string :image
      t.string :qr_code_image
      t.integer :manager_id

      t.timestamps
    end
  end
end
