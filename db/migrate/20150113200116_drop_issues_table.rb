class DropIssuesTable < ActiveRecord::Migration
  def up
    drop_table :issues
  end

  def down
  end
end
