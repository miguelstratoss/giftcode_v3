class AddFieldRedebanCodeIdToRedebanDetailedMovementCertificate < ActiveRecord::Migration
  def change
    add_column :redeban_detailed_movement_certificates, :redeban_code_id, :integer
  end
end
