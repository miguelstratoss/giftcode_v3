class CreateAdminControlAccessAlerts < ActiveRecord::Migration
  def change
    create_table :admin_control_access_alerts do |t|
      t.integer :admin_user_id
      t.string :alert_for_access_path

      t.timestamps
    end
  end
end
