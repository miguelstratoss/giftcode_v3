class AddFieldExpirationDateToRedebanCode < ActiveRecord::Migration
  def change
    add_column :redeban_codes, :expiration_date, :string
  end
end
