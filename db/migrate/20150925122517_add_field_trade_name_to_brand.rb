class AddFieldTradeNameToBrand < ActiveRecord::Migration
  def change
    add_column :brands, :trade_name, :string
  end
end
