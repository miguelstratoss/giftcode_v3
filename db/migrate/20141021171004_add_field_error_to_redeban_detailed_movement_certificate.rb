class AddFieldErrorToRedebanDetailedMovementCertificate < ActiveRecord::Migration
  def change
    add_column :redeban_detailed_movement_certificates, :error, :string
  end
end
