class CreateSuggests < ActiveRecord::Migration
  def change
    create_table :suggests do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
  end
end
