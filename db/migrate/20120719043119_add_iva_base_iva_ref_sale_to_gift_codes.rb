class AddIvaBaseIvaRefSaleToGiftCodes < ActiveRecord::Migration
  def change
    add_column :gift_codes, :iva, :decimal, :precision => 10, :scale => 2, :default => 16

    add_column :gift_codes, :base_iva, :decimal, :precision => 10, :scale => 2

    add_column :gift_codes, :ref_sale, :string

  end
end
