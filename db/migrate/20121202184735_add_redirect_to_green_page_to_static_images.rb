class AddRedirectToGreenPageToStaticImages < ActiveRecord::Migration
  def change
    add_column :static_images, :redirect_to_green_page, :boolean

  end
end
