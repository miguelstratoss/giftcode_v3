class AddLogNoveltyAndLogConsecutiveToRedebanCode < ActiveRecord::Migration
  def change
    add_column :redeban_codes, :log_novelty, :integer
    add_column :redeban_codes, :log_consecutive, :string
    add_column :redeban_codes, :log_value, :decimal, :precision => 10, :scale => 2
  end
end
