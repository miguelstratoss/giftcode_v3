class AddFieldFechaEmisionToBatch < ActiveRecord::Migration
  def change
    add_column :batches, :fecha_emision, :date
  end
end
