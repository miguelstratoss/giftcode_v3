class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.references :brand
      t.references :user
      t.boolean :enabled

      t.timestamps
    end
    add_index :favorites, :brand_id
    add_index :favorites, :user_id
  end
end
