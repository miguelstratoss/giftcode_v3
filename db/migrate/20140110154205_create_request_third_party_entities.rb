class CreateRequestThirdPartyEntities < ActiveRecord::Migration
  def change
    create_table :request_third_party_entities do |t|
      t.integer :third_party_entity_id
      t.integer :gift_code_id
      t.text :data_sent
      t.text :data_received

      t.timestamps
    end
  end
end
