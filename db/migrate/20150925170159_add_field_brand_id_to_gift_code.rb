class AddFieldBrandIdToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :brand_id, :integer
  end
end
