class AddBrandIdToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :brand_id, :integer
  end
end
