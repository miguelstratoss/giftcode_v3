class AddEnableForSellingToOffices < ActiveRecord::Migration
  def change
    add_column :offices, :enable_for_selling, :boolean, :default => true

  end
end
