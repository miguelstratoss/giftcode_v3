class CreateRedebanCodes < ActiveRecord::Migration
  def change
    create_table :redeban_codes do |t|
      t.string :code
      t.integer :campaing_id
      t.boolean :enabled

      t.timestamps
    end
  end
end
