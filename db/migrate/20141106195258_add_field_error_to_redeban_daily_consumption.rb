class AddFieldErrorToRedebanDailyConsumption < ActiveRecord::Migration
  def change
    add_column :redeban_daily_consumptions, :error, :string
  end
end
