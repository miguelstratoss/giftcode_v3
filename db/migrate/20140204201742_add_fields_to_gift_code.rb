class AddFieldsToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :send_notifications_now, :boolean
    add_column :gift_codes, :send_date_time, :datetime
  end
end
