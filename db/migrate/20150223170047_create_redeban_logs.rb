class CreateRedebanLogs < ActiveRecord::Migration
  def change
    create_table :redeban_logs do |t|
      t.string :name
      t.integer :number_records
      t.integer :successful
      t.integer :rejected

      t.timestamps
    end
  end
end
