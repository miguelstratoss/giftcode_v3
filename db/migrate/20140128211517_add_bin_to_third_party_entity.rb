class AddBinToThirdPartyEntity < ActiveRecord::Migration
  def change
    add_column :third_party_entities, :bin, :string
  end
end
