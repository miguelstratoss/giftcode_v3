class AddFieldIdentificationNumberToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :identification_number, :string
  end
end
