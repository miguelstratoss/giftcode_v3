class CreateThirdPartyRegistrations < ActiveRecord::Migration
  def change
    create_table :third_party_registrations do |t|
      t.string :received_sign
      t.datetime :received_date
      t.integer :brand_code_id

      t.timestamps
    end
  end
end
