class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :gift_code_id
      t.decimal :cost, :precision => 10, :scale => 2
      t.string :terminal_id
      t.string :consecutive
      t.datetime :terminal_date
      t.string :state
      t.string :authorization_number

      t.timestamps
    end
  end
end
