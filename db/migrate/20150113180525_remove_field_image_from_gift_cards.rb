class RemoveFieldImageFromGiftCards < ActiveRecord::Migration
  def up
    remove_column :gift_cards, :image
  end

  def down
    add_column :gift_cards, :image, :string
  end
end
