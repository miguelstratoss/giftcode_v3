class AddGiftCodeIdToThirdPartyTransaction < ActiveRecord::Migration
  def change
    add_column :third_party_transactions, :gift_code_id, :integer
  end
end
