class AddMotiveIdAndBodyMessageToFreeCampaigns < ActiveRecord::Migration
  def change
    add_column :free_campaigns, :motive_id, :integer

    add_column :free_campaigns, :body_message, :text

  end
end
