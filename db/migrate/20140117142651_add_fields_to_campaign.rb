class AddFieldsToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :cost_admin, :decimal, :precision => 10, :scale => 2
    add_column :campaigns, :iva_cost_admin, :decimal, :precision => 10, :scale => 2
  end
end
