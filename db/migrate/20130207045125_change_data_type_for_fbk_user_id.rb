class ChangeDataTypeForFbkUserId < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.change :fbk_user_id, 'bigint', :limit => 8
    end
  end
end
