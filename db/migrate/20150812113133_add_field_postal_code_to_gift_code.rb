class AddFieldPostalCodeToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :to_postal_code, :string
  end
end
