class RenameColumnConvenioRedebanFromCampaigns < ActiveRecord::Migration
  def up
    rename_column :campaigns, :convenio_redeban, :convenio
  end

  def down
    rename_column :campaigns, :convenio, :convenio_redeban
  end
end
