class CreateLogRequestBrandPayments < ActiveRecord::Migration
  def change
    create_table :log_request_brand_payments do |t|
      t.string :dir_ip
      t.string :init_date
      t.string :end_date
      t.string :incocredito
      t.string :state
      t.timestamps
    end
  end
end
