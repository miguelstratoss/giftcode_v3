class AddFieldPointsUsedToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :points_used, :integer
  end
end
