class AddImageWhiteLabelToBrand < ActiveRecord::Migration
  def change
    add_column :brands, :image_white_label, :string
  end
end
