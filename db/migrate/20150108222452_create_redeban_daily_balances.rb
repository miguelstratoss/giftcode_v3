class CreateRedebanDailyBalances < ActiveRecord::Migration
  def change
    create_table :redeban_daily_balances do |t|
      t.integer :redeban_code_id
      t.string :file_name
      t.integer :consecutivo
      t.integer :saldo_inicial
      t.integer :estado
      t.integer :valor_saldo

      t.timestamps
    end
  end
end
