class AddApprobationNumberToThirdPartyTransaction < ActiveRecord::Migration
  def change
    add_column :third_party_transactions, :approbation_number, :integer
  end
end
