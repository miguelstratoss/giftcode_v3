class CreateCarouselBrands < ActiveRecord::Migration
  def change
    create_table :carousel_brands do |t|
      t.integer :brand_id
      t.integer :position

      t.timestamps
    end
  end
end
