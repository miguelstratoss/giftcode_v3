class DropStaticImageTypesTable < ActiveRecord::Migration
  def up
    drop_table :static_image_types
  end

  def down
  end
end
