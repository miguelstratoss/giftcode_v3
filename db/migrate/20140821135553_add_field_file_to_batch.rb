class AddFieldFileToBatch < ActiveRecord::Migration
  def change
    add_column :batches, :file, :string
  end
end
