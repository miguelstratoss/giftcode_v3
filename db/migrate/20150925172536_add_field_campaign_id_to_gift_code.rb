class AddFieldCampaignIdToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :campaign_id, :integer
  end
end
