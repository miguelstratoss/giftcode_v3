class CreateDownloads < ActiveRecord::Migration
  def change
    create_table :downloads do |t|
      t.integer :gift_code_id
      t.integer :user_id
      t.string :token
      t.string :view_id
      t.string :download_type

      t.timestamps
    end
  end
end
