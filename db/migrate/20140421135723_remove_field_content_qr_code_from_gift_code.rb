class RemoveFieldContentQrCodeFromGiftCode < ActiveRecord::Migration
  def up
    remove_column :gift_codes, :content_qr_code
  end

  def down
    add_column :gift_codes, :content_qr_code, :string
  end
end
