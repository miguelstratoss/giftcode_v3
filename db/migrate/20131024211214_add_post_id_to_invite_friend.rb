class AddPostIdToInviteFriend < ActiveRecord::Migration
  def change
    add_column :invite_friends, :fbk_post_id, :string
  end
end
