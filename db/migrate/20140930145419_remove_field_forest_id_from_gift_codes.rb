class RemoveFieldForestIdFromGiftCodes < ActiveRecord::Migration
  def up
    remove_column :gift_codes, :forest_id
  end

  def down
    add_column :gift_codes, :forest_id, :integer
  end
end
