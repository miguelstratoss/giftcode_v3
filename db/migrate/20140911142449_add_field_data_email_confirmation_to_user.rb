class AddFieldDataEmailConfirmationToUser < ActiveRecord::Migration
  def change
    add_column :users, :data_email_confirmation, :boolean
  end
end
