class AddActiveForPaymentToBrandCode < ActiveRecord::Migration
  def change
    add_column :brand_codes, :active_for_payment, :boolean
  end
end
