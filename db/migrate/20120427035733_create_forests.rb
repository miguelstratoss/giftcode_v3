class CreateForests < ActiveRecord::Migration
  def change
    create_table :forests do |t|
      t.integer :country_id
      t.string :name
      t.string :image
      t.text :description

      t.timestamps
    end
  end
end
