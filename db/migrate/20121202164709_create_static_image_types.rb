class CreateStaticImageTypes < ActiveRecord::Migration
  def change
    create_table :static_image_types do |t|
      t.string :name
      t.integer :height
      t.integer :width

      t.timestamps
    end
  end
end
