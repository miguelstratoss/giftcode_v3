class DropForestsTable < ActiveRecord::Migration
  def up
    drop_table :forests
  end

  def down
  end
end
