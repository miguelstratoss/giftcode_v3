class CreateFacebookWalls < ActiveRecord::Migration
  def change
    create_table :facebook_walls do |t|
      t.integer :user_id
      t.string :fbk_post_id

      t.timestamps
    end
  end
end
