class AddReversedToThirdPartyTransaction < ActiveRecord::Migration
  def change
    add_column :third_party_transactions, :reversed, :boolean
  end
end
