class AddShortDescriptionToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :short_description, :string

  end
end
