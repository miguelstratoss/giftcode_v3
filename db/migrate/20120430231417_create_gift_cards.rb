class CreateGiftCards < ActiveRecord::Migration
  def change
    create_table :gift_cards do |t|
      t.integer :campaign_id
      t.decimal :cost, :precision => 10, :scale => 2
      t.string :image
      t.integer :valid_for_days

      t.timestamps
    end
  end
end
