class ChangeColumnRenameColumnStateChangeStateIdToRedebanCode < ActiveRecord::Migration
  def up
    rename_column :redeban_codes, :state, :state_id
  end

  def down
  end
end
