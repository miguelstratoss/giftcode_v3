class CreatePaymentAnswers < ActiveRecord::Migration
  def change
    create_table :payment_answers do |t|
      t.integer :gift_code_id
      t.integer :pol_state
      t.decimal :cost, :precision => 10, :scale => 2
      t.decimal :iva, :precision => 10, :scale => 2
      t.string :currency_short_name
      t.integer :payment_type_id
      t.text :params

      t.timestamps
    end
  end
end
