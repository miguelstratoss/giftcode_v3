class AddBirthdayAndFbkUserIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :birthday, :date

    add_column :users, :fbk_user_id, :bigint

  end
end
