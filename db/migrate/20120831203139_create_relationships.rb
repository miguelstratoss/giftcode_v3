class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :user_id
      t.string :fbk_user_id
      t.string :fbk_friend_id
      t.integer :relationship_state_id

      t.timestamps
    end
  end
end
