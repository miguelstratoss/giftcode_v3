class CreateGiftCodes < ActiveRecord::Migration
  def change
    create_table :gift_codes do |t|
      t.integer :forest_id
      t.integer :currency_id
      t.integer :from_user_id
      t.integer :to_user_id
      t.integer :to_city_id
      t.integer :gift_card_id
      t.integer :motive_id
      t.decimal :cost, :precision => 10, :scale => 2
      t.string :to_user_name
      t.string :to_mobile_phone
      t.string :to_email
      t.integer :valid_for_days
      t.string :title_message
      t.text :body_message
      t.string :content_qr_code
      t.binary :binary_qr_code_image
      t.decimal :available_balance, :precision => 10, :scale => 2
      t.string :view_id
      t.string :token

      t.timestamps
    end
  end
end
