class AddEnableForSellingToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :enable_for_selling, :boolean, :default => false

  end
end
