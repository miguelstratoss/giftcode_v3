class AddFieldGiftCardIdToRedebanCode < ActiveRecord::Migration
  def change
    add_column :redeban_codes, :gift_card_id, :integer
  end
end
