class RemoveFieldMotiveIdFromGiftCodes < ActiveRecord::Migration
  def up
    remove_column :gift_codes, :motive_id
  end

  def down
    add_column :gift_codes, :motive_id, :integer
  end
end
