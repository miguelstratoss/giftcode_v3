class RemoveTpFieldsFromThirdPartyTransactions < ActiveRecord::Migration
  def up
    remove_column :third_party_transactions, :id_transaction_state
    remove_column :third_party_transactions, :id_transaction_type
  end

  def down
    add_column :third_party_transactions, :id_transaction_type, :integer
    add_column :third_party_transactions, :id_transaction_state, :integer
  end
end
