class CreateRelationshipStates < ActiveRecord::Migration
  def change
    create_table :relationship_states do |t|
      t.string :name

      t.timestamps
    end
  end
end
