class DropTeamMembersTable < ActiveRecord::Migration
  def up
    drop_table :team_members
  end

  def down
  end
end
