class AddFieldNameNotificationToNotificationUser < ActiveRecord::Migration
  def change
    add_column :notification_users, :name_notification, :string
  end
end
