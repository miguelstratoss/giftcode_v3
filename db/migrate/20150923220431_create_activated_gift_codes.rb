class CreateActivatedGiftCodes < ActiveRecord::Migration
  def change
    create_table :activated_gift_codes do |t|
      t.integer :brand_id
      t.integer :campaign_id
      t.integer :gift_code_id
      t.integer :gift_card_id

      t.timestamps
    end
  end
end
