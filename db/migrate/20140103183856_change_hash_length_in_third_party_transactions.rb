class ChangeHashLengthInThirdPartyTransactions < ActiveRecord::Migration
  def up
    change_column :third_party_transactions, :received_hash, :string, :limit => 1000
  end

  def down
    change_column :third_party_transactions, :received_hash, :string
  end
end
