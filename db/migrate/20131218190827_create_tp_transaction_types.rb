class CreateTpTransactionTypes < ActiveRecord::Migration
  def change
    create_table :tp_transaction_types do |t|
      t.integer :id
      t.string :name

      t.timestamps
    end
  end
end
