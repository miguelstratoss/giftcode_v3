class CreateCampaignStates < ActiveRecord::Migration
  def change
    create_table :campaign_states do |t|
      t.string :name

      t.timestamps
    end
  end
end
