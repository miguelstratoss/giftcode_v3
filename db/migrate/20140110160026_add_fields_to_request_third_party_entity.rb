class AddFieldsToRequestThirdPartyEntity < ActiveRecord::Migration
  def change
    add_column :request_third_party_entities, :response_code, :string
    add_column :request_third_party_entities, :response_message, :string
  end
end
