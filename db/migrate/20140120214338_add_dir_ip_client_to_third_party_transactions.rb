class AddDirIpClientToThirdPartyTransactions < ActiveRecord::Migration
  def change
    add_column :third_party_transactions, :dir_ip_client, :string
  end
end
