class CreateStaticImages < ActiveRecord::Migration
  def change
    create_table :static_images do |t|
      t.string :image
      t.integer :priority
      t.boolean :enabled
      t.integer :static_image_type_id

      t.timestamps
    end
  end
end
