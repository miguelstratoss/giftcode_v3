class CreateRedebanMovements < ActiveRecord::Migration
  def change
    create_table :redeban_movements do |t|
      t.integer :brand_id
      t.text :line
      t.integer :admin_user_id
      t.string :file
      t.integer :number_line

      t.timestamps
    end
  end
end
