class CreateEncryptGiftCodes < ActiveRecord::Migration
  def change
    create_table :encrypt_gift_codes do |t|
      t.string :salt
      t.string :iv
      t.string :hash2
      t.string :code

      t.timestamps
    end
  end
end
