class AddFinalResultToPaymentConfirmations < ActiveRecord::Migration
  def change
    add_column :payment_confirmations, :final_result, :string

  end
end
