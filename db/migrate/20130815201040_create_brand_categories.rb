class CreateBrandCategories < ActiveRecord::Migration
  def change
    create_table :brand_categories do |t|
      t.references :brand
      t.references :category

      t.timestamps
    end
    add_index :brand_categories, :brand_id
    add_index :brand_categories, :category_id
  end
end
