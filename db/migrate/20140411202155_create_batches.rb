class CreateBatches < ActiveRecord::Migration
  def change
    create_table :batches do |t|
      t.references :campaign
      t.integer :created_by

      t.timestamps
    end
    add_index :batches, :campaign_id
  end
end
