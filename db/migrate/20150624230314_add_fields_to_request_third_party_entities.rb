class AddFieldsToRequestThirdPartyEntities < ActiveRecord::Migration
  def change
    add_column :request_third_party_entities, :payment_type, :string
    add_column :request_third_party_entities, :card_type, :string
    add_column :request_third_party_entities, :card_franchise, :string
    add_column :request_third_party_entities, :total_value, :string
    add_column :request_third_party_entities, :approbation_number, :string
    add_column :request_third_party_entities, :bin, :string
  end
end
