class CreateThirdPartyTransactions < ActiveRecord::Migration
  def change
    create_table :third_party_transactions do |t|
      t.integer :id_transaction_type
      t.integer :id_transaction_state
      t.integer :third_party_entity_id
      t.integer :brand_code_id
      t.integer :giftcode_id
      t.float :transaction_value
      t.string :transaction_currency
      t.float :new_balance
      t.string :received_hash
      t.string :dir_ip

      t.timestamps
    end
  end
end
