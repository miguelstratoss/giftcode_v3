class CreateThirdPartyBlackLists < ActiveRecord::Migration
  def change
    create_table :third_party_black_lists do |t|
      t.string :dir_ip
      t.boolean :blocked

      t.timestamps
    end
  end
end
