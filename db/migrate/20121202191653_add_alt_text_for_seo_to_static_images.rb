class AddAltTextForSeoToStaticImages < ActiveRecord::Migration
  def change
    add_column :static_images, :alt_text_for_seo, :string

  end
end
