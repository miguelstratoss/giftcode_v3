class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :name
      t.text :consultations
      t.text :condition
      t.string :mailer
      t.boolean :enabled

      t.timestamps
    end
  end
end
