class AddSalesCodeToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :sales_code, :string
  end
end
