class AddBinToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :bin, :string
  end
end
