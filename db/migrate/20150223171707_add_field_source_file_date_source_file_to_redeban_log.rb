class AddFieldSourceFileDateSourceFileToRedebanLog < ActiveRecord::Migration
  def change
    add_column :redeban_logs, :source_file, :string
    add_column :redeban_logs, :date_source_file, :date
  end
end
