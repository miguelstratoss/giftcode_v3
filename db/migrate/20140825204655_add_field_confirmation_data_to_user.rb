class AddFieldConfirmationDataToUser < ActiveRecord::Migration
  def change
    add_column :users, :confirmation_data, :boolean
  end
end
