class DropMotivesTable < ActiveRecord::Migration
  def up
    drop_table :motives
  end

  def down
  end
end
