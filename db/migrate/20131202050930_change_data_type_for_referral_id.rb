class ChangeDataTypeForReferralId < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.change :referral_id, 'bigint', :limit => 8
    end
  end
end
