class AddGeneratedByGiftCodeToBrandCodes < ActiveRecord::Migration
  def change
    add_column :brand_codes, :generated_by_gift_code, :boolean
  end
end
