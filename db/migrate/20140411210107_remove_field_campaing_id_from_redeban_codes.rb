class RemoveFieldCampaingIdFromRedebanCodes < ActiveRecord::Migration
  def up
    remove_column :redeban_codes, :campaing_id
  end

  def down
    add_column :redeban_codes, :campaing_id, :integer
  end
end
