class AddFieldsShowQrAndShowBarcodeToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :show_qr, :boolean
    add_column :campaigns, :show_barcode, :boolean
  end
end
