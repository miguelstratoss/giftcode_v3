class RemoveFieldGiftCardIdFromBatches < ActiveRecord::Migration
  def up
    remove_column :batches, :gift_card_id
  end

  def down
    add_column :batches, :gift_card_id, :integer
  end
end
