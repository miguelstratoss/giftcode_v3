class AddFieldIpToOffice < ActiveRecord::Migration
  def change
    add_column :offices, :ip, :string
  end
end
