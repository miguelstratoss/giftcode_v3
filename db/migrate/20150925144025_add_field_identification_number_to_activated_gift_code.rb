class AddFieldIdentificationNumberToActivatedGiftCode < ActiveRecord::Migration
  def change
    add_column :activated_gift_codes, :identification_number, :integer
  end
end
