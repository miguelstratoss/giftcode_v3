class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :time_zone
      t.integer :city_id
      t.integer :language_id
      t.integer :sex_id
      t.date :birthday_date
      t.string :mailing_address
      t.string :mobile_phone
      t.string :office_phone
      t.string :image

      t.timestamps
    end
  end
end
