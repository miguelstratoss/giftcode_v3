class AddFieldsToThirdPartyTransaction < ActiveRecord::Migration
  def change
    add_column :third_party_transactions, :office_id, :integer
    add_column :third_party_transactions, :terminal_id, :integer
  end
end
