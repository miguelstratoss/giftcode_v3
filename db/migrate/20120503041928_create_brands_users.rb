class CreateBrandsUsers < ActiveRecord::Migration
  def up
    create_table :brands_users, :id => false do |t|
      t.references :brand
      t.references :user
    end
  end

  def down
    drop_table :brands_users
  end
end
