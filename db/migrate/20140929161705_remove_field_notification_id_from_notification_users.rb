class RemoveFieldNotificationIdFromNotificationUsers < ActiveRecord::Migration
  def up
    remove_column :notification_users, :notification_id
  end

  def down
    add_column :notification_users, :notification_id, :integer
  end
end
