class AddFavoritePriorityToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :favorite_priority, :integer, :default => 1

  end
end
