class CreateTeamMembers < ActiveRecord::Migration
  def change
    create_table :team_members do |t|
      t.string :position
      t.string :name
      t.text :biography
      t.string :email
      t.boolean :enabled
      t.integer :priority
      t.string :twitter_name
      t.string :url_facebook
      t.string :url_linkedin
      t.string :url_twitter
      t.string :url_google_plus

      t.timestamps
    end
  end
end
