class AddUtmSourceAndUtmMediumToUsers < ActiveRecord::Migration
  def change
    add_column :users, :utm_source, :string
    add_column :users, :utm_medium, :string
  end
end
