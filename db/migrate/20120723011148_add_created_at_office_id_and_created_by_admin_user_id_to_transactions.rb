class AddCreatedAtOfficeIdAndCreatedByAdminUserIdToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :created_at_office_id, :integer

    add_column :transactions, :created_by_admin_user_id, :integer

  end
end
