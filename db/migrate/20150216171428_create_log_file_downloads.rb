class CreateLogFileDownloads < ActiveRecord::Migration
  def change
    create_table :log_file_downloads do |t|
      t.string :name
      t.integer :admin_user_id

      t.timestamps
    end
  end
end
