class AddSmsAuthorizationAndIdentificationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :sms_authorization, :boolean, :default => true

    add_column :users, :identification, :string

  end
end
