class AddFieldsGiftCardIdAndChargeStateIdToRedebanCode < ActiveRecord::Migration
  def change
    add_column :redeban_codes, :gift_card_id, :integer
    add_column :redeban_codes, :charge_state_id, :integer
  end
end
