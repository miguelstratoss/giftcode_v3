class AddFieldsToThirdPartyTransactions < ActiveRecord::Migration
  def change
    add_column :third_party_transactions, :tp_transaction_type_id, :integer
    add_column :third_party_transactions, :tp_transaction_state_id, :integer
    add_column :third_party_transactions, :related_transaction_id, :integer
  end
end
