class AddFieldDecryptToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :decrypt, :integer
  end
end
