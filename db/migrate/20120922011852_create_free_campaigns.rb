class CreateFreeCampaigns < ActiveRecord::Migration
  def change
    create_table :free_campaigns do |t|
      t.integer :office_id
      t.integer :admin_user_id
      t.decimal :cost, :precision => 10, :scale => 2
      t.integer :valid_for_days
      t.timestamps
    end
  end
end
