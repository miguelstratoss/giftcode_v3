class RemoveBinFromThirdPartyEntity < ActiveRecord::Migration
  def up
    remove_column :third_party_entities, :bin
  end

  def down
    add_column :third_party_entities, :bin, :string
  end
end
