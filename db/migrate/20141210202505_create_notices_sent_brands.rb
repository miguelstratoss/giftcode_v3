class CreateNoticesSentBrands < ActiveRecord::Migration
  def change
    create_table :notices_sent_brands do |t|
      t.integer :campaign_id
      t.integer :type_notification
      t.text :details

      t.timestamps
    end
  end
end
