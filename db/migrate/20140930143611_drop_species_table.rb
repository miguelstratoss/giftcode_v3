class DropSpeciesTable < ActiveRecord::Migration
  def up
    drop_table :species
  end

  def down
  end
end
