class DropStaticImagesTable < ActiveRecord::Migration
  def up
    drop_table :static_images
  end

  def down
  end
end
