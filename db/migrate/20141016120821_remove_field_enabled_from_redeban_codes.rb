class RemoveFieldEnabledFromRedebanCodes < ActiveRecord::Migration
  def up
    remove_column :redeban_codes, :enabled
  end

  def down
    add_column :redeban_codes, :enabled, :boolean
  end
end
