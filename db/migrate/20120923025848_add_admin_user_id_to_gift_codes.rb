class AddAdminUserIdToGiftCodes < ActiveRecord::Migration
  def change
    add_column :gift_codes, :admin_user_id, :integer

  end
end
