class AddEnableForSellingToGiftCards < ActiveRecord::Migration
  def change
    add_column :gift_cards, :enable_for_selling, :boolean, :default => true

  end
end
