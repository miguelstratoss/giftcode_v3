class ChangeIntegerFormatConvenioRedebanInCampaign < ActiveRecord::Migration
  def up
    change_column :campaigns, :convenio_redeban, :string
  end

  def down
    change_column :campaigns, :convenio_redeban, :integer
  end
end
