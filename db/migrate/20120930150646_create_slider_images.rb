class CreateSliderImages < ActiveRecord::Migration
  def change
    create_table :slider_images do |t|
      t.integer :brand_id
      t.integer :priority
      t.boolean :enabled
      t.string :image

      t.timestamps
    end
  end
end
