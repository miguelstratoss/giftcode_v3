class AddFieldsAddressAndStateAndCityToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :to_address, :string
    add_column :gift_codes, :to_state, :string
    add_column :gift_codes, :to_city, :string
    add_column :gift_codes, :to_country, :string
  end
end
