class CreateSpecies < ActiveRecord::Migration
  def change
    create_table :species do |t|
      t.string :name
      t.string :image
      t.string :description

      t.timestamps
    end
  end
end
