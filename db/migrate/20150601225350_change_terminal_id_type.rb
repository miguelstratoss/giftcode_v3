class ChangeTerminalIdType < ActiveRecord::Migration
  def up
    change_column :third_party_transactions, :terminal_id, :string
  end

  def down
    change_column :third_party_transactions, :terminal_id, :integer
  end
end
