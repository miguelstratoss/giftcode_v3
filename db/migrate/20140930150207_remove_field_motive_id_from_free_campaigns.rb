class RemoveFieldMotiveIdFromFreeCampaigns < ActiveRecord::Migration
  def up
    remove_column :free_campaigns, :motive_id
  end

  def down
    add_column :free_campaigns, :motive_id, :integer
  end
end
