class AddConfigurationToThirdPartyEntity < ActiveRecord::Migration
  def change
    add_column :third_party_entities, :configuration, :text
  end
end
