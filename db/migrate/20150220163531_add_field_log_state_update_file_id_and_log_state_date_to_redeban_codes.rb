class AddFieldLogStateUpdateFileIdAndLogStateDateToRedebanCodes < ActiveRecord::Migration
  def change
    add_column :redeban_codes, :log_error_update_file_id, :integer
    add_column :redeban_codes, :log_update_date, :datetime
    add_column :redeban_codes, :redeban_log_id, :integer

  end
end
