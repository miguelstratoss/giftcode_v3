class AddFieldTypeToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :type, :string
  end
end
