class CreateCredibancoFranquicia < ActiveRecord::Migration
  def change
    create_table :credibanco_franquicia do |t|
      t.string :name
      t.integer :codigo_identificacion
      t.integer :length
      t.boolean :ahorro
      t.boolean :corriente
      t.boolean :credito
      t.boolean :enabled

      t.timestamps
    end
  end
end
