class CreateCredibancoFranquiciaPrefixes < ActiveRecord::Migration
  def change
    create_table :credibanco_franquicia_prefixes do |t|
      t.integer :bin
      t.integer :credibanco_franquicia_id
      t.boolean :enabled

      t.timestamps
    end
  end
end
