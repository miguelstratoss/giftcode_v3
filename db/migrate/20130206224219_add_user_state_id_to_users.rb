class AddUserStateIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :user_state_id, :integer, :default => 1

  end
end
