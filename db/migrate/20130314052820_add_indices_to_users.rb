class AddIndicesToUsers < ActiveRecord::Migration
  def change
    #For users
    add_index :users, :fbk_user_id
    add_index :users, :user_state_id

    #For friendships
    add_index :friendships, :user_id
    add_index :friendships, :friend_id
    add_index :friendships, [:user_id, :friend_id], :unique=>true
  end
end
