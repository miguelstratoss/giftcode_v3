class AddFieldTypeToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :type, :string
  end
end
