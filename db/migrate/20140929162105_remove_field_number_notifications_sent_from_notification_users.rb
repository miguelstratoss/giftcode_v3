class RemoveFieldNumberNotificationsSentFromNotificationUsers < ActiveRecord::Migration
  def up
    remove_column :notification_users, :number_notifications_sent
  end

  def down
    add_column :notification_users, :number_notifications_sent, :integer
  end
end
