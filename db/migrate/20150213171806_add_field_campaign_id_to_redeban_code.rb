class AddFieldCampaignIdToRedebanCode < ActiveRecord::Migration
  def change
    add_column :redeban_codes, :campaign_id, :integer
  end
end
