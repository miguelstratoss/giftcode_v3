class CreateGiftCodeStates < ActiveRecord::Migration
  def change
    create_table :gift_code_states do |t|
      t.string :name

      t.timestamps
    end
  end
end
