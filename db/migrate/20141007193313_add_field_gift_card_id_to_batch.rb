class AddFieldGiftCardIdToBatch < ActiveRecord::Migration
  def change
    add_column :batches, :gift_card_id, :integer
  end
end
