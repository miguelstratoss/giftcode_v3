class AddFieldCampaignPointIdToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :campaign_point_id, :integer
  end
end
