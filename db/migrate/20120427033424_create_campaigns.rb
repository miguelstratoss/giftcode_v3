class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.integer :brand_id
      t.string :name
      t.string :message
      t.boolean :send_mail
      t.boolean :send_sms
      t.integer :forest_id
      t.integer :country_id

      t.timestamps
    end
  end
end
