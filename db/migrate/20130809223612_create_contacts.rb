class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :company
      t.references :issue
      t.text :message

      t.timestamps
    end
    add_index :contacts, :issue_id
  end
end
