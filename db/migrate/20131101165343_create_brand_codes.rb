class CreateBrandCodes < ActiveRecord::Migration
  def change
    create_table :brand_codes do |t|
      t.integer :third_party_entity_id
      t.integer :campaign_id
      t.string :code

      t.timestamps
    end
  end
end
