class AddInputTypeIdToThirdPartyTransaction < ActiveRecord::Migration
  def change
    add_column :third_party_transactions, :input_type_id, :integer
  end
end
