class RenameTableActivatedGiftCodesToMovementsGiftCodes < ActiveRecord::Migration
  def up
    rename_table :activated_gift_codes, :movements_gift_codes
  end

  def down
    rename_table :movements_gift_codes, :activated_gift_codes
  end
end
