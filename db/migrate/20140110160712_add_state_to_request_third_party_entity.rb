class AddStateToRequestThirdPartyEntity < ActiveRecord::Migration
  def change
    add_column :request_third_party_entities, :state, :string
  end
end
