class RemoveGiftcodeIdFromThirdPartyTransaction < ActiveRecord::Migration
  def up
    remove_column :third_party_transactions, :giftcode_id
  end

  def down
    add_column :third_party_transactions, :giftcode_id, :integer
  end
end
