class AddFieldDateOfPurchaseToGiftCode < ActiveRecord::Migration
  def change
    add_column :gift_codes, :date_of_purchase, :datetime
  end
end
