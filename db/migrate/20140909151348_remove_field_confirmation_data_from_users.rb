class RemoveFieldConfirmationDataFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :confirmation_data
  end

  def down
    add_column :users, :confirmation_data, :boolean
  end
end
