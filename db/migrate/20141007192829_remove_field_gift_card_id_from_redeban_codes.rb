class RemoveFieldGiftCardIdFromRedebanCodes < ActiveRecord::Migration
  def up
    remove_column :redeban_codes, :gift_card_id
  end

  def down
    add_column :redeban_codes, :gift_card_id, :integer
  end
end
