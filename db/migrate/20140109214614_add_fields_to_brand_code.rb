class AddFieldsToBrandCode < ActiveRecord::Migration
  def change
    add_column :brand_codes, :terminal, :string
    add_column :brand_codes, :type_terminal, :string
  end
end
