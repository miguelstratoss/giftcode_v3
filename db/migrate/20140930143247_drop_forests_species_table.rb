class DropForestsSpeciesTable < ActiveRecord::Migration
  def up
    drop_table :forests_species
  end

  def down
  end
end
