class AddFieldsConsecutiveAndTypeActAndDateProcessToRedebanLog < ActiveRecord::Migration
  def change
    add_column :redeban_logs, :consecutive, :integer
    add_column :redeban_logs, :type_act, :string
    add_column :redeban_logs, :date_process, :datetime
  end
end
