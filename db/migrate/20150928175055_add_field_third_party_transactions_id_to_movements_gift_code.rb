class AddFieldThirdPartyTransactionsIdToMovementsGiftCode < ActiveRecord::Migration
  def change
    add_column :movements_gift_codes, :third_party_transaction_id, :integer
  end
end
