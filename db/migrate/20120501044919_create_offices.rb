class CreateOffices < ActiveRecord::Migration
  def change
    create_table :offices do |t|
      t.string :office_name
      t.string :office_address
      t.string :nit
      t.string :contact_name
      t.string :contact_email
      t.integer :brand_id
      t.integer :city_id

      t.timestamps
    end
  end
end
