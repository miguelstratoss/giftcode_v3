class AddFreeCampaignIdToGiftCodes < ActiveRecord::Migration
  def change
    add_column :gift_codes, :free_campaign_id, :integer

  end
end
