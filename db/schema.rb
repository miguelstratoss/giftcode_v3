# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20151008121651) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_control_access_alerts", :force => true do |t|
    t.integer  "admin_user_id"
    t.string   "alert_for_access_path"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "password_salt"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "role_id"
    t.integer  "brand_id"
    t.integer  "office_id"
  end

  add_index "admin_users", ["authentication_token"], :name => "index_admin_users_on_authentication_token", :unique => true
  add_index "admin_users", ["confirmation_token"], :name => "index_admin_users_on_confirmation_token", :unique => true
  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true
  add_index "admin_users", ["unlock_token"], :name => "index_admin_users_on_unlock_token", :unique => true

  create_table "batches", :force => true do |t|
    t.integer  "campaign_id"
    t.integer  "created_by"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "file"
    t.date     "fecha_emision"
  end

  add_index "batches", ["campaign_id"], :name => "index_batches_on_campaign_id"

  create_table "brand_categories", :force => true do |t|
    t.integer  "brand_id"
    t.integer  "category_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "brand_categories", ["brand_id"], :name => "index_brand_categories_on_brand_id"
  add_index "brand_categories", ["category_id"], :name => "index_brand_categories_on_category_id"

  create_table "brand_codes", :force => true do |t|
    t.integer  "third_party_entity_id"
    t.integer  "campaign_id"
    t.string   "code"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.boolean  "active_for_payment"
    t.string   "terminal"
    t.string   "type_terminal"
  end

  create_table "brands", :force => true do |t|
    t.string   "name"
    t.string   "contact_name"
    t.string   "contact_email"
    t.string   "sn_twitter_name"
    t.string   "nit"
    t.text     "description"
    t.string   "url_website"
    t.string   "url_facebook"
    t.string   "url_twitter"
    t.string   "url_youtube"
    t.string   "url_linkedin"
    t.string   "image"
    t.string   "qr_code_image"
    t.integer  "manager_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "short_description"
    t.string   "large_image"
    t.string   "logo2_image"
    t.string   "qr_code_example_image"
    t.boolean  "enable_for_selling",    :default => false
    t.integer  "favorite_priority",     :default => 1
    t.string   "image_email"
    t.string   "image_white_label"
    t.integer  "yo_quiero_facebook"
    t.string   "image_yo_quiero"
    t.string   "trade_name"
  end

  create_table "campaign_states", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "campaigns", :force => true do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.string   "message"
    t.boolean  "send_mail"
    t.boolean  "send_sms"
    t.integer  "country_id"
    t.datetime "created_at",                                                                 :null => false
    t.datetime "updated_at",                                                                 :null => false
    t.integer  "currency_id"
    t.integer  "campaign_state_id"
    t.decimal  "iva",                       :precision => 10, :scale => 2, :default => 16.0
    t.string   "sales_code"
    t.decimal  "cost_admin",                :precision => 10, :scale => 2
    t.decimal  "iva_cost_admin",            :precision => 10, :scale => 2
    t.date     "begin_date"
    t.date     "end_date"
    t.decimal  "cost",                      :precision => 10, :scale => 2
    t.integer  "points_required"
    t.integer  "number_of_free_gift_codes"
    t.boolean  "enabled"
    t.string   "type"
    t.string   "observations"
    t.string   "convenio"
    t.boolean  "show_qr"
    t.boolean  "show_barcode"
    t.boolean  "office_redeem_bono"
    t.boolean  "redeban"
  end

  create_table "carousel_brands", :force => true do |t|
    t.integer  "brand_id"
    t.integer  "position"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.integer  "position"
    t.boolean  "enabled"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "_class"
  end

  create_table "cities", :force => true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "contacts", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "company"
    t.integer  "issue_id"
    t.text     "message"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "contacts", ["issue_id"], :name => "index_contacts_on_issue_id"

  create_table "countries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "bin"
  end

  create_table "credibanco_franquicia", :force => true do |t|
    t.string   "name"
    t.integer  "codigo_identificacion"
    t.integer  "length"
    t.boolean  "ahorro"
    t.boolean  "corriente"
    t.boolean  "credito"
    t.boolean  "enabled"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "identificacion"
  end

  create_table "credibanco_franquicia_prefixes", :force => true do |t|
    t.integer  "bin"
    t.integer  "credibanco_franquicia_id"
    t.boolean  "enabled"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "currencies", :force => true do |t|
    t.string   "name"
    t.string   "symbol"
    t.string   "short_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "custom_qr_codes", :force => true do |t|
    t.string   "image"
    t.string   "content"
    t.integer  "size"
    t.binary   "binary_qr_code"
    t.integer  "created_by"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "downloads", :force => true do |t|
    t.integer  "gift_code_id"
    t.integer  "user_id"
    t.string   "token"
    t.string   "view_id"
    t.string   "download_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "encrypt_gift_codes", :force => true do |t|
    t.string   "salt"
    t.string   "iv"
    t.string   "hash2"
    t.string   "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "facebook_walls", :force => true do |t|
    t.integer  "user_id"
    t.string   "fbk_post_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "favorites", :force => true do |t|
    t.integer  "brand_id"
    t.integer  "user_id"
    t.boolean  "enabled"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "favorites", ["brand_id"], :name => "index_favorites_on_brand_id"
  add_index "favorites", ["user_id"], :name => "index_favorites_on_user_id"

  create_table "free_campaigns", :force => true do |t|
    t.integer  "office_id"
    t.integer  "admin_user_id"
    t.decimal  "cost",           :precision => 10, :scale => 2
    t.integer  "valid_for_days"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.text     "body_message"
    t.integer  "currency_id"
  end

  create_table "friendship_states", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "friendships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.integer  "friendship_state_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "friendships", ["friend_id"], :name => "index_friendships_on_friend_id"
  add_index "friendships", ["user_id", "friend_id"], :name => "index_friendships_on_user_id_and_friend_id", :unique => true
  add_index "friendships", ["user_id"], :name => "index_friendships_on_user_id"

  create_table "gift_cards", :force => true do |t|
    t.integer  "campaign_id"
    t.decimal  "cost",               :precision => 10, :scale => 2
    t.integer  "valid_for_days"
    t.datetime "created_at",                                                          :null => false
    t.datetime "updated_at",                                                          :null => false
    t.boolean  "enable_for_selling",                                :default => true
    t.string   "convenio"
  end

  create_table "gift_code_states", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "gift_codes", :force => true do |t|
    t.integer  "from_user_id"
    t.integer  "to_user_id"
    t.integer  "to_city_id"
    t.integer  "gift_card_id"
    t.decimal  "cost",                   :precision => 10, :scale => 2
    t.string   "to_mobile_phone"
    t.string   "to_email"
    t.integer  "valid_for_days"
    t.string   "title_message"
    t.text     "body_message"
    t.binary   "binary_qr_code_image"
    t.decimal  "available_balance",      :precision => 10, :scale => 2
    t.string   "view_id"
    t.string   "token"
    t.datetime "created_at",                                                              :null => false
    t.datetime "updated_at",                                                              :null => false
    t.integer  "gift_code_state_id"
    t.decimal  "iva",                    :precision => 10, :scale => 2, :default => 16.0
    t.decimal  "base_iva",               :precision => 10, :scale => 2
    t.string   "ref_sale"
    t.boolean  "email_sent_to_buyer"
    t.boolean  "email_sent_to_receiver"
    t.boolean  "sms_sent_to_receiver"
    t.integer  "free_campaign_id"
    t.integer  "admin_user_id"
    t.string   "redemption_pin"
    t.string   "from_first_name"
    t.string   "from_last_name"
    t.string   "from_email"
    t.string   "to_first_name"
    t.string   "to_last_name"
    t.integer  "office_id"
    t.boolean  "send_notifications_now"
    t.datetime "send_date_time"
    t.integer  "decrypt"
    t.integer  "campaign_point_id"
    t.string   "type"
    t.integer  "points_used"
    t.string   "to_address"
    t.string   "to_state"
    t.string   "to_city"
    t.string   "to_country"
    t.string   "to_postal_code"
    t.integer  "brand_id"
    t.integer  "campaign_id"
    t.string   "identification_number"
    t.datetime "date_of_purchase"
  end

  create_table "input_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "invite_friends", :force => true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "fbk_post_id"
  end

  create_table "languages", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "log_file_downloads", :force => true do |t|
    t.string   "name"
    t.integer  "admin_user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "log_request_brand_payments", :force => true do |t|
    t.string   "dir_ip"
    t.string   "init_date"
    t.string   "end_date"
    t.string   "incocredito"
    t.string   "state"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "logins", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "movements_gift_codes", :force => true do |t|
    t.integer  "brand_id"
    t.integer  "campaign_id"
    t.integer  "gift_code_id"
    t.integer  "gift_card_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "identification_number"
    t.integer  "third_party_transaction_id"
  end

  create_table "notices_sent_brands", :force => true do |t|
    t.integer  "campaign_id"
    t.integer  "type_notification"
    t.text     "details"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "notification_users", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "name_notification"
  end

  create_table "oauth_access_grants", :force => true do |t|
    t.integer  "resource_owner_id", :null => false
    t.integer  "application_id",    :null => false
    t.string   "token",             :null => false
    t.integer  "expires_in",        :null => false
    t.text     "redirect_uri",      :null => false
    t.datetime "created_at",        :null => false
    t.datetime "revoked_at"
    t.string   "scopes"
  end

  add_index "oauth_access_grants", ["token"], :name => "index_oauth_access_grants_on_token", :unique => true

  create_table "oauth_access_tokens", :force => true do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",             :null => false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",        :null => false
    t.string   "scopes"
  end

  add_index "oauth_access_tokens", ["refresh_token"], :name => "index_oauth_access_tokens_on_refresh_token", :unique => true
  add_index "oauth_access_tokens", ["resource_owner_id"], :name => "index_oauth_access_tokens_on_resource_owner_id"
  add_index "oauth_access_tokens", ["token"], :name => "index_oauth_access_tokens_on_token", :unique => true

  create_table "oauth_applications", :force => true do |t|
    t.string   "name",         :null => false
    t.string   "uid",          :null => false
    t.string   "secret",       :null => false
    t.text     "redirect_uri", :null => false
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "oauth_applications", ["uid"], :name => "index_oauth_applications_on_uid", :unique => true

  create_table "offices", :force => true do |t|
    t.string   "office_name"
    t.string   "office_address"
    t.string   "nit"
    t.string   "contact_name"
    t.string   "contact_email"
    t.integer  "brand_id"
    t.integer  "city_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.boolean  "enable_for_selling",  :default => true
    t.string   "phone"
    t.string   "mobile_phone"
    t.string   "bank_account_number"
    t.string   "bank_name"
    t.string   "bank_account_type"
    t.string   "local_company_name"
    t.string   "schedule"
    t.string   "ip"
  end

  create_table "payment_answers", :force => true do |t|
    t.integer  "gift_code_id"
    t.integer  "pol_state"
    t.decimal  "cost",                :precision => 10, :scale => 2
    t.decimal  "iva",                 :precision => 10, :scale => 2
    t.string   "currency_short_name"
    t.integer  "payment_type_id"
    t.text     "params"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.string   "final_result"
  end

  create_table "payment_confirmations", :force => true do |t|
    t.integer  "gift_code_id"
    t.integer  "pol_state"
    t.decimal  "cost",                :precision => 10, :scale => 2
    t.decimal  "iva",                 :precision => 10, :scale => 2
    t.string   "currency_short_name"
    t.integer  "payment_type_id"
    t.text     "params"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.string   "final_result"
  end

  create_table "recommendations", :force => true do |t|
    t.integer  "user_id"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "redeban_codes", :force => true do |t|
    t.string   "code"
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.boolean  "already_used"
    t.integer  "batch_id"
    t.string   "salt"
    t.string   "iv"
    t.string   "hash2"
    t.string   "expiration_date"
    t.integer  "state_id"
    t.integer  "gift_card_id"
    t.integer  "charge_state_id"
    t.integer  "campaign_id"
    t.integer  "log_file_download_id"
    t.integer  "log_error_update_file_id"
    t.datetime "log_update_date"
    t.integer  "redeban_log_id"
    t.integer  "log_novelty"
    t.string   "log_consecutive"
    t.decimal  "log_value",                :precision => 10, :scale => 2
  end

  create_table "redeban_daily_balances", :force => true do |t|
    t.integer  "redeban_code_id"
    t.string   "file_name"
    t.integer  "consecutivo"
    t.integer  "saldo_inicial"
    t.integer  "estado"
    t.integer  "valor_saldo"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "redeban_daily_consumptions", :force => true do |t|
    t.integer  "redeban_code_id"
    t.string   "file_name"
    t.decimal  "valor_de_consumo"
    t.date     "fecha_vencimiento"
    t.integer  "estado"
    t.decimal  "valor_del_saldo"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "error"
  end

  create_table "redeban_detailed_movement_certificates", :force => true do |t|
    t.string   "file_name"
    t.string   "nombre_convenio"
    t.datetime "fecha_hora_movimiento"
    t.string   "descripcion_movimiento"
    t.integer  "codigo_establecimiento"
    t.integer  "codigo_terminal"
    t.string   "tipo_transacion"
    t.decimal  "valor_transaccion"
    t.integer  "num_referencia"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "redeban_code_id"
    t.string   "error"
  end

  create_table "redeban_logs", :force => true do |t|
    t.string   "name"
    t.integer  "number_records"
    t.integer  "successful"
    t.integer  "rejected"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "source_file"
    t.date     "date_source_file"
    t.integer  "consecutive"
    t.string   "type_act"
    t.datetime "date_process"
  end

  create_table "redeban_movements", :force => true do |t|
    t.integer  "brand_id"
    t.text     "line"
    t.integer  "admin_user_id"
    t.string   "file"
    t.integer  "number_line"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "request_third_party_entities", :force => true do |t|
    t.integer  "third_party_entity_id"
    t.integer  "gift_code_id"
    t.text     "data_sent"
    t.text     "data_received"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "response_code"
    t.string   "response_message"
    t.string   "state"
    t.string   "payment_type"
    t.string   "card_type"
    t.string   "card_franchise"
    t.string   "total_value"
    t.string   "approbation_number"
    t.string   "bin"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "settings", :force => true do |t|
    t.string   "name"
    t.string   "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sexes", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "states", :force => true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "suggests", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "third_party_black_lists", :force => true do |t|
    t.string   "dir_ip"
    t.boolean  "blocked"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "third_party_entities", :force => true do |t|
    t.string   "name"
    t.string   "secret"
    t.string   "incocredito_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.text     "configuration"
  end

  create_table "third_party_registrations", :force => true do |t|
    t.string   "received_sign"
    t.datetime "received_date"
    t.integer  "brand_code_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "third_party_transactions", :force => true do |t|
    t.integer  "third_party_entity_id"
    t.integer  "brand_code_id"
    t.float    "transaction_value"
    t.string   "transaction_currency"
    t.float    "new_balance"
    t.string   "received_hash",           :limit => 1000
    t.string   "dir_ip"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.integer  "tp_transaction_type_id"
    t.integer  "tp_transaction_state_id"
    t.integer  "related_transaction_id"
    t.integer  "gift_code_id"
    t.integer  "office_id"
    t.string   "terminal_id"
    t.boolean  "reversed"
    t.boolean  "confirmed"
    t.string   "dir_ip_client"
    t.integer  "input_type_id"
    t.integer  "approbation_number"
    t.integer  "brand_id"
    t.integer  "campaign_id"
    t.integer  "gift_card_id"
  end

  create_table "tp_transaction_states", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tp_transaction_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "transactions", :force => true do |t|
    t.integer  "gift_code_id"
    t.decimal  "cost",                     :precision => 10, :scale => 2
    t.string   "terminal_id"
    t.string   "consecutive"
    t.datetime "terminal_date"
    t.string   "state"
    t.string   "authorization_number"
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.integer  "created_at_office_id"
    t.integer  "created_by_admin_user_id"
  end

  create_table "user_states", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "time_zone"
    t.integer  "city_id"
    t.integer  "language_id"
    t.integer  "sex_id"
    t.date     "birthday_date"
    t.string   "mailing_address"
    t.string   "mobile_phone"
    t.string   "office_phone"
    t.string   "image"
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.string   "encrypted_password",                   :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "password_salt"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                      :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.string   "facebook_token"
    t.datetime "facebook_expires_at"
    t.boolean  "facebook_expires"
    t.boolean  "show_introduction",                    :default => false
    t.boolean  "sms_authorization",                    :default => true
    t.string   "identification"
    t.integer  "user_state_id",                        :default => 1
    t.date     "birthday"
    t.integer  "fbk_user_id",             :limit => 8
    t.datetime "last_fbk_sync_at"
    t.string   "fbk_location_name"
    t.string   "fbk_timezone"
    t.string   "fbk_locale"
    t.integer  "referral_id",             :limit => 8
    t.datetime "date_referral"
    t.datetime "registration_date"
    t.string   "utm_source"
    t.string   "utm_medium"
    t.boolean  "birthday_confirmation"
    t.boolean  "city_confirmation"
    t.boolean  "data_email_confirmation"
    t.string   "email_fbk_or_temp"
    t.string   "token_validate_email"
    t.string   "email_fbk"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["fbk_user_id"], :name => "index_users_on_fbk_user_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true
  add_index "users", ["user_state_id"], :name => "index_users_on_user_state_id"

end
