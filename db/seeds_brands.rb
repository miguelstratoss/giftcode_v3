brand = Brand.find_by_name('Abril')
if brand.nil?
  brand = Brand.create(:name => 'Abril' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Accessorize')
if brand.nil?
  brand = Brand.create(:name => 'Accessorize', :sn_twitter_name => '@accessorizecolombia')
else
  brand.update_attributes :sn_twitter_name => '@accessorizecolombia'
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Adidas')
if brand.nil?
  brand = Brand.create(:name => 'Adidas', :sn_twitter_name => '@adidasCO'  )
else
  brand.update_attributes :sn_twitter_name => '@adidasCO'
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
brand_category_2 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Aeropostal')
if brand.nil?
  brand = Brand.create(:name => 'Aeropostal' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Agua de Coco')
if brand.nil?
  brand = Brand.create(:name => 'Agua de Coco' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Alkosto')
if brand.nil?
  brand = Brand.create(:name => 'Alkosto' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Americanino')
if brand.nil?
  brand = Brand.create(:name => 'Americanino' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Andrés Carne de Res')
if brand.nil?
  brand = Brand.create(:name => 'Andrés Carne de Res', :sn_twitter_name =>  '@Andres_C_de_Res')
else
  brand.update_attributes :sn_twitter_name => '@Andres_C_de_Res'
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Archie\'s')
if brand.nil?
  brand = Brand.create(:name => 'Archie\'s', :sn_twitter_name =>  '@ArchiesCO')
else
  brand.update_attributes :sn_twitter_name => '@ArchiesCO'
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Armi')
if brand.nil?
  brand = Brand.create(:name => 'Armi')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Arturo Calle')
if brand.nil?
  brand = Brand.create(:name => 'Arturo Calle', :sn_twitter_name => '@Arturo_Calle' )
else
  brand.update_attributes :sn_twitter_name => '@Arturo_Calle'
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
brand_category_2 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
brand_category_3 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Azulu')
if brand.nil?
  brand = Brand.create(:name => 'Azulu', :sn_twitter_name => '@AzuluOficial' )
else
  brand.update_attributes :sn_twitter_name => '@AzuluOficial'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Balsámico')
if brand.nil?
  brand = Brand.create(:name => 'Balsámico')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Beer-Pub')
if brand.nil?
  brand = Brand.create(:name => 'Beer-Pub')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Bennetton')
if brand.nil?
  brand = Brand.create(:name => 'Bennetton')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Bershka')
if brand.nil?
  brand = Brand.create(:name => 'Bershka')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Bkul')
if brand.nil?
  brand = Brand.create(:name => 'Bkul')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Bodytech')
if brand.nil?
  brand = Brand.create(:name => 'Bodytech', :sn_twitter_name => '@ClubBODYTECH' )
else
  brand.update_attributes :sn_twitter_name => '@ClubBODYTECH'
end
brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Bogotá Beer Company')
if brand.nil?
  brand = Brand.create(:name => 'Bogotá Beer Company', :sn_twitter_name => '@BogotaBeerCo' )
else
  brand.update_attributes :sn_twitter_name => '@BogotaBeerCo'
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Bombalu')
if brand.nil?
  brand = Brand.create(:name => 'Bombalu')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Boots\'n Bagks')
if brand.nil?
  brand = Brand.create(:name => 'Boots\'n Bagks', :sn_twitter_name => '@soybootsnbags')
else
  brand.update_attributes :sn_twitter_name => '@soybootsnbags'
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Bosi')
if brand.nil?
  brand = Brand.create(:name => 'Bosi', :sn_twitter_name => '@mybosi')
else
  brand.update_attributes :sn_twitter_name => '@mybosi'
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Brahma')
if brand.nil?
  brand = Brand.create(:name => 'Brahma', :sn_twitter_name => '@BrahmaFootwear')
else
  brand.update_attributes :sn_twitter_name => '@BrahmaFootwear'
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Buffalo Wings')
if brand.nil?
  brand = Brand.create(:name => 'Buffalo Wings')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Burger King')
if brand.nil?
  brand = Brand.create(:name => 'Burger King')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)

brand = Brand.find_by_name('Burger Town')
if brand.nil?
  brand = Brand.create(:name => 'Burger Town')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('California Inn')
if brand.nil?
  brand = Brand.create(:name => 'California Inn')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Carbón 100')
if brand.nil?
  brand = Brand.create(:name => 'Carbón 100')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Caretas')
if brand.nil?
  brand = Brand.create(:name => 'Caretas', :sn_twitter_name =>  '@Caretas_Col' )
else
  brand.update_attributes :sn_twitter_name => '@Caretas_Col'
end
brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Carlos Nieto')
if brand.nil?
  brand = Brand.create(:name => 'Carlos Nieto' )
else
  #brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Carolina Cruz')
if brand.nil?
  brand = Brand.create(:name => 'Carolina Cruz', :sn_twitter_name =>  '@carocruzosorio' )
else
  brand.update_attributes :sn_twitter_name => '@carocruzosorio'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Carulla')
if brand.nil?
  brand = Brand.create(:name => 'Carulla')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Cascabel')
if brand.nil?
  brand = Brand.create(:name => 'Cascabel')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurante').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Celio')
if brand.nil?
  brand = Brand.create(:name => 'Celio')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Chevignon')
if brand.nil?
  brand = Brand.create(:name => 'Chevignon')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Cinabón')
if brand.nil?
  brand = Brand.create(:name => 'Cinabón')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Cine Colombia')
if brand.nil?
  brand = Brand.create(:name => 'Cine Colombia', :sn_twitter_name =>  '@Cine_Colombia')
else
  brand.update_attributes :sn_twitter_name => '@Cine_Colombia'
end
brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Cinemark')
if brand.nil?
  brand = Brand.create(:name => 'Cinemark', :sn_twitter_name =>  '@CinemarkCol')
else
  brand.update_attributes :sn_twitter_name => '@CinemarkCol'
end
brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Cinépolis')
if brand.nil?
  brand = Brand.create(:name => 'Cinépolis', :sn_twitter_name =>  '@cinepolisco')
else
  brand.update_attributes :sn_twitter_name => '@cinepolisco'
end
brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Clonhadas')
if brand.nil?
  brand = Brand.create(:name => 'Clonhadas', :sn_twitter_name =>  '@Clonhadas1')
else
  brand.update_attributes :sn_twitter_name => '@Clonhadas1'
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Converse')
if brand.nil?
  brand = Brand.create(:name => 'Converse', :sn_twitter_name =>  '@conversecol')
else
  brand.update_attributes :sn_twitter_name => '@conversecol'
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Colsubsidio')
if brand.nil?
  brand = Brand.create(:name => 'Colsubsidio')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Crepes & Waffles')
if brand.nil?
  brand = Brand.create(:name => 'Crepes & Waffles' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Crocs')
if brand.nil?
  brand = Brand.create(:name => 'Crocs', :sn_twitter_name =>  '@crocs' )
else
  brand.update_attributes :sn_twitter_name => '@crocs'
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Desigual')
if brand.nil?
  brand = Brand.create(:name => 'Desigual', :sn_twitter_name =>  '@Desigual' )
else
  brand.update_attributes :sn_twitter_name => '@Desigual'
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Diesel')
if brand.nil?
  brand = Brand.create(:name => 'Diesel' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Divercity')
if brand.nil?
  brand = Brand.create(:name => 'Divercity', :sn_twitter_name => '@Divercity_Col' )
else
  brand.update_attributes :sn_twitter_name => '@Divercity_Col'
end
brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Do iT')
if brand.nil?
  brand = Brand.create(:name => 'Do iT', :sn_twitter_name => '@Doit_Accesorios' )
else
  brand.update_attributes :sn_twitter_name => '@Doit_Accesorios'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Dogger')
if brand.nil?
  brand = Brand.create(:name => 'Dogger' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Doménico')
if brand.nil?
  brand = Brand.create(:name => 'Doménico' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Don Jediondo')
if brand.nil?
  brand = Brand.create(:name => 'Don Jediondoo' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Dunkin\' Donuts')
if brand.nil?
  brand = Brand.create(:name => 'Dunkin\' Donuts', :sn_twitter_name => '@dunkincolombia' )
else
  brand.update_attributes :sn_twitter_name => '@dunkincolombia'
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('El Corral')
if brand.nil?
  brand = Brand.create(:name => 'El Corral')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('El Trompo')
if brand.nil?
  brand = Brand.create(:name => 'El Trompo')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('ELA')
if brand.nil?
  brand = Brand.create(:name => 'ELA')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Entertainment Store')
if brand.nil?
  brand = Brand.create(:name => 'Entertainment Store', :sn_twitter_name => '@estorecolombia')
else
  brand.update_attributes :sn_twitter_name => '@estorecolombia'
end
brand_category_1 = Category.find_by_name('Música').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('EPK')
if brand.nil?
  brand = Brand.create(:name => 'EPK', :sn_twitter_name => '@EPKCOLOMBIA')
else
  brand.update_attributes :sn_twitter_name => '@EPKCOLOMBIA'
end
brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('ESPRIT')
if brand.nil?
  brand = Brand.create(:name => 'ESPRIT')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Éxito')
if brand.nil?
  brand = Brand.create(:name => 'Éxito')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Falabella')
if brand.nil?
  brand = Brand.create(:name => 'Falabella', :sn_twitter_name => '@Falabella_co')
else
  brand.update_attributes :sn_twitter_name => '@Falabella_co'
end
brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('FDS Fuera De Serie')
if brand.nil?
  brand = Brand.create(:name => 'FDS Fuera De Serie')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Fedco')
if brand.nil?
  brand = Brand.create(:name => 'Fedco', :sn_twitter_name => '@Tiendas_Fedco')
else
  brand.update_attributes :sn_twitter_name => '@Tiendas_Fedco'
end
brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Forever 21')
if brand.nil?
  brand = Brand.create(:name => 'Forever 21')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
brand_category_2 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Freeport Sport')
if brand.nil?
  brand = Brand.create(:name => 'Freeport Sport', :sn_twitter_name => '@freeportstore')
else
  brand.update_attributes :sn_twitter_name => '@freeportstore'
end
brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Fridays - TGI')
if brand.nil?
  brand = Brand.create(:name => 'Fridays - TGI')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Frisby')
if brand.nil?
  brand = Brand.create(:name => 'Frisby')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Gap')
if brand.nil?
  brand = Brand.create(:name => 'Gap')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Gef')
if brand.nil?
  brand = Brand.create(:name => 'Gef')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Girbaud')
if brand.nil?
  brand = Brand.create(:name => 'Girbaud')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Groggy')
if brand.nil?
  brand = Brand.create(:name => 'Groggy')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Hard Rock Café')
if brand.nil?
  brand = Brand.create(:name => 'Hard Rock Café', :sn_twitter_name => '@HardRockBogota')
else
  brand.update_attributes :sn_twitter_name => '@HardRockBogota'
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Home Sentry')
if brand.nil?
  brand = Brand.create(:name => 'Home Sentry')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Hooters')
if brand.nil?
  brand = Brand.create(:name => 'Hooters')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Hush Puppies')
if brand.nil?
  brand = Brand.create(:name => 'Hush Puppies')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('iShop')
if brand.nil?
  brand = Brand.create(:name => 'iShop', :sn_twitter_name => '@ishopcol')
else
  brand.update_attributes :sn_twitter_name => '@ishopcol'
end
brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Ice Watch')
if brand.nil?
  brand = Brand.create(:name => 'Ice Watch', :sn_twitter_name => '@icewatchbrand')
else
  brand.update_attributes :sn_twitter_name => '@icewatchbrand'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Inkanta')
if brand.nil?
  brand = Brand.create(:name => 'Inkanta', :sn_twitter_name => '@inkanta')
else
  brand.update_attributes :sn_twitter_name => '@inkanta'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Jeno\'s Pizza')
if brand.nil?
  brand = Brand.create(:name => 'Jeno\'s Pizza')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Kenzo')
if brand.nil?
  brand = Brand.create(:name => 'Kenzo')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Kevins')
if brand.nil?
  brand = Brand.create(:name => 'Kevins', :sn_twitter_name => '@kevinsjoyeros')
else
  brand.update_attributes :sn_twitter_name => '@kevinsjoyeros'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Koaj')
if brand.nil?
  brand = Brand.create(:name => 'Koaj')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Kokoriko')
if brand.nil?
  brand = Brand.create(:name => 'Kokoriko')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Kolor Nails')
if brand.nil?
  brand = Brand.create(:name => 'Kolor Nails', :sn_twitter_name => '@KolorNails' )
else
  brand.update_attributes :sn_twitter_name => '@KolorNails'
end
brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Ktronix')
if brand.nil?
  brand = Brand.create(:name => 'Ktronix' )
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('La Barbería')
if brand.nil?
  brand = Brand.create(:name => 'La Barbería' , :sn_twitter_name => '@labarberia_bta')
else
  brand.update_attributes :sn_twitter_name => '@labarberia_bta'
end
brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('La Música')
if brand.nil?
  brand = Brand.create(:name => 'La Música')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Música').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Le Collezioni')
if brand.nil?
  brand = Brand.create(:name => 'Le Collezioni')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Lego')
if brand.nil?
  brand = Brand.create(:name => 'Lego')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Leonisa')
if brand.nil?
  brand = Brand.create(:name => 'Leonisa')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Levi\'s')
if brand.nil?
  brand = Brand.create(:name => 'Levi\'s')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Librería Lerner')
if brand.nil?
  brand = Brand.create(:name => 'Librería Lerner', :sn_twitter_name => '@LibreriaLerner')
else
  brand.update_attributes :sn_twitter_name => '@LibreriaLerner'
end
brand_category_1 = Category.find_by_name('Libros').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Librería Nacional')
if brand.nil?
  brand = Brand.create(:name => 'Librería Nacional', :sn_twitter_name => '@lib_nacional')
else
  brand.update_attributes :sn_twitter_name => '@lib_nacional'
end
brand_category_1 = Category.find_by_name('Libros').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Los Sánduches del Sr. Ostia')
if brand.nil?
  brand = Brand.create(:name => 'Los Sánduches del Sr. Ostia')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Mac Center')
if brand.nil?
  brand = Brand.create(:name => 'Mac Center')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Mango - MNG')
if brand.nil?
  brand = Brand.create(:name => 'Mango - MNG')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Mario Hernandez')
if brand.nil?
  brand = Brand.create(:name => 'Mario Hernandez', :sn_twitter_name => '@muymario')
else
  brand.update_attributes :sn_twitter_name => '@muymario'
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Marlon Becerra')
if brand.nil?
  brand = Brand.create(:name => 'Marlon Becerra', :sn_twitter_name => '@marlonbecerra')
else
  brand.update_attributes :sn_twitter_name => '@marlonbecerra'
end
brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Marruecos')
if brand.nil?
  brand = Brand.create(:name => 'Marruecos')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Mc Donald\'s')
if brand.nil?
  brand = Brand.create(:name => 'Mc Donald\'s')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Mercado')
if brand.nil?
  brand = Brand.create(:name => 'Mercado')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Mercedes Salazar')
if brand.nil?
  brand = Brand.create(:name => 'Mercedes Salazar', :sn_twitter_name => '@mercedessalazar')
else
  brand.update_attributes :sn_twitter_name => '@mercedessalazar'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Montblanc')
if brand.nil?
  brand = Brand.create(:name => 'Montblanc', :sn_twitter_name => '@montblanc_world')
else
  brand.update_attributes :sn_twitter_name => '@montblanc_world'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Mother Care')
if brand.nil?
  brand = Brand.create(:name => 'Mother Care', :sn_twitter_name => '@mothercarecol')
else
  brand.update_attributes :sn_twitter_name => '@mothercarecol'
end
brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Mr. Lee')
if brand.nil?
  brand = Brand.create(:name => 'Mr. Lee', :sn_twitter_name => '@mothercarecol')
else
  brand.update_attributes :sn_twitter_name => '@mothercarecol'
end
brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Multiópticas')
if brand.nil?
  brand = Brand.create(:name => 'Multiópticas')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Myriam Camhi')
if brand.nil?
  brand = Brand.create(:name => 'Myriam Camhi')
else
  brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Naf Naf')
if brand.nil?
  brand = Brand.create(:name => 'Naf Naf')
else
  brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('New Balance')
if brand.nil?
  brand = Brand.create(:name => 'New Balance', :sn_twitter_name => '@NewBalanceCO')
else
  brand.update_attributes :sn_twitter_name => '@NewBalanceCO'
end
brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('no Project')
if brand.nil?
  brand = Brand.create(:name => 'no Project')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Norberto Peluquería')
if brand.nil?
  brand = Brand.create(:name => 'Norberto Peluquería')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Offcorss')
if brand.nil?
  brand = Brand.create(:name => 'Offcorss', :sn_twitter_name => '@OFFCORSS')
else
  brand.update_attributes :sn_twitter_name => '@OFFCORSS'
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Oma')
if brand.nil?
  brand = Brand.create(:name => 'Oma')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Opposite')
if brand.nil?
  brand = Brand.create(:name => 'Opposite')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Opticentro')
if brand.nil?
  brand = Brand.create(:name => 'Opticentro')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Panamericana')
if brand.nil?
  brand = Brand.create(:name => 'Panamericana', :sn_twitter_name => '@PanamericanaLib')
else
  brand.update_attributes :sn_twitter_name => '@PanamericanaLib'
end
brand_category_1 = Category.find_by_name('Libros').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Papa John´s')
if brand.nil?
  brand = Brand.create(:name => 'Papa John\'s')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Pat Primo')
if brand.nil?
  brand = Brand.create(:name => 'Pat Primo')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Paul Frank')
if brand.nil?
  brand = Brand.create(:name => 'Paul Frank', :sn_twitter_name => '@paulfranktweets')
else
  brand.update_attributes :sn_twitter_name => '@paulfranktweets'
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Payless')
if brand.nil?
  brand = Brand.create(:name => 'Payless')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('People Plays')
if brand.nil?
  brand = Brand.create(:name => 'People Plays')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Pepe Ganga')
if brand.nil?
  brand = Brand.create(:name => 'Pepe Ganga', :sn_twitter_name => '@PepeGangaco')
else
  brand.update_attributes :sn_twitter_name => '@PepeGangaco'
end
brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Pilatos')
if brand.nil?
  brand = Brand.create(:name => 'Pilatos')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Polo Club')
if brand.nil?
  brand = Brand.create(:name => 'Polo Club')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Poor Designer')
if brand.nil?
  brand = Brand.create(:name => 'Poor Designer', :sn_twitter_name => '@poordesigner')
else
  brand.update_attributes :sn_twitter_name => '@poordesigner'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Presto')
if brand.nil?
  brand = Brand.create(:name => 'Presto')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Procinal')
if brand.nil?
  brand = Brand.create(:name => 'Procinal', :sn_twitter_name => '@cineprocinal')
else
  brand.update_attributes :sn_twitter_name => '@cineprocinal'
end
brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Pronto')
if brand.nil?
  brand = Brand.create(:name => 'Pronto')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Puma')
if brand.nil?
  brand = Brand.create(:name => 'Puma', :sn_twitter_name => '@PUMA_Col')
else
  brand.update_attributes :sn_twitter_name => '@PUMA_Col'
end
brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Pylones')
if brand.nil?
  brand = Brand.create(:name => 'Pylones', :sn_twitter_name => '@pylonescolombia')
else
  brand.update_attributes :sn_twitter_name => '@pylonescolombia'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Ralph Lauren')
if brand.nil?
  brand = Brand.create(:name => 'Ralph Lauren')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Randys')
if brand.nil?
  brand = Brand.create(:name => 'Randys')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Reinder')
if brand.nil?
  brand = Brand.create(:name => 'Reinder')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Replay\'s')
if brand.nil?
  brand = Brand.create(:name => 'Replay\'s')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Ripley')
if brand.nil?
  brand = Brand.create(:name => 'Ripley',:sn_twitter_name => '@Ripley_Colombia')
else
  brand.update_attributes :sn_twitter_name => '@Ripley_Colombia'
end
brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Rosépistol')
if brand.nil?
  brand = Brand.create(:name => 'Rosépistol')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Runing Balboa')
if brand.nil?
  brand = Brand.create(:name => 'Runing Balboa', :sn_twitter_name => '@runningbalboaco')
else
  brand.update_attributes :sn_twitter_name => '@runningbalboaco'
end
brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Sandwich Qbano')
if brand.nil?
  brand = Brand.create(:name => 'Sandwich Qbano', :sn_twitter_name => '@runningbalboaco')
else
  brand.update_attributes :sn_twitter_name => '@runningbalboaco'
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Santorini')
if brand.nil?
  brand = Brand.create(:name => 'Santorini')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Seven & Seven')
if brand.nil?
  brand = Brand.create(:name => 'Seven & Seven', :sn_twitter_name => '@sevensevenco')
else
  brand.update_attributes :sn_twitter_name => '@sevensevenco'
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Sony Store')
if brand.nil?
  brand = Brand.create(:name => 'Sony Store')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Speedo')
if brand.nil?
  brand = Brand.create(:name => 'Speedo', :sn_twitter_name => '@speedocolombia')
else
  brand.update_attributes :sn_twitter_name => '@speedocolombia'
end
brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Spining Center')
if brand.nil?
  brand = Brand.create(:name => 'Spining Center', :sn_twitter_name => '@SpinningCenter')
else
  brand.update_attributes :sn_twitter_name => '@SpinningCenter'
end
brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Sportline')
if brand.nil?
  brand = Brand.create(:name => 'Sportline')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
brand_category_2 = Category.find_by_name('Zapatoss').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Spring Step')
if brand.nil?
  brand = Brand.create(:name => 'Spring Step')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Stradivarius')
if brand.nil?
  brand = Brand.create(:name => 'Stradivarius')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Stromboli')
if brand.nil?
  brand = Brand.create(:name => 'Stromboli')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Studio F.')
if brand.nil?
  brand = Brand.create(:name => 'Studio F.')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Subway')
if brand.nil?
  brand = Brand.create(:name => 'Subway')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Super Dry')
if brand.nil?
  brand = Brand.create(:name => 'Super Dry')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Swatch')
if brand.nil?
  brand = Brand.create(:name => 'Swatch', :sn_twitter_name => '@swatch_CO')
else
  # brand.update_attributes :sn_twitter_name => '@swatch_CO'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Taco Bell')
if brand.nil?
  brand = Brand.create(:name => 'Taco Bell')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Tango discos')
if brand.nil?
  brand = Brand.create(:name => 'Tango discos', :sn_twitter_name => '@tangodiscos')
else
  brand.update_attributes :sn_twitter_name => '@tangodiscos'
end
brand_category_1 = Category.find_by_name('Música').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Tennis')
if brand.nil?
  brand = Brand.create(:name => 'Tennis')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('The Khalifa')
if brand.nil?
  brand = Brand.create(:name => 'The Khalifa')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('The North Face')
if brand.nil?
  brand = Brand.create(:name => 'The North Face')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Tommy Hilfiger')
if brand.nil?
  brand = Brand.create(:name => 'Tommy Hilfiger')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Tornamesa')
if brand.nil?
  brand = Brand.create(:name => 'Tornamesa', :sn_twitter_name => '@latornamesa')
else
  brand.update_attributes :sn_twitter_name => '@latornamesa'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Totto')
if brand.nil?
  brand = Brand.create(:name => 'Totto', :sn_twitter_name => '@TottoColombia')
else
  brand.update_attributes :sn_twitter_name => '@TottoColombia'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Umbro')
if brand.nil?
  brand = Brand.create(:name => 'Umbro')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Vélez')
if brand.nil?
  brand = Brand.create(:name => 'Vélez')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Victoria Secret')
if brand.nil?
  brand = Brand.create(:name => 'Victoria Secret', :sn_twitter_name => '@victoriassecret')
else
  brand.update_attributes :sn_twitter_name => '@victoriassecret'
end
brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('We Love Nails')
if brand.nil?
  brand = Brand.create(:name => 'We Love Nails', :sn_twitter_name => '@welovenailsbar')
else
  brand.update_attributes :sn_twitter_name => '@welovenailsbar'
end
brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Wings')
if brand.nil?
  brand = Brand.create(:name => 'Wings')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Wok')
if brand.nil?
  brand = Brand.create(:name => 'Wok')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Wok')
if brand.nil?
  brand = Brand.create(:name => 'Wok')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)



brand = Brand.find_by_name('Zara')
if brand.nil?
  brand = Brand.create(:name => 'Zara')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


brand = Brand.find_by_name('Zephir')
if brand.nil?
  brand = Brand.create(:name => 'Zephir')
else
  # brand.update_attributes :sn_twitter_name => ''
end
brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)


