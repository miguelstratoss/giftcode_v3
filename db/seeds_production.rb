#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
sexM = Sex.create(:name => 'Masculino')
sexF = Sex.create(:name => 'Femenino')

category1 = Category.create(:name => "Zapatos", :_class => "shoes",:position => 1, :enabled => true )
category2 = Category.create(:name => "Ropa", :_class => "cloath", :position => 2, :enabled => true)
category3 = Category.create(:name => "Música", :_class => "music", :position => 3, :enabled => true)
category4 = Category.create(:name => "Libros", :_class => "books", :position => 4, :enabled => true)
category5 = Category.create(:name => "Cine", :_class => "movies", :position => 5, :enabled => true)
category6 = Category.create(:name => "Restaurantes", :_class => "drinks", :position => 6, :enabled => true)
category7 = Category.create(:name => "Tecnología", :_class => "tech", :position => 7, :enabled => true)
category8 = Category.create(:name => "Belleza", :_class => "personalcare", :position => 8, :enabled => true)
category9 = Category.create(:name => "Accesorios", :_class => "accesories", :position => 9, :enabled => true)
category10 = Category.create(:name => "Deportes", :_class => "sports", :position => 10, :enabled => true)
category11 = Category.create(:name => "Tiendas por departamentos", :_class => "departaments", :position => 11, :enabled => true)
category12 = Category.create(:name => "Bebés", :_class => "babies", :position => 12, :enabled => true)

#For countries, states, and cities
colombia = Country.create(:name => 'Colombia')
usa = Country.create(:name => 'Estados Unidos')
state1 = Country.find_by_name("Colombia").states.create(:name => "Cundinamarca")
city1 = State.find_by_name("Cundinamarca").cities.create(:name => "Bogotá")
state2 = Country.find_by_name("Colombia").states.create(:name => "Antioquia")
city2 = State.find_by_name("Antioquia").cities.create(:name => "Medellín")
state3 = Country.find_by_name("Colombia").states.create(:name => "Atlántico")
city3 = State.find_by_name("Atlántico").cities.create(:name => "Barranquilla")
state4 = Country.find_by_name("Estados Unidos").states.create(:name => "California")
city4 = State.find_by_name("California").cities.create(:name => "San Francisco")

state5 = Country.find_by_name("Colombia").states.create(:name => "Valle de cauca")
city5 = State.find_by_name("Valle de cauca").cities.create(:name => "Cali")

state6 = Country.find_by_name("Colombia").states.create(:name => "Bolívar")
city6 = State.find_by_name("Bolívar").cities.create(:name => "Cartagena")

state7 = Country.find_by_name("Colombia").states.create(:name => "Quindío")
city7 = State.find_by_name("Quindío").cities.create(:name => "Armenia")


city8 = State.find_by_name("Antioquia").cities.create(:name => "Bello")

state9 = Country.find_by_name("Colombia").states.create(:name => "Nariño")
city9 = State.find_by_name("Nariño").cities.create(:name => "Ipiales")

state10 = Country.find_by_name("Colombia").states.create(:name => "Caldas")
city10 = State.find_by_name("Caldas").cities.create(:name => "Manizales")

state11 = Country.find_by_name("Colombia").states.create(:name => "Huila")
city11 = State.find_by_name("Huila").cities.create(:name => "Neiva")


state12 = Country.find_by_name("Colombia").states.create(:name => "Risaralda")
city12 = State.find_by_name("Risaralda").cities.create(:name => "Pereira")

state13 = Country.find_by_name("Colombia").states.create(:name => "Magdalena")
city13 = State.find_by_name("Magdalena").cities.create(:name => "Santa Marta")

state14 = Country.find_by_name("Colombia").states.create(:name => "Meta")
city14 = State.find_by_name("Meta").cities.create(:name => "Villavicencio")



#For campaign state
campaign_state1 = CampaignState.create(:name => "Habilitada")
campaign_state2 = CampaignState.create(:name => "Deshabilitada")

#For languages
lanE = Language.create(:name => 'Español')

#For currencies
currency = Currency.create(:name => "Pesos colombianos", :symbol => "$", :short_name => "COP")

#For roles
role1 = Role.create(:name => 'Admin_Brand')
role2 = Role.create(:name => 'Admin_GiftCode')
role3 = Role.create(:name => 'Admin_Brand_Office')

#For user states
us1 = UserState.create(:name => 'Registrado')
us2 = UserState.create(:name => 'NO Registrado')
us3 = UserState.create(:name => 'Eliminado')

#For friendship states
fs1 = FriendshipState.create(:name => 'Sin relacion')
fs2 = FriendshipState.create(:name => 'Invitado')
fs3 = FriendshipState.create(:name => 'Amigo')
fs4 = FriendshipState.create(:name => 'Eliminado')

#For giftcodes states
gift_code_state_1 = GiftCodeState.create(:name => 'Generado y Pendiente de Pago')
gift_code_state_2 = GiftCodeState.create(:name => 'Pago aceptado')
gift_code_state_3 = GiftCodeState.create(:name => 'Pago rechazado')
gift_code_state_4 = GiftCodeState.create(:name => 'Gratuito y exclusivo de una oficina')
gift_code_state_5 = GiftCodeState.create(:name => 'Generado Oficina pendiente')
gift_code_state_6 = GiftCodeState.create(:name => 'Generado Oficina confirmado')

#For users
user1 = User.create(:first_name => 'none', :last_name => 'none', :email => 'none@greencode.com.co', :password => 'bhunjimkocftr876', :password_confirmation => 'bhunjimkocftr876')

#For issues
issue = Issue.create(:name => 'Comercial')

s1 = Setting.create( :name => 'Show Full Version', :value => 'true')

#ThirdParty TransactionStates
TpTransactionState.create(:id => 1, :name => 'success')
TpTransactionState.create(:id => 2 ,:name => 'wrong_sign')
TpTransactionState.create(:id => 3, :name => 'giftcard_not_found')
TpTransactionState.create(:id => 4 ,:name => 'wrong_credentials_brand')
TpTransactionState.create(:id => 5 ,:name => 'wrong_credentials_commerce_id')
TpTransactionState.create(:id => 6 ,:name => 'no_funds')
TpTransactionState.create(:id => 7, :name => 'insufficient_funds')
TpTransactionState.create(:id => 8, :name => 'value_format_error')
TpTransactionState.create(:id => 9, :name => 'could_not_save')
TpTransactionState.create(:id => 10, :name => 'target_transaction_not_found')
TpTransactionState.create(:id => 11, :name => 'dir_ip_blocked')
TpTransactionState.create(:id => 12, :name => 'giftcard_expired')
TpTransactionState.create(:id => 99, :name => 'unknown_error')

#ThirdParty TransactionTypes
TpTransactionType.create(:id => 1, :name => 'marketplace_request' )
TpTransactionType.create(:id => 2, :name => 'funds_request' )
TpTransactionType.create(:id => 3, :name => 'redemption' )
TpTransactionType.create(:id => 4, :name => 'pending_redemption' )
TpTransactionType.create(:id => 5, :name => 'confirm_redemption' )
TpTransactionType.create(:id => 6, :name => 'reverse_redemption' )


#To create several brands
if Brand.find_by_name('Vélez').nil?
  brand = Brand.create(:name => 'Vélez', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@velez.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Spring Step').nil?
  brand = Brand.create(:name => 'Spring Step', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@springstep.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
  brand_category_2 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mario Hernández').nil?
  brand = Brand.create(:name => 'Mario Hernández', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@mariohernandez.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bosi').nil?
  brand = Brand.create(:name => 'Bosi', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@bosi.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Adidas').nil?
  brand = Brand.create(:name => 'Adidas', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@adidas.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
  brand_category_2 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Nike').nil?
  brand = Brand.create(:name => 'Nike', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@nike.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
  brand_category_2 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Arturo Calle').nil?
  brand = Brand.create(:name => 'Arturo Calle', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@arturocalle.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
  brand_category_2 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
  brand_category_3 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Armi').nil?
  brand = Brand.create(:name => 'Armi', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@armi.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Chevignon').nil?
  brand = Brand.create(:name => 'Chevignon', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@chevignon.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('FDS').nil?
  brand = Brand.create(:name => 'FDS', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@fds.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Gef').nil?
  brand = Brand.create(:name => 'Gef', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@gef.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Naf Naf').nil?
  brand = Brand.create(:name => 'Naf Naf', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@nafnaf.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Levi\'s').nil?
  brand = Brand.create(:name => 'Levi\'s', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@levis.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Studio F').nil?
  brand = Brand.create(:name => 'Studio F', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@studiof.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Tennis').nil?
  brand = Brand.create(:name => 'Tennis', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@tennis.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Zara').nil?
  brand = Brand.create(:name => 'Zara', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@tennis.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pilatos').nil?
  brand = Brand.create(:name => 'Pilatos', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@pilatos.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Tania').nil?
  brand = Brand.create(:name => 'Tania', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@tania.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Totto').nil?
  brand = Brand.create(:name => 'Totto', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@totto.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Tango Discos').nil?
  brand = Brand.create(:name => 'Tango Discos', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@tangodiscos.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Música').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Librería Nacional').nil?
  brand = Brand.create(:name => 'Librería Nacional', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@librerianacional.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Libros').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Panamericana').nil?
  brand = Brand.create(:name => 'Panamericana', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@panamericana.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Libros').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Cine Colombia').nil?
  brand = Brand.create(:name => 'Cine Colombia', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@cinecolombia.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Cinemark').nil?
  brand = Brand.create(:name => 'Cinemark', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@cinemark.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('El Corral').nil?
  brand = Brand.create(:name => 'El Corral', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@elcorral.com', :enable_for_selling => true )
  #brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Kokoriko').nil?
  brand = Brand.create(:name => 'Kokoriko', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@kokoriko.com', :enable_for_selling => true )
  #brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Crepes & Waffles').nil?
  brand = Brand.create(:name => 'Crepes & Waffles', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@crepesywaffles.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Hard Rock Café').nil?
  brand = Brand.create(:name => 'Hard Rock Café', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@hardrockcafe.com', :enable_for_selling => true )
  #brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bogotá Beer Company').nil?
  brand = Brand.create(:name => 'Bogotá Beer Company', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@bogotabeercompany.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mac Donald\'s').nil?
  brand = Brand.create(:name => 'Mac Donald\'s', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@macdonalds.com', :enable_for_selling => true )
  #brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('iShop').nil?
  brand = Brand.create(:name => 'iShop', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@ishop.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Ktronix').nil?
  brand = Brand.create(:name => 'Ktronix', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@ktronix.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Fedco').nil?
  brand = Brand.create(:name => 'Fedco', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@fedco.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('La Riviera').nil?
  brand = Brand.create(:name => 'La Riviera', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@lariviera.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Boot\'s \'N Bags').nil?
  brand = Brand.create(:name => 'Boot\'s \'N Bags', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@bootsnbags.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carolina Cruz').nil?
  brand = Brand.create(:name => 'Carolina Cruz', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@carolinacruz.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Éxito').nil?
  brand = Brand.create(:name => 'Éxito', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@exito.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pepe Ganga').nil?
  brand = Brand.create(:name => 'Pepe Ganga', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@pepeganga.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Falabella').nil?
  brand = Brand.create(:name => 'Falabella', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@falabella.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Baby Ganga').nil?
  brand = Brand.create(:name => 'Baby Ganga', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@babyganga.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Offcorss').nil?
  brand = Brand.create(:name => 'Offcorss', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@offcors.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end

#Siguiente lote de marcas
if Brand.find_by_name('Hush Puppies').nil?
  brand = Brand.create(:name => 'Hush Puppies', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Santorini').nil?
  brand = Brand.create(:name => 'Santorini', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Payless').nil?
  brand = Brand.create(:name => 'Payless', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Puma').nil?
  brand = Brand.create(:name => 'Puma', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Boot\'s \'N Bags').present?
  brand = Brand.find_by_name('Boot\'s \'N Bags')
  brand_category_1 = Category.find_by_name('Zapatos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('ESPRIT').nil?
  brand = Brand.create(:name => 'ESPRIT', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Luber').nil?
  brand = Brand.create(:name => 'Luber', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('California Inn').nil?
  brand = Brand.create(:name => 'California Inn', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Diesel').nil?
  brand = Brand.create(:name => 'Diesel', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Tommy Hilfiger').nil?
  brand = Brand.create(:name => 'Tommy Hilfiger', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Zephir').nil?
  brand = Brand.create(:name => 'Zephir', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Groggy').nil?
  brand = Brand.create(:name => 'Groggy', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carlos Nieto').nil?
  brand = Brand.create(:name => 'Carlos Nieto', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Aeropostal').nil?
  brand = Brand.create(:name => 'Aeropostal', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bennetton').nil?
  brand = Brand.create(:name => 'Bennetton', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Forever 21').nil?
  brand = Brand.create(:name => 'Forever 21', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Girbaud').nil?
  brand = Brand.create(:name => 'Girbaud', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Seven & Seven').nil?
  brand = Brand.create(:name => 'Seven & Seven', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pronto').nil?
  brand = Brand.create(:name => 'Pronto', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Americanino').nil?
  brand = Brand.create(:name => 'Americanino', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Ralph Lauren').nil?
  brand = Brand.create(:name => 'Ralph Lauren', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Abril').nil?
  brand = Brand.create(:name => 'Abril', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Paul & Bear').nil?
  brand = Brand.create(:name => 'Paul & Bear', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Stradivarius').nil?
  brand = Brand.create(:name => 'Stradivarius', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bershka').nil?
  brand = Brand.create(:name => 'Bershka', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mario Hernández').present?
  brand = Brand.find_by_name('Mario Hernández')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Totto').present?
  brand = Brand.find_by_name('Totto')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Americanino').present?
  brand = Brand.find_by_name('Americanino')
  brand_category_1 = Category.find_by_name('Ropa').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Entertainment Store').nil?
  brand = Brand.create(:name => 'Entertainment Store', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Música').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Librería Lerner').nil?
  brand = Brand.create(:name => 'Librería Lerner', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@admin.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Libros').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Procinal').nil?
  brand = Brand.create(:name => 'Procinal', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Cinépolis').nil?
  brand = Brand.create(:name => 'Cinépolis', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Cine').brand_categories.create(:brand_id => brand.id)
end

if Brand.find_by_name('Juan Valdez').nil?
  brand = Brand.create(:name => 'Juan Valdez', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Don Jediondo').nil?
  brand = Brand.create(:name => 'Don Jediondo', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Buffalo Wings').nil?
  brand = Brand.create(:name => 'Buffalo Wings', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Wings').nil?
  brand = Brand.create(:name => 'Wings', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Burger King').nil?
  brand = Brand.create(:name => 'Burger King', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mr. Lee').nil?
  brand = Brand.create(:name => 'Mr. Lee', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Subway').nil?
  brand = Brand.create(:name => 'Subway', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Sandwich Qbano').nil?
  brand = Brand.create(:name => 'Sandwich Qbano', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carbón 100').nil?
  brand = Brand.create(:name => 'Carbón 100', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Frisby').nil?
  brand = Brand.create(:name => 'Frisby', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Dogger').nil?
  brand = Brand.create(:name => 'Dogger', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Andrés Exprés').nil?
  brand = Brand.create(:name => 'Andrés Exprés', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Taco Bell').nil?
  brand = Brand.create(:name => 'Taco Bell', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Papa John\'s').nil?
  brand = Brand.create(:name => 'Papa John\'s', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Archie\'s').nil?
  brand = Brand.create(:name => 'Archie\'s', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Jeno\'s Pizza').nil?
  brand = Brand.create(:name => 'Jeno\'s Pizza', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('El Corral').present?
  brand = Brand.find_by_name('El Corral')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Hard Rock Café').present?
  brand = Brand.find_by_name('Hard Rock Café')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Juan Valdez').present?
  brand = Brand.find_by_name('Juan Valdez')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Kokoriko').present?
  brand = Brand.find_by_name('Kokoriko')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mac Donald\'s').present?
  brand = Brand.find_by_name('Mac Donald\'s')
  brand_category_1 = Category.find_by_name('Restaurantes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mac Center').nil?
  brand = Brand.create(:name => 'Mac Center', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Sony Store').nil?
  brand = Brand.create(:name => 'Sony Store  ', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tecnología').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Marlon Becerra').nil?
  brand = Brand.create(:name => 'Marlon Becerra', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Norberto Peluquería').nil?
  brand = Brand.create(:name => 'Norberto Peluquería', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bodytech').nil?
  brand = Brand.create(:name => 'Bodytech', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Spinning Center').nil?
  brand = Brand.create(:name => 'Spinning Center', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Belleza').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pylones').nil?
  brand = Brand.create(:name => 'Pylones', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Montblanc').nil?
  brand = Brand.create(:name => 'Montblanc', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Swatch').nil?
  brand = Brand.create(:name => 'Swatch', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Inkanta').nil?
  brand = Brand.create(:name => 'Inkanta', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Ice Watch').nil?
  brand = Brand.create(:name => 'Ice Watch', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Azulu').nil?
  brand = Brand.create(:name => 'Azulu', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Mario Hernández').present?
  brand = Brand.find_by_name('Mario Hernández')
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Totto').present?
  brand = Brand.find_by_name('Totto')
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carolina Cruz').present?
  brand = Brand.find_by_name('Carolina Cruz')
  brand_category_1 = Category.find_by_name('Accesorios').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Freeport Sport').nil?
  brand = Brand.create(:name => 'Freeport Sport', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Speedo').nil?
  brand = Brand.create(:name => 'Speedo', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('The North Face').nil?
  brand = Brand.create(:name => 'The North Face', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Puma').present?
  brand = Brand.find_by_name('Puma')
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Bodytech').present?
  brand = Brand.find_by_name('Bodytech')
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Spinning Center').present?
  brand = Brand.find_by_name('Spinning Center')
  brand_category_1 = Category.find_by_name('Deportes').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('La Polar').nil?
  brand = Brand.create(:name => 'La Polar', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Ripley').nil?
  brand = Brand.create(:name => 'Ripley', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Carulla').nil?
  brand = Brand.create(:name => 'Carulla', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Alkosto').nil?
  brand = Brand.create(:name => 'Alkosto', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Jumbo').nil?
  brand = Brand.create(:name => 'Jumbo', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Tiendas por departamentos').brand_categories.create(:brand_id => brand.id)
end

if Brand.find_by_name('Travesuras').nil?
  brand = Brand.create(:name => 'Travesuras', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('EPK').nil?
  brand = Brand.create(:name => 'EPK', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Divercity').nil?
  brand = Brand.create(:name => 'Divercity', :short_description => 'prueba', :contact_name => 'admin', :contact_email=>'admin@marca.com', :enable_for_selling => true )
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end
if Brand.find_by_name('Pepe Ganga').present?
  brand = Brand.find_by_name('Pepe Ganga')
  brand_category_1 = Category.find_by_name('Bebés').brand_categories.create(:brand_id => brand.id)
end