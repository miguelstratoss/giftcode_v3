# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)
run Greencode::Application

if Rails.env.prestaging? || Rails.env.staging? || Rails.env.production?
  DelayedJobWeb.use Rack::Auth::Basic do |username, password|
    username == 'giftcode' && password == 'delayjob_giftcode_2013'
  end
end
