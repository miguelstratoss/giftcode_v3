task :email_by_day_referrals => :environment do
  puts "email al final del dia..."
  User.email_by_day_referrals
  puts "se enviaron email al final del dia"
end

task :verify_transaction => :environment do
  gift_codes = GiftCode.where(
      :gift_code_state_id => [ GiftCodeState::STATES[:generated_and_payment_pending],GiftCodeState::STATES[:generated_external_web_pending]],
      :created_at =>  (Time.zone.now - 30.minutes)..Time.zone.now
  )
  puts 'GiftCodes'
  puts gift_codes.count
  gift_codes.each do |gc|
    payment =  Payments.new( :ref_sale => gc.ref_sale )
    payment.finalize
    puts 'payment'
  end
end

task :send_giftcode => :environment do
  gift_codes = GiftCode.where(
      :gift_code_state_id => [ GiftCodeState::STATES[:payment_accepted],GiftCodeState::STATES[:generated_office_confirmed],GiftCodeState::STATES[:generated_external_web_payment_accepted] ],
      :email_sent_to_receiver => [false, nil],
      :send_date_time => ( (Time.zone.today.to_time )..(Time.zone.today + ( ( Time.zone.now.hour ).hour + 59.minutes )  ) )
  )
  gift_codes.each do |gc|
    if gc.send_date_time.past?
      begin
        case gc.gift_code_state_id
          when GiftCodeState::STATES[:generated_external_web_payment_accepted]
            GiftCodeNotifier.receiver_gift_code_whitelabel(gc).deliver
          when GiftCodeState::STATES[:payment_accepted], GiftCodeState::STATES[:generated_office_confirmed], GiftCodeState::STATES[:free_points]
            GiftCodeNotifier.receiver_gift_code(gc).deliver
        end
        gc.update_attribute(:email_sent_to_receiver, true)
      rescue
        gc.update_attribute(:email_sent_to_receiver, false)
      end
      #Send email to buyer
      begin
        if !gc.email_sent_to_buyer
          case gc.gift_code_state_id
            when GiftCodeState::STATES[:generated_external_web_payment_accepted]
              GiftCodeNotifier.buyer_gift_code_whitelabel_send_pending(gc).deliver
            when GiftCodeState::STATES[:payment_accepted], GiftCodeState::STATES[:payment_accepted], GiftCodeState::STATES[:free_points]
              GiftCodeNotifier.buyer_gift_code_send_pending(gc).deliver
          end
          gc.update_attribute(:email_sent_to_buyer, true)
        end
      rescue
        gc.update_attribute(:email_sent_to_buyer, false)
      end
    end
  end
end

#NOT RUN THIS TASK!!!
task :remove_friendship_of_users_deleted => :environment do
  relationships_deleted = 0
  friendships = Friendship.where(['user_id NOT IN (?)', User.pluck('id') ])
  relationships_deleted = friendships.count
  # friendships.destroy_all
  puts "Friendships deleted: "+relationships_deleted.to_s
end

#NOT RUN THIS TASK!!!
task :remove_friendship_of_friends_deleted => :environment do
  relationships_deleted = 0
  friendships = Friendship.where(['friend_id NOT IN (?)', User.pluck('id') ])
  relationships_deleted = friendships.count
  # friendships.destroy_all
  puts "Friendships deleted: "+relationships_deleted.to_s
end

#NOT RUN THIS TASK!!!
task :remove_favorites_of_users_deleted => :environment do
  favorites_deleted = 0
  favorites = Favorite.where(['user_id NOT IN (?)', User.pluck('id') ])
  favorites_deleted = favorites.count
  # favorites.destroy_all
  puts "Favorites deleted: "+favorites_deleted.to_s
end

#NOT RUN THIS TASK!!!
task :update_versions_image_users => :environment do
  images_updated = 0
  users = User.registered.ordered_desc_by_creation_date
  users.each do |user|
    if user.image.present? && user.fbk_user_id.present?
      #user.remote_image_url = "https://graph.facebook.com/#{user.fbk_user_id}/picture?width=140&height=140"
      #user.save
      images_updated = images_updated + 1
    end
  end
  puts "Users with images updated: "+images_updated.to_s
end

#NOT RUN THIS TASK!!!
task :recreate_versions_image_users => :environment do
  images_recreated = 0
  users = User.registered.ordered_desc_by_creation_date
  users.each do |user|
    if user.image.present? && user.fbk_user_id.nil?
      #user.image.recreate_versions!
      images_recreated = images_recreated + 1
    end
  end
  puts "Users with images recreated: "+images_recreated.to_s
end

#NOT RUN THIS TASK!!!
task :remove_invite_friends_of_friends_deleted => :environment do
  invite_friends_deleted = 0
  invite_friends = InviteFriend.where(['friend_id NOT IN (?)', User.pluck('id') ])
  invite_friends_deleted = invite_friends.count
  # invite_friends.destroy_all
  puts "Invite friends deleted: "+invite_friends_deleted.to_s
end

#NOT RUN THIS TASK!!! ONLY Execute on demo and pre staging environments
task :remove_non_registered_users => :environment do
  if Rails.env.prestaging? || Rails.env.staging?
    non_registered_users_deleted = 0
    non_registered_users = User.non_registered
    non_registered_users.each do |user|
      if user.gift_codes_received.count == 0
        non_registered_users_deleted = non_registered_users_deleted + 1
        # user.destroy
      end
    end
    puts 'Non registered users deleted (without giftcodes received): '+non_registered_users_deleted.to_s
  end
end

#NOT RUN THIS TASK!!! ONLY Execute on demo and pre staging environments
task :remove_third_party_transactions => :environment do
  if Rails.env.prestaging? || Rails.env.staging?
    third_party_transactions_deleted = 0
    third_party_transactions_deleted = ThirdPartyTransaction.all.count
    # ThirdPartyTransaction.all.destroy_all
    puts 'Third party transactions deleted: '+third_party_transactions_deleted.to_s
  end
end

#NOT RUN THIS TASK!!!
task :user_unknown_change_email => :environment do
  unknown = 0
  User.where(:user_state_id =>  UserState::STATES[:non_registered], :fbk_user_id => nil).each do |user|
    # user.email_fbk_or_temp = user.email
    # user.email = 'unknown_' + user.id.to_s + '@giftcode.co'
    # user.save
    unknown = unknown + 1
  end
  puts "Users with id fbk unknown: " + unknown.to_s
end

#NOT RUN THIS TASK!!!
task :user_copy_email_fbk => :environment do
  count = 0
  User.where(:user_state_id =>  UserState::STATES[:registered]).each do |user|
    # user.email_fbk = user.email
    # user.save
    count = count + 1
  end
  puts "User: " + count.to_s
end

task :user_not_favorite_brands => :environment do
  count = 0
  User.where(:user_state_id =>  UserState::STATES[:registered]).each do |user|
    if user.brands.count == 0 && user.registration_date.present? && (user.registration_date + 1.week ) < Time.zone.now && NotificationUser.where( user_id: user.id, name_notification: 'favorite_brands' ).count == 0
      begin
        email = GiftCodeNotifier.favorite_brands(user)
        email.deliver
        NotificationUser.create( user_id: user.id, name_notification: 'favorite_brands' )
      rescue => e
        puts e
        puts 'Error: no se envio notificacion "no ha marcado marcas favoritas" al usuario "' + user.id.to_s + '"'
      end
      count = count + 1
    end
  end
  puts 'se enviaron: ' + count.to_s + ' a usuarios que no tienen marcas favoritas.'
end


task :users_points_available_redeem => :environment do
  count = 0
  if [4,5].include?( Time.zone.now.wday ) #dia de la semanan jueves o viernes
    if CampaignPoint.number_campaign_enabled > 0
      User.where(:user_state_id =>  UserState::STATES[:registered]).each do |user|
        if user.available_points > 0
          campaign = CampaignPoint.campaign_enabled.where("points_required <= ?", user.available_points ).order("points_required ASC").limit(3)
          if campaign.count > 0
            begin
              email = GiftCodeNotifier.redeem(user, campaign )
              email.deliver
              NotificationUser.create( user_id: user.id, name_notification: 'redeem' )
            rescue => e
              puts e
              puts 'Error: no se envio notificacion "no ha marcado marcas favoritas" al usuario "' + user.id.to_s + '"'
            end
            count = count + 1
          end
        end
      end
      puts 'se enviaron: ' + count.to_s + ' a usuarios que tienen puntos disponibles para redimir'
    else
      puts 'no hay campañas de puntos'
    end
  else
    puts 'esta tarea se ejecta los jueves y viernes'
  end
end

task :sitemap_update => :environment do
  Rake::Task['sitemap:refresh'].invoke
end

task :update_show_qr_barcode => :environment do
  Campaign.all.each do |campaign|
    campaign.show_qr = true
    campaign.show_barcode = true
    campaign.save
  end
end

task :update_show_qr_barcode => :environment do
  Campaign.all.each do |campaign|
    if !campaign.convenio.blank? || !campaign.convenio.nil?
      campaign.redeban = true
      campaign.save
    end
  end
end

task :consolidado => :environment  do
  include ActionView::Helpers::NumberHelper
  # bbc id 29  campaign id 3
  output = Array.new
  time = Time.zone.now - 0.day
  GiftCode
  .where("date_of_purchase < ?" ,time.end_of_day)
  .where(brand_id: 29, campaign_id: 3, :gift_code_state_id => [ GiftCodeState::STATES[:payment_accepted],GiftCodeState::STATES[:generated_office_confirmed],GiftCodeState::STATES[:generated_external_web_payment_accepted] ] )
  .where("available_balance != 0")
  .order("identification_number DESC")
  .order("gift_card_id DESC")
  .find_in_batches( batch_size: 100 ) do | batch|
    batch.each do |gift_code|
      #consolidado movimiento diario
      # cmd = 0
      value = gift_code.third_party_transactions.where(created_at: time.beginning_of_day .. time.end_of_day, tp_transaction_type_id:TpTransactionType::TYPES[:redemption] , tp_transaction_state_id: TpTransactionState::STATES[:success] ).sum(:transaction_value)
      value = number_with_delimiter( "%.2f" %  value, separator: '',delimiter:'').to_s
      balance = gift_code.third_party_transactions.where("third_party_transactions.created_at < ?" ,time.end_of_day).where( tp_transaction_type_id:TpTransactionType::TYPES[:redemption] , tp_transaction_state_id: TpTransactionState::STATES[:success] ).sum(:transaction_value)
      balance = number_with_delimiter( "%.2f" % (gift_code.cost - balance), separator: '',delimiter:'').to_s
      output << gift_code.id.to_s + "," + gift_code.gift_card_id.to_s + "," +  gift_code.identification_number.to_s + "," + value.to_s + "," + I18n.l(gift_code.date_of_purchase, format: :admin) + "," + balance.to_s
    end
  end
  puts output
end